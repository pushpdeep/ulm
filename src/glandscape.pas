{ ---------------------------------------------------------------------------- }
{ Copyright (c) 1995-2017, Stéphane Legendre and the ULM development team      }
{                                                                              }
{ This file is part of ULM.                                                    }
{                                                                              }
{ ULM is free software: you can redistribute it and/or modify it               }
{ under the terms of the GNU General Public License as published               }
{ by the Free Software Foundation, either version 3 of the License,            }
{ or (at your option) any later version.                                       }
{                                                                              }
{ ULM is distributed in the hope that it will be useful,                       }
{ but WITHOUT ANY WARRANTY; without even the implied warranty                  }
{ of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                      }
{ See the GNU General Public License for more details.                         }
{                                                                              }
{ You should have received a copy of the GNU General Public License            }
{ along with ULM.  If not, see <http://www.gnu.org/licenses/>.                 }
{ ---------------------------------------------------------------------------- }

unit glandscape;

{  @@@@@@   form for displaying growth rate (lambda) isoclines   @@@@@@  }

{$MODE Delphi}

interface

uses SysUtils,Classes,Graphics,Controls,Forms,Dialogs,StdCtrls,ExtCtrls,
     jmath;

type
  tform_landscape = class(TForm)
    top_panel: TPanel;
    bottom_panel: TPanel;
    middle_panel: TPanel;
    button_exec: TButton;
    label_lambdamin: TLabel;
    edit_lambdamin: TEdit;
    label_lambdamax: TLabel;
    edit_lambdamax: TEdit;
    label_mat: TLabel;
    edit_mat: TEdit;
    label_lambda: TLabel;
    edit_lambda: TEdit;
    label_x: TLabel;
    edit_x: TEdit;
    label_xmin: TLabel;
    edit_xmin: TEdit;
    label_xmax: TLabel;
    edit_xmax: TEdit;
    label_y: TLabel;
    edit_y: TEdit;
    label_ymin: TLabel;
    edit_ymin: TEdit;
    label_ymax: TLabel;
    edit_ymax: TEdit;
    procedure erreur(s : string);
    procedure init_landscape;
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure button_execClick(Sender: TObject);
    procedure exec_landscape(m,x,y : integer;xmi,xma,ymi,yma : extended);
    procedure eigenval(var err : boolean);
    function  check_param : boolean;
  private
     x_mat : integer; { pointer to matrix }
     x_var : integer; { pointer to variable x in abscissa X }
     y_var : integer; { pointer to variable y in ordinate Y }
     lambdamin,lambdamax : extended; { bounds on dominant eigenvalue }
     lambda : cvec_type; { complex eigenvalues }
     lambda1 : extended; { dominant eigenvalue }
     x0,y0       : extended; { actual value of x and y }
     xminl,xmaxl : extended; { min,max bounds on x }
     yminl,ymaxl : extended; { min,max bounds on y }
  public
  end;

var form_landscape: tform_landscape;

implementation

uses jglobvar,jsymb,jutil,jsyntax,jeval,jmatrix,
     ggraph, gulm;

{$R *.lfm}

procedure tform_landscape.erreur(s : string);
{ unit specific error procedure }
begin
  erreur_('Landscape - ' + s);
end;

procedure tform_landscape.FormCreate(Sender: TObject);
begin
  xminl := 0.0;
  xmaxl := 1.0;
  yminl := 0.0;
  ymaxl := 1.0;
  x_mat := 0;
  x_var := 0;
  y_var := 0;
end;

procedure tform_landscape.init_landscape;
var i : integer;
begin
  init_form(form_landscape);
  x_mat := 0;
  x_var := 0;
  y_var := 0;
  for i := 1 to modele_nb do
    if ( modele[i].xmat <> 0 ) then
      begin
        x_mat := modele[i].xmat;
        break;
      end;
  if ( x_mat <> 0 ) then edit_mat.Text := s_ecri_mat(x_mat)
end;

procedure tform_landscape.eigenval(var err : boolean);
{ eigenvalues }
begin
  err := true;
  with mat[x_mat] do
    begin
      matvalprop(size,val,lambda);
      if err_math then
        begin
          erreur('Error in eigenvalues computation');
          err_math := false;
          exit;
        end;
      if ( lambda[1].im <> 0.0 ) then
        begin
          erreur('No real dominant eigenvalue found');
          exit;
        end;
      lambda1 := lambda[1].re;
      if ( lambda1 <= 0.0 ) then
        begin
          erreur('Dominant eigenvalue <= 0');
          exit;
        end;
    end;
  err := false;
end;

procedure tform_landscape.FormActivate(Sender: TObject);
var err : boolean;
begin
  if ( x_mat = 0 ) then
    begin
      erreur('No matrix-type model');
      edit_mat.Text := '';
      edit_lambda.Text := '';
      exit;
    end;
  edit_mat.Text := s_ecri_mat(x_mat);
  eigenval(err); { eigenvalues }
  if err then exit;
  edit_lambda.Text := Format('%10.6f',[lambda1]);
  { check that matrix is constant: }
  if mattimedep(x_mat) then
    begin
      erreur('matrix is time-dependent');
      exit;
    end;
  if not matnonneg(mat[x_mat].size,mat[x_mat].val) then
    begin
      erreur('matrix is negative');
      exit;
    end;
  if matvecdep(x_mat,modele[mat[x_mat].xmodele].xvec) then
    begin
      erreur('matrix is vector-dependent');
      exit;
    end;
  if matrandom(x_mat) then
    begin
      erreur('matrix is random');
      exit;
    end;
  if ( x_var = 0 ) then
    begin
      edit_x.Text := '';
      edit_xmin.Text := '';
      edit_xmax.Text := '';
    end
  else
    begin
      edit_x.Text := s_ecri_var(x_var);
      edit_xmin.Text := Format('%10.2f',[xminl]);
      edit_xmax.Text := Format('%10.2f',[xmaxl]);
    end;
  if ( y_var = 0 ) then
    begin
      edit_y.Text := '';
      edit_ymin.Text := '';
      edit_ymax.Text := '';
    end
  else
    begin
      edit_y.Text := s_ecri_var(y_var);
      edit_ymin.Text := Format('%10.2f',[yminl]);
      edit_ymax.Text := Format('%10.2f',[ymaxl]);
    end;
  AutoSize := False;
end;

procedure tform_landscape.exec_landscape(m,x,y : integer;xmi,xma,ymi,yma : extended);
{ compute and display growth rate landscape of matrix m, represented by isoclines lambda(x,y) }
{ relatively to parameters x and y with bounds xmi,xma and ymi,yma respectively }
{ use dichotomy to determine isoclines }
label 1,2;
const maxtab = 100;
var j,k,iter,maxniv,niv,h,i0,ms : integer;
    x_exp_sav,x_exp_type_sav,y_exp_sav,y_exp_type_sav,x_ree,y_ree : integer;
    lambda : cvec_type;
    dx,dy,xx,yy,eps,ex,ey,y1,y2,dlamb,lamb1,lamb,lamb0 : extended;
    nbk : ismalltab_type;
    val_lab : rsmalltab_type;
begin
  form_graph.status_landscape(x_var,y_var);
  form_ulm.status_run('Fitness landscape');
  ms := clock;
  with mat[m],form_graph do
    begin
      if not gplus then efface(nil);
      matvalprop(size,val,lambda); { eigenvalues }
      if err_math then
        begin
          err_math := false;
          exit;
        end;
      if ( lambda[1].im <> 0.0 ) then exit;
      lamb  := lambda[1].re; { dominant eigenvalue }
      lamb0 := lamb; { actual value of dominant eigenvalue }
      x_ree := ree_reg1; { get pointer to value of x from internal register 1 }
      with variable[x] do
        begin
          { save expression of x because value of x will be varied: }
          x_exp_sav := exp;
          x_exp_type_sav := exp_type;
          { temporary expression: }
          exp_type := type_ree;
          exp := x_ree;
        end;
      y_ree := ree_reg2; { get pointer to value of x from internal register 2 }
      with variable[y] do
        begin
          { save expression of y because value of y will be varied }
          y_exp_sav := exp;
          y_exp_type_sav := exp_type;
          { temporary expression: }
          exp_type := type_ree;
          exp := y_ree;
        end;
      ex := xma - xmi; { range in X }
      ey := yma - ymi; { range in Y }
      dx := ex/maxtab; { deltaX }
      dy := ey/maxtab; { deltaY }
      ree[x_ree].val := xmi; { set x to minimal value }
      ree[y_ree].val := ymi; { set y to minimal value }
      init_eval; { init evaluator }
      eval_mat(m); { evaluate matrix }
      matvalprop(size,val,lambda); { eigenvalues }
      if err_math then
        begin
          err_math := false;
          goto 2;
        end;
      if ( lambda[1].im <> 0.0 ) then goto 2;
      lambdamin := lambda[1].re; { corresponding value of lambda1 }
      ree[x_ree].val := xma; { set x to maximal value }
      ree[y_ree].val := ymi; { set y to minimal value }
      init_eval; { init evaluator }
      eval_mat(m); { evaluate matrix }
      matvalprop(size,val,lambda); { eigenvalues }
      if err_math then
        begin
          err_math := false;
          goto 2;
        end;
      if ( lambda[1].im <> 0.0 ) then goto 2;
      lambdamin := min(lambdamin,lambda[1].re); { definitive minimal value of lambda1 }
      ree[x_ree].val := xma; { maximal value of x }
      ree[y_ree].val := yma; { maximal value of y }
      init_eval; { init evaluator }
      eval_mat(m); { evaluate matrix }
      matvalprop(size,val,lambda); { eigenvalues }
      if err_math then
        begin
          err_math := false;
          goto 2;
        end;
      if ( lambda[1].im <> 0.0 ) then goto 2;
      lambdamax := lambda[1].re; { corresponding value of lambda1 }
      ree[x_ree].val := xmi; { set x to minimal value }
      ree[y_ree].val := yma; { set y to maximal value }
      init_eval; { init evaluator }
      eval_mat(m); { evaluate matrix }
      matvalprop(size,val,lambda); { eigenvalues }
      if err_math then
        begin
          err_math := false;
          goto 2;
        end;
      if ( lambda[1].im <> 0.0 ) then goto 2;
      lambdamax := max(lambdamax,lambda[1].re); { definitive maximal value of lambda1 }
      calcul_echelle(lambdamin,lambdamax,dlamb,lamb1); { determine scale in lambda }
      maxniv := trunc((lambdamax - lambdamin)/dlamb); { number of levels (isoclines) of the lambda landscape }
      eps := 0.00005; { tiny value for dichotomy }
      j := 0; { index for graphic lambda-curves  }
      if ( lamb0 > lambdamin ) and ( lamb0 < lambdamax ) then
        i0 := 0 { actual value of lambda within region }
      else
        i0 := 1;
      for niv := i0 to maxniv do { loop over levels; each value of niv corresponds to an isocline }
        begin
          if ( niv = 0 ) then
            lamb := lamb0
          else
            lamb := lamb1 + niv*dlamb;
          k := 0;
          for h := 0 to maxtab do { loop over x value }
            begin
              xx := xmi + h*dx; { increment x value }
              ree[x_ree].val := xx;
              y1 := ymi;
              y2 := yma;
              ree[y_ree].val := y1;
              init_eval;
              eval_mat(m);
              matvalprop(size,val,lambda);
              if err_math then
                begin
                  err_math := false;
                  goto 1;
                end;
              if ( lambda[1].im <> 0.0 ) or ( lambda[1].re >= lamb ) then goto 1;
              ree[y_ree].val := y2; { first guess for y }
              init_eval;
              eval_mat(m);
              matvalprop(size,val,lambda);
              if err_math then
                begin
                  err_math := false;
                  goto 1;
                end;
              if ( lambda[1].im <> 0.0 ) or ( lambda[1].re <= lamb ) then goto 1;
              iter := 0;
              repeat { iteration for dichotomy to find y value }
                yy := (y1 + y2)/2.0; { midpoint }
                ree[y_ree].val := yy;
                init_eval;
                eval_mat(m);
                matvalprop(size,val,lambda);
                if err_math then
                  begin
                    err_math := false;
                    goto 1;
                  end;
                if ( lambda[1].im <> 0.0 ) then goto 1;
                if ( lambda[1].re <= lamb ) then
                  y1 := yy
                else
                  y2 := yy;
                iter := iter + 1;
                if ( iter > 30 ) then goto 1;
              until ( abs(lambda[1].re - lamb) <= eps );
              valgraph_x[j] := xx; { graphics X-coordinate of isocline }
              valgraph_y[1][j] := yy; { graphics Y-coordinate of isocline }
              j := j + 1;
              k := k + 1;
1:
              with form_ulm do { interruption procedure }
                begin
                  procproc;
                  if runstop then
                    begin
                      runstop := false;
                      goto 2;
                    end;
                end;
            end;{ loop h }
          nbk[niv] := k; { number of points in isocline of level niv  }
          val_lab[niv] := lamb; { label of isocline = lambda1 }
        end; { loop niv }
      gland(i0,maxniv,nbk,val_lab,xmi,xma,ymi,yma,x0,y0);
      { display isoclines in number maxniv }
      { array nbk contains number of points in each isocline }
      { array val_lab contains the value of the labels (lambda1) associated with each isocline }
      { x0,y0 are the coordinates of the actual value of x and y in the model, }
      { corresponding to growth rate lamb0 and represented by a circle }
    end;
2 :
  { back to original expressions: }
  with variable[x] do
    begin
      exp := x_exp_sav;
      exp_type := x_exp_type_sav;
    end;
  with variable[y] do
    begin
      exp := y_exp_sav;
      exp_type := y_exp_type_sav;
    end;
  form_ulm.run_initExecute(nil); { initialize }
  form_ulm.status_t_exec(clock - ms);
end;

procedure tform_landscape.button_execClick(Sender: TObject);
begin
  if not check_param then exit;
  if ( x_mat = 0 ) or ( x_var = 0 ) or ( y_var = 0 ) then exit;
  edit_lambdamin.Text := '';
  edit_lambdamax.Text := '';
  exec_landscape(x_mat,x_var,y_var,xminl,xmaxl,yminl,ymaxl);
  edit_lambdamin.Text := Format('%10.4g',[lambdamin]);
  edit_lambdamax.Text := Format('%10.4g',[lambdamax]);
end;

function  tform_landscape.check_param : boolean;
{ check validity of parameters }
var x,tx : integer;
begin
  check_param := false;
  trouve_obj(tronque(minuscule(edit_mat.Text)),x,tx);
  if ( x = 0 ) or ( tx <> type_mat ) then
    begin
      erreur('Unknown matrix name');
      edit_mat.Text := s_ecri_mat(x_mat);
      exit;
    end;
  x_mat := x;
  if not test_variable(edit_x.Text,x) then
    begin
      erreur('Variable X: unknown variable name');
      if ( x_var <> 0 ) then
        edit_x.Text := s_ecri_var(x_var)
      else
        edit_x.Text := '';
      exit;
    end;
  x_var := x;
  x0 := variable[x_var].val; { actual value of x, before variation }
  if not test_variable(edit_y.Text,x) then
    begin
      erreur('Variable Y: unknown variable name');
      if ( y_var <> 0 ) then
        edit_y.Text := s_ecri_var(y_var)
      else
        edit_y.Text := '';
      exit;
    end;
  y_var := x;
  y0 := variable[y_var].val; { actual value of y, before variation }
  if ( edit_xmin.Text = '' ) and ( edit_xmax.Text = '' ) then
  { default value for bounds in X and Y: }
    begin
      xminl := 0.5*x0;
      edit_xmin.Text := Format('%10.2f',[xminl]);
      xmaxl := 1.5*x0;
      if ( xmaxl = xminl ) then xmaxl := 1.0;
      edit_xmax.Text := Format('%10.2f',[xmaxl]);
    end;
  if ( edit_ymin.Text = '' ) and ( edit_ymax.Text = '' ) then
    begin
      yminl := 0.5*y0;
      edit_ymin.Text := Format('%10.2f',[yminl]);
      ymaxl := 1.5*y0;
      if ( ymaxl = yminl ) then ymaxl := 1.0;
      edit_ymax.Text := Format('%10.2f',[ymaxl]);
    end;
  { Setting xminl, xmaxl, yminl and ymaxl }
  if not TryStrToFloat(edit_xmin.Text, xminl) then
    begin
      erreur('Invalid value "' + edit_xmin.Text + '" for Xmin"'); exit;
    end;
  if not TryStrToFloat(edit_xmax.Text, xmaxl) then
    begin
      erreur('Invalid value "' + edit_xmax.Text + '" for Xmin"'); exit;
    end;
  if not TryStrToFloat(edit_ymin.Text, yminl) then
    begin
      erreur('Invalid value "' + edit_ymin.Text + '" for Ymin"'); exit;
    end;
  if not TryStrToFloat(edit_ymax.Text, ymaxl) then
    begin
      erreur('Invalid value "' + edit_ymax.Text + '" for Ymax"'); exit;
    end;
  FormActivate(nil);
  check_param := true;
end;

end.
