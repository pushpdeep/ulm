{ ---------------------------------------------------------------------------- }
{ Copyright (c) 1995-2017, Stéphane Legendre and the ULM development team      }
{                                                                              }
{ This file is part of ULM.                                                    }
{                                                                              }
{ ULM is free software: you can redistribute it and/or modify it               }
{ under the terms of the GNU General Public License as published               }
{ by the Free Software Foundation, either version 3 of the License,            }
{ or (at your option) any later version.                                       }
{                                                                              }
{ ULM is distributed in the hope that it will be useful,                       }
{ but WITHOUT ANY WARRANTY; without even the implied warranty                  }
{ of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                      }
{ See the GNU General Public License for more details.                         }
{                                                                              }
{ You should have received a copy of the GNU General Public License            }
{ along with ULM.  If not, see <http://www.gnu.org/licenses/>.                 }
{ ---------------------------------------------------------------------------- }

unit jutil;

{$MODE Delphi}

{  @@@@@@  some useful procedures  @@@@@@  }

interface

uses SysUtils,Classes,Forms,Grids,StdCtrls;

procedure iwriteln(s : string);
procedure bwriteln(slist : TStrings);
procedure erreur_(s : string);
procedure init_form(f : TForm);
function  clock : integer;
function  s_ecri_t_exec(ms : integer) : string;

implementation

uses Dialogs,jglobvar,gulm;

procedure iwriteln(s : string);
{ print a line in the memo of the main window }
begin
  if not notext then form_ulm.memo_interp.Lines.Add(s);
  if outputfile then writeln(ficout,s);
end;

procedure bwriteln(slist : TStrings);
{ print a sequence of lines in the memo of the main window }
var i : integer;
begin
  with form_ulm do with memo_interp.Lines do
    begin
      if ( Count > 10000 ) then Clear; { lose old lines }
      if not notext then AddStrings(slist);
      if outputfile then
        for i := 0 to slist.Count - 1 do writeln(ficout,slist[i]);
    end;
end;

procedure erreur_(s : string);
{ generic display of error message, string s }
begin
  MessageDlg(s,mtError,[mbOk],0);
end;

procedure init_stringgrid(w : TStringGrid);
var i,j : integer;
begin
  with w do
    begin
      for i := 0 to RowCount - 1 do
        for j := 0 to ColCount - 1 do Cells[j,i] := '';
    end;
end;

procedure init_edit(w : TEdit);
begin
  with w do Text := '';
end;

procedure init_memo(w : TMemo);
begin
  with w do Clear;
end;

procedure init_form(f : TForm);
{ initialize some visual components of form f }
var i : integer;
begin
  with f do
    for i := 0 to ComponentCount-1 do
      begin
        if ( Components[i] is TStringGrid ) then init_stringgrid(Components[i] as TStringGrid);
        if ( Components[i] is TEdit )       then init_edit(Components[i]       as TEdit);
        if ( Components[i] is TMemo )       then init_memo(Components[i]       as TMemo);
      end;
end;

function  clock : integer;
{ number of milliseconds }
var TS : TTimeStamp;
begin
  TS := DateTimeToTimeStamp(Now);
  clock := Round(TimeStampToMSecs(TS));
end; 

function  s_ecri_t_exec(ms : integer) : string;
{ convert number of milliseconds to string h,mn,s to display execution time }
var h,mn,sec : integer;
begin
  sec := ms div 1000;
  if ( sec = 0 ) then
    begin
      s_ecri_t_exec := IntToStr(ms) + ' ms';
      exit;
    end;
  if ( sec < 60 ) then
    begin
      s_ecri_t_exec := IntToStr(sec) + ' s';
      exit;
    end;
  mn := sec div 60;
  sec  := sec mod 60;
  if ( mn < 60 ) then
    begin
      s_ecri_t_exec := IntToStr(mn) + ' mn ' + IntToStr(sec) + ' s';
      exit;
    end;
  h  := mn div 60;
  mn := mn mod 60;
  s_ecri_t_exec := IntToStr(h)   + ' h '  + IntToStr(mn)  + ' mn';
end;

end.

