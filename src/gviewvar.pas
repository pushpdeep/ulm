{ ---------------------------------------------------------------------------- }
{ Copyright (c) 1995-2017, Stéphane Legendre and the ULM development team      }
{                                                                              }
{ This file is part of ULM.                                                    }
{                                                                              }
{ ULM is free software: you can redistribute it and/or modify it               }
{ under the terms of the GNU General Public License as published               }
{ by the Free Software Foundation, either version 3 of the License,            }
{ or (at your option) any later version.                                       }
{                                                                              }
{ ULM is distributed in the hope that it will be useful,                       }
{ but WITHOUT ANY WARRANTY; without even the implied warranty                  }
{ of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                      }
{ See the GNU General Public License for more details.                         }
{                                                                              }
{ You should have received a copy of the GNU General Public License            }
{ along with ULM.  If not, see <http://www.gnu.org/licenses/>.                 }
{ ---------------------------------------------------------------------------- }

unit gviewvar;

{$MODE Delphi}

{ @@@@@@  form to display variables: expression, initial value, present value  @@@@@@ }

interface

uses
  SysUtils,Classes,Variants,Graphics,Controls,Forms,Dialogs,StdCtrls,Grids,ExtCtrls,
  jglobvar,jmath;

type
  XStringGrid = class(TStringGrid)
    private
      XPrefWidth: Integer;
      XPrefHeight: Integer;
    published
      property PrefWidth: Integer read XPrefWidth write XPrefWidth;
      property PrefHeight: Integer read XPrefHeight write XPrefHeight;
      procedure CalculatePreferredSize(var PreferredWidth: Integer;
        var PreferredHeight: Integer; WithThemeSpace: Boolean); override;
  end;

type 
  tform_view_variables = class(TForm)
    bottom_panel: TPanel;
    radio_panel: TPanel;
    nbvar_label: TLabel;
    change_label: TLabel;
    alpha_order_button: TRadioButton;
    eval_order_button: TRadioButton;
    stringgrid1: XStringGrid;
    procedure FormCreate(Sender: TObject);
    procedure status;
    procedure FormActivate(Sender: TObject);
    procedure stringgrid1SelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure change_variable(x : integer; s : string);
    procedure stringgrid1SetEditText(Sender: TObject; ACol, ARow: Integer;
      const Value: String);
    procedure stringgrid1KeyPress(Sender: TObject; var Key: Char);
    procedure eval_order_buttonClick(Sender: TObject);
    procedure alpha_order_buttonClick(Sender: TObject);
  private
    alphabetical_order : boolean; { switch alphabetic order/evaluation order }
    ordre_var : array[1..variable_nb_max] of integer; { array to sort variables }
    x_var : integer; { pointer to variable to modify }
    s_exp : string;  { expression for this varaible }
    procedure erreur(s : string);
    procedure ordre_alphabetique;
    procedure ordre_evaluation;
  public
    err_viewvar : boolean;
  end;

var  form_view_variables: tform_view_variables;

implementation

uses jutil,jsymb,jsyntax,jeval,gulm;

{$R *.lfm}

procedure XStringGrid.CalculatePreferredSize(var PreferredWidth: Integer;
  var PreferredHeight: Integer; WithThemeSpace: Boolean);
begin
    PreferredWidth := Self.XPrefWidth;
    PreferredHeight := Self.XPrefHeight;
end;

procedure tform_view_variables.erreur(s : string);
{ unit specific error procedure }
begin
  erreur_('View Variables - ' + s);
  err_viewvar := true;
end;

procedure tform_view_variables.status;
begin
  nbvar_label.Caption := 'Number of variables: ' + IntToStr(variable_nb);
  if ( x_var <> 0 ) then
    change_label.Caption := 'Change: ' + s_ecri_var(x_var) + ' = ' + s_exp
  else
    change_label.Caption := '';
end;

procedure tform_view_variables.FormCreate(Sender: TObject);
begin
  with StringGrid1 do
    begin
      cells[0,0] := ' Name ';
      cells[1,0] := ' Initial value ';
      cells[2,0] := ' Current value ';
      cells[3,0] := ' Expression ';
    end;
  x_var := 0;
  s_exp := '';
  alphabetical_order := false;
  err_viewvar := false;
end;

procedure tform_view_variables.ordre_alphabetique;
{ display variables in alphabetic order }
{ sorting procedure: Shell's method, Numerical Recipes p. 229, p. 724 }
label 1;
var x,y,z,u,v,w,h,ln2 : integer;
    s1,s2 : string;
begin
  for x := 1 to variable_nb do ordre_var[x] := x;
  ln2 := trunc(ln(variable_nb)*1.442695022 + 0.00001);
  z := variable_nb;
  for x := 1 to ln2 do
    begin
      z := z div 2;
      u := variable_nb - z;
      for y := 1 to u do
        begin
          v := y;
1:
          w := v + z;
          s1 := s_ecri_var(ordre_var[w]);
          s2 := s_ecri_var(ordre_var[v]);
          if ( s1 < s2 ) then
            begin
              h := ordre_var[v];
              ordre_var[v] := ordre_var[w];
              ordre_var[w] := h;
              v := v - z;
              if ( v >= 1 ) then goto 1;
            end;
        end;
    end;
end;

procedure tform_view_variables.ordre_evaluation;
{ display variables in evaluation order (as determined by the evaluator, see jeval.pas) }
var k,i,x : integer;
begin
  ordre_var[1] := xtime; { time is the first variable }
  k := 1;
  { constants }
  for x := 2 to variable_nb do with variable[x] do
    if ( exp_type = type_ree ) then
      if ( xrel = 0 ) and ( xvec = 0 ) then
        begin
          k := k + 1;
          ordre_var[k] := x;
        end;
  { vector-variables }
  for x := 1 to modele_nb do with modele[x] do
    if ( xmat <> 0 ) then
      for i := 1 to size do with vec[xvec] do
        begin
          k := k + 1;
          ordre_var[k] := exp[i];
        end
    else
      for i := 1 to size do with rel[xrel[i]] do
        begin
          k := k + 1;
          ordre_var[k] := xvar;
        end;
  { relation-variables }
  for x := 1 to rel_nb do with rel[x] do
    if ( xmodele = 0 ) then
      begin
        k := k + 1;
        ordre_var[k] := xvar;
      end;
  { all other variables }
  for x := 1 to ordre_eval_nb do
    begin
      k := k + 1;
      ordre_var[k] := ordre_eval[x];
    end;
end;

procedure tform_view_variables.FormActivate(Sender: TObject);
var i,x,total_height : integer;
begin
  with stringgrid1 do
    begin
      RowCount := 1 + variable_nb;
      if alphabetical_order then
        ordre_alphabetique
      else
        ordre_evaluation;
      for i := 1 to variable_nb do
        begin
          x := ordre_var[i];
          with variable[x] do
            begin
              Cells[0,i] := s_ecri_var(x);          { name }
              Cells[1,i] := s_ecri_val(val0);       { initial value }
              Cells[2,i] := s_ecri_val_variable(x); { current value }
              Cells[3,i] := s_ecri(exp,exp_type);   { expression }
            end;
        end;
      total_height := 5;
      for i := 0 to variable_nb do
        begin
          total_height := total_height + RowHeights[i];
        end;
      PrefHeight := imin(total_height, Screen.Height - 100);
      AutoSizeColumns();
      PrefWidth := ColWidths[0] + ColWidths[1] + ColWidths[2] + ColWidths[3] + 5;
    end;
  status;
  AutoSize := False;
end;

procedure tform_view_variables.change_variable(x : integer; s : string);
{ modify the expression of variable x by expression s }
var v,tv : integer;
begin
  lirexp(s,v,tv); { interpret expression }
  if err_syntax then
    begin
      err_syntax  := false;
      exit;
    end;
  if ( variable[x].xrel <> 0 ) then
    if ( tv <> type_ree ) then
      begin
        erreur('relation-variable must be set to a real value');
        exit;
      end;
  set_variable(x,v,tv); { set new expression to the variable }
  form_ulm.run_initExecute(nil); { initialize }
end;

procedure tform_view_variables.StringGrid1SelectCell(Sender: TObject; ACol,
  ARow: Integer; var CanSelect: Boolean);
begin
  canselect := ( arow > 1 ) and ( acol = 3 );
end;

procedure tform_view_variables.stringgrid1SetEditText(Sender: TObject;
  ACol, ARow: Integer; const Value: String);
begin
  with stringgrid1 do
    begin
      s_exp := propre(minuscule(value)); { new expression for variable }
      x_var := trouve_variable(trouve_dic(Cells[0,arow])); { find the variable by name }
    end;
end;

procedure tform_view_variables.stringgrid1KeyPress(Sender: TObject;
  var Key: Char);
begin
  if ( key = chr(13) ) then { return pressed }
    if ( x_var <> 0 ) then
      begin
        change_variable(x_var,s_exp); { change expression of variable pointed by x_var }
        FormActivate(nil);
      end;
  x_var := 0; { change has been performed }
  s_exp := '';
end;

procedure tform_view_variables.eval_order_buttonClick(Sender: TObject);
begin
  alphabetical_order := false;
  FormActivate(nil);
end;

procedure tform_view_variables.alpha_order_buttonClick(Sender: TObject);
begin
  alphabetical_order := true;
  FormActivate(nil);
end;

end.
