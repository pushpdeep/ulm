{ ---------------------------------------------------------------------------- }
{ Copyright (c) 1995-2017, Stéphane Legendre and the ULM development team      }
{                                                                              }
{ This file is part of ULM.                                                    }
{                                                                              }
{ ULM is free software: you can redistribute it and/or modify it               }
{ under the terms of the GNU General Public License as published               }
{ by the Free Software Foundation, either version 3 of the License,            }
{ or (at your option) any later version.                                       }
{                                                                              }
{ ULM is distributed in the hope that it will be useful,                       }
{ but WITHOUT ANY WARRANTY; without even the implied warranty                  }
{ of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                      }
{ See the GNU General Public License for more details.                         }
{                                                                              }
{ You should have received a copy of the GNU General Public License            }
{ along with ULM.  If not, see <http://www.gnu.org/licenses/>.                 }
{ ---------------------------------------------------------------------------- }

unit gstocsensib;

{  @@@@@@   form for computing stochastic sensitivities of matrix model   @@@@@@  }

{$MODE Delphi}

interface

uses SysUtils,Classes,Graphics,Controls,Forms,Dialogs,StdCtrls,ExtCtrls,Grids,
     jglobvar;

type
  tform_stocsensib = class(TForm)
    top_panel: TPanel;
    main_panel: TPanel;
    sensit_bottom_panel: TPanel;
    elast_bottom_panel: TPanel;
    bottom_panel: TPanel;
    StringGrid_sens: TStringGrid;
    StringGrid_elas: TStringGrid;
    Label_sens: TLabel;
    Label_elas: TLabel;
    Label_sens_x: TLabel;
    Label_mat: TLabel;
    label_lambda_moy: TLabel;
    Edit_sens_x: TEdit;
    Label_sens_x_tot: TLabel;
    Label_elas_x_tot: TLabel;
    Edit_sens_x_tot: TEdit;
    Edit_elas_x_tot: TEdit;
    Edit_mat: TEdit;
    Edit_lambda_moy: TEdit;
    edit_nb_cyc: TEdit;
    label_nb_cyc: TLabel;
    button_run: TButton;
    button_init: TButton;
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure init_stoc_sensib;
    procedure erreur(s : string);
    procedure Edit_sens_xReturnPressed(Sender: TObject);
    procedure Edit_matReturnPressed(Sender: TObject);
    procedure construc_mat_deriv(m,x : integer);
    procedure run_stoc_sensib(m,x,nb_cyc : integer; var err : boolean);
    procedure stoc_sensib_interp(m,x,nb_cyc : integer);
    procedure affichage(m,x : integer);
    procedure affichage_interp(m,x : integer);
    procedure button_runClick(Sender: TObject);
    procedure button_initClick(Sender: TObject);
    function  check_param : boolean;
  private
    x_mat : integer;  { pointer to matrix }
    x_var : integer;  { pointer to variable for sensitivity to lower level parameter }
    nb_cyc : integer; { number of time steps for the computation }
    lambda_moy : extended; { average dominant eigenvalue }
    mat_sens : rmat_type;  { matrix of sensitivities }
    mat_elas : rmat_type;  { matrix of elasticities }
    mat_deriv_exp,mat_deriv_exp_type : imat_type; { matrix of expression/expression-type of derivatives }
    sens_x_tot,elas_x_tot : extended; { total sensitivity/elasticity of lower level parameter }
  public
  end;

var form_stocsensib: tform_stocsensib;

implementation

uses jutil,jsymb,jmath,jsyntax,jeval,gulm;

{$R *.lfm}

procedure tform_stocsensib.erreur(s : string);
{ unit specific error procedure }
begin
  erreur_('Stochastic sensitivities - ' + s);
end;

procedure tform_stocsensib.FormCreate(Sender: TObject);
begin
  x_mat := 0;
  x_var := 0;
  nb_cyc  := 100;
end;

procedure tform_stocsensib.construc_mat_deriv(m,x : integer);
{ compute matrices of derivatives of matrix m with respect to }
{ lower level parameter pointed by x: matrices of expressions and their types }
var i,j : integer;
begin
  with mat[m] do
    for i := 1 to size do
      for j := 1 to size do
        begin
          deriv(exp[i,j],exp_type[i,j],x,mat_deriv_exp[i,j],mat_deriv_exp_type[i,j]);
          if err_eval then exit;
        end;
end;

procedure tform_stocsensib.FormActivate(Sender: TObject);
begin
  if ( x_mat = 0 ) then
    begin
      erreur('No matrix-type model');
      edit_mat.Text := '';
      edit_lambda_moy.Text := '';
      exit;
    end;
  edit_mat.Text := s_ecri_mat(x_mat);
  if ( x_var = 0 ) then
    edit_sens_x.Text := ''
  else
    edit_sens_x.Text := s_ecri_var(x_var);
  edit_nb_cyc.Text := IntToStr(nb_cyc);
  AutoSize := False;
end;

procedure tform_stocsensib.init_stoc_sensib;
var i : integer;
begin
  init_form(form_stocsensib);
  nb_cyc := 100;
  x_mat := 0;
  x_var := 0;
  for i := 1 to modele_nb do
    if ( modele[i].xmat <> 0 ) then
      begin
        x_mat := modele[i].xmat;
        break;
      end;
  edit_lambda_moy.Text := '';
end;

procedure tform_stocsensib.run_stoc_sensib(m,x,nb_cyc : integer; var err : boolean);
{ computation of stochastic sensitivities according to formula of Tuljapurkar }
{ Tuljapurkar S. 1990. Population dynamics in variable environments. }
{ Lecture notes in Biomathematics, Springer Verlag. Chap. 11, pp. 87-90. }
var i,j,icyc,ms : integer;
    h,r,bv,ev,lambda1,som,soms,some : extended;
    v,w : rvec_type;
    lambda,vv,ww : cvec_type;
    b,e : rmat_type;
begin
  with form_ulm do
    status_run('Stoc. sensitivities ' + IntToStr(nb_cyc));
  ms := clock;
  err := true;
  if ( x <> 0 ) then { lower level parameter pointed by x }
    begin
      construc_mat_deriv(m,x); { matrix of derivatives }
      if err_eval then exit;
    end;
  with mat[m] do
    begin
      soms := 0.0;
      some := 0.0;
      for i := 1 to size do
        for j := 1 to size do
          begin
            b[i,j] := 0.0;
            e[i,j] := 0.0;
          end;
      lambda_moy := 0.0;
      matvalprop(size,val,lambda); { complex eigenvalues }
      if err_math then exit;
      matvecpropdroite(size,val,lambda[1],ww); { right eigenvector for the next time step }
      if err_math then exit;
      lambda1 := lambda[1].re;
      lambda_moy := lambda_moy + lambda1; { average dominant eigenvalue }
      for i := 1 to size do w[i] := ww[i].re;
      vecnormalise1(size,w); { normalize w: sum of entries = 1 }
      for icyc := 1 to nb_cyc do
        begin
          run_t;
          matvec(size,val,w,w);
          matvalprop(size,val,lambda);
          if err_math then exit;
          lambda1 := lambda[1].re;
          if ( lambda1 = 0.0 ) then exit;
          lambda_moy := lambda_moy + lambda1;
          matvecpropgauche(size,val,lambda[1],vv);  { left eigenvector }
          if err_math then exit;
          for i := 1 to size do v[i] := vv[i].re;
          r := v[1];
          if ( r <> 0.0 ) then
            for i := 1 to size do v[i] := v[i]/r; { normalize v by first entry }
          h := vecpscal(size,v,w); { scalar product v.w }
          if ( h = 0.0 ) then exit;
          som := 0.0;
          for i := 1 to size do
            for j := 1 to size do
              begin
                bv := v[i]*w[j]/h;
                if ( x <> 0 ) then { sensityvity to lower level paameter }
                  begin
                    r := eval(mat_deriv_exp[i,j],mat_deriv_exp_type[i,j]);
                    if err_eval then exit;
                    bv  := bv*r;
                    som := som + bv;
                    ev := variable[x].val*bv/lambda1;
                  end
                else { sensitivity to matrix entry }
                  ev := val[i,j]*bv/lambda1;
                b[i,j] := ((icyc-1)*b[i,j] + bv)/icyc; { compute moving average }
                e[i,j] := ((icyc-1)*e[i,j] + ev)/icyc; { compute moving average }
              end;
          if ( x <> 0 ) then { compute moving average }
            begin
              soms := ((icyc-1)*soms + som)/icyc;
              some := ((icyc-1)*some + som*(variable[x].val/lambda1))/icyc;
            end;
          matvecpropdroite(size,val,lambda[1],ww); { right eigenvector for the next time step }
          if err_math then exit;
          for i := 1 to size do w[i] := ww[i].re;
          vecnormalise1(size,w); { normalize w: sum of entries = 1 }
          with form_ulm do { interruption procedure }
            begin
              procproc;
              if runstop then exit;
              if ( t__ mod 100 = 0 ) then status_time;
            end;
        end;
      lambda_moy := lambda_moy/(1+nb_cyc); { average dominant eigenvalue }
      for i := 1 to size do
        for j := 1 to size do
          begin
            mat_sens[i,j] := b[i,j];
            mat_elas[i,j] := e[i,j];
          end;
      if ( x <> 0 ) then { total sensitivity/elasticity of lower level parameter }
        begin
          sens_x_tot := soms;
          elas_x_tot := some;
        end;
      with form_ulm do
        begin
          status_t_exec(clock - ms);
          status_time;
        end;
    end;
  err := false;
end;

procedure tform_stocsensib.affichage(m,x : integer);
{ display stochastic sensitivities/elasticities }
var i,j,n : integer;
begin
  n := mat[m].size;
  with stringgrid_sens do
    begin
      FixedCols := 1;
      FixedRows := 1;
      ColCount  := n + 1;
      RowCount  := n + 1;
      for i := 1 to ColCount-1 do Cells[i,0] := IntToStr(i);
      for i := 1 to RowCount-1 do Cells[0,i] := IntToStr(i);
      for i := 1 to n do
        for j := 1 to n do
          Cells[j,i] := Format('%10.4f',[mat_sens[i,j]]);
      AutoSizeColumns(); 
    end;
  with stringgrid_elas do
    begin
      FixedCols := 1;
      FixedRows := 1;
      ColCount  := n + 1;
      FixedCols := 1;
      RowCount  := n + 1;
      FixedRows := 1;
      for i := 1 to n do Cells[i,0] := IntToStr(i);
      for i := 1 to n do Cells[0,i] := IntToStr(i);
      for i := 1 to n do
        for j := 1 to n do
          Cells[j,i] := Format('%10.4f',[mat_elas[i,j]]);
      AutoSizeColumns(); 
    end;
  edit_lambda_moy.Text := Format('%10.6g',[lambda_moy]);
  if ( x <> 0 ) then
    begin
      edit_sens_x_tot.Text := Format('%10.4g',[sens_x_tot]);
      edit_elas_x_tot.Text := Format('%10.4g',[elas_x_tot]);
    end
  else
    begin
      edit_sens_x_tot.Text := '';
      edit_elas_x_tot.Text := '';
    end;
end;

procedure tform_stocsensib.affichage_interp(m,x : integer);
{ results that appear in the main window under the sensitivity command }
{ see procedure interp_sensibilite in unit jinterp.pas }
var i,j,n : integer;
    s : string;
begin
  n := mat[m].size;
  iwriteln('Stochastic sensitivities to lambda (' + IntToStr(nb_cyc) + ' time steps):');
  for i := 1 to n do
    begin
      s := '';
      for j := 1 to n do s := s + Format('%10.4f',[mat_sens[i,j]]);
      iwriteln(s);
    end;
  iwriteln('Stochastic elasticities to lambda (' + IntToStr(nb_cyc) + ' time steps):');
  for i := 1 to n do
    begin
      s := '';
      for j := 1 to n do s := s + Format('%10.4f',[mat_elas[i,j]]);
      iwriteln(s);
    end;
  edit_lambda_moy.Text := Format('%10.6g',[lambda_moy]);
  if ( x <> 0 ) then
    begin
      iwriteln('Stochastic sensitivity lambda to ' + s_ecri_var(x) +
               ' = ' + Format('%1.4g',[sens_x_tot]));
      iwriteln('Stochastic elasticity lambda to ' + s_ecri_var(x) +
               ' = ' + Format('%1.4g',[elas_x_tot]));
    end
  else
    begin
      edit_sens_x_tot.Text := '';
      edit_elas_x_tot.Text := '';
    end;
end;

procedure tform_stocsensib.stoc_sensib_interp(m,x,nb_cyc : integer);
{ results that appear in the main window under the sensitivity command }
var err : boolean;
begin
  run_stoc_sensib(m,x,nb_cyc,err);
  if err then
    begin
      iwriteln('Stochastic Sensitivities - Error at t = ' + IntToStr(t__));
      err_math := false;
      err_eval := false;
      exit;
    end;
  affichage_interp(m,x);
end;

procedure tform_stocsensib.button_runClick(Sender: TObject);
var err : boolean;
begin
  check_param;
  if ( x_mat = 0 ) then exit;
  run_stoc_sensib(x_mat,x_var,nb_cyc,err);
  if err then
    begin
      iwriteln('Stochastic Sensitivities - Error at t = ' + IntToStr(t__));
      err_math := false;
      err_eval := false;
      exit;
    end;
  affichage(x_mat,x_var);
end;

procedure tform_stocsensib.button_initClick(Sender: TObject);
begin
  with form_ulm do run_initExecute(nil);
  init_random; { init system random generator for computation of eigenvectors }
end;

function  tform_stocsensib.check_param : boolean;
{ check the validity of the parameter values entered }
var x,tx,n : integer;
    s : string;
begin
  check_param := false;
  trouve_obj(tronque(minuscule(edit_mat.Text)),x,tx);
  if ( x = 0 ) or ( tx <> type_mat ) then
    begin
      erreur('Unknown matrix name');
      edit_mat.Text := s_ecri_mat(x_mat);
      exit;
    end;
  x_mat := x;
  s := edit_sens_x.Text;
  if ( s = '' ) then
    x := 0
  else
    if not test_variable(edit_sens_x.Text,x) then
      begin
        erreur('Unknown variable name');
        edit_sens_x.Text := s_ecri_var(x_var);
        exit;
      end;
  x_var := x;
  if not est_entier(edit_nb_cyc.Text,n) then
    begin
      erreur('Number of time steps: integer expected');
      edit_nb_cyc.Text := IntToStr(nb_cyc);
      exit;
    end;
  if ( n <= 0 ) then
    begin
      erreur('Number of time steps: positive integer expected');
      edit_nb_cyc.Text := IntToStr(nb_cyc);
      exit;
    end;
  nb_cyc := n;
  FormActivate(nil);
  check_param := true;
end;

procedure Tform_stocsensib.Edit_matReturnPressed(Sender: TObject);
{ enter name of matrix }
var x,tx : integer;
begin
  trouve_obj(tronque(minuscule(edit_mat.Text)),x,tx);
  if ( x = 0 ) or ( tx <> type_mat ) then
    begin
      erreur('Matrix: unknown matrix name');
      edit_mat.Text := s_ecri_mat(x_mat);
      exit;
    end;
  x_mat := x;
  FormActivate(nil);
end;

procedure Tform_stocsensib.Edit_sens_xReturnPressed(Sender: TObject);
{ enter name of variable }
var x : integer;
    s : string;
begin
  s := edit_sens_x.Text;
  if ( s = '' ) then
    begin
      x_var := 0;
      FormActivate(nil);
      exit;
    end;
  if not test_variable(edit_sens_x.Text,x) then
    begin
      erreur('Variable: unknown variable name');
      edit_sens_x.Text := s_ecri_var(x_var);
      exit;
    end;
  x_var := x;
  FormActivate(nil);
end;

end.

