{ ---------------------------------------------------------------------------- }
{ Copyright (c) 1995-2017, Stéphane Legendre and the ULM development team      }
{                                                                              }
{ This file is part of ULM.                                                    }
{                                                                              }
{ ULM is free software: you can redistribute it and/or modify it               }
{ under the terms of the GNU General Public License as published               }
{ by the Free Software Foundation, either version 3 of the License,            }
{ or (at your option) any later version.                                       }
{                                                                              }
{ ULM is distributed in the hope that it will be useful,                       }
{ but WITHOUT ANY WARRANTY; without even the implied warranty                  }
{ of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                      }
{ See the GNU General Public License for more details.                         }
{                                                                              }
{ You should have received a copy of the GNU General Public License            }
{ along with ULM.  If not, see <http://www.gnu.org/licenses/>.                 }
{ ---------------------------------------------------------------------------- }

unit jrun;

{$MODE Delphi}

{  @@@@@@   run models   @@@@@@  }

interface

uses   ggraph;

procedure run(nb_cycle : integer);
procedure run_param(nb_cycle,xp : integer;amin,amax,da : extended);
procedure run_graph_distrib(fg : tform_graph);
procedure run_fin_graph_distrib(fg : tform_graph);

implementation

uses   Sysutils,Classes,
       jglobvar,
       jutil,
       jmath,
       jsyntax,
       jeval,
       jinterp,
       gulm,gtext;

{ ------  graphics  (graph.pas) ------ }

procedure run_graph_traj(fg : tform_graph);
{ trajectory }
var x,k : integer;
begin
  with fg do
    if ( t_graph_traj <= maxgraph ) then
      begin
        valgraph_x[t_graph_traj] := variable[vargraph_x].val;
        for k := 1 to nb_vargraph_y do
          begin
            x := vargraph_y[k];
            with variable[x] do valgraph_y[k][t_graph_traj] := val
          end;
      end;
end;

procedure run_graph_distrib(fg : tform_graph);
{ distribution }
var k,v : integer;
begin
  with fg do
    begin
      for k := 1 to nb_vargraph_y do
        begin
          v := trunc(variable[vargraph_y[k]].val/d_distrib);
          if ( v <= maxgraph ) then
            if distrib0  then
              valgraph_y[k][v] := valgraph_y[k][v] + 1.0
            else
              if ( v > 0 ) then
                valgraph_y[k][v] := valgraph_y[k][v] + 1.0;
        end;
    end;
end;

procedure run_init_graph_traj(fg : tform_graph);
begin
  with fg do
    dt_graph := (nb_cycle + maxgraph - 1) div maxgraph;
  run_graph_traj(fg);
end;

procedure run_init_graph_distrib(fg : tform_graph);
var i,k : integer;
begin
  with fg do
    begin
      for i := 0 to maxgraph do
        begin
          valgraph_x[i] := i*d_distrib;
          for k := 1 to nb_vargraph_y do valgraph_y[k][i] := 0.0;
        end;
        if distrib0 then run_graph_distrib(fg);
    end;
end;

procedure run_init_graph;
var f : integer;
    fg : tform_graph;
begin
  for f := 1 to maxform_graph do
    begin
      fg := tab_form_graph[f];
      with fg do if Visible then
        begin
          status_run(nb_cycle);
          t_graph_traj  := 0;
          if distrib then
            run_init_graph_distrib(fg)
          else
            run_init_graph_traj(fg);
          status_dt_graph;
        end;
    end;
end;

procedure run_graph(icyc : integer);
var f : integer;
    fg : tform_graph;
begin
  for f := 1 to maxform_graph do
    begin
      fg := tab_form_graph[f];
      with fg do if Visible then if ( icyc mod dt_graph = 0 ) then
        begin
          t_graph_traj := t_graph_traj + 1;
          if distrib then
            run_graph_distrib(fg)
          else
            run_graph_traj(fg);
        end;
    end;
end;

procedure run_fin_graph_distrib(fg : tform_graph);
label 1;
var imax,i0,i,k : integer;
    a : extended;
begin
  with fg do
    begin
      if distrib0 then i0 := 0 else i0 := 1;
      imax := 1+i0;
      for i := maxgraph downto i0 do
        for k := 1 to nb_vargraph_y do
          if ( valgraph_y[k][i] <> 0 ) then
            begin
              imax := i;
              goto 1;
            end;
1:
      for k := 1 to nb_vargraph_y do
        begin
          a := 0.0;
          for i := i0 to imax do a := a + valgraph_y[k][i];
          if ( a <> 0.0 ) then
            for i := i0 to imax do
              valgraph_y[k][i] := valgraph_y[k][i]/a;
        end;
      gdistrib(imax+1);
    end;
end;

procedure run_fin_graph_traj(fg : tform_graph);
begin
  with fg do if ( t_graph_traj > 0 ) then
    if gscatter then
      gscat(imin(t_graph_traj,maxgraph))
    else
      gtraj(imin(t_graph_traj,maxgraph));
end;

procedure run_fin_graph;
var f : integer;
    fg : tform_graph;
begin
  for f := 1 to maxform_graph do
    begin
      fg := tab_form_graph[f];
      with fg do if Visible then
        begin
          if not gplus then efface(nil);
          if distrib then
            run_fin_graph_distrib(fg)
          else
            run_fin_graph_traj(fg);
        end;
    end;
end;

{ ------  results in output text file  ------ }

procedure run_fic(icyc : integer);
var i,k : integer;
begin
  if ( icyc mod dt_texte_interp <> 0 ) then exit;
  for i := 1 to fic_nb do with fic[i] do
    begin
      write(f,t__:1);
      for k := 1 to var_fic_nb do with variable[var_fic[k]] do
        write(f,hortab,val:1:precis[k]);
      writeln(f);
    end;
end;

procedure run_init_fic;
begin
  run_fic(0);
end;

procedure run_fin_fic;
var i : integer;
begin
  for i := 1 to fic_nb do Flush(fic[i].f);
end;

{ ------  results in main window  ------ }

procedure run_texte_interp(icyc : integer);
var j : integer;
begin
  if texte_interp then
    if ( icyc mod dt_texte_interp = 0 ) then
      begin
        lines_run.Add('t = ' + IntToStr(t__));
        for j := 1 to nb_vartexte_interp do
          with variable[vartexte_interp[j]] do
            lines_run.Add('  ' + s_ecri_dic(nom) + ' = ' + Format('%1.4f',[val]));
      end;
end;

procedure run_fin_texte_interp;
var j : integer;
begin
  if not texte_interp then
    begin
      lines_run.Add('t = ' + IntToStr(t__));
      for j := 1 to nb_vartexte_interp do
        with variable[vartexte_interp[j]] do
          lines_run.Add('  ' + s_ecri_dic(nom) + ' = ' + Format('%1.4f',[val]));
    end;
  bwriteln(lines_run);
end;

procedure run_fin_texte_interp_param;
var j : integer;
begin
  lines_run.Add('t = ' + IntToStr(t__));
  for j := 1 to nb_vartexte_interp do
    with variable[vartexte_interp[j]] do
      lines_run.Add('  ' + s_ecri_dic(nom) + ' = ' + Format('%1.4f',[val]));
  bwriteln(lines_run);
end;

{ ------  output in results windows (gtext.pas) ------ }

procedure run_init_text;
var f : integer;
    ft : tform_text;
begin
  for f := 1 to maxform_text do
    begin
      ft := tab_form_text[f];
      with ft do if Visible then
        begin
          t_text := t__;
          if ( t__ = 0 ) then init_text_run;
        end;
    end;
end;

procedure run_text(icyc : integer);
var f : integer;
    ft : tform_text;
begin
  for f := 1 to maxform_text do
    begin
      ft := tab_form_text[f];
      with ft do if Visible then if ( icyc mod dt_text = 0 ) then
        begin
          t_text := t_text + 1;
          maj_text_run;
        end;
    end;
end;

procedure run_fin_text;
var f : integer;
    ft : tform_text;
begin
  for f := 1 to maxform_text do
    begin
      ft := tab_form_text[f];
      with ft do if Visible then fin_text_run;
    end;
end;

{ ------  procedure run  ------ }

var p1 : array[1..modele_nb_max] of extended; { memorize pop sizes of each model }
    t1 : integer; { memorize time }

procedure init_run;
var x : integer;
begin
  t1 := t__;
  for x := 1 to modele_nb do with modele[x] do p1[x] := pop;
  lines_run.Clear;
end;

procedure fin_run;
var x : integer;
    a : extended;
begin
  for x := 1 to modele_nb do with modele[x] do
    begin
      iwriteln(s_ecri_modele(x));
      a := exp((ln0(pop) - ln0(pop0))/t__);
      iwriteln(Format('  growth rate from [t = 0] -> %1.6f',[a]));
      if ( t1 > 0 ) then
        if ( t__ > t1 ) then
          begin
            a := exp((ln0(pop) - ln0(p1[x]))/(t__ - t1));
            iwriteln('  growth rate from [t = ' + IntToStr(t1) +
                     '] -> ' + Format('%1.6f',[a]));
          end;
    end;
end;

procedure run(nb_cycle : integer);
{ nb_cycle = number of time steps }
var icyc : integer;
begin
  init_run;
  run_init_text;
  run_init_graph;
  run_init_fic;
  for icyc := 1 to nb_cycle do { loop over time steps }
    begin
      run_t; { run one time step }
      run_graph(icyc);
      run_texte_interp(icyc);
      run_text(icyc);
      run_fic(icyc);
      with form_ulm do { interrupt procedure }
        begin
          procproc;
          if runstop then
            begin
              runstop := false;
              break;
            end;
          if ( t__ mod 100 = 0 ) then status_time;
        end;
    end;
  run_fin_graph;
  run_fin_texte_interp;
  run_fin_text;
  run_fin_fic;
  fin_run;
end;

procedure run_param(nb_cycle,xp : integer;amin,amax,da : extended);
{ multiple runs with parameter variation }
{ nb_cycle = number of time steps }
{ xp = pointer to parameter variable }
{ amin,amax = minimum and maximum value of parameter }
{ da = delta interval for variation }
{ see interp.pas, procedure interp_param }
var icyc : integer;
    a,j : extended;
begin
  a := amin;
  j := 0.0;
  variable[xp].exp := ree_reg1;
  while ( a <= amax ) do { loop over parameter values }
    begin
      ree[ree_reg1].val := a; { store value in register reg1 }
      init_eval;
      init_run;
      run_init_graph;
      for icyc := 1 to nb_cycle do  { loop over time setps }
        begin
          run_t; { run one time step (jeval.pas) }
          run_graph(icyc); { graphics }
          with form_ulm do
            begin
              procproc;
              if runstop then break;
              if ( t__ mod 100 = 0 ) then status_time;
            end;
        end;
      run_fin_graph;
      run_fin_texte_interp_param;
      fin_run;
      with form_ulm do if runstop then { interrupt }
        begin
          runstop := false;
          break;
        end;
      j := j + 1.0;
      a := amin + j*da; { increment parameter value }
    end;
  with form_ulm do
    begin
      param := false;
      variable[xparam].exp_type := param_exp_type_sav; { restore original values }
      variable[xparam].exp := param_exp_sav;
    end;
  init_eval;
  form_ulm.status_run('Init');
end;

end.

