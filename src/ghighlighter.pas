{ ---------------------------------------------------------------------------- }
{ Copyright (c) 1995-2017, Stéphane Legendre and the ULM development team      }
{                                                                              }
{ This file is part of ULM.                                                    }
{                                                                              }
{ ULM is free software: you can redistribute it and/or modify it               }
{ under the terms of the GNU General Public License as published               }
{ by the Free Software Foundation, either version 3 of the License,            }
{ or (at your option) any later version.                                       }
{                                                                              }
{ ULM is distributed in the hope that it will be useful,                       }
{ but WITHOUT ANY WARRANTY; without even the implied warranty                  }
{ of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                      }
{ See the GNU General Public License for more details.                         }
{                                                                              }
{ You should have received a copy of the GNU General Public License            }
{ along with ULM.  If not, see <http://www.gnu.org/licenses/>.                 }
{ ---------------------------------------------------------------------------- }

unit ghighlighter;
(*
 This file is based on the simple highlighter example on from lazarus.
  Edited by Guilhem in 2016 

 The ULM syntax highlighter recognize 3 kinds of patterns:
  - Comments: (line starting with {) Are colored in dark red.
  - Definitions: (token in the defkeywords array) Are colored in purple.
  - Math: (token in the mathkeywords array) Are displayed in bold.

 Note: Token starts with a space (or begining of the line) and finish
   with either a space, a endline or a opening parenthesis.
*)

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Graphics, SynEditTypes, SynEditHighlighter;

type

  { TSynUlmHl }
  TSynUlmHl = class(TSynCustomHighlighter)
  private
    FDefAttri: TSynHighlighterAttributes;
    FMathAttri: TSynHighlighterAttributes;
    FCommentAttri: TSynHighlighterAttributes;
    fIdentifierAttri: TSynHighlighterAttributes;

    procedure SetIdentifierAttri(AValue: TSynHighlighterAttributes);
    procedure SetDefAttri(AValue: TSynHighlighterAttributes);
    procedure SetMathAttri(AValue: TSynHighlighterAttributes);
    procedure SetCommentAttri(AValue: TSynHighlighterAttributes);

  protected
    FTokenPos, FTokenEnd: Integer;
    FLineText: String;
  public
    procedure SetLine(const NewValue: String; LineNumber: Integer); override;
    procedure Next; override;
    function  GetEol: Boolean; override;
    procedure GetTokenEx(out TokenStart: PChar; out TokenLength: integer); override;
    function  GetTokenAttribute: TSynHighlighterAttributes; override;
  public
    function GetToken: String; override;
    function GetTokenPos: Integer; override;
    function GetTokenKind: integer; override;
    function GetDefaultAttribute(Index: integer): TSynHighlighterAttributes; override;
    constructor Create(AOwner: TComponent); override;
  published
    (* Define attributes for the different highlights. *)
    property DefAttri: TSynHighlighterAttributes read FDefAttri
      write SetDefAttri;
    property MathAttri: TSynHighlighterAttributes read FMathAttri
      write SetMathAttri;
    property IdentifierAttri: TSynHighlighterAttributes read fIdentifierAttri
      write SetIdentifierAttri;
    property CommentAttri: TSynHighlighterAttributes read fCommentAttri
      write SetCommentAttri;

    end;
    Const
       mathkeywords : array [1..53] of string = ('t', 'cos', 'sin', 'tan',
         'acos', 'asin', 'atan', 'ln', 'ln0', 'log', 'exp', 'sqrt', 'fact',
         'abs', 'trunc', 'round', 'gauss', 'rand', 'ber', 'gamm', 'poisson',
         'geom', 'expo', 'if', 'min', 'max', 'stepf', 'gaussf', 'lognormf',
         'binomf', 'poissonf', 'nbinomf', 'nbinom1f', 'tabf', 'beta1f', 'betaf',
         'gratef', 'bdf', 'lambdaf', 'prevf', 'textf', 'meanf', 'variancef',
         'skewnessf', 'cvf', 'meanzf', 'variancezf', 'cvzf', 'nzf', 'nef',
         'nif', 'extratef', 'immratef');
       defkeywords : array [1..9] of string = ('defvar', 'deffun', 'defrel',
         'defvec', 'defmat', 'defmod', 'mat', 'vec', 'rel');
implementation


constructor TSynUlmHl.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);

  (* Create and initialize the attributes *)

  FMathAttri := TSynHighlighterAttributes.Create('math', 'math');
  AddAttribute(FMathAttri);
  FMathAttri.Style := [fsBold];

  FCommentAttri := TSynHighlighterAttributes.Create('comment', 'comment');
  AddAttribute(FCommentAttri);
  FCommentAttri.Foreground :=  clNavy;

  FDefAttri := TSynHighlighterAttributes.Create('def', 'def');
  AddAttribute(FDefAttri);
  FDefAttri.Style := [fsBold];
  FDefAttri.Foreground := clPurple;

  fIdentifierAttri := TSynHighlighterAttributes.Create('ident', 'ident');
  AddAttribute(fIdentifierAttri);
end;

(* Setters for attributes / This allows using in Object inspector*)
procedure TSynUlmHl.SetIdentifierAttri(AValue: TSynHighlighterAttributes);
begin
  fIdentifierAttri.Assign(AValue);
end;

procedure TSynUlmHl.SetCommentAttri(AValue: TSynHighlighterAttributes);
begin
  FCommentAttri.Assign(AValue);
end;

procedure TSynUlmHl.SetDefAttri(AValue: TSynHighlighterAttributes);
begin
  FDefAttri.Assign(AValue);
end;

procedure TSynUlmHl.SetMathAttri(AValue: TSynHighlighterAttributes);
begin
  FMathAttri.Assign(AValue);
end;

procedure TSynUlmHl.SetLine(const NewValue: String; LineNumber: Integer);
begin
   { This function is called by SynEdit before a line gets painted (or before highlight info is needed) }
   { This is also called, each time the text changes for *all* changed lines }
   { and may even be called for all lines after the change up to the end of text. }
   { After SetLine was called "GetToken*" should return information about the first token on the line.}
   { Note: Spaces are token too. }
   inherited;
   FLineText := NewValue;
   // Next will start at "FTokenEnd", so set this to 1
   FTokenEnd := 1;
   Next;
end;

procedure TSynUlmHl.Next;
var
  l: Integer;
begin
   {    Scan to the next token, on the line that was set by "SetLine"
    "GetToken*"  should return info about that next token. }
  // FTokenEnd should be at the start of the next Token (which is the Token we want)
  FTokenPos := FTokenEnd;
  // assume empty, will only happen for EOL
  FTokenEnd := FTokenPos;

  // Scan forward
  // FTokenEnd will be set 1 after the last char. That is:
  // - The first char of the next token
  // - or past the end of line (which allows GetEOL to work)

  l := length(FLineText);
  If FTokenPos > l then
    // At line end
    exit
  else
     if FLineText[FTokenEnd] in [#9, #40..#47, #60..#62, ' '] then
    // At Space? Find end of spaces
    while (FTokenEnd <= l) and (FLineText[FTokenEnd] in [#0..#32, #40..#47, #60..#62]) do inc(FTokenEnd)
  else
    // At Non-Space? Find end of Non-spaces
     while (FTokenEnd <= l) and not(FLineText[FTokenEnd] in [#9,' ',#40..#47,#60..#62]) do
    begin
       inc (FTokenEnd);
       if FLineText[FTokenEnd] in ['(',':'] then  exit ; // Stop the token so in 'func(' or 'func:', 'func' is a token. 
    end;
end;

function TSynUlmHl.GetEol: Boolean;
begin
  Result := FTokenPos > length(FLineText);
end;

procedure TSynUlmHl.GetTokenEx(out TokenStart: PChar; out TokenLength: integer);
begin
  TokenStart := @FLineText[FTokenPos];
  TokenLength := FTokenEnd - FTokenPos;
end;

function TSynUlmHl.GetTokenAttribute: TSynHighlighterAttributes;
   var
      match : integer;
      i	    : integer;
begin
   match := 0;
   if copy(FLineText, 0, 1) = '{' then
      begin
	 Result := CommentAttri;
	 match := 1;
      end;

    if match=0 then
      begin

   for i := 1 to Length(mathkeywords) do
      begin
	 if LowerCase(copy(FLineText, FTokenPos, FTokenEnd - FTokenPos)) = mathkeywords[i] then
	    begin
	       Result := MathAttri;
	       match := 1;
	       Break;
	    end;
      end;
      end;
   if match=0 then
      begin
   for i := 1 to Length(defkeywords) do
      begin
	 if LowerCase(copy(FLineText, FTokenPos, FTokenEnd - FTokenPos)) = defkeywords[i] then
	    begin
	       Result := DefAttri;
	       match := 1;
	       Break;
	    end;
      end;
      end;

   if match = 0 then Result := IdentifierAttri;
end;

function TSynUlmHl.GetToken: String;
begin
  Result := copy(FLineText, FTokenPos, FTokenEnd - FTokenPos);
end;

function TSynUlmHl.GetTokenPos: Integer;
begin
  Result := FTokenPos - 1;
end;

function TSynUlmHl.GetDefaultAttribute(Index: integer): TSynHighlighterAttributes;
begin
  // Some default attributes
  case Index of
    SYN_ATTR_IDENTIFIER: Result := fIdentifierAttri;
    else Result := nil;
  end;
end;

function TSynUlmHl.GetTokenKind: integer;
var
  a: TSynHighlighterAttributes;
begin
  // Map Attribute into a unique number
  a := GetTokenAttribute;
  Result := 0;
  if a = FMathAttri then Result := 2;
  if a = fIdentifierAttri then Result := 3;
  if a = FDefAttri then Result := 4;
  if a = FCommentAttri then Result := 1;
end;

end.
