{ ---------------------------------------------------------------------------- }
{ Copyright (c) 1995-2017, Stéphane Legendre and the ULM development team      }
{                                                                              }
{ This file is part of ULM.                                                    }
{                                                                              }
{ ULM is free software: you can redistribute it and/or modify it               }
{ under the terms of the GNU General Public License as published               }
{ by the Free Software Foundation, either version 3 of the License,            }
{ or (at your option) any later version.                                       }
{                                                                              }
{ ULM is distributed in the hope that it will be useful,                       }
{ but WITHOUT ANY WARRANTY; without even the implied warranty                  }
{ of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                      }
{ See the GNU General Public License for more details.                         }
{                                                                              }
{ You should have received a copy of the GNU General Public License            }
{ along with ULM.  If not, see <http://www.gnu.org/licenses/>.                 }
{ ---------------------------------------------------------------------------- }

unit jsymb;

{$MODE Delphi}


{  @@@@@@   management of lists and internal objects   @@@@@@  }

interface

uses jglobvar;

function  cons(ca,typ,cd : integer) : integer;
function  list(x,tx : integer) : integer;

procedure init_pile;
procedure push(x : integer);
function  pop : integer;

function  cre_dic(mot : string) : integer;
function  s_get_dic(x : integer) : string;
function  trouve_dic(s : string) : integer; 

function  cre_ree(val1 : extended) : integer;

function  trouve_variable(nom1 : integer) : integer;
function  cre_variable(nom1 : integer) : integer;
procedure set_variable(x,exp1,exp_type1: integer);

function  trouve_fun(nom1 : integer) : integer;
function  cre_fun(nom1 : integer) : integer;
procedure set_fun(x,nb_arg1 : integer;xarg1 : larg_type);
function  cre_arg(nom1 : integer) : integer;

function  trouve_rel(nom1 : integer) : integer;
function  cre_rel(nom1 : integer) : integer;
procedure set_rel(x : integer;exp1,exp_type1,var1 : integer);

function  trouve_vec(nom1 : integer) : integer;
function  cre_vec(nom1,size1 : integer) : integer;
procedure set_vec(x : integer;exp1,exp_type1 : ivec_type);
function  trouve_mat(nom1 : integer) : integer;
function  cre_mat(nom1,size1 : integer) : integer;
procedure set_mat(x : integer;exp1,exp_type1 : imat_type;
                  repro_nb1 : integer;repro_i1,repro_j1 : ivec_type);

function  trouve_modele(nom1 : integer) : integer;
function  cre_modele(nom1 : integer) : integer;

function  trouve_fic(nam1 : string) : integer;
function  cre_fic(nam1 : string) : integer;

procedure trouve_obj(s : string;var x,tx : integer);
procedure init_symb;

var  err_symb : boolean;

implementation

uses  jutil;

const pile_nb_max = 1000;

var   pile : array[1..pile_nb_max] of integer;
      sp   : integer;

procedure erreur_symb(s : string);
{ unit specific error procedure }
begin
  s := 'Symb - ' + s;
  erreur_(s);
  iwriteln(s);
end;

{ ------  lists  ------ }
 
procedure init_lis;
{ init list space (array lis) }
var x : integer; 
begin
  for x := 1 to lis_nb_max do lis[x].cdr := x + 1; { chain all cells: initial free list }
  lis[lis_nb_max].cdr := 0; { last cell points to 0 = empty list = nil }
  lis_lib := 1; { free list points to first free cell }
  lis_nb  := 0; { initial number of allocated cells }
end; 
 
procedure mark_lis(x : integer);
{ mark lists that are currently in use for garbage collector by negating their cdr values }
var z : integer; 
begin 
  while ( x <> 0 ) do with lis[x] do
    begin 
      z := cdr; 
      if ( z < 0 ) then exit; 
      cdr := -z - 1; 
      if ( car_type = type_lis ) then mark_lis(car); 
      x := z; 
    end;  
end; 
 
procedure gc(ca,typ,cd : integer);
{ internal garbage collector }
{ called when function cons has no list cell available }
{ ca, typ, cd are the car, type of car, and cdr of the cell to construct }
var x,i,j,n : integer;
begin 
  if ( typ = type_lis ) then mark_lis(ca); { mark car of cell to construct }
  if ( cd <> 0 ) then mark_lis(cd); { mark cdr of cell to construct }
  mark_lis(dic);  { mark lists of the dictionary }
  for x := 1 to sp do mark_lis(pile[x]); { mark lists of the internal stack }
  for x := 1 to variable_nb do with variable[x] do
    if ( exp_type = type_lis ) then mark_lis(exp); { mark lists of variable expressions }
  for x := 1 to fun_nb do with fun[x] do
    if ( exp_type = type_lis ) then mark_lis(exp); { mark lists of function expressions }
  for x := 1 to rel_nb do with rel[x] do
    if ( exp_type = type_lis ) then mark_lis(exp); { mark lists of relation expressions }
  for x := 1 to mat_nb do with mat[x] do
    for i := 1 to size do
      for j := 1 to size do
        if ( exp_type[i,j] = type_lis ) then mark_lis(exp[i,j]);  { mark lists of matrix entries expressions }
  n := 0; 
  for x := 1 to lis_nb_max do with lis[x] do { collect free (unmarked) cells }
    if ( cdr >= 0 ) then 
      begin 
        cdr := lis_lib;
        lis_lib := x; { add them to the free list }
        n := n + 1; 
      end 
    else 
      cdr := -cdr - 1; { unmark list }
  lis_nb := lis_nb - n;
  {iwriteln('-> gc : ' + IntToStr(n) + ' cells free');}
end; 
 
function  cons(ca,typ,cd : integer) : integer;
{ construct cell in list space }
{ ca = car, typ = type of car, cd = cdr }
var x : integer; 
begin 
  if ( lis_lib = 0 ) then gc(ca,typ,cd); { invoke garbage collector }
  if ( lis_lib = 0 ) then 
    begin 
      erreur_symb('no more room for lists');
      halt;  { halt program ! }
    end; 
  x := lis_lib;  { get cell from free list }
  with lis[x] do
    begin 
      lis_lib  := cdr; { update free list }
      car_type := typ; 
      car := ca; 
      cdr := cd; 
    end; 
  lis_nb := lis_nb + 1; 
  cons := x; 
end;

function  list(x,tx : integer) : integer;
begin
  list := cons(x,tx,0);
end;

{ ------ internal stack ------ }

procedure init_pile;
{ init internal stack }
begin
  sp := 0; { stack pointer = 0 }
end;

procedure push(x : integer);
{ push x on stack }
begin
  sp := sp + 1;
  if ( sp > pile_nb_max ) then
    begin
      erreur_symb('ULM stack overflow');
      halt; { !!! }
    end;
  pile[sp] := x;
end;

function  pop : integer;
{ pop stack }
begin
  pop := pile[sp];
  sp  := sp - 1;
end;

{ ------  dictionary  ------ }

procedure init_dic;
{ init dictionary }
begin 
  dic     := 0; { empty list }
  dic_nb  := 0; 
  char_nb := 0;
end; 
 
function  cre_chaine(mot : string) : integer;
{ allocate word (list of characters) in the dictionary }
var x : integer; 
    n,i : byte;
begin 
  n := length(mot); 
  x := 0;
  for i := n downto 1 do x := cons(ord(mot[i]),type_char,x);
  cre_chaine := x; 
end; 
 
function  cre_dic(mot : string) : integer;
{ allocate cell in the dictionary }
{ the dictionary is a list, each cell in the list points }
{ to a list of characters representing a word }
var n : byte; 
    x : integer;
begin 
  n := length(mot);
  x := cre_chaine(mot);
  dic := cons(x,type_lis,dic); 
  dic_nb  := dic_nb + 1; 
  char_nb := char_nb + n;
  cre_dic := x; 
end; 
 
function  s_get_dic(x : integer): string;
{ retrieve word pointed by x in the dictionary }
var s : string;
begin 
  s := '';
  while ( x <> 0 ) do with lis[x] do
    begin 
      s := s + chr(car);
      x := cdr; 
    end;
  s_get_dic := s;
end; 
 
function  trouve_dic(s : string) : integer;
{ search for word (string s) in the dictionary }
var x : integer; 
    mot : string; 
begin 
  x := dic;
  while ( x <> 0 ) do with lis[x] do
    begin
      mot := s_get_dic(car);
      if ( mot = s ) then 
        begin
          trouve_dic := car;
          exit;
        end;  
      x := cdr;   
    end;
  trouve_dic := 0; { not found }
end; 

{ ------  real numbers  ------ }

procedure mark_ree_lis(x : integer);
{ mark real number lists for garbage collector of array ree }
begin
  while ( x <> 0 ) do with lis[x] do
    begin
      if ( car_type = type_lis ) then mark_ree_lis(car);
      if ( car_type = type_ree ) then ree[car].mark := 1;
      x := cdr;
    end;
end;

procedure mark_ree;
{ mark real number cells for garbage collector of array ree }
var x,i,j : integer;
begin
  for x := 1 to ree_nb_max do ree[x].mark := 0;
  ree[ree_zero].mark := 1; { predefined cells }
  ree[ree_un  ].mark := 1;
  ree[ree_deux].mark := 1;
  ree[ree_reg1].mark := 1;
  ree[ree_reg2].mark := 1;
  for x := 1 to variable_nb do with variable[x] do
    begin
      if ( exp_type = type_ree ) then ree[exp].mark := 1;
      if ( exp_type = type_lis ) then mark_ree_lis(exp);
    end;
  for x := 1 to fun_nb do with fun[x] do
    begin
      if ( exp_type = type_ree ) then ree[exp].mark := 1;
      if ( exp_type = type_lis ) then mark_ree_lis(exp);
    end;
  for x := 1 to vec_nb do with vec[x] do
    for i := 1 to size do
      begin
        if ( exp_type[i] = type_ree ) then ree[exp[i]].mark := 1;
        if ( exp_type[i] = type_lis ) then mark_ree_lis(exp[i]);
      end;
  for x := 1 to mat_nb do with mat[x] do
    for i := 1 to size do
      for j := 1 to size do
        begin
          if ( exp_type[i,j] = type_ree ) then ree[exp[i,j]].mark := 1;
          if ( exp_type[i,j] = type_lis ) then mark_ree_lis(exp[i,j]);
        end;
end;        
        
procedure libere_ree;
{ garbage collector for real numbers array ree }
var x,y,n: integer;
begin
  mark_ree;
  n := 0;
  x := ree_deb;
  while ( x <> 0 ) do
    begin
      y := ree[x].suiv;
      if ( y <> 0 ) then
        if ( ree[y].mark = 0 ) then
          begin
            ree[x].suiv := ree[y].suiv;
            ree[y].suiv := ree_lib;
            ree_lib := y; { update free list of array ree }
            n := n + 1;
          end
        else
          x := y
      else
       x := y;
    end;
  ree_nb := ree_nb - n;
  {iwriteln('-> ' + IntToStr(n) + ' reals free');}
end;

function  cre_ree(val1 : extended) : integer;
{ allocate cell in array ree, real value val1 }
var x : integer;
begin
  if ( ree_lib = 0 ) then libere_ree; { call to garbage collector of array ree }
  if ( ree_lib = 0 ) then
    begin
      erreur_symb('too many real constants');
      halt;
    end;
  { check if val1 has a predefined value: }
  if ( val1 = 0.0 ) then 
    begin
      cre_ree := ree_zero;
      exit;
    end;
  if ( val1 = 1.0 ) then 
    begin
      cre_ree := ree_un;
      exit;
    end;
  if ( val1 = 2.0 ) then 
    begin
      cre_ree := ree_deux;
      exit;
    end;
  { otherwise construct cell in array ree: }
  x := ree_lib;
  with ree[x] do
    begin
      val     := val1;
      ree_lib := suiv; { update free list in array ree }
      suiv    := ree_deb;
    end;
  ree_deb := x;
  ree_nb  := ree_nb + 1;
  cre_ree := x;
end;

procedure init_ree;
{ init array ree }
var x : integer;
begin
  for x := 1 to ree_nb_max do ree[x].suiv := x + 1; { chain all cells }
  ree[ree_nb_max].suiv := 0; { last cell points to nil }
  ree_zero := 1;
  ree[ree_zero].val  := 0.0; { 0 }
  ree[ree_zero].suiv := 0;
  ree_un   := 2;
  ree[ree_un  ].val  := 1.0; { 1 }
  ree[ree_un  ].suiv := 1;
  ree_deux := 3;
  ree[ree_deux].val  := 2.0; { 2 }
  ree[ree_deux].suiv := 2;
  ree_reg1 := 4;
  ree[ree_reg1].val  := 0.0; { register 1 }
  ree[ree_reg1].suiv := 3;
  ree_reg2 := 5;
  ree[ree_reg2].val  := 0.0; { register 2 }
  ree[ree_reg2].suiv := 4;
  ree_lib := 6; { free list points to first free cell in array ree }
  ree_deb := 5; { first occupied cell }
  ree_nb  := 3;
end;

{ ------  variables  ------ }
 
function  trouve_variable(nom1 : integer) : integer;
{ search for variable whose name has entry nom1 in the dictionary }
var x : integer;
begin
  for x := 1 to variable_nb do with variable[x] do
    if ( nom  = nom1 ) then
      begin
        trouve_variable := x;
        exit;
      end;
  trouve_variable := 0; { not found }
end;

function  cre_variable(nom1 : integer) : integer;
{ allocate cell in array variable }
var i : integer;
begin
  variable_nb := variable_nb + 1;
  if ( variable_nb > variable_nb_max ) then
    begin
      erreur_symb('too many variables');
      halt;
    end;
  { set variable attributes to default values: }
  with variable[variable_nb] do
    begin
      nom  := nom1;
      xrel := 0;
      xvec := 0;
      exp  := 0;
      exp_type := type_inconnu;
      val0 := 0.0;
      val  := 0.0;
      te   := 0;
      for i := 1 to maxprev do prev[i] := 0.0;
      sum  := 0.0;
      sum2 := 0.0;
      moy  := 0.0;
      variance := 0.0;
      moyz  := 0.0;
      variancez := 0.0;
      nz   := 0;
      ne   := 0;
      ni   := 0;
      er   := 0.0;
      ir   := 0.0;
    end;
  cre_variable := variable_nb;
end;

procedure set_variable(x,exp1,exp_type1: integer);
{ set expression exp1 of type exp_type1 }
{ for variable at entry x of array variable }
begin
  with variable[x] do
    begin
      exp_type := exp_type1;
      exp := exp1;
    end;
end;

function  cre_variable_0(s : string;a : extended) : integer;
{ allocate cell for predefined variable with name s and real value a }
var nom,x : integer;
begin
  nom := cre_dic(s);
  x   := cre_variable(nom);
  set_variable(x,cre_ree(a),type_ree);
  variable_nb_predef := variable_nb_predef + 1;
  cre_variable_0 := x;
end;

procedure init_variable;
{ init }
begin
  variable_nb := 0; 
  variable_nb_predef := 0; 
  xtime := cre_variable_0('t',0.0); { the only predefined variable is t for time and it is set to 0 }
end; 

{ ------  function arguments  ------ }

function  trouve_arg(nom1 : integer) : integer;
{ search for argument whose name has entry nom1 in the dictionary }
var x : integer;
begin
  for x := 1 to arg_nb do with arg[x] do
    if ( nom  = nom1 ) then
      begin
        trouve_arg := x;
        exit;
      end;
  trouve_arg := 0;
end;

function  cre_arg(nom1 : integer) : integer;
{ allocate cell in array arg }
begin
  arg_nb := arg_nb + 1;
  if ( arg_nb > arg_nb_max ) then
    begin
      erreur_symb('too many function arguments');
      halt;
    end;
  with arg[arg_nb] do
    begin
      nom := nom1;
      val := 0.0;
    end;
  cre_arg := arg_nb;
end;

procedure init_arg;
begin
  arg_nb := 0;
end;

{ ------  functions  ------ }

function  trouve_fun(nom1 : integer) : integer;
{ search for function whose name has entry nom1 in the dictionary }
var x : integer;
begin
  for x := 1 to fun_nb do with fun[x] do
    if ( nom  = nom1 ) then
      begin
        trouve_fun := x;
        exit;
      end;
  trouve_fun := 0;
end;

function  cre_fun(nom1 : integer) : integer;
{ allocate cell in array fun }
var i : integer;
begin
  fun_nb := fun_nb + 1;
  if ( fun_nb > fun_nb_max ) then
    begin
      erreur_symb('too many functions');
      halt;
    end;
  with fun[fun_nb] do
    begin
      nom    := nom1;
      nb_arg := 0;
      for i := 1 to larg_nb_max do xarg[i] := 0;
      exp := 0;
      exp_type := type_inconnu;
    end;
  cre_fun := fun_nb;
end;

procedure set_fun(x,nb_arg1 : integer;xarg1 : larg_type);
{ set number of arguments nb_arg1 and list of arguments xarg1 }
{ for function at entry x of array fun }
begin
  with fun[x] do
    begin
      nb_arg := nb_arg1;
      xarg   := xarg1;
    end;
end;

function  cre_fun_0(s : string;nb_arg : integer) : integer;
{ allocate predefined function with name s and number of arguments nb_arg }
var nom,x,i : integer;
    xarg : larg_type;
begin
  nom := cre_dic(s);
  x := cre_fun(nom);
  for i := 1 to nb_arg do xarg[i] := cre_arg(1); { argument name is irrelevant }
  set_fun(x,nb_arg,xarg);
  fun[x].exp := 0;
  fun[x].exp_type := type_lis;
  fun_nb_predef := fun_nb_predef + 1;
  cre_fun_0 := x;
end;

procedure init_fun;
{ allocate predefined functions }
{ this is the first step to add a novel function }
begin
  fun_nb := 0;
  fun_nb_predef := 0;
  fun_if        := cre_fun_0('if',3);  { function if has 3 arguments }
  fun_min       := cre_fun_0('min',0); { function min has a list of arguments }
  fun_max       := cre_fun_0('max',0); { ... }
  fun_stepf     := cre_fun_0('stepf',3);
  fun_gaussf    := cre_fun_0('gaussf',2);
  fun_lognormf  := cre_fun_0('lognormf',2);
  fun_binomf    := cre_fun_0('binomf',2);
  fun_poissonf  := cre_fun_0('poissonf',2);
  fun_nbinomf   := cre_fun_0('nbinomf',2);
  fun_nbinom1f  := cre_fun_0('nbinom1f',2);
  fun_betaf     := cre_fun_0('betaf',2);
  fun_beta1f    := cre_fun_0('beta1f',2);
  fun_tabf      := cre_fun_0('tabf',0);
  fun_bicof     := cre_fun_0('bicof',2);
  fun_gratef    := cre_fun_0('gratef',1);
  fun_bdf       := cre_fun_0('bdf',4);
  fun_lambdaf   := cre_fun_0('lambdaf',2);
  fun_prevf     := cre_fun_0('prevf',2);
  fun_textf     := cre_fun_0('textf',1);

  fun_meanf      := cre_fun_0('meanf',1);
  fun_variancef  := cre_fun_0('variancef',1);
  fun_skewnessf  := cre_fun_0('skewnessf',1);
  fun_cvf        := cre_fun_0('cvf',1);
  fun_meanzf     := cre_fun_0('meanzf',1);
  fun_variancezf := cre_fun_0('variancezf',1);
  fun_cvzf       := cre_fun_0('cvzf',1);
  fun_nzf        := cre_fun_0('nzf',1);
  fun_nef        := cre_fun_0('nef',1);
  fun_nif        := cre_fun_0('nif',1);
  fun_extratef   := cre_fun_0('extratef',1);
  fun_immratef   := cre_fun_0('immratef',1);
end;

{ ------  relations  ------ }

procedure init_rel;
begin
  rel_nb := 0;
end;

function  trouve_rel(nom1 : integer) : integer;
{ search for relation whose name has entry nom1 in the dictionary }
var x : integer;
begin
  for x := 1 to rel_nb do with rel[x] do
    if ( nom = nom1 ) then
      begin
        trouve_rel := x;
        exit;
      end;
  trouve_rel := 0;
end;

function  cre_rel(nom1 : integer) : integer;
{ allocate cell in array rel }
begin
  rel_nb := rel_nb + 1;
  if ( rel_nb > rel_nb_max ) then
    begin
      erreur_symb('too many relations');
      halt;
    end;
  with rel[rel_nb] do
    begin
      nom := nom1;
      xmodele := 0;
      exp_type := type_inconnu;
      exp  := 0;
      val0 := 0.0;
      val  := 0.0;
    end;
  cre_rel := rel_nb;
end;

procedure set_rel(x : integer;exp1,exp_type1,var1 : integer);
{ set expression exp1, expression type exp_type1, and associated variable var1 }
{ for relation at entry x of array rel }
begin 
  with rel[x] do
    begin 
      exp  := exp1; 
      exp_type := exp_type1; 
      xvar := var1; 
    end; 
end;

{ ------  vectors  ------ }

procedure init_vec;
begin
  vec_nb := 0;
end;

function  trouve_vec(nom1 : integer) : integer;
{ search for vector whose name has entry nom1 in the dictionary }
var x : integer;
begin
  for x := 1 to vec_nb do with vec[x] do
    if ( nom = nom1 ) then
      begin
        trouve_vec := x;
        exit;
      end;
  trouve_vec := 0;
end;

function  cre_vec(nom1,size1 : integer) : integer;
{ allocate cell in array vec }
var i : integer;
begin
  vec_nb := vec_nb + 1;
  if ( vec_nb > vec_nb_max ) then
    begin
      erreur_symb('too many vectors');
      halt;
    end;
  with vec[vec_nb] do
    begin
      nom  := nom1;
      size := size1;
      xmodele := 0;
      for i := 1 to size do 
        begin
          exp_type[i] := type_inconnu;
          exp[i]  := 0;
          val0[i] := 0.0;
          val[i]  := 0.0;
        end;
    end;
  cre_vec := vec_nb;
end;

procedure set_vec(x : integer;exp1,exp_type1 : ivec_type);
{ set expressions exp1 and expression types exp_type1 }
{ for vector entries of vector located at entry x of array vec }
begin
  with vec[x] do
    begin
      exp_type := exp_type1;
      exp  := exp1;
    end;
end;

{ ------  matrices  ------ }

procedure init_mat;
begin
  mat_nb := 0;
end;

function  trouve_mat(nom1 : integer) : integer;
{ search for matrix whose name has entry nom1 in the dictionary }
var x : integer;
begin
  for x := 1 to mat_nb do with mat[x] do
    if ( nom = nom1 ) then
      begin
        trouve_mat := x;
        exit;
      end;
  trouve_mat := 0;
end;

function  cre_mat(nom1,size1 : integer) : integer;
{ allocate cell in array mat }
var i,j : integer;
begin
  mat_nb := mat_nb + 1;
  if ( mat_nb > mat_nb_max ) then
    begin
      erreur_symb('too many matrices');
      halt;
    end;
  with mat[mat_nb] do
    begin
      nom  := nom1;
      size := size1;
      xmodele := 0;
      for i := 1 to size do
        for j := 1 to size do
          begin
            exp_type[i,j] := type_inconnu;
            exp[i,j]  := 0;
            val0[i,j] := 0.0;
            val[i,j]  := 0.0;
          end;
    end;
  cre_mat := mat_nb;
end;

procedure set_mat(x : integer;exp1,exp_type1 : imat_type;
                  repro_nb1 : integer;repro_i1,repro_j1 : ivec_type);
{ set expressions exp1 and expression types exp_type1, }
{ and description of reproductive entries (repro_nb1, repro_i1, repro_j1) }
{ for matrix entries of matrix located at entry x of array mat }
begin
  with mat[x] do
    begin
      exp_type := exp_type1;
      exp      := exp1;
      repro_nb := repro_nb1;
      repro_i  := repro_i1;
      repro_j  := repro_j1;
    end;
end;

{ ------  models  ------ }

procedure init_modele;
begin
  modele_nb := 0;
end;

function  trouve_modele(nom1 : integer) : integer;
{ search for model whose name has entry nom1 in the dictionary }
var x : integer;
begin
  for x := 1 to modele_nb do with modele[x] do
    if ( nom = nom1 ) then
      begin
        trouve_modele := x;
        exit;
      end;
  trouve_modele := 0;
end;

function  cre_modele(nom1 : integer) : integer;
{ allocate cell in array model }
var i : integer;
begin
  modele_nb := modele_nb + 1;
  if ( modele_nb > modele_nb_max ) then
    begin
      erreur_symb('too many models');
      halt;
    end;
  with modele[modele_nb] do
    begin
      nom  := nom1;
      size := 0;
      xmat := 0;
      xvec := 0;
      for i := 1 to matmax do xrel[i] := 0;
      pop0 := 0;
      pop  := 0;
    end;
  cre_modele := modele_nb;
end;

{ ------  output files  ------ }

procedure init_fic;
begin
  fic_nb := 0;
end;

function  trouve_fic(nam1 : string) : integer;
{ search for file whose name is the string nam1 }
var i : integer;
begin
  for i := 1 to fic_nb do with fic[i] do
    if ( nam = nam1 ) then
      begin
        trouve_fic := i;
        exit;
      end;
  trouve_fic := 0;
end;

function  cre_fic(nam1 : string) : integer;
{ allocate cell in array fic }
var i : integer;
begin
  fic_nb := fic_nb + 1;
  if ( fic_nb > fic_nb_max ) then
    begin
      erreur_symb('too many files');
      exit;
    end;
  with fic[fic_nb] do
    begin
      nam := nam1;
      var_fic_nb := 0;
      for i := 1 to var_fic_nb_max do
        begin
          var_fic[i] := 0; { output variables not specified yet }
          precis[i]  := 4; { default precision set to 4 places }
        end;
    end;
  cre_fic := fic_nb;
end;

{ ------ }

procedure trouve_obj(s : string;var x,tx : integer);
{ search for internal object of name s }
{ returns entry x and internal type tx }
{ or x = 0 and tx = 0 if not found }
var nom : integer;
begin
  x  := 0;
  tx := 0;
  nom := trouve_dic(s);
  if ( nom = 0 ) then exit;
  x := trouve_variable(nom);
  if ( x <> 0 ) then
    begin
      tx := type_variable;
      exit
    end;
  x := trouve_fun(nom);
  if ( x <> 0 ) then
    begin
      tx := type_fun;
      exit
    end;
  x := trouve_rel(nom);
  if ( x <> 0 ) then
    begin
      tx := type_rel;
      exit
    end;
  x := trouve_vec(nom);
  if ( x <> 0 ) then
    begin
      tx := type_vec;
      exit
    end;
  x := trouve_mat(nom);
  if ( x <> 0 ) then
    begin
      tx := type_mat;
      exit
    end;
  x := trouve_modele(nom);
  if ( x <> 0 ) then
    begin
      tx := type_modele;
      exit
    end;
end;

{ ------  init  unit ------ }

procedure init_symb;
begin
  init_lis;
  init_pile;
  init_dic;
  init_ree;
  init_variable;
  init_arg;
  init_fun;
  init_rel;
  init_vec;
  init_mat;
  init_modele;
  init_fic;
end;

end.
