{ ---------------------------------------------------------------------------- }
{ Copyright (c) 1995-2017, Stéphane Legendre and the ULM development team      }
{                                                                              }
{ This file is part of ULM.                                                    }
{                                                                              }
{ ULM is free software: you can redistribute it and/or modify it               }
{ under the terms of the GNU General Public License as published               }
{ by the Free Software Foundation, either version 3 of the License,            }
{ or (at your option) any later version.                                       }
{                                                                              }
{ ULM is distributed in the hope that it will be useful,                       }
{ but WITHOUT ANY WARRANTY; without even the implied warranty                  }
{ of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                      }
{ See the GNU General Public License for more details.                         }
{                                                                              }
{ You should have received a copy of the GNU General Public License            }
{ along with ULM.  If not, see <http://www.gnu.org/licenses/>.                 }
{ ---------------------------------------------------------------------------- }

unit jsyntax;

{$MODE Delphi}

{  @@@@@@   analysis of mathematical expressions - text input and output   @@@@@@  }

interface

uses Classes;

procedure init_syntax;

function  lowcase(car : char) : char;
function  minuscule(s : string) : string;
function  position(s : string; c : char) : integer;
function  sous_chaine(s : string;i1,long : integer) : string;
function  propre(s : string) : string;
function  tronque(s : string) : string;
procedure coupe(s : string;pos : integer;var s1,s2 : string);
procedure separe(s : string; sep : char);

function  est_reserve(s : string) : boolean;
function  est_nom(s : string) : boolean;
function  est_entier(s : string;var n : integer) : boolean;
function  est_reel(s : string;var a : extended) : boolean;
function  lir_variable(s : string) : integer;
function  test_variable(s : string;var x : integer) : boolean;
procedure lirexp(s : string;var x,tx : integer);

function  s_ecri_dic(x : integer) : string;
function  s_ecri_var(x : integer) : string;
function  s_ecri_val(a : extended) : string;
function  s_ecri_val_variable(x : integer) : string;
function  s_ecri_variable(x : integer) : string;
procedure b_ecri_variable(x : integer);
procedure b_ecri_list_variable;
function  s_ecri_fun(x : integer) : string;
function  s_ecri_fonction(x : integer) : string;
procedure b_ecri_list_fun;
function  s_ecri_rel(x : integer) : string;
function  s_ecri_mat(x : integer) : string;
procedure b_ecri_relation(x : integer);
procedure b_ecri_list_rel_indep;
procedure b_ecri_list_rel_model(x : integer);
function  s_ecri_vec(x : integer) : string;
procedure b_ecri_vec(x : integer);
procedure b_ecri_vec2(x : integer);
function  s_ecri_val_vec(x : integer) : string;
procedure b_ecri_list_vec;
procedure b_ecri_mat(x : integer);
procedure b_ecri_mat2(x : integer);
procedure b_ecri_val_mat(x : integer);
procedure b_ecri_list_mat;
function  s_ecri_model(x : integer) : string;
function  s_ecri_modele(x : integer) : string;
procedure b_ecri_modele(x : integer);
procedure b_ecri_list_modele;

function  s_ecri(x,tx : integer) : string;
function  s_ecri2(x,tx : integer) : string;
procedure b_ecri(x,tx : integer);

var fun_compil : integer; 
    crea_var   : boolean; 
    crea_fun   : boolean;

    lines_syntax : TStrings; { used to write objects }
    lines_separe : TStrings; { used to parse strings }
    err_syntax : boolean;

implementation

uses   SysUtils,jglobvar,jsymb,jutil;

procedure init_syntax;
begin
  lines_syntax := TStringList.Create;
  lines_separe := TStringList.Create;
  DefaultFormatSettings.DecimalSeparator := '.';
end;

procedure fin_syntax;
begin
  lines_syntax.Free;
  lines_separe.Free;
end;

procedure erreur_syntaxe(s : string);
{ unit specific error procedure }
begin
  s := 'Syntax - ' + s;
  erreur_(s);
  err_syntax := true;
end;

{ -----  manipulation of strings ----- }

function  lowcase(car : char) : char;
{ returns low case version of uppercase character car }
var icar : integer;
begin
  icar := ord(car);
  if ( ( icar <= ord('Z') ) and ( icar >= ord('A') ) ) then 
    begin
      icar := icar + ord('a') - ord('A');
      car  := chr(icar);
    end;
  lowcase := car;
end;

function  minuscule(s : string) : string;
{ turns string s into low case }
var i : integer;
    t : string;
begin
  t := '';
  for i := 1 to length(s) do t := t + lowcase(s[i]);
  minuscule := t;
end;
 
function  position(s : string; c : char) : integer;
{ returns the first position (index) of char c in string s }
begin
  position := pos(c,s);
end;

function  sous_chaine(s : string;i1,long : integer) : string;
{ returns the substring of length long extracted from string s at position i1 }
begin
  sous_chaine := copy(s,i1,long);
end; 

function  propre(s : string) : string;
{ delete all blank characters from string s }
var i : integer;
    t : string;
begin
  t := '';
  for i := 1 to length(s) do
    if ( s[i] <> ' ' ) then  t := t + s[i];
  propre := t;
end; 

function  tronque(s : string) : string;
{ delete all blank characters that are suffix or postfix of string s }
var i,pos1,pos2,ls : integer;
    t : string;
begin
  ls  := length(s);
  if ( ls = 0 ) then
    begin
      tronque := '';
      exit;
    end;
  pos1 := ls;
  for i := 1 to ls do
    if ( s[i] <> ' ' ) then 
      begin
        pos1 := i;
        break;
      end;
   pos2 := 0;
   for i := ls downto 1 do
    if ( s[i] <> ' ' ) then 
      begin
        pos2 := i;
        break;
      end;
   t := '';
   for i := pos1 to pos2 do t := t + s[i];
   tronque := t;
end; 

procedure coupe(s : string;pos : integer;var s1,s2 : string);
{ parse string s at position pos into two substrings s1 and s2 }
var ls : integer;
begin
  ls := length(s);
  s1 := sous_chaine(s,1,pos-1);
  s2 := sous_chaine(s,pos+1,ls-pos);
end;

procedure separe(s : string; sep : char);
{ parse string s into substrings according to the separator sep }
{ parsed substrings are returned as entries of the TStrings object lines_separe }
var pos : integer;
    s1  : string;
begin
  with lines_separe do
    begin
      Clear;
      s := tronque(s);
      repeat
        pos := position(s,sep);
        if ( pos = 0 ) then
          begin
            Append(s);
            exit
          end;
        coupe(s,pos,s1,s);
        Append(s1);
      until false;
    end;
end;

{ ----- syntactic analysis ----- }

function  est_chiffre(car : char;var val : integer) : boolean;
{ returns true if character car is a digit }
begin
  val := ord(car) - ord('0');
  est_chiffre := ( val >= 0 ) and ( val <= 9 );
end;  

function  est_lettre(car : char) : boolean;
{ returns true if character car is a letter }
begin
  est_lettre := car in ['a'..'z'];
end;  

function  est_nom(s : string) : boolean;
{ returns true if string s is a valid name }
var c : char;
    i : integer;
    bool : boolean;
begin
  if ( s = '' ) then
    begin
      est_nom := false;
      exit;
    end;
  bool := est_lettre(s[1]);
  for i := 2 to length(s) do
    begin
      c := s[i];
      bool := bool and 
       ( est_lettre(c) or
       ( c in ['_','$','!','%','&'] ) or { '#' is reserved }
       ( c in ['0'..'9'] ) );
    end;
  est_nom := bool;
end;

function  est_entier(s : string;var n : integer) : boolean;
{ returns true if string s represents an integer }
{ in which case returns the corresponding integer n }
var i,j,l : integer;
begin
  est_entier := false;
  l := length(s);
  if ( l = 0 ) then exit;
  n := 0;
  for i := 1 to l do
    begin
      if not est_chiffre(s[i],j) then exit;
      n := 10*n + j;
    end;
  est_entier := true;
end;

{function  est_entier(s : string;var n : integer) : boolean;
begin
  est_entier := TryStrToInt(s,n);
end;}

function  est_nombre(s : string;var r : extended) : boolean;
{ returns true if string s represents a nonnegative number (integer) }
{ in which case returns the corresponding number r }
var i,j,l : integer;
begin
  est_nombre := false;
  l := length(s);
  if ( l = 0 ) then exit;
  r := 0;
  for i := 1 to l do
    begin
      if not est_chiffre(s[i],j) then exit;
      r := 10.0*r + j;
    end;
  est_nombre := true;
end;

function  est_reel0(s : string;var a : extended) : boolean;
{ returns true if string s represents a nonnegative real number (with or without decimal point) }
{ in which case returns the corresponding number a }
var pos,i : integer;
    s1,s2 : string;
    a1,a2 : extended;
begin
  est_reel0 := false;
  if ( length(s) = 0 ) then exit;
  pos := position(s,'.');
  if ( pos = 0 ) then
    begin
      if not est_nombre(s,a1) then exit
      else
        begin
          a := a1;
          est_reel0 := true;
          exit;
        end;
    end;
  coupe(s,pos,s1,s2);
  if ( s1 <> '' ) then
    if not est_nombre(s1,a1) then exit
    else
  else
    a1 := 0.0;
  if ( s2 <> '' ) then
    if not est_nombre(s2,a2) then
      exit
    else
      for i := 1 to length(s2) do a2 := a2/10.0
  else
    a2 := 0.0;
  a := a1 + a2;
  est_reel0 := true;
end;

{function  est_reel(s : string;var a : extended) : boolean;
begin
  est_reel := TryStrToFloat(s,a);
end;}

function  est_reel(s : string;var a : extended) : boolean;
{ returns true if string s represents a real number (with or without decimal point) }
{ in which case returns the corresponding real number a }
var ls : integer;
begin
  est_reel := false;
  ls := length(s);
  if ( ls = 0 ) then exit;
  if ( s[1] = '-' ) then
    begin
      s := sous_chaine(s,2,ls-1);
      est_reel := est_reel0(s,a);
      a := -a;
    end 
  else
    est_reel := est_reel0(s,a);
end;

function  est_op2(c : char;var op2 : integer) : boolean;
{ returns true if string s is a binary operator }
{ in which case returns the operator as op2 }
begin
  est_op2 := true;
  case c of
    '+'  : op2  := op2_plus;
    '-'  : op2  := op2_moins;
    '*'  : op2  := op2_mult;
    '/'  : op2  := op2_div;
    '^'  : op2  := op2_puis;
    '<'  : op2  := op2_infe;
    '>'  : op2  := op2_supe;
    '\'  : op2  := op2_mod;
    '@'  : op2  := op2_convol;
    else est_op2 := false;
  end;
end;   

function  prio_op2(op2 : integer) : integer;
{ priority of binary operator op2 for analysis of mathematical expressions }
begin
  case op2 of
    op2_plus   : prio_op2 := 4;
    op2_moins  : prio_op2 := 4;
    op2_mult   : prio_op2 := 2;
    op2_div    : prio_op2 := 2;
    op2_puis   : prio_op2 := 1;
    op2_infe   : prio_op2 := 3;
    op2_supe   : prio_op2 := 3;
    op2_mod    : prio_op2 := 2;
    op2_convol : prio_op2 := 3;
    else;
  end;
end;   

function  est_op1(var s : string;var op1 : integer) : boolean;
{ returns true if string s is a unary operator }
{ in which case returns the operator as op1 }
begin
  est_op1 := true;
  if ( s = '-' )   then op1 := op1_moins
  else
  if ( s = 'cos' ) then op1 := op1_cos
  else
  if ( s = 'sin' ) then op1 := op1_sin
  else
  if ( s = 'tan' ) then op1 := op1_tan
  else  if ( s = 'acos' )  then op1 := op1_acos
  else  if ( s = 'asin' )  then op1 := op1_asin
  else  if ( s = 'atan' )  then op1 := op1_atan
  else  if ( s = 'ln' )    then op1 := op1_ln
  else  if ( s = 'log' )   then op1 := op1_log
  else  if ( s = 'exp' )   then op1 := op1_exp
  else  if ( s = 'fact' )  then op1 := op1_fact
  else  if ( s = 'sqrt' )  then op1 := op1_sqrt
  else  if ( s = 'abs' )   then op1 := op1_abs
  else  if ( s = 'trunc' ) then op1 := op1_trunc
  else  if ( s = 'round' ) then op1 := op1_round
  else  if ( s = 'gauss' ) then op1 := op1_gauss
  else  if ( s = 'rand' )  then op1 := op1_rand
  else  if ( s = 'ber' )   then op1 := op1_ber
  else  if ( s = 'gamm' )  then op1 := op1_gamm
  else  if ( s = 'poisson' ) then op1 := op1_poisson
  else  if ( s = 'geom' )  then op1 := op1_geom
  else  if ( s = 'expo' )  then op1 := op1_expo
  else  if ( s = 'ln0' )   then op1 := op1_ln0
  else
    est_op1 := false;
end;  

function  est_reserve(s : string) : boolean;
{ returns true if string s is a reserved word }
begin
  est_reserve :=
    ( s = 't'     ) or
    ( s = '-'     ) or 
    ( s = 'cos'   ) or ( s = 'sin'  ) or ( s = 'tan'  ) or 
    ( s = 'acos'  ) or ( s = 'asin' ) or ( s = 'atan' ) or
    ( s = 'ln'    ) or ( s = 'ln0' )  or
    ( s = 'log'   ) or ( s = 'exp'  ) or
    ( s = 'sqrt'  ) or ( s = 'fact' ) or
    ( s = 'abs'   ) or ( s = 'trunc') or ( s = 'round') or
    ( s = 'gauss' ) or ( s = 'rand' ) or ( s = 'ber'  ) or
    ( s = 'gamm'  ) or ( s = 'poisson' ) or
    ( s = 'geom'  ) or ( s = 'expo'    ) or
    ( s = 'if'    ) or ( s = 'min') or ( s = 'max') or
    ( s = 'stepf' ) or
    ( s = 'gaussf') or ( s = 'lognormf') or
    ( s = 'binomf') or ( s = 'poissonf') or
    ( s = 'nbinomf') or ( s = 'nbinom1f') or
    ( s = 'tabf' )  or
    ( s = 'beta1f') or ( s = 'betaf') or
    ( s = 'gratef') or ( s = 'bdf' ) or
    ( s = 'lambdaf' ) or
    ( s = 'prevf')  or ( s = 'textf' ) or
    ( s = 'meanf')  or ( s = 'variancef' ) or
    ( s = 'skewnessf' ) or ( s = 'cvf') or
    ( s = 'meanzf') or ( s = 'variancezf' ) or
    ( s = 'cvzf') or
    ( s = 'nzf' ) or ( s = 'nef' ) or ( s = 'nif' ) or
    ( s = 'extratef' ) or ( s = 'immratef' );
end;

function  test_variable(s : string;var x : integer) : boolean;
{ returns true if string s is the name of a known variable }
{ in which case returns pointer x to the variable }
{ x is the index entry in the array variable }
var nom : integer;
begin
  test_variable := false;
  x := 0;
  if ( s = '' ) then exit;
  nom := trouve_dic(tronque(minuscule(s)));
  if ( nom = 0 ) then exit;
  x := trouve_variable(nom);
  if ( x <> 0 ) then test_variable := true;
end;

function  lir_variable(s : string) : integer;
{ reads string s as the name of a variable }
{ returns pointer to the variable }
{ the variable is created if crea_var = true }
var nom : integer;
begin
  nom := trouve_dic(s);
  if ( nom = 0 ) then
    begin
      if est_reserve(s) then
        begin
          erreur_syntaxe(s+' reserved word');
          exit;
        end;
      if crea_var then
        begin
          nom := cre_dic(s);
          lir_variable := cre_variable(nom);
        end
      else
        lir_variable := 0;
    end
  else
    lir_variable := trouve_variable(nom);
end;
 
function lir_arg(s : string;f : integer) : integer;
{ reads string s as the name of an argument of function f }
{ returns pointer to the argument }
var nam,i : integer;
begin
  nam := trouve_dic('_'+s); { argument name is prefixed by '_' in the dictionary }
  if ( nam = 0 ) then
    begin
      lir_arg := 0;
      exit;
    end;
  with fun[f] do
    for i := 1 to nb_arg do
      if ( nam = arg[xarg[i]].nom ) then
        begin
          lir_arg := xarg[i];
          exit;
        end;
  lir_arg := 0;
end;

function  lir_fun(s : string) : integer;
{ reads string s as the name of a function }
{ returns pointer to the function }
{ the function is created if crea_fun = true }
var nom : integer;
begin
  nom := trouve_dic(s);
  if ( nom = 0 ) then
    begin
      if est_reserve(s) then
        begin
          erreur_syntaxe(s + ' reserved word');
          exit;
        end;
      if crea_fun then
        begin
          nom := cre_dic(s);
          lir_fun := cre_fun(nom);
        end
      else
        lir_fun := 0;
    end
  else
    lir_fun := trouve_fun(nom);
end;

function lirlarg(s : string; f,narg : integer) : integer;
{ reads string s as the list of arguments of function f that expects narg arguments }
{ contructs and returns the list of arguments (type_lis) }
label 10;
var x,tx,i,n,ls : integer;
    c : char;
    s1,s2,niv : string;
begin
  if ( s = '' ) then
    begin
      erreur_syntaxe('missing arguments in function ' + s_ecri_fun(f));
      exit;
    end;
  ls := length(s);
  niv := '';
  for i := 1 to ls do niv := niv + ' ';
  n  := 0;
  for i := 1 to ls do
    begin
      c := s[i];
      if ( c = '(' ) then n := n + 1;
      if ( c = ')' ) then n := n - 1;
      if ( n = 0 ) then niv[i] := '0';
    end;
  if ( n > 0 ) then
    begin
      erreur_syntaxe(s);
      exit;
    end;
  for i := 1 to ls do
    if ( niv[i] = '0' ) and ( s[i] = ',' ) then goto 10;
  if ( narg > 1 ) then
    begin
      erreur_syntaxe('missing arguments in function ' + s_ecri_fun(f));
      exit;
    end;
  lirexp(s,x,tx);
  lirlarg := cons(x,tx,0);
  exit;
10 :
  if ( narg <= 1 ) and ( fun[f].nb_arg <> 0 ) then
    begin
      erreur_syntaxe('too many arguments in function ' + s_ecri_fun(f));
      exit;
    end;
  coupe(s,i,s1,s2);
  if ( s1 = '' ) or ( s2 = '' ) then
    begin
      erreur_syntaxe(s);
      exit;
    end;
  lirexp(s1,x,tx);
  if ( narg > 0 ) then
    lirlarg := cons(x,tx,lirlarg(s2,f,narg-1))
  else
    lirlarg := cons(x,tx,lirlarg(s2,f,0));
end;

procedure lirterm(s : string;var x,tx : integer);
{ reads string s as a term of a mathematical expression }
{ returns the corresponding object x of type tx }
{ e.g., x points to a function of type tx = type_fun }
var x2,tx2,op1,f,pos,ls : integer;
    a : extended;
    s1,s2 : string;
begin
  ls := length(s);
  if (s[ls] <> ')') then
    if est_nom(s) then
      begin
        if ( fun_compil <> 0 ) then
          begin
            x := lir_arg(s,fun_compil);
            if ( x <> 0 ) then
              begin
                tx := type_arg;
                exit;
              end;
          end;
        x  := lir_variable(s);
        tx := type_variable;
        if ( x = 0 ) then erreur_syntaxe(s);
        exit;
      end
    else
      begin
        if est_reel(s,a) then
          begin
            tx := type_ree;
            x  := cre_ree(a);
            exit;
          end 
        else
          begin
            erreur_syntaxe(s);
            exit;
          end;
      end;
  pos := position(s,'(');
  if ( pos = 0 ) then
    begin
      erreur_syntaxe(s);
      exit;
    end;
  coupe(sous_chaine(s,1,ls-1),pos,s1,s2);
  if ( pos > 1 ) then
    begin
      if not est_op1(s1,op1) then { function }
        begin
          if not est_nom(s1) then
            begin
              erreur_syntaxe(s);
              exit;
            end;
          f := lir_fun(s1);
          if ( f = 0 ) then
             begin
               erreur_syntaxe(s);
               exit;
             end;
          tx := type_lis;
          x  := cons(f,type_fun,lirlarg(s2,f,fun[f].nb_arg));
        end
      else { unary operator (op x) }
        begin
          lirexp(s2,x2,tx2);
          tx := type_lis;
          x  := cons(op1,type_op1,cons(x2,tx2,0));
        end;
    end
  else   {  case of a list (...)   }
    begin
      s1 := sous_chaine(s,2,ls-2);
      lirexp(s1,x,tx);
    end;
end;

procedure lirexp(s : string;var x,tx : integer);
{ reads string s as a mathematical expression }
{ returns the corresponding object x of type tx }
{ e.g., x points to a list of type type_lis }
label 10;
var op2,op1,x1,x2,tx1,tx2,prio,i,ls,n : integer;
    c : char;
    s1,s2,niv,pri : string;
begin
  ls := length(s);
  if ( ls = 0 ) then
    begin
      x  := 0;
      tx := type_lis ;
      exit;
    end;
  niv := '';
  pri := '';
  for i := 1 to ls do
    begin
      niv := niv + ' ';
      pri := pri + ' ';
    end;
  n := 0;
  for i := 1 to ls do
    begin
      c := s[i];
      if est_op2(c,op2) then pri[i] := chr(prio_op2(op2)); { priority of binary operator }
      if ( c = '(' ) then n := n + 1;
      if ( c = ')' ) then n := n - 1;
      if ( n = 0 ) then niv[i] := '0';
    end;
  if ( n > 0 ) then
    begin
      erreur_syntaxe(s);
      exit;
    end;
  for prio := 4 downto 1 do
    for i := ls downto 1 do
      if ( niv[i] = '0' ) and ( pri[i] = chr(prio) ) then goto 10;
  lirterm(s,x,tx); { read term }
  exit;
10 :
  if est_op2(s[i],op2) then; { do nothing, this is expected }
  coupe(s,i,s1,s2); { parse string s into s1 s2 at position i }
  if ( s1 = '' ) then
    if ( op2 = op2_moins ) then
      begin
        op1 := op1_moins; { special case of -exp }
        if ( ls <= 1 ) then
          begin
            erreur_syntaxe(s);
            exit;
          end;
        lirexp(s2,x2,tx2); { read expression s2 }
        if ( tx2 = type_ree ) then
          begin
            tx := type_ree;
            x  := cre_ree(-ree[x2].val);
            exit;
          end;
        tx := type_lis;
        x  := cons(op1,type_op1,cons(x2,tx2,0)); { construct (op1 x2) }
      end
    else
      begin
        erreur_syntaxe(s);
        exit;
      end
  else
    begin
      lirexp(s1,x1,tx1); { read expression s1 }
      if ( s2 = '' ) then
        begin
          erreur_syntaxe(s);
          exit;
        end;
      lirexp(s2,x2,tx2); { read expression s2 }
      tx := type_lis;
      x  := cons(op2,type_op2,cons(x1,tx1,cons(x2,tx2,0))); { construct (op2 x1 x2) }
    end;
end;

{  ------   text output of objects  ------  }

{ functions  called s_ecri_xxx return strings }
{ procedures aclled b_ecri_xxx put several strings in the stringlist lines_syntax }

function  s_ecri_dic(x : integer) : string;
{ returns dictionary entry (word) x as a string }
var s : string;
begin
  s := '';
  while ( x <> 0 ) do with lis[x] do
    begin
      s := s + chr(car);
      x := cdr;
    end;
  s_ecri_dic := s;
end;

function  s_ecri_val(a : extended) : string;
var s : string;
begin
  s := FloatToStr(a);
  if ( length(s) > 10 ) then s := Format('%1.4g',[a]);
  s_ecri_val := s;
end;

function  s_ecri_ree(x : integer) : string;
begin
  s_ecri_ree := s_ecri_val(ree[x].val);
end;

{ ------ variables ------ }

function  s_ecri_var(x : integer) : string;
begin
  with variable[x] do s_ecri_var := s_ecri_dic(nom);
end;

procedure b_ecri_variable(x : integer);
begin
  with variable[x],lines_syntax do
    begin
      Append(s_ecri_var(x) + ' = ' + s_ecri(exp,exp_type));
      Append(s_ecri_val(val0) + ' -> ' + s_ecri_val_variable(x));
    end;
end;

function  s_ecri_variable(x : integer) : string;
begin
  s_ecri_variable := s_ecri_var(x) + ' -> ' + s_ecri_val_variable(x);
end;

function  s_ecri_val_variable(x : integer) : string;
begin
  with variable[x] do s_ecri_val_variable := s_ecri_val(val)
end;

function  s_ecri_exp_variable(x : integer) : string;
begin
  with variable[x] do
    s_ecri_exp_variable := s_ecri_var(x) + ' = ' + s_ecri(exp,exp_type) + ' -> ' + s_ecri_val_variable(x);
end;

procedure b_ecri_list_variable;
var x : integer;
begin
  with lines_syntax do
    begin
      Clear;
      for x := 1 to variable_nb do with variable[x] do
        Append(s_ecri_exp_variable(x));
    end;
end;

function  s_ecri_dic2(x : integer) : string;
begin
  s_ecri_dic2 := s_ecri_dic(lis[x].cdr);
end;

{ ------ functions ------ }

function s_ecri_arg(x : integer) : string;
begin
  with arg[x] do s_ecri_arg := s_ecri_dic2(nom);
end;

function  s_ecri_fun(x : integer) : string;
begin
  with fun[x] do s_ecri_fun := s_ecri_dic(nom);
end;

function  s_ecri_fonction(x : integer) : string;
var i : integer;
    s : string;
begin
  with fun[x] do
    begin
      s := s_ecri_fun(x) + '(';
      for i := 1 to nb_arg-1 do s := s + s_ecri_arg(xarg[i]) + ',';
      s := s + s_ecri_arg(xarg[nb_arg]) + ') = ';
      s := s + s_ecri(exp,exp_type);
    end;
  s_ecri_fonction := s;
end;

procedure b_ecri_fonction(x : integer);
begin
  lines_syntax.Append(s_ecri_fonction(x));
end;

procedure b_ecri_list_fun;
var x : integer;
begin
  with lines_syntax do
    begin
      Clear;
      for x := fun_nb_predef+1 to fun_nb do
        Append(s_ecri_fonction(x));
    end;
end;

{ ------ models ------ }

function  s_ecri_model(x : integer) : string;
begin
  with modele[x] do s_ecri_model := s_ecri_dic(nom);
end;

function  s_ecri_modele(x : integer) : string;
begin
  with modele[x] do
    s_ecri_modele := 'MODEL ' + s_ecri_model(x) + ' -> ' +
                     Format('pop = %1.1f',[pop]);
end;

procedure b_ecri_modele(x : integer);
var i : integer;
    s : string;
begin
  with modele[x],lines_syntax do
    begin
      Append(s_ecri_modele(x));
      if ( xmat <> 0 ) then
        begin
          s := 'mat: ' + s_ecri_dic(mat[xmat].nom);
          Append(s);
          s := 'vec: ' + s_ecri_dic(vec[xvec].nom);
          Append(s);
        end
      else
        begin
          s := '  rel: ';
          for i := 1 to size-1 do s := s + s_ecri_rel(xrel[i]) + ', ';
          s := s + s_ecri_rel(xrel[size]);
          Append(s);
        end;
    end;
end;

procedure b_ecri_list_modele;
var x,i : integer;
begin
  with lines_syntax do
    begin
      Clear;
      for x := 1 to modele_nb do
        begin
          b_ecri_modele(x);
          with modele[x] do
            if ( xmat <> 0 ) then
              begin
                b_ecri_mat2(xmat);
                b_ecri_vec2(xvec);
              end
            else
              for i := 1 to size do b_ecri_relation(xrel[i]);
        end;
    end;
end;

{ ------ relations ------ }

function  s_ecri_rel(x : integer) : string;
begin
  with rel[x] do s_ecri_rel := s_ecri_dic(nom);
end;

function  s_ecri_relation(x : integer) : string;
begin
  with rel[x] do s_ecri_relation :=
    s_ecri_var(xvar) + ' = ' + s_ecri(exp,exp_type) +
                       ' -> ' + s_ecri_val_variable(xvar);
end;

procedure  b_ecri_relation(x : integer);
begin
  with rel[x],lines_syntax do
    begin
      Append('Relation ' + s_ecri_rel(x));
      Append(s_ecri_relation(x));
    end;
end;
 
procedure b_ecri_list_rel_indep;
var x : integer;
begin
  with lines_syntax do
    begin
      Clear;
      for x := 1 to rel_nb do with rel[x] do
        if ( xmodele = 0 ) then b_ecri_relation(x);
    end;
end;

procedure b_ecri_list_rel_model(x : integer);
var i : integer;
begin
  with lines_syntax,modele[x] do
    begin
      Clear;
      for i := 1 to size do Append(s_ecri_rel(xrel[i]));
    end;
end;

{ ------ vectors ------ }

function  s_ecri_vec(x : integer) : string;
begin
  with vec[x] do s_ecri_vec := s_ecri_dic(nom);
end;

procedure b_ecri_vec(x : integer);
var i : integer;
    s : string;
begin
  with lines_syntax,vec[x] do
    begin
      Clear;
      Append(s_ecri_dic(nom) + '(' + IntToStr(size) + ')');
      s := '';
      for i := 1 to size-1 do s := s + s_ecri(exp[i],exp_type[i]) + ',';
      s := s + s_ecri(exp[size],exp_type[size]);
      Append(s);
      Append('->');
      Append(s_ecri_val_vec(x));
    end;
end;

procedure b_ecri_vec2(x : integer);
var i : integer;
    s : string;
begin
  with lines_syntax,vec[x] do
    begin
      Append('VECTOR ' + s_ecri_dic(nom) + '(' + IntToStr(size) + ')');
      s := '';
      for i := 1 to size-1 do s := s + s_ecri(exp[i],exp_type[i]) + ',';
      s := s + s_ecri(exp[size],exp_type[size]);
      Append(s);
    end;
end;

function  s_ecri_val_vec(x : integer) : string;
var i : integer;
    s : string;
begin
  with vec[x] do
    begin
      s := '';
      for i := 1 to size-1 do s := s + s_ecri_val(val[i]) + ',';
      s := s + s_ecri_val(val[size]);
    end;
  s_ecri_val_vec := s;
end;

procedure b_ecri_list_vec;
var x : integer;
begin
  with lines_syntax do
    begin
      Clear;
      for x := 1 to vec_nb do with vec[x] do
        Append(s_ecri_dic(nom) + '(' + IntToStr(size) + ')');
    end;
end;

{ ------ matrices ------ }

function  s_ecri_mat(x : integer) : string;
begin
  with mat[x] do s_ecri_mat := s_ecri_dic(nom);
end;

procedure b_ecri_mat(x : integer);
var i,j,n : integer;
    s : string;
    matf : imat_type;
begin
  with lines_syntax,mat[x] do
    begin
      Clear;
      Append(s_ecri_dic(nom) + '(' + IntToStr(size) + ')');
      for i := 1 to size do
        for j := 1 to size do
          matf[i,j] := 0;
      if ( repro_nb > 0 ) then
        for n := 1 to repro_nb do matf[repro_i[n],repro_j[n]] := 1;
      for i := 1 to size do
        begin
          s := '';
          for j := 1 to size-1 do
            if ( matf[i,j] = 1 ) then
              s := s + s_ecri(exp[i,j],exp_type[i,j]) + '# ,'
            else
              s := s + s_ecri(exp[i,j],exp_type[i,j]) + ',';
          if ( matf[i,size] = 1 ) then
            s := s + s_ecri(exp[i,size],exp_type[i,size]) + '#'
          else
            s := s + s_ecri(exp[i,size],exp_type[i,size]);
          Append(s);
        end;
      Append('->');
      b_ecri_val_mat(x);
    end;
end;

procedure b_ecri_mat2(x : integer);
var i,j,n : integer;
    s : string;
    matf : imat_type;
begin
  with lines_syntax,mat[x] do
    begin
      Append('MATRIX ' + s_ecri_dic(nom) + '(' + IntToStr(size) + ')');
      for i := 1 to size do
        for j := 1 to size do
          matf[i,j] := 0;
      if ( repro_nb > 0 ) then
        for n := 1 to repro_nb do matf[repro_i[n],repro_j[n]] := 1;
      for i := 1 to size do
        begin
          s := '';
          for j := 1 to size-1 do
            if ( matf[i,j] = 1 ) then
              s := s + s_ecri(exp[i,j],exp_type[i,j]) + '# ,'
            else
              s := s + s_ecri(exp[i,j],exp_type[i,j]) + ',';
          if ( matf[i,size] = 1 ) then
            s := s + s_ecri(exp[i,size],exp_type[i,size]) + '#'
          else
            s := s + s_ecri(exp[i,size],exp_type[i,size]);
          Append(s);
        end;
    end;
end;

procedure b_ecri_val_mat(x : integer);
var i,j,n : integer;
    s : string;
    matf : imat_type;
begin
  with lines_syntax,mat[x] do
    begin
      for i := 1 to size do
        for j := 1 to size do
          matf[i,j] := 0;
      if ( repro_nb > 0 ) then
        for n := 1 to repro_nb do matf[repro_i[n],repro_j[n]] := 1;
      for i := 1 to size do
        begin
          s := '';
          for j := 1 to size-1 do
            if ( matf[i,j] = 1 ) then
              s := s + Format('%10.4f',[val[i,j]]) + '# , '
            else
              s := s + Format('%10.4f',[val[i,j]]) + ', ';
          if ( matf[i,size] = 1 ) then
            s := s + Format('%10.4f',[val[i,size]]) + '#'
          else
            s := s + Format('%10.4f',[val[i,size]]);
          Append(s);
        end;
    end;
end; 

procedure b_ecri_list_mat;
var x : integer;
begin
  with lines_syntax do
    begin
      Clear;
      for x := 1 to mat_nb do with mat[x] do
        Append(s_ecri_dic(nom) + '(' + IntToStr(size) + ')');
    end;
end;

{ ------ mathematical expressions ------ }

function  s_ecri_op1(x : integer) : string;
begin
  case x of
    op1_moins   : s_ecri_op1 := '-';
    op1_cos     : s_ecri_op1 := 'cos';
    op1_sin     : s_ecri_op1 := 'sin';
    op1_tan     : s_ecri_op1 := 'tan';
    op1_acos    : s_ecri_op1 := 'acos';
    op1_asin    : s_ecri_op1 := 'asin';
    op1_atan    : s_ecri_op1 := 'atan';
    op1_ln      : s_ecri_op1 := 'ln';
    op1_ln0     : s_ecri_op1 := 'ln0';
    op1_log     : s_ecri_op1 := 'log';
    op1_exp     : s_ecri_op1 := 'exp';
    op1_fact    : s_ecri_op1 := 'fact';
    op1_sqrt    : s_ecri_op1 := 'sqrt';
    op1_abs     : s_ecri_op1 := 'abs';
    op1_trunc   : s_ecri_op1 := 'trunc';
    op1_round   : s_ecri_op1 := 'round';
    op1_gauss   : s_ecri_op1 := 'gauss';
    op1_rand    : s_ecri_op1 := 'rand';
    op1_ber     : s_ecri_op1 := 'ber';
    op1_gamm    : s_ecri_op1 := 'gamm';
    op1_poisson : s_ecri_op1 := 'poisson';
    op1_geom    : s_ecri_op1 := 'geom';
    op1_expo    : s_ecri_op1 := 'expo';
    else s_ecri_op1 := 'ecri_op1?';
  end;
end;

function  s_ecri_op2(x : integer) : string;
begin
  case x of
    op2_plus   : s_ecri_op2 := ' + ';
    op2_mult   : s_ecri_op2 := ' * ';
    op2_moins  : s_ecri_op2 := ' - ';
    op2_div    : s_ecri_op2 := ' / ';
    op2_puis   : s_ecri_op2 := '^';
    op2_infe   : s_ecri_op2 := ' < ';
    op2_supe   : s_ecri_op2 := ' > ';
    op2_mod    : s_ecri_op2 := ' \ ';
    op2_convol : s_ecri_op2 := ' @ ';
    else s_ecri_op2 := 'ecri_op2?';
  end;
end;

function  s_ecri_lis(x : integer) : string;
{ output list x }
var y,ty,z,prio : integer;
    s : string;
begin
  s := '';
  if ( x = 0 ) then
    begin
      s_ecri_lis := '';
      exit;
    end;
  with lis[x] do
    begin
      case car_type of
      type_op1 : begin
                   s  := s + s_ecri_op1(car);
                   z  := cdr;
                   y  := lis[z].car;
                   ty := lis[z].car_type;
                   s  := s + '(';
                   s  := s + s_ecri(y,ty);
                   s  := s + ')';
                 end;
      type_op2 : begin
                   z  := cdr;
                   y  := lis[z].car;
                   ty := lis[z].car_type;
                   prio := prio_op2(car);
                   if ( ( prio <= 3 ) and ( ty = type_lis ) ) then s := s + '(';
                   s := s + s_ecri(y,ty);
                   if ( ( prio <= 3 ) and ( ty = type_lis ) ) then s := s + ')';
                   s := s + s_ecri_op2(car);
                   z  := lis[z].cdr;
                   y  := lis[z].car;
                   ty := lis[z].car_type;
                   if {( ( prio <= 3 ) and} ( ty = type_lis ) then s := s + '(';
                   s := s + s_ecri(y,ty);
                   if {( ( prio <= 3 ) and} ( ty = type_lis ) then s := s + ')';
                 end;
      type_fun : begin
                   with fun[car] do
                     begin
                       s := s + s_ecri_fun(car);
                       s := s + '(';
                       z  := lis[x].cdr;
                       while z <> 0 do with lis[z] do
                         begin
                           y  := car;
                           ty := car_type;
                           s := s + s_ecri(y,ty);
                           z := cdr;
                           if ( z <> 0 ) then s := s + ',';
                         end;
                       s := s + ')';
                     end;
                 end;
      else
        begin
          s := s + '(';
          while x <> 0 do with lis[x] do
            begin
              s := s + s_ecri(car,car_type);
              x := cdr;
              if ( x <> 0 ) then s := s + ' ';
            end;
          s := s + ')';
        end
      end;
    end;
  s_ecri_lis := s;
end;

{ ------  general  ------ }

function  s_ecri(x,tx : integer) : string;
{ output object x of type tx }
begin
  case tx of
    type_lis       : s_ecri := s_ecri_lis(x);
    type_ree       : s_ecri := s_ecri_ree(x);
    type_variable  : s_ecri := s_ecri_var(x);
    type_fun       : s_ecri := s_ecri_fun(x);
    type_arg       : s_ecri := s_ecri_arg(x);
    type_rel       : s_ecri := s_ecri_rel(x);
    type_vec       : s_ecri := s_ecri_vec(x);
    type_mat       : s_ecri := s_ecri_mat(x);
    type_modele    : s_ecri := s_ecri_model(x);
    type_inconnu   : s_ecri := ' ? ';
    else
      s_ecri := 'ecri? ';
  end;
end;

function  s_ecri2(x,tx : integer) : string;
begin
  case tx of
    type_lis       : s_ecri2 := s_ecri_lis(x);
    type_ree       : s_ecri2 := s_ecri_ree(x);
    type_variable  : s_ecri2 := s_ecri_variable(x);
    type_fun       : s_ecri2 := s_ecri_fun(x);
    type_arg       : s_ecri2 := s_ecri_arg(x);
    type_rel       : s_ecri2 := s_ecri_rel(x);
    type_vec       : s_ecri2 := s_ecri_vec(x);
    type_mat       : s_ecri2 := s_ecri_mat(x);
    type_modele    : s_ecri2 := s_ecri_modele(x);
    type_inconnu   : s_ecri2 := ' ? ';
    else
      s_ecri2 := 'ecri2? ';
  end;
end;

procedure b_ecri(x,tx : integer);
begin
  lines_syntax.Clear;
  case tx of
    type_lis       : ;
    type_ree       : ;
    type_variable  : b_ecri_variable(x);
    type_fun       : b_ecri_fonction(x);
    type_arg       : ;
    type_rel       : b_ecri_relation(x);
    type_vec       : b_ecri_vec(x);
    type_mat       : b_ecri_mat(x);
    type_modele    : b_ecri_modele(x);
    type_inconnu   : ;
    else;
  end;
end;
 
end.
