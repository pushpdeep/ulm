{ ---------------------------------------------------------------------------- }
{ Copyright (c) 1995-2017, Stéphane Legendre and the ULM development team      }
{                                                                              }
{ This file is part of ULM.                                                    }
{                                                                              }
{ ULM is free software: you can redistribute it and/or modify it               }
{ under the terms of the GNU General Public License as published               }
{ by the Free Software Foundation, either version 3 of the License,            }
{ or (at your option) any later version.                                       }
{                                                                              }
{ ULM is distributed in the hope that it will be useful,                       }
{ but WITHOUT ANY WARRANTY; without even the implied warranty                  }
{ of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                      }
{ See the GNU General Public License for more details.                         }
{                                                                              }
{ You should have received a copy of the GNU General Public License            }
{ along with ULM.  If not, see <http://www.gnu.org/licenses/>.                 }
{ ---------------------------------------------------------------------------- }

unit gedit;

{$MODE Delphi}

{  @@@@@@   form to display/edit model file(s)   @@@@@@  }

interface

uses
  SysUtils, Classes, Variants, Graphics, Controls, Forms, Dialogs,
  ComCtrls, ExtCtrls, StdCtrls, Menus, ActnList, StdActns, SynEdit, ghighlighter;

type

  { tform_edit }

  tform_edit = class(TForm)
    MainMenu1: TMainMenu;
    StatusBar1: TStatusBar;
    DummySynEdit: TSynEdit; {Only used to set options in gedit.lfm and apply them to the new synedit dynamically created.}
    FSynUlmHl: TSynUlmHl;
    ToolBar1: TToolBar;
    ActionList1: TActionList;
    file_compil: TAction;
    File1: TMenuItem;
    Open1: TMenuItem;
    Save1: TMenuItem;
    saveAs1: TMenuItem;
    Compile1: TMenuItem;
    EditCut1: TEditCut;
    EditCopy1: TEditCopy;
    EditPaste1: TEditPaste;
    file_open_button: TToolButton;
    file_save_button: TToolButton;
    Separator1: TToolButton;
    Separator2: TToolButton;
    edit_cut_button: TToolButton;
    N1: TMenuItem;
    Edit1: TMenuItem;
    Copy1: TMenuItem;
    Paste1: TMenuItem;
    file_open: TAction;
    file_save: TAction;
    file_saveas: TAction;
    memo1: TMemo; { contains the lines of the current model file }
    N2: TMenuItem;
    Exit1: TMenuItem;
    N3: TMenuItem;
    New1: TMenuItem;
    file_exit: TAction;
    file_new: TAction;
    edit_copy_button: TToolButton;
    edit_paste_button: TToolButton;
    ToolButton3: TToolButton;
    file_compil_button: TToolButton;
    file_new_button: TToolButton;
    file_close: TAction;
    Close1: TMenuItem;
    pagecontrol1: TPageControl; { different pages for different model files }
    procedure memo1Change(Sender: TObject);
    procedure synChange(Sender: TObject);
    procedure newpage(filename : string);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure status(filename : string);
    procedure file_openExecute(Sender: TObject);
    procedure file_saveExecute(Sender: TObject);
    procedure file_saveasExecute(Sender: TObject);
    procedure file_newExecute(Sender: TObject);
    procedure file_exitExecute(Sender: TObject);
    procedure file_compilExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var action1: TCloseAction);
    procedure PageControl1Change(Sender: TObject);
    procedure file_closeExecute(Sender: TObject);
  private
  public
  end;

var  form_edit: tform_edit;

implementation

uses jglobvar,gulm;

{$R *.lfm}


procedure tform_edit.synChange(Sender: TObject);
begin
  memo1.Lines := TSynEdit(pagecontrol1.ActivePage.Controls[0]).Lines;
  status(TSynEdit(pagecontrol1.ActivePage.Controls[0]).Hint);
end;

procedure tform_edit.memo1Change(Sender: TObject);
begin

end;

procedure tform_edit.newpage(filename : string);
{ create new page for model file with name filename }
{ called from gulm.pas, see procedure tform_ulm.FormShow }
var
   syn	: TSynEdit;
begin
  with TTabSheet.Create(nil) do
    begin
      PageControl := pagecontrol1;
      Caption := ExtractFileName(filename);
    end;
   
   syn := TSynEdit.Create(nil); // Create a new TSynEdit object.
   syn.Parent := pagecontrol1.Pages[pagecontrol1.PageCount - 1]; // Put in the current tab.
   syn.Highlighter := FSynUlmHl; // Bind the syntax highlighter.
   syn.Align  := alClient;
   syn.Hint := filename;
   syn.OnChange := synChange; // Update memo1 on change.
   syn.Lines.Text := memo1.Lines.Text; // Use the text from the current buffer.
   syn.Keystrokes :=  DummySynEdit.Keystrokes; // Use the default keybindings from DummySynEdit.
   syn.Font := DummySynEdit.Font;
   
  with pagecontrol1 do ActivePageIndex := PageCount - 1;
  status(filename);
end;

procedure tform_edit.FormCreate(Sender: TObject);
begin
  FSynUlmHl:= TSynUlmHl.Create(Self);  
  Left := 0;
  Width := Trunc(Screen.Width / 2) - 8;
  Top := Trunc(Screen.Height / 3);
  Height := Screen.Height - Top - 30; {30px is height of the titlebar in Window Vista}
  memo1.Clear;
  pagecontrol1.Parent := Self;
  pagecontrol1.Align  := alClient;
end;

procedure tform_edit.status(filename : string);
begin
  statusbar1.Panels[0].Text := filename;
end;

procedure tform_edit.FormShow(Sender: TObject);
begin
  status(nomfic);
end;

procedure tform_edit.file_openExecute(Sender: TObject);
{ open file, see gulm.pas }
begin
  form_ulm.fileopenExecute(nil);
end;

procedure tform_edit.file_saveExecute(Sender: TObject);
{ save file, see gulm.pas }
begin
  form_ulm.filesaveExecute(nil);
end;

procedure tform_edit.file_saveasExecute(Sender: TObject);
{ save file, see gulm.pas }
begin
  form_ulm.filesaveasExecute(nil);
end;

procedure tform_edit.file_newExecute(Sender: TObject);
{ new file, see gulm.pas }
begin
  form_ulm.filenewExecute(nil);
end;

procedure tform_edit.file_exitExecute(Sender: TObject);
begin
   Close;
   Visible := false; {Close the window: do not close the tabs, if the window is oppened again, they will still be here)}
end;

procedure tform_edit.file_compilExecute(Sender: TObject);
{ compile file, see gulm.pas }
begin
  form_ulm.filecompileExecute(nil);
  status(nomfic);
end;

procedure tform_edit.FormClose(Sender: TObject; var action1: TCloseAction);
{ called on closing main form, see gulm.pas procedure tform_ulm.FormClose }
var i,res : integer;
    cancel : boolean;
begin
  if ( Sender = self ) then
    begin
      action1 := caHide; { the edit form cannot be closed, it is just hidden! }
      exit;
    end;
  cancel  := false;
  { save file(s) }
  with pagecontrol1 do
    for i := 0 to PageCount - 1 do with Pages[i] do
      begin
        if TSynEdit(Controls[0]).Modified then
          begin
            nomfic := TSynEdit(Controls[0]).Hint;
            res := MessageDlg('Save file ' + ExtractFileName(nomfic) + ' ?',
                      mtConfirmation,[mbYes,mbNo,mbCancel],0);
            case res of
              mrYes    : form_ulm.filesaveExecute(nil);
              mrNo     : ;
              mrCancel : cancel := true;
            end;
          end;
      end;
  if cancel then action1 := caNone else action1 := caFree;
end;

procedure tform_edit.pagecontrol1Change(Sender: TObject);
{ on change }
begin
  with pagecontrol1 do
    if ( PageCount > 0) and (ActivePageIndex >= 0 ) then with ActivePage do
      if ( ControlCount > 0 ) then
        begin
          memo1.Lines := TSynEdit(Controls[0]).Lines;
          nomfic := TSynEdit(Controls[0]).Hint;
          status(nomfic);
        end;
end;

procedure tform_edit.file_closeExecute(Sender: TObject);
{ close file, save if modified }
var res, i, closed : integer;
begin
  with pagecontrol1.ActivePage do
    begin
      if TSynEdit(Controls[0]).Modified then
        begin
          res := MessageDlg('Save file ' + ExtractFileName(nomfic) + ' ?',
                      mtConfirmation,[mbYes,mbNo,mbCancel],0);
          case res of
            mrYes : form_ulm.filesaveExecute(nil);
            mrCancel : exit;
          else
          end;
        end;
      TabVisible := false;
      TSynEdit(Controls[0]).Hint := 'closed';
      TSynEdit(Controls[0]).Clear;
    end;

   // If the last tab is closed, create a new tab.
   closed := 0;
   for i := 0 to pagecontrol1.PageCount - 1 do
     if(TSynEdit(pagecontrol1.Pages[i].Controls[0]).Hint = 'closed') then
       closed:=closed+1;
   if closed = pagecontrol1.PageCount then form_ulm.filenewExecute(nil);
   
   // Avoid having ActivePageIndex to -1 (which leads to access violations in OnChange).  
   if (pagecontrol1.ActivePageIndex = -1) then pagecontrol1.ActivePageIndex := 0;
end;
end.
