{ ---------------------------------------------------------------------------- }
{ Copyright (c) 1995-2017, Stéphane Legendre and the ULM development team      }
{                                                                              }
{ This file is part of ULM.                                                    }
{                                                                              }
{ ULM is free software: you can redistribute it and/or modify it               }
{ under the terms of the GNU General Public License as published               }
{ by the Free Software Foundation, either version 3 of the License,            }
{ or (at your option) any later version.                                       }
{                                                                              }
{ ULM is distributed in the hope that it will be useful,                       }
{ but WITHOUT ANY WARRANTY; without even the implied warranty                  }
{ of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                      }
{ See the GNU General Public License for more details.                         }
{                                                                              }
{ You should have received a copy of the GNU General Public License            }
{ along with ULM.  If not, see <http://www.gnu.org/licenses/>.                 }
{ ---------------------------------------------------------------------------- }

unit gtextset;

{$MODE Delphi}

{  @@@@@@   form for setting variables to display (gtext.pas)  @@@@@@  }

interface

uses
  SysUtils, Classes, Variants, Graphics, Controls, Forms, Dialogs,
  Grids, ExtCtrls, StdCtrls, Buttons,
  gtext;

type
  tform_textset = class(TForm)
    bottom_panel: TPanel;
    button_panel: TPanel;
    dt_text_panel: TPanel;
    StatusPanel: TPanel;
    nbvar_label: TLabel;
    sg_vartext: TStringGrid;
    ok_button: TBitBtn;
    apply_button: TBitBtn;
    cancel_button: TBitBtn;
    dt_text_edit: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure status;
    procedure ok_buttonClick(Sender: TObject);
    procedure apply_buttonClick(Sender: TObject);
    procedure cancel_buttonClick(Sender: TObject);
    procedure sg_vartextSelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
  private
    err_textset : boolean;
    procedure erreur(s : string);
  public
    ft : tform_text;
  end;

var  form_textset: tform_textset;

implementation

uses jglobvar,jutil,jsymb,jsyntax;

{$R *.lfm}

procedure tform_textset.erreur(s : string);
{ unit specific error procedure }
begin
  erreur_('Text Settings - ' + s);
  err_textset := true;
end;

procedure tform_textset.status;
begin
  nbvar_label.Caption := 'Number of variables : ' + IntToStr(ft.nb_vartext);
end;

procedure tform_textset.FormCreate(Sender: TObject);
begin
  Left := 0;
  Width := Trunc(Screen.Width / 2) - 8;
  Top := Trunc(Screen.Height / 3); {approximately - will be fine-tuned on activation}
end;

procedure tform_textset.FormActivate(Sender: TObject);
var k : integer;
begin
  with sg_vartext do
    begin
      form_textset.Caption := 'Text settings <' + IntToStr(ft.ift) + '>';
      form_textset.Top := ft.Top;
      form_textset.Height := ft.Height;
      ColCount  := maxvartext + 1;
      FixedCols := 1;
      RowCount  := 2;
      FixedRows := 1;
      for k := 0 to ColCount-1 do Cells[k,0] := IntToStr(k);
      for k := 0 to ft.nb_vartext do
        Cells[k,1] := s_ecri_var(ft.vartext[k]);
      for k := ft.nb_vartext + 1 to ColCount-1 do
        Cells[k,1] := '';
      dt_text_edit.Text := IntToStr(ft.dt_text);
    end;
  status;
end;

procedure tform_textset.ok_buttonClick(Sender: TObject);
begin
  apply_buttonClick(nil);
  if not err_textset then Close;
end;

procedure tform_textset.apply_buttonClick(Sender: TObject);
var n,k,x,tx,dt_text1 : integer;
    vartext1 : array[0..maxvartext] of integer;
    s : string;
begin
  err_textset := false;
  with ft,sg_vartext do
    begin
      if est_entier(dt_text_edit.Text,dt_text1) then
        if ( dt_text1 <= 0 ) then
          begin
            erreur('Sampling interval: positive integer expected');
            exit;
          end
        else
      else
        begin
          erreur('Sampling interval: positive integer expected');
          exit;
        end;
      n := 0;
      for k := 1 to maxvartext do
        begin
          s := Cells[k,1];
          if ( s <> '' )  then
            begin
              trouve_obj(s,x,tx);
              if ( tx = type_variable ) then
                begin
                  n := n + 1;
                  vartext1[n] := x;
                end
              else
                begin
                  erreur('unknown variable name');
                  exit;
                end;
            end;
        end;
      nb_vartext := n;
      for k := 1 to n do vartext[k] := vartext1[k];
      for k := n+1 to maxvartext do cells[k,1] := '';
      dt_text := dt_text1;
      init_text_run;
      FormActivate(nil);
    end;
  FormActivate(nil);
end;

procedure tform_textset.cancel_buttonClick(Sender: TObject);
begin
  Close;
end;

procedure tform_textset.sg_vartextSelectCell(Sender: TObject; ACol,
  ARow: Integer; var CanSelect: Boolean);
begin
  canselect := ( arow = 1 ) and ( acol > 0 );
end;

end.
