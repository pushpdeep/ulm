{ ---------------------------------------------------------------------------- }
{ Copyright (c) 1995-2017, Stéphane Legendre and the ULM development team      }
{                                                                              }
{ This file is part of ULM.                                                    }
{                                                                              }
{ ULM is free software: you can redistribute it and/or modify it               }
{ under the terms of the GNU General Public License as published               }
{ by the Free Software Foundation, either version 3 of the License,            }
{ or (at your option) any later version.                                       }
{                                                                              }
{ ULM is distributed in the hope that it will be useful,                       }
{ but WITHOUT ANY WARRANTY; without even the implied warranty                  }
{ of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                      }
{ See the GNU General Public License for more details.                         }
{                                                                              }
{ You should have received a copy of the GNU General Public License            }
{ along with ULM.  If not, see <http://www.gnu.org/licenses/>.                 }
{ ---------------------------------------------------------------------------- }

unit gcorrel;

{$MODE Delphi}

{ @@@@@@  form to display cross/auto-correlation of variables along time  @@@@@@ }

interface

uses SysUtils,Graphics,Controls,Forms,Dialogs,StdCtrls,
     jmath;

type
  tform_correl = class(TForm)
    label_x1: TLabel;
    edit_x1: TEdit;
    label_x2: TLabel;
    edit_x2: TEdit;
    label_nb_cycle: TLabel;
    edit_nb_cycle: TEdit;
    label_nb_val: TLabel;
    edit_nb_val: TEdit;
    button_run: TButton;
    button_init: TButton;
    procedure erreur(s : string);
    procedure init_correl;
    procedure run_correl(x,y,n,m : integer);
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure button_initClick(Sender: TObject);
    procedure button_runClick(Sender: TObject);
    function  check_param : boolean;
  private
    x1,x2 : integer;    { pointers to variables pour correlation }
    nb_cycle : integer; { number of time steps }
    nb_val   : integer; { number of values }
  public
  end;

var form_correl: tform_correl;

implementation

uses jglobvar,jutil,jsyntax,jeval,
     ggraph,gulm;

{$R *.lfm}

procedure tform_correl.erreur(s : string);
{ unit specific error procedure }
begin
  erreur_('Correlation - ' + s);
end;

procedure tform_correl.init_correl;
begin
  with form_graph do x1 := vargraph_y[1]; { default variable }
  x2 := x1;
end;

procedure tform_correl.FormCreate(Sender: TObject);
begin
  x1 := 0;
  x2 := 0;
  nb_cycle := 400;
  nb_val := 100;
end;

procedure tform_correl.FormActivate(Sender: TObject);
begin
  edit_x1.Text := s_ecri_var(x1);
  edit_x2.Text := s_ecri_var(x2);
  edit_nb_cycle.Text := IntToStr(nb_cycle);
  edit_nb_val.Text   := IntToStr(nb_val);
  AutoSize := False;
end;

procedure tform_correl.button_initClick(Sender: TObject);
begin
  with form_ulm do run_initExecute(nil);
end;

procedure correlation(x,y,n,m : integer;var c : vecvec_type);
{ correlation of variables x et y over n points, m values returned in array c }
var i : integer;
    tab1,tab2 : vecvec_type;
begin
  SetLength(tab1,n+1);
  SetLength(tab2,n+1);
  tab1[0] := variable[x].val;
  tab2[0] := variable[y].val;
  for i := 1 to n do
    begin
      run_t;
      tab1[i] := variable[x].val;
      tab2[i] := variable[y].val;
      with form_ulm do { interrupt procedure }
        begin
          procproc;
          if runstop then
            begin
              runstop := false;
              exit;
            end;
          if ( t__ mod 100 = 0 ) then status_time;
        end;
    end;
  correl(tab1,tab2,n+1,m,c);
end;

procedure tform_correl.run_correl(x,y,n,m : integer);
{ display correlation function on graphics }
var i,ms : integer;
    c : vecvec_type;
begin
  with form_ulm do status_run('Correlation ' + IntToStr(m));
  ms := clock;
  SetLength(c,m);
  correlation(x,y,n,m,c); { compute correlation }
  with form_graph do
    begin
      status_correl(x,y,n,m);
      for i := 0 to m-1 do
        begin
          valgraph_x[i]    := i;
          valgraph_y[1][i] := c[i];
        end;
      if not gplus then efface(nil);
      gcourb(m-1); { display correlation }
    end;
  with form_ulm do
    begin
      status_t_exec(clock - ms);
      status_time;
    end;
end;

procedure tform_correl.button_runClick(Sender: TObject);
begin
  check_param;
  run_correl(x1,x2,nb_cycle,nb_val);
end;

function  tform_correl.check_param : boolean;
{ correl var1 var2 nb_cycle nb_val }
var x,n : integer;
begin
  check_param := false;
  if not test_variable(edit_x1.Text,x) then
    begin
      erreur('unknown variable name');
      edit_x1.Text := s_ecri_var(x1);
      exit;
    end;
  x1 := x;
  if not test_variable(edit_x2.Text,x) then
    begin
      erreur('unknown variable name');
      edit_x2.Text := s_ecri_var(x2);
      exit;
    end;
  x2 := x;
  if not est_entier(edit_nb_cycle.Text,n) then
    begin
      erreur('Number of time steps: integer expected');
      edit_nb_cycle.Text := IntToStr(nb_cycle);
      exit;
    end;
  nb_cycle := n;
  if not est_entier(edit_nb_val.Text,n) then
    begin
      erreur('Number of values: integer expected');
      edit_nb_val.Text := IntToStr(nb_val);
      exit;
    end;
  if ( n >= nb_cycle ) then
    begin
      erreur('Number of values: must be less than number of time steps');
      edit_nb_val.Text := IntToStr(nb_val);
      exit;
    end;
  if ( n >= maxgraph ) then
    begin
      erreur('Number of values: must be less than ' + IntToStr(maxgraph));
      edit_nb_val.Text := IntToStr(nb_val);
      exit;
    end;
  nb_val := n;
  FormActivate(nil);
  check_param := true;
end;

end.
