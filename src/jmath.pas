{ ---------------------------------------------------------------------------- }
{ Copyright (c) 1995-2017, Stéphane Legendre and the ULM development team      }
{                                                                              }
{ This file is part of ULM.                                                    }
{                                                                              }
{ ULM is free software: you can redistribute it and/or modify it               }
{ under the terms of the GNU General Public License as published               }
{ by the Free Software Foundation, either version 3 of the License,            }
{ or (at your option) any later version.                                       }
{                                                                              }
{ ULM is distributed in the hope that it will be useful,                       }
{ but WITHOUT ANY WARRANTY; without even the implied warranty                  }
{ of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                      }
{ See the GNU General Public License for more details.                         }
{                                                                              }
{ You should have received a copy of the GNU General Public License            }
{ along with ULM.  If not, see <http://www.gnu.org/licenses/>.                 }
{ ---------------------------------------------------------------------------- }

unit jmath;

{$MODE Delphi}

{  @@@@@@   mathematical library   @@@@@@  }

{ Many procedures in this unit are taken from: }
{ Press WH, BP Flannery, SA Teukolski & WT Vetterling. 1986. }
{ Numerical Recipes. Cambridge University Press.}
{ The procedures are referred as NumRec p. xxx, p. yyy, }
{ with xxx the page of the FORTRAN code, yyy the page of the Pascal code. }
{ The Pascal code has sometimes been adapted. }

interface

uses   jglobvar;

type   matmat_type = array of array of extended; { square array of unspecified size }
       vecvec_type = array of extended; { array of unspecified size }

       cmx  = record { complex number }
                re,im : extended;
              end;

       cmat_type = array[1..matmax,1..matmax] of cmx;
       cvec_type = array[1..matmax] of cmx;

procedure init_math;

function  imin(a,b : integer) : integer;
function  imax(a,b : integer) : integer;
function  min(a,b : extended) : extended;
function  max(a,b : extended) : extended;

procedure init_alea;
procedure init_random;
function  rand(a : extended) : extended;
function  ber(a : extended) : extended;
function  gauss(m,sigma : extended) : extended;
function  lognorm(m,sigma : extended) : extended;
function  geom(p : extended) : extended;
function  exponential(a : extended) : extended;
function  betaf(a,b : extended) : extended;
function  beta1f(m,s : extended) : extended;
function  gamm(a : extended) : extended;
function  poisson(xm : extended) : extended;
function  poissonf(n : integer; m : extended) : extended; 
function  binomf(n : integer;p : extended) : extended;
function  nbinom1f(m : extended;s : extended) : extended;
function  nbinomf(r : extended;p : extended) : extended;
function  tabf(tab : rlarg_type;nb_arg : integer) : extended;
function  bdf(n : integer;b,d,delta : extended) : extended;
function  erf(x : extended) : extended;
function  erfc(x : extended) : extended;
function  phinormal(z : extended) : extended;

function  ln0(a : extended) : extended;
function  log(a : extended) : extended;
function  log0(a : extended) : extended;
function  fact(n : integer) : extended;
function  bicof(n,k : integer) : extended;

function  cmxarg(z : cmx ) : extended;
function  cmxmod(z : cmx ) : extended;
procedure cmxconj(z1 : cmx;var z : cmx);
procedure cmxsom(z1,z2 : cmx;var z : cmx);
procedure cmxdiff(z1,z2 : cmx;var z : cmx);
procedure cmxprod(z1,z2 : cmx;var z : cmx);
procedure cmxdiv(z1,z2 : cmx;var z : cmx);

procedure matvec(n : integer;a: rmat_type;v : rvec_type;var w : rvec_type);function  vecpscal(n : integer;v,w : rvec_type) : extended;
procedure cvecpscal(n : integer;v,w : cvec_type;var z : cmx);
function  vecnorm(n : integer;v : rvec_type) : extended;
function  vecnorma(n : integer;v : rvec_type) : extended;
procedure vecnormalise1(n : integer;var v : rvec_type);
procedure vecnormalise_pscal(n : integer;w : rvec_type;var v : rvec_type);
procedure matvalprop(n : integer;a : rmat_type;var lambda : cvec_type);
procedure matvecpropdroite(n : integer;a : rmat_type;lambda : cmx;var w : cvec_type);
procedure matvecpropgauche(n : integer;a : rmat_type;lambda : cmx;var v : cvec_type);

procedure fft(var data : vecvec_type;nn,isign : integer);
procedure correl(data1,data2 : vecvec_type;n,m : integer;var c : vecvec_type);
function  coeff_correl(n : integer; datax,datay : vecvec_type) : extended;
procedure coeff_regress(n : integer; datax,datay : vecvec_type;var a,b : extended);

function  matnonneg(n : integer;a : rmat_type) : boolean;
function  matirr(n : integer;a : rmat_type) : boolean;
function  matpos(n : integer;a : rmat_type) : boolean;
function  matprim(n : integer;a : rmat_type) : boolean;

procedure matvalvecd1(n : integer;a : rmat_type;var w : rvec_type;var lambda1 : extended);
procedure matvecg1(n : integer;a : rmat_type;var v : rvec_type);
procedure matkovvecg(n : integer;a : rmat_type;var v : rvec_type);

const  dpi = 2*pi;
       graine00 = 1618; { basic seed of random generator }

var    graine0 : integer; { user defined basic seed }
       graine  : integer; { current seed (montecarlo) }

       err_math : boolean; { indicator of math error }

implementation

uses   jutil;

const  maxbicoftab = 50;

var    glinext,glinextp : integer;
       glma : array[1..55] of integer; { used by random generator ran3 }

       gaussflip : integer;
       gaussgg   : extended; { used by gaussian distribution }
  
       poissonoldm,poissonsq,poissonalxm,
       poissong : extended; { used by Poisson distribution }

       binomnold : integer;
       binompold,binomoldg,binomen,
       binompc,binomplog,binompclog : extended; { used by binomial distribution }

       faclntab : array[1..100] of extended; { log factorial numbers table }
       bicoftab : array[0..maxbicoftab,0..maxbicoftab] of extended; { binomial coefficients table }


procedure erreur_math(s : string);
{ unit specific error procedure }
begin
  iwriteln('Math - ' + s);
  err_math := true;
end;

procedure init_math;
{ initializations }
var i,j : integer;
begin
  graine0  := graine00; { random generator seed }
  graine   := graine00;
  RandSeed := graine00; { seed of system random generator }
  for i := 1 to 100 do faclntab[i] := -1.0;
  for i := 0 to maxbicoftab do
    for j := 0 to maxbicoftab do bicoftab[i,j] := -1.0;
  err_math := false;
end;

procedure init_random;
begin
  RandSeed := graine00;
end;


{ ------  utilitary functions  ------ }

function  min(a,b : extended) : extended;
begin
  if ( a < b ) then min := a else min := b;
end;

function imin(a,b : integer) : integer;
begin
  if ( a < b ) then imin := a else imin := b;
end;

function  max(a,b : extended) : extended;
begin
  if ( a > b ) then max := a else max := b;
end;

function imax(a,b : integer) : integer;
begin
  if ( a > b ) then imax := a else imax := b;
end;


{ ------  random distributions  ------ }

function ran3(idum : integer) : extended;
{ portable random generator; uniform distribution over [0,1] }
{ all random distributions below call this generator }
{ NumRec p. 199, p. 715 }
const mbig  = 1000000000;
      mseed = 161803398;
      mz    = 0;
      fac   = 1.0e-9;
var   i,ii,k,mj,mk : integer;
begin
  if ( idum < 0 ) then
    begin
      mj := mseed + idum;
      mj := mj mod mbig;
      glma[55] := mj;
      mk := 1;
      for i := 1 to 54 do
        begin
          ii := 21*i mod 55;
          glma[ii] := mk;
          mk := mj - mk;
          if ( mk < mz ) then mk := mk + mbig;
          mj := glma[ii];
        end;
      for k := 1 to 4 do
        for i := 1 to 55 do
          begin
            glma[i] := glma[i] - glma[1 + (i + 30) mod 55];
            if ( glma[i] < mz ) then glma[i] := glma[i] + mbig;
          end;
      glinext  := 0;
      glinextp := 31;
    end;
  glinext := glinext + 1;
  if ( glinext = 56 ) then glinext := 1;
  glinextp := glinextp + 1;
  if ( glinextp = 56 ) then glinextp := 1;
  mj := glma[glinext] - glma[glinextp];
  if ( mj < mz ) then mj := mj + mbig;
  glma[glinext] := mj;
  ran3 := mj*fac;
end;

procedure init_alea;
{ init random distributions }
var r : extended;
begin
  gaussflip   := 0;
  poissonoldm := -1.0;
  binomnold   := -1;
  binompold   := -1.0;
  r := ran3(-graine);
end;

function  rand(a : extended) : extended;
{ uniform distribution over [0,a], a > 0 }
begin
  rand := ran3(1)*a; 
end;

function  ber(a : extended) : extended;
{ Bernoulli distribution, a in [0,1] }
{ P(X = 0)= 1-a; P(X = 1) = a }
begin
  if ( ran3(1) >= a ) then
    ber := 0.0
  else
    ber := 1.0;
end;

function  gauss0 : extended;
{ normal distribution with mean 0 and standard deviation 1 }
{ NumRec p. 203, p. 716 }
var v1,v2,fac,r : extended;
begin
  if ( gaussflip = 0 ) then
    begin
      gaussflip := 1;
      repeat
        v1 := 2.0*ran3(1) - 1.0;
        v2 := 2.0*ran3(1) - 1.0;
        r  := sqr(v1) + sqr(v2);
      until r < 1.0;
      fac := sqrt(-2.0*ln(r)/r);
      gaussgg  := v1*fac;
      gauss0  := v2*fac;
    end
  else
    begin
      gaussflip := 0;
      gauss0 := gaussgg;
    end;
end;

function  gauss(m,sigma : extended) : extended;
{ normal distribution with mean m and standard deviation sigma }
begin
  gauss := m + gauss0*sigma;
end;

function  lognorm(m,sigma : extended) : extended;
{ lognormal distribution with mean m > 0 and standard deviation sigma }
var c : extended;
begin
  c := sigma/m;
  c := ln(c*c + 1.0);
  lognorm := exp(gauss(ln(m) - 0.5*c,sqrt(c)));
end;

function  geom(p : extended) : extended;
{ geometric distribution with parameter p }
{ P(X = k) = p(1-p)^k, mean = (1-p)/p  }
{ P(X = 0) = p }
var x : extended;
begin
  if ( p = 0.0 ) or ( p = 1.0 ) then
    geom := 0.0
  else
    begin
      repeat
        x := ran3(1);
      until x > 0.0;
      geom := trunc(ln(x)/ln(1.0 - p));
    end;
end;

function  exponential(a : extended) : extended;
{ exponential distribution with parameter a }
{ density f(x) = a*exp(-ax), mean = 1/a, variance = 1/a^2 }
{ NumRec p. 201, p. 716 }
var u : extended;
begin
  repeat
    u := ran3(1)
  until u > 0;
  exponential := -ln(u)/a;
end;

function  gammln(xx : extended) : extended;
{ ln of Euler gamma fucntion }
{ NumRec p. 157, p. 704 }
const stp  = 2.50662827465;
var x,tmp,ser : extended;
begin
  x   := xx - 1.0;
  tmp := x + 5.5;
  tmp := (x + 0.5)*ln(tmp) - tmp;
  ser := 1.0 + 76.18009173/(x+1.0) - 86.50532033/(x+2.0) + 24.01409822/(x+3.0)
         - 1.231739516/(x+4.0) + 0.120858003e-2/(x+5.0) - 0.536382e-5/(x+6.0);
  gammln := tmp + ln(stp*ser);
end;

function  gamm(a : extended) : extended;
{ gamma distribution with parameter a > 0 }
{ NumRec p. 206, p. 716 (adapted) }
var am,e,s,v1,v2,x,y : extended;
begin
  if ( a > 1 ) then
    begin
      repeat
        repeat
          repeat
            v1 := 2.0*ran3(1) - 1.0;
            v2 := 2.0*ran3(1) - 1.0;
          until ( sqr(v1) + sqr(v2) <= 1.0 );
          y  := v2/v1;
          am := a - 1.0;
          s  := sqrt(2.0*am + 1.0);
          x  := s*y + am;
        until ( x > 0.0 );
        s := am*ln(x/am) - s*y;
        if ( s > -500.0 ) then
          e := (1.0 + sqr(y))*exp(s)
        else
          e := 0.0;
      until ( ran3(1) <= e );
    end
  else
    begin
      s := exp(1);
      s := s/(a+s);
      repeat
        v1 := ran3(1);
        v2 := ran3(1);
        if ( v1 < s ) then 
          begin
            x := exp(ln(v2)/a);
            e := exp(-x); 
          end
        else
          begin
            x := 1.0 - ln(v2);
            e := exp(ln(x)*(a-1.0));
          end
      until ( ran3(1) < e );
    end;
  gamm := x;
end;

function  betaf(a,b : extended) : extended;
{ beta distribution with parameters a > 0, b > 0 }
var u,v :extended;
begin
  u := gamm(a);
  v := gamm(b);
  betaf := u/(u + v);
end;

function  beta1f(m,s : extended) : extended;
{ beta distribution computed to have mean m> 0 and standard deviation s > 0 }
{ the conditions are 0 < m < 1 and 0 < s^2 < m*(1-m) }
var a,b : extended;
begin
  a := m*m*(1.0 - m)/(s*s) - m;
  b := a/m - a;
  beta1f := betaf(a,b);
end;

function  tabf(tab : rlarg_type;nb_arg : integer) : extended;
{ tabulated integer distribution: returns k with probability p_k = tab[k+1] }
{ P(X = k) = p_k, k = 0, ..., n = nb_arg-1, 0 <= p_k <= 1, p_0 + ... + p_n = 1 }
var i : integer;
    x,s : extended;
begin
  x := ran3(1);
  s := 0.0;
  for i := 1 to nb_arg do
    begin
      s := s + tab[i];
      if ( tab[i] <> 0.0 ) then
        if ( x <= s ) then break;
      if ( s > 1.0 ) then break;
    end;
  tabf := i-1;
end;

function  poisson(xm : extended) : extended;
{ Poisson distribution with mean xm }
{ mean = xm = m, variance = m, P(X = k) = exp(-m)m^k/m! }
{ NumRec p. 207, p. 717 (adapted) }
var em,t,y,s : extended;
begin
  if ( xm < 12.0 ) then
    begin
      if ( xm <> poissonoldm ) then
        begin
          poissonoldm := xm;
          poissong := exp(-xm);
        end;
      em := -1.0;
      t  := 1.0;
      repeat
        em := em + 1.0;
        t  := t*ran3(1);
      until ( t <= poissong );
    end
  else
    begin
      if ( xm <> poissonoldm ) then
        begin
          poissonoldm := xm;
          poissonsq   := sqrt(2.0*xm);
          poissonalxm := ln(xm);
          poissong    := xm*poissonalxm - gammln(xm + 1.0);
        end;
      repeat
        repeat
          y  := pi*ran3(1);
          y  := sin(y)/cos(y);
          em := poissonsq*y + xm;
        until ( em >= 0.0 );
        em := trunc(em);
        s := em*poissonalxm - gammln(em + 1.0) - poissong;
        if ( s > -500.0 ) then
          t := 0.9*(1.0 + sqr(y))*exp(s)
        else
          t := 0.0;
      until ( ran3(1) <= t );
    end;
  poisson := em;
end;

function  poissonf(n : integer; m : extended) : extended; 
{ sum of n trials of the Poisson distribution with mean m }
var i : integer;
    s : extended;
begin
  s := 0.0;
  for i := 1 to n do s := s + poisson(m);
  poissonf := s;
end;

function  binomf(n : integer;p : extended) : extended;
{ binomial distribution with parameters n >= 0, p in [0,1] }
{ P(X = k) = C(k,n)p^k(1-p)^(n-k), mean n*p, variance n*p*(1-p) }
{ NumRec p. 208, p. 717 (adapted) }
var  j : integer;
     u,pp,am,em,g,angle,sq,t,y,s : extended;
begin
  if ( n < 25 ) then
    begin
      if ( p <= 0.5 ) then pp := p else pp := 1.0 - p;
      u := 0.0;
      for j := 1 to n do 
        if ( ran3(1) < pp ) then u := u + 1.0;
      if ( p <> pp ) then u := n - u;
      binomf := u;
    end
  else
    begin
      if ( p <= 0.5 ) then pp := p else pp := 1.0 - p;
      am := n*pp;
      if ( am < 1 ) then
        begin
          g := exp(-am);
          t := 1.0;
          j := -1;
          repeat
            j := j + 1;
            t := t*ran3(1);
          until ( t < g ) or ( j = n );
          u := j;
        end
      else
        begin
          if ( n <> binomnold ) then
            begin
              binomen   := n;
              binomoldg := gammln(binomen + 1.0);
              binomnold := n;
            end;
          if ( pp <> binompold ) then
            begin
              binompc    := 1.0 - pp;
              binomplog  := ln(pp);
              binompclog := ln(binompc);
              binompold  := pp;
            end;
          sq := sqrt(2.0*am*binompc);
          repeat
            repeat
              angle := pi*ran3(1);
              if ( angle = pisur2 ) then
                y := 1.0
              else
                y := sin(angle)/cos(angle);
              em := sq*y + am;
            until ( em >= 0.0 ) and ( em < (binomen + 1.0) );
            em := trunc(em); 
            s := binomoldg - gammln(em + 1.0)
                 - gammln(binomen - em + 1.0) + em*binomplog + (binomen-em)*binompclog;
            if ( s > -500.0 ) then
              t := 1.2*sq*(1.0 + sqr(y))*exp(s)
            else
              t := 0.0;
          until ( ran3(1) <= t );
          u := em;
        end;
      if ( p <> pp ) then u := n - u;
      binomf := u;
    end;
end;

function  nbinomf(r : extended;p : extended) : extended;
{ negative binomial distribution with parameters r > 0, p in ]0,1] }
{ P(X = k) = C(k+r-1,r-1)p^r(1-p)^k, mean r*(1-p)/p, variance r*(1-p)/p^2 }
var y : extended;
begin
  y := gamm(r);
  nbinomf := poisson(y*(1.0-p)/p);
end;

function  nbinom1f(m : extended;s : extended) : extended;
{ negative binomial distribution computed to have mean m and standard deviation s }
{ the condition is 0 < m < s^2 }
var p,r : extended;
begin
  p := m/(s*s);
  r := p*m/(1.0-p);
  nbinom1f := nbinomf(r,p);
end;

function  bdf(n : integer;b,d,delta : extended) : extended;
{ sum of n trials of the geometric distribution to simulate in discrete time }
{ a continuous time birth-death process }
{ The formulas have been devised by Amaury Lambert }
{ b = birth rate, d = death rate, delta = time step; b, d, delta >= 0 }
var i : integer;
    r,c,e,p0,rk,s : extended;
begin
  r  := b - d;
  if ( r < 0.0 ) then
    begin
      e := exp(r*delta);
      c  := d - b*e;
      p0 := d*(1.0 - e)/c;
      rk := -r/c;
    end
  else
    if ( r = 0.0 ) then
      begin
        c  := 1.0 + b*delta;
        p0 := b*delta/c;
        rk := 1/c;
      end
    else
      begin
        e  := exp(-r*delta);
        c  := b - d*e;
        p0 := d*(1.0 - e)/c;
        rk := r*e/c;
      end;
  s := 0.0;
  for i := 1 to n do
    if ( ber(p0) = 0.0 ) then s := s + geom(rk) + 1.0;
  bdf := s;
end;

function  erfc(x : extended) : extended;
{ complementary error function }
const s1pi = 0.56418958354775628; { 1/sqrt(pi) }
      eps  = 0.000000000001;
var a,b,c,d,n,u,q1,q2 : extended;
begin
  if ( abs(x) <= 2.2 ) then
     begin
       erfc := 1.0 - erf(x);
       exit;
     end;
  if ( x <= 0.0 ) then
    begin
      erfc := 2.0 - erfc(-x);
      exit;
    end;
  a := 1.0;
  b := x;
  c := x;
  d := x*x + 0.5;
  q2 := b/d;
  n := 1.0;
  repeat
    u := a*n + b*x;
    a := b;
    b := u;
    u := c*n + d*x;
    c := d;
    d := u;
    n := n + 0.5;
    q1 := q2;
    q2 := b/d;
  until (abs((q1 - q2)/q2) < eps );
  erfc := s1pi*q2*exp(-x*x);
end;

function  erf(x : extended) : extended;
{ error function }
const s2pi = 1.1283791670955126; { 2/sqrt(pi) }
      eps  = 0.000000000001;
var j : integer;
    u,sum,x2 : extended;
begin
  if ( abs(x) > 2.2 ) then
    begin
      erf := 1.0 - erfc(x);
      exit;
    end;
  x2 := x*x;
  u := x;
  sum := x;
  j := 1;
  repeat
    u := u*x2/j;
    sum := sum - u/(2.0*j + 1.0);
    j := j + 1;
    u := u*x2/j;
    sum := sum + u/(2.0*j + 1.0);
    j := j + 1;
  until (abs(u/sum) < eps );
  erf := s2pi*sum;
end;

function  phinormal(z : extended) : extended;
{ cumulative distribution Phi of the normal distribution }
{ Phi(z) = P(X < z) }
begin
  phinormal := 0.5*(1.0 + erf(z/sqrt(2.0)));
end;


{ ------  other mathematical functions  ------ }

function  ln0(a : extended) : extended;
{ Neperian logarithm ln with ln(0) = 0 }
begin
  if ( a <= 0.0 ) then ln0 := 0.0 else ln0 := ln(a);
end;

function  log(a : extended) : extended;
{ decimal logarithm = ln base 10 }
begin
  log := ln(a)/ln(10.0);
end;

function  log0(a : extended) : extended;
{ log with log(0) = 0 }
begin
  if ( a <= 0.0 ) then log0 := 0.0 else log0 := log(a);
end;

function  facln(n : integer) : extended;
{ logarithm of factorial n: ln(n!) }
{ NumRec p. 159, p. 705 }
begin
  if ( n <= 99 ) then
    begin
      if ( faclntab[n+1] < 0.0 ) then
        faclntab[n+1] := gammln(n + 1.0 );
      facln := faclntab[n+1];
    end
  else
    facln := gammln(n + 1.0);
end;

function  fact(n : integer) : extended;
{ factorial n = n! }
begin
  if ( n <= 0 ) then
    fact := 1.0
  else
    fact := exp(facln(n));
end;

function  bicof(n,k : integer) : extended;
{ binomial coefficient C(n,k) }
{ NumRec p. 158, p. 705 }
var r : extended;
begin
  if ( k > n ) then
    begin
      bicof := 0.0;
      exit;
    end;
  if ( n <= maxbicoftab ) and ( k <= maxbicoftab ) then
    begin
      if ( bicoftab[n,k] < 0.0 ) then
        begin
          r := exp(facln(n) - facln(k) - facln(n-k));
          if ( r < bigint ) then
            bicoftab[n,k] := round(r)
          else
            bicoftab[n,k] := r;
        end;
      bicof := bicoftab[n,k];
    end
  else
    begin
      r := exp(facln(n) - facln(k) - facln(n-k));
      if ( r < bigint ) then
        bicof := round(r)
      else
        bicof := r;
    end;
end;


{ ------  complex numbers  ------ }

function  cmxmod(z : cmx ) : extended;
{ modulus |z| }
begin
  with z do cmxmod := sqrt(sqr(re)+sqr(im)); 
end;

function  cmxarg(z : cmx) : extended;
{ argument arg(z) }
var h : extended;
begin
  with z do
    if ( re = 0.0 ) then
      if ( im < 0.0 ) then
        h := 3.0*pisur2
      else
        if ( im = 0.0 ) then
          h := 0.0
        else
          h := pisur2
    else
      if ( im <> 0.0 ) then
        h := arctan(im/re)
      else
        if ( re > 0.0 ) then
          h := 0.0
        else
          h := pi;
  while ( h < 0.0 ) do h := h + dpi;
  cmxarg := h;
end;

procedure cmxconj(z1 : cmx;var z : cmx);
{ z = complex conjugate of z1 }
begin
  z.re := z1.re;
  z.im := -z1.im;
end;

procedure cmxsom(z1,z2 : cmx;var z : cmx);
{ z = z1 + z2 }
begin
  z.re := z1.re + z2.re;
  z.im := z1.im + z2.im;
end;

procedure cmxdiff(z1,z2 : cmx;var z : cmx);
{ z = z1 - z2 }
begin
  z.re := z1.re - z2.re;
  z.im := z1.im - z2.im;
end;

procedure cmxprod(z1,z2 : cmx;var z : cmx);
{ z = z1*z2 }
begin
  z.re := z1.re*z2.re - z1.im*z2.im;
  z.im := z1.re*z2.im + z1.im*z2.re;
end;

procedure cmxdiv(z1,z2 : cmx;var z : cmx);
{ z = z1/z2 }
var u : extended;
begin
  u := z2.re*z2.re + z2.im*z2.im;
  z.re := (z1.re*z2.re + z1.im*z2.im)/u;
  z.im := (z1.im*z2.re - z1.re*z2.im)/u;
end;

procedure  cmxinv(z1 : cmx;var z : cmx);
{ z = 1/z1 }
var u : extended;
begin
  u := z1.re*z1.re + z1.im*z1.im;
  z.re := z1.re/u;
  z.im := -z1.im/u;
end;


{ ------  operations on vectors and matrices  ------ }

procedure matprod(n : integer;a,b : rmat_type;var c : rmat_type);
{ matrix product c = a*b }
var i,j,k : integer;
    x : extended;
begin
  for i:= 1 to n do
    for j := 1 to n do
      begin
        x := 0.0;
        for k := 1 to n do
          x := x + a[i,k]*b[k,j];
        c[i,j] := x;
      end;
end;

procedure mattranspose(n : integer;var a: rmat_type);
{ matrix a transformed in its transpose a' }
var i,j : integer;
    r : extended;
begin
  for i := 1 to n-1 do
    for j := i+1 to n do
      begin
        r := a[i,j];
        a[i,j] := a[j,i];
        a[j,i] := r;
      end;
end;

procedure matvec(n : integer;a: rmat_type;v : rvec_type;var w : rvec_type);
{ product matrix vector: w = av }
var i,j : integer;
    r : extended;
begin
  for i := 1 to n do
    begin
      r := 0.0;
      for j := 1 to n do r := r + a[i,j]*v[j];
      w[i] := r;
    end;
end;

procedure vecmat(n : integer;a: matmat_type;v : array of extended;var w : array of extended);
{ product vector matrix: w' = v'a }
var i,j : integer;
    r : extended;
begin
  for j := 1 to n do
    begin
      r := 0.0;
      for i := 1 to n do r := r + v[i]*a[i,j];
      w[j] := r;
    end;
end;

function  vecnorm(n : integer;v : rvec_type) : extended;
{ euclidian norm of vector v }
var i : integer;
    r : extended;
begin
  r := 0.0;
  for i := 1 to n do r := r + sqr(v[i]);
  vecnorm := sqrt(r);
end;

function  vecnorma(n : integer;v : rvec_type) : extended;
{ other norm of vector v }
var i : integer;
    r : extended;
begin
  r := 0.0;
  for i := 1 to n do r := r + abs(v[i]);
  vecnorma := r;
end;

procedure vecnormalise1(n : integer;var v : rvec_type);
{ normalization of vector: v changed such that v_1 + ... + v_n = 1 }
var i : integer;
    r : extended;
begin
  r := 0.0;
  for i := 1 to n do r := r + v[i];
  if ( r = 0.0 ) then exit;
  for i := 1 to n do v[i] := v[i]/r;
end;

procedure vecnormalise_pscal(n : integer;w : rvec_type;var v : rvec_type);
{ v changed in v such that scalar product v.w = 1 }
var i : integer;
    r : extended;
begin
  r := 0.0;
  for i := 1 to n do r := r + v[i]*w[i];
  if ( r = 0.0 ) then exit;
  for i := 1 to n do v[i] := v[i]/r;
end;

function  vecpscal(n : integer;v,w : rvec_type) : extended;
{ scalar product of real vectors v and w: v.w }
var i : integer;
    r : extended;
begin
  r := 0.0;
  for i:= 1 to n do r := r + v[i]*w[i];
  vecpscal := r;
end;

procedure  cvecpscal(n : integer;v,w : cvec_type;var z : cmx);
{ complex scalar product of complex vectors v and w: <v,w> = sum(j;v[j]*w'[j]) }
var i : integer;
begin
  z.re := 0.0;
  z.im := 0.0;
  for i := 1 to n do 
    begin
      z.re := z.re + v[i].re*w[i].re + v[i].im*w[i].im;
      z.im := z.im + v[i].re*w[i].im - v[i].im*w[i].re;
    end;
end;


{  ------  computation of eigenvalues  ------  }

procedure matequil(n : integer;var a : rmat_type);
{ matrix a of size nxn is transformed into a balanced matrix that has the same eigenvalues }
{ NumRec p. 366, p. 752 }
var i,j,last : integer;
    s,r,g,f,c,sqrdx,radix : extended;
begin
  radix := 2.0;
  sqrdx := sqr(radix);
  repeat
    last := 1;
    for i := 1 to n do
      begin
        c := 0.0;
        r := 0.0;
        for j := 1 to n do
          if ( j <> i ) then
            begin
              c := c + abs(a[j,i]);
              r := r + abs(a[i,j]);
            end;
        if ( ( c <> 0.0 ) and ( r <> 0.0 ) ) then
          begin
            g := r/radix;
            f := 1.0;
            s := c + r;
            while ( c < g ) do
              begin
                f := f*radix;
                c := c*sqrdx;
              end;
            g := r*radix;
            while ( c > g ) do
              begin
                f := f/radix;
                c := c/sqrdx;
              end;
            if ( (c+r)/f < 0.95*s ) then
              begin
                last := 0;
                g := 1.0/f;
                for j := 1 to n do a[i,j] := a[i,j]*g;
                for j := 1 to n do a[j,i] := a[j,i]*f;
              end;
          end;
      end;
  until last <> 0;
end;

procedure mathess(n : integer;var a : rmat_type);
{ matrix a of size nxn is transformed into Hessenberg matrix }
{ NumRec p. 368, p. 752 }
var m,i,j : integer;
    x,y : extended;
begin
  if ( n > 2 ) then
    for m := 2 to n-1 do
      begin
        x := 0.0;
        i := m;
        for j := m to n do
          if ( abs(a[j,m-1]) > abs(x) ) then
            begin
              x := a[j,m-1];
              i := j;
            end;
        if ( i <> m ) then
          begin
            for j := m-1 to n do
              begin
                y := a[i,j];
                a[i,j] := a[m,j];
                a[m,j] := y;
              end;
            for j := 1 to n do
              begin
                y := a[j,i];
                a[j,i] := a[j,m];
                a[j,m] := y;
              end;
          end;
        if ( x <> 0.0 ) then
          for i := m+1 to n do
            begin
              y := a[i,m-1];
              if ( y <> 0.0 ) then
                begin
                  y := y/x;
                  a[i,m-1] := y;
                  for j := m to n do a[i,j] := a[i,j] - y*a[m,j];
                  for j := 1 to n do a[j,m] := a[j,m] + y*a[j,i];
                end;
            end;
      end;
end;

procedure mathqr(n : integer;a : rmat_type;var wr,wi : rvec_type);
{ qr algorithm for finding eigenvalues of Hessenberg matrix a of size nxn }
{ wr,wi real and imaginary parts of eigenvalues }
{ warning: matrix a is destroyed in the process }
{ NumRec p. 374, p. 753 }

function sign(a,b : extended) : extended;
begin
  if ( b < 0.0 ) then sign := -abs(a) else sign := abs(a);
end;

var nn,m,l,k,j,its,i,mmin : integer;
    x,y,z,u,v,w,r,s,t,p,q,anorm : extended;
label 2,3,4;
begin
  anorm := abs(a[1,1]);
  for i := 2 to n do
    for j := i-1 to n do
      anorm := anorm + abs(a[i,j]);
  nn := n;
  t := 0.0;
  while ( nn >= 1 ) do
    begin
      its := 0;
2 :   for l := nn downto 2 do 
        begin
          s := abs(a[l-1,l-1]) + abs(a[l,l]);
          if ( s = 0.0 ) then s := anorm;
          if ( ( abs(a[l,l-1]) + s ) = s ) then goto 3;
        end;
      l := 1;
3 :   x := a[nn,nn];
      if ( l = nn ) then
        begin
          wr[nn] := x + t;
          wi[nn] := 0.0;
          nn := nn - 1;
        end
      else
        begin
          y := a[nn-1,nn-1];
          w := a[nn,nn-1]*a[nn-1,nn];
          if ( l = nn-1 ) then
            begin
              p := 0.5*(y - x);
              q := sqr(p) + w;
              z := sqrt(abs(q));
              x := x + t;
              if ( q >= 0.0 ) then
                begin
                  z := p + sign(z,p);
                  wr[nn] := x + z;
                  wr[nn-1] := wr[nn];
                  if ( z <> 0.0 ) then wr[nn] := x - w/z;
                  wi[nn] := 0.0;
                  wi[nn-1] := 0.0;
                end
              else
                begin
                  wr[nn] := x + p;
                  wr[nn-1] := wr[nn];
                  wi[nn] := z;
                  wi[nn-1] := -z;
                end;
              nn := nn - 2;
            end
          else
            begin
              if ( its = 30 ) then
                begin
                  erreur_math('mathqr: too many iterations');
                  exit;
                end;
              if ( ( its = 10 ) or ( its = 20 ) ) then
                begin
                  t := t + x;
                  for i := 1 to nn do a[i,i] := a[i,i] - x;
                  s := abs(a[nn,nn-1]) + abs(a[nn-1,nn-2]);
                  x := 0.75*s;
                  y := x;
                  w := -0.4375*sqr(s);
                end;
              its := its + 1;
              for m := nn-2 downto 1 do
                begin
                  z := a[m,m];
                  r := x - z;
                  s := y - z;
                  p := (r*s - w )/a[m+1,m] + a[m,m+1];
                  q := a[m+1,m+1] - z - r - s;
                  r := a[m+2,m+1];
                  s := abs(p) + abs(q) + abs(r);
                  p := p/s;
                  q := q/s;
                  r := r/s;
                  if ( m = 1 ) then goto 4;
                  u := abs(a[m,m-1])*(abs(q) + abs(r));
                  v := abs(p)*abs(a[m-1,m-1]) + abs(z) + abs(a[m+1,m+1]);
                  if ( (u+v) = v ) then goto 4;
                end;
4 :           for i := m+2 to nn do
                begin
                  a[i,i-2] := 0.0;
                  if ( i <> (m+2) ) then a[i,i-3] := 0.0;
                end;
              for k := m to nn-1 do
                begin
                  if ( k <> m ) then
                    begin
                      p := a[k,k-1];
                      q := a[k+1,k-1];
                      r := 0.0;
                      if ( k <> (nn-1) ) then r := a[k+2,k-1];
                      x := abs(p) + abs(q) + abs(r);
                      if ( x <> 0.0 ) then
                        begin
                          p := p/x;
                          q := q/x;
                          r := r/x;
                        end;
                    end;
                  s := sign(sqrt(sqr(p) + sqr(q) + sqr(r)), p);
                  if ( s <> 0.0 ) then
                    begin
                      if ( k = m ) then
                        if ( l <> m ) then a[k,k-1] := -a[k,k-1] else
                      else 
                        a[k,k-1] := -s*x;
                      p := p + s;
                      x := p/s;
                      y := q/s;
                      z := r/s;
                      q := q/p;
                      r := r/p;
                      for j := k to nn do
                        begin
                          p := a[k,j] + q*a[k+1,j];
                          if ( k <> (nn-1) ) then 
                            begin
                              p := p + r*a[k+2,j];
                              a[k+2,j] := a[k+2,j] - p*z;
                            end;
                          a[k+1,j] := a[k+1,j] - p*y;
                          a[k,j]   := a[k,j] - p*x;
                        end;
                      mmin := imin(nn,k+3);
                      for i := l to mmin do
                        begin
                          p := x*a[i,k] + y*a[i,k+1];
                          if ( k <> ( nn-1) ) then 
                            begin
                              p := p + z*a[i,k+2];
                              a[i,k+2] := a[i,k+2] - p*r;
                            end;
                          a[i,k+1] := a[i,k+1] - p*q;
                          a[i,k] := a[i,k] - p;
                        end;
                    end;
                end;
              goto 2;
            end;
        end;
    end;
end;

procedure matvalprop(n : integer;a : rmat_type;var lambda : cvec_type);
{ complex eigenvalues of matrix a of size nxn returned in complex vector lambda }
var i : integer;
    wr,wi : rvec_type;

procedure ordre_valprop(n : integer;var lambda : cvec_type);
{ sort eigenvalues by decreasing modulus }
{ sorting algorithm = shell's method }
{ NumRec p. 229, p. 724 }
label 1;
var x,y,z,u,v,w,ln2 : integer;
    r1,r2 : extended;
    h : cmx;
begin
  ln2 := trunc(ln(n)*1.442695022 + 0.00001);
  z := n;
  for x := 1 to ln2 do
    begin
      z := z div 2;
      u := n - z;
      for y := 1 to u do
        begin
          v := y;
1:
          w := v + z;
          if ( lambda[w].im = 0.0 ) then
            r1 := lambda[w].re
          else
            r1 := sqrt(sqr(lambda[w].re) + sqr(lambda[w].im));
          if ( lambda[v].im = 0.0 ) then
            r2 := lambda[v].re
          else
            r2 := sqrt(sqr(lambda[v].re) + sqr(lambda[v].im));
          if ( r1 > r2 ) or { case cyclic matrix }
          ( (r1 + 0.0001 > r2) and ((lambda[w].im = 0.0) and (lambda[v].im <> 0.0)) )
          then
            begin
              h := lambda[v];
              lambda[v] := lambda[w];
              lambda[w] := h;
              v := v - z;
              if ( v >= 1 ) then goto 1;
            end;
        end;
    end;
end;

begin
  matequil(n,a); { transform a into balanced matrix }
  mathess(n,a);  { transform into Hessenberg matrix }
  mathqr(n,a,wr,wi); { qr algorithm to find eigenvalues }
  for i := 1 to n do with lambda[i] do
    begin
      re := wr[i];
      im := wi[i];
    end;
  ordre_valprop(n,lambda); { sort eigenvalues }
end;


{  ------  computation of eigenvectors  ------ }

const  matmax2 = 2*matmax; { handle vectors of twice the size }

type   rmat2_type = array[1..matmax2,1..matmax2] of extended;
       rvec2_type = array[1..matmax2] of extended;
       ivec2_type = array[1..matmax2] of integer;

function  vecnorm_2(n : integer;v : rvec2_type) : extended;
{ euclidian norm of vector v : ||v|| }
var i : integer;
    r : extended;
begin
  r := 0.0;
  for i := 1 to n do r := r + sqr(v[i]);
  vecnorm_2 := sqrt(r);
end;

procedure vecnormalise_2(n : integer;var v : rvec2_type);
{ normalize vector by euclidian norm: v changed in v/||v|| }
var i : integer;
    r : extended;
begin
  r := vecnorm_2(n,v);
  if ( r = 0.0 ) then exit;
  for i := 1 to n do v[i] := v[i]/r;
end;

function  vecpscal_2(n : integer;v,w : rvec2_type) : extended;
{ scalar product of vectors v et w: v.w }
var i : integer;
    r : extended;
begin
  r := 0.0;
  for i:= 1 to n do r := r + v[i]*w[i];
  vecpscal_2 := r;
end;
 
procedure matlu(n : integer;var a : rmat2_type;var indx : ivec2_type;var d : extended);
{ lu decomposition of matrix a of size nxn, used to solve linear system (next procedure) }
{ indx describes the permutation performed on the rows of a }
{ d = +1 or -1  signature of the permutation }
{ NumRec p. 35, p. 683 }
const tiny = 1.0e-20;
var i,j,k,imax : integer;
    sum,dum,big : extended;
    vv : rvec2_type;
begin
  d := 1.0;
  for i := 1 to n do
    begin
      big := 0.0;
      for j := 1 to n do 
        if ( abs(a[i,j]) > big ) then
          big := abs(a[i,j]);
      if ( big = 0.0 ) then
        begin
          erreur_math('singular matrix in eigenvector computation');
          exit;
        end;
      vv[i] := 1.0/big; 
    end;
  for j := 1 to n do
    begin
      for i := 1 to j-1 do
        begin
          sum := a[i,j];
          for k := 1 to i-1 do sum := sum - a[i,k]*a[k,j];
          a[i,j] := sum;
        end;
      big := 0.0;
      for i := j to n do
        begin
          sum := a[i,j];
          for k := 1 to j-1 do sum := sum - a[i,k]*a[k,j];
          a[i,j] := sum;
          dum := vv[i]*abs(sum);
          if ( dum >= big ) then 
            begin
              big := dum;
              imax := i;
            end;
        end;
      if ( j <> imax ) then
        begin
          for k := 1 to n do
            begin
              dum := a[imax,k];
              a[imax,k] := a[j,k];
              a[j,k] := dum;
            end;
          d := -d;
          vv[imax] := vv[j];
        end;
        indx[j] := imax;
        if ( a[j,j] = 0.0 ) then a[j,j] := tiny;
        if ( j <> n ) then
          begin
            dum := 1.0/a[j,j];
            for i := j+1 to n do a[i,j] := a[i,j]*dum;
          end;
    end;
end;

procedure matlusolv(n : integer;var a : rmat2_type;indx : ivec2_type;var b : rvec2_type);
{ solve linear system ax = b, where a has lu decomposition from procedure matlu above }
{ b is transformed into the solution vector }
{ NumRec p 33-38, p. 683-684 }
var i,j,ii,ip : integer;
    sum : extended;
begin
  ii := 0;
  for i := 1 to n do
    begin
      ip := indx[i];
      sum := b[ip];
      b[ip] := b[i];
      if ( ii <> 0 ) then
        for j := ii to i-1 do sum := sum - a[i,j]*b[j]
      else
        if ( sum <> 0.0 ) then ii := i;
      b[i] := sum;
    end;
  for i := n downto 1 do
    begin
      sum := b[i];
      if ( i < n ) then
        for j := i+1 to n do sum := sum - a[i,j]*b[j];
      b[i] := sum/a[i,i];
    end;
end;

procedure matvecpropdroite(n : integer;a : rmat_type;lambda : cmx;var w : cvec_type);
{ right complex eigenvector w associated with eigenvalue lambda of matrix a, a.w = lambda.w }
{ code developped according to inverse iteration method described in NumRec pp 377-380 }

label 1,2;
var i,j,its,nn,ntry : integer;
    indx : ivec2_type; { double size for case complex lambda }
    d,eps,r : extended;
    lambd : cmx;
    x,x1,y,y1,e : rvec2_type; { double size for case complex lambda }
    aaa : rmat2_type; { double size for case complex lambda }
    bool : boolean;
begin

  ntry := 0;

{ case real lambda: }

  if ( lambda.im = 0.0 ) then
    begin
      bool := true;
      for i := 1 to n do
        for j := 1 to n do
          if ( i = j ) then
            bool := bool and (a[i,i] = lambda.re)
          else
            bool := bool and (a[i,j] = 0.0);
      if bool then
        begin
          for i := 1 to n do
            begin
              w[i].re := 1.0;
              w[i].im := 0.0;
            end;
          exit;
        end;
1 :   ntry := ntry + 1;
      lambd := lambda;
      for i := 1 to n do x[i] := random;
      { random initial guess }
      { use system random generator, not portable generator ran3 }
      { to avoid interference with program random functions }
      its := 0;
      repeat
        if its = 0 then 
          begin
            r := lambd.re/100.0;
            if ( r = 0.0 ) then r := 0.1;
          end
        else
          begin
            r := vecnorm_2(n,x1);
            r := r*r/vecpscal_2(n,x1,y1);
          end;
        lambd.re := lambd.re +  r;
        for i := 1 to n do
          for j := 1 to n do aaa[i,j] := a[i,j];
        for i := 1 to n do aaa[i,i] := a[i,i] - lambd.re;
        its := its + 1;
        if ( its > 10 ) then
          begin
            if ( ntry <= 5 ) then goto 1;
            erreur_math('too many iterations to find right eigenvector');
            exit;
          end;
        matlu(n,aaa,indx,d); { lu decomposition of aaa }
        if err_math then exit;
        y := x;
        x1 := x;
        matlusolv(n,aaa,indx,y); { solve linear system }
        y1 := y;
        vecnormalise_2(n,y);
        for i := 1 to n do e[i] := x[i] - y[i];
        eps := vecnorm_2(n,e);
        x := y;
      until ( ( eps < 1.0e-4 ) or ( abs(r) < 1.0e-6 ) ); 
      for i := 1 to n do
        begin
          w[i].re := x[i];
          w[i].im := 0.0;
        end;
      exit;
    end;
    
{ case complex lambda: }
  
  nn := 2*n;
  if ( nn > matmax2 ) then
    begin
      erreur_math('size too big in eigenvector computation');
      exit;
    end;
2:
  ntry := ntry + 1;
  lambd := lambda;
  for i := 1 to nn do x[i] := random;
  its := 0;
  repeat
    if ( its = 0 ) then
      r := cmxmod(lambd)/100.0
    else
      begin
        r := vecnorm_2(nn,x1);
        r := r*r/vecpscal_2(nn,x1,y1);
      end;
    lambd.re := lambd.re +  r;
    for i := 1 to n do
      for j := 1 to n do
        begin
          aaa[i,j]     := a[i,j];
          aaa[i+n,j+n] := a[i,j];
          aaa[i+n,j]   := 0.0;
          aaa[i,j+n]   := 0.0;
        end;
    for i := 1 to n do 
      begin
        aaa[i,i]     := a[i,i] - lambd.re;
        aaa[i+n,i+n] := a[i,i] - lambd.re;
        aaa[i,i+n]   := lambd.im;
        aaa[i+n,i]   := -lambd.im;
      end;
    its := its + 1;
    if ( its > 10 ) then
      begin
        if ( ntry <= 5 ) then goto 2;
        erreur_math('too many iterations to find right eigenvector');
        exit;
      end;
    matlu(nn,aaa,indx,d);
    if err_math then exit;
    y := x;
    x1 := x;
    matlusolv(nn,aaa,indx,y);
    y1 := y;
    vecnormalise_2(nn,y);
    for i := 1 to nn do e[i] := x[i] - y[i];
    eps := vecnorm_2(nn,e);
    x := y;
  until ( ( eps < 1.0e-4 ) or ( abs(r) < 1.0e-6 ) ); 
  for i := 1 to n do
    begin
      w[i].re := x[i];
      w[i].im := x[i+n];
    end;
end;

procedure matvecpropgauche(n : integer;a : rmat_type;lambda : cmx;var v : cvec_type);
{ left complex eigenvector v associated with eigenvalue lambda of matrix a, v'.a = lambda.v' }
{ code developped according to inverse iteration method described in NumRec pp 377-380 }
label 1,2;
var i,j,its,nn,ntry : integer;
    indx : ivec2_type; { double size for case complex lambda }
    d,eps,r : extended;
    lambd : cmx;
    x,x1,y,y1,e : rvec2_type; { double size for case complex lambda }
    aaa : rmat2_type; { double size for case complex lambda }
    bool : boolean;
begin

  ntry := 0;
  
{ case real lambda: }

  if ( lambda.im = 0.0 ) then 
    begin
      bool := true;
      for i := 1 to n do
        for j := 1 to n do
          if ( i = j ) then
            bool := bool and (a[i,i] = lambda.re)
          else
            bool := bool and (a[i,j] = 0.0);
      if bool then
        begin
          for i := 1 to n do
            begin
              v[i].re := 1.0;
              v[i].im := 0.0;
            end;
          exit;
        end;
1 :  ntry := ntry + 1;
     lambd := lambda;
     for i := 1 to n do x[i] := random;
     its := 0;
     repeat
        if ( its = 0 ) then
          begin
            r := lambd.re/100.0;
            if ( r = 0.0 ) then r := 0.1;
          end
        else
          begin
            r := vecnorm_2(n,x1);
            r := r*r/vecpscal_2(n,x1,y1);
          end;
        lambd.re := lambd.re + r;
        for i := 1 to n do
          for j := 1 to n do aaa[i,j] := a[j,i];
        for i := 1 to n do aaa[i,i] := a[i,i] - lambd.re;
        its := its + 1;
        if ( its > 10 ) then
          begin
            if ( ntry <= 5 ) then goto 1;
            erreur_math('too many iterations to find left eigenvector');
            exit;
          end;
        matlu(n,aaa,indx,d);
        if err_math then exit;
        y := x;
        x1 := x;
        matlusolv(n,aaa,indx,y);
        y1 := y;
        vecnormalise_2(n,y);
        for i := 1 to n do e[i] := x[i] - y[i];
        eps := vecnorm_2(n,e);
        x := y;
      until ( ( eps < 1.0e-4 ) or ( abs(r) < 1.0e-6 ) ); 
      for i := 1 to n do
        begin
          v[i].re := x[i];
          v[i].im := 0.0;
        end;
      exit;
    end;

{ case complex lambda: }
  
  nn := 2*n;
  if ( nn > matmax2 ) then
    begin
      erreur_math('size too big in eigenvector computation');
      exit;
    end;
2:
  ntry := ntry + 1;
  lambd := lambda;
  for i := 1 to nn do  x[i] := random;
  its := 0;
  repeat
    if ( its = 0 ) then
      r := cmxmod(lambd)/100.0
    else
      begin
        r := vecnorm_2(nn,x1);
        r := r*r/vecpscal_2(nn,x1,y1);
      end;
    lambd.re := lambd.re +  r;
    for i := 1 to n do
      for j := 1 to n do
        begin
          aaa[i,j]     := a[j,i];
          aaa[i+n,j+n] := a[j,i];
          aaa[i+n,j]   := 0.0;
          aaa[i,j+n]   := 0.0;
        end;
    for i := 1 to n do 
      begin
        aaa[i,i]     := a[i,i] - lambd.re;
        aaa[i+n,i+n] := a[i,i] - lambd.re;
        aaa[i,i+n]   := -lambd.im;
        aaa[i+n,i]   := lambd.im;
      end;
    its := its + 1;
    if ( its > 10 ) then
      begin
        if ( ntry <= 5 ) then goto 2;
        erreur_math('too many iterations to find left eigenvector');
        exit;
      end;
    matlu(nn,aaa,indx,d);
    if err_math then exit;
    y := x;
    x1 := x;
    matlusolv(nn,aaa,indx,y);
    y1 := y;
    vecnormalise_2(nn,y);
    for i := 1 to nn do e[i] := x[i] - y[i];
    eps := vecnorm_2(nn,e);
    x := y;
  until ( ( eps < 1.0e-4 ) or ( abs(r) < 1.0e-6 ) ); 
  for i := 1 to n do
    begin
      v[i].re := x[i];
      v[i].im := x[i+n];
    end;
end;


{ ------   Fourier spectrum ------ }

procedure fft(var data : vecvec_type;nn,isign : integer);
{ Fast Fourier Transform over nn points, nn a power of 2 }
{ isign = 1 -> direct transform, isign = -1 -> inverse transform }
{ NumRec p. 394, p. 754 }
var  ii,jj,n,mmax,m,j,istep,i : integer;
     wtemp,wr,wpr,wpi,wi,theta,tempi,tempr : extended;
begin
  n := 2*nn;
  j := 1;
  for ii := 1 to nn do
    begin
      i := 2*ii - 1;
      if ( j > i ) then 
        begin
          tempr := data[j];
          tempi := data[j+1];
          data[j]   := data[i];
          data[j+1] := data[i+1];
          data[i]   := tempr;
          data[i+1] := tempi;
        end;
      m := n div 2;
      while ( m >= 2) and ( j > m ) do
        begin
          j := j - m;
          m := m div 2;
        end;
      j := j + m;
    end;
  mmax := 2;
  while ( n > mmax ) do
    begin
      istep := 2*mmax;
      theta := 2.0*pi/(isign*mmax);
      wpr := -2.0*sqr(sin(0.5*theta));
      wpi := sin(theta);
      wr  := 1.0;
      wi  := 0.0;
      for ii := 1 to ( mmax div 2 ) do
        begin
          m := 2*ii - 1;
          for jj := 0 to ( (n-m) div istep ) do
            begin
              i := m + jj*istep;
              j := i + mmax;
              tempr := wr*data[j]   - wi*data[j+1];
              tempi := wr*data[j+1] + wi*data[j];
              data[j]   := data[i]   - tempr;
              data[j+1] := data[i+1] - tempi;
              data[i]   := data[i]   + tempr;
              data[i+1] := data[i+1] + tempi;
            end;
          wtemp := wr;
          wr := wr*wpr - wi*wpi + wr;
          wi := wi*wpr + wtemp*wpi + wi;
        end;
      mmax := istep;
    end;
end;


{ ------  correlation ------ }

procedure correl(data1,data2 : vecvec_type;n,m : integer;var c : vecvec_type);
{ cross-correlation of arrays data1 and data2 [0..n-1] }
{ returned in array c of size m }
var i,j : integer;
    s,e1,e2 : extended;
begin
  e1 := 0.0;
  e2 := 0.0;
  for i := 0 to n-1 do
    begin
      e1 := e1 + data1[i];
      e2 := e2 + data2[i];
    end;
  e1 := e1/n;
  e2 := e2/n;
  for i := 0 to m-1 do
    begin
      s := 0.0;
      for j := 0 to n-1-i do s := s + (data1[i+j]-e1)*(data2[j]-e2);
      c[i] := s/n;
    end;
end;

function  coeff_correl(n : integer; datax,datay : vecvec_type) : extended;
{ correlation coefficient (Pearson) of arrays datax and datay [0..n-1] }
var i : integer;
    xmoy,ymoy,sxx,syy,sxy,x,y : extended;
begin
  xmoy := 0.0;
  ymoy := 0.0;
  for i := 0 to n-1 do
    begin
      xmoy := xmoy + datax[i];
      ymoy := ymoy + datay[i];
    end;
  xmoy := xmoy/n;
  ymoy := ymoy/n;
  sxx := 0.0;
  syy := 0.0;
  sxy := 0.0;
  for i := 0 to n-1 do
    begin
      x := datax[i] - xmoy;
      y := datay[i] - ymoy;
      sxx := sxx + sqr(x);
      syy := syy + sqr(y);
      sxy := sxy + x*y;
    end;
  if ( sxx = 0.0 ) or ( syy = 0.0 ) then
    coeff_correl := 0.0
  else
    coeff_correl := sxy/sqrt(sxx*syy);
end;

procedure coeff_regress(n : integer; datax,datay : vecvec_type;var a,b : extended);
{ coefficients of regression line y = ax + b across arrays datax, datay [0..n-1] }
var i : integer;
    xmoy,ymoy,sxx,sxy,x,y : extended;
begin
  xmoy := 0.0;
  ymoy := 0.0;
  for i := 0 to n-1 do
    begin
      xmoy := xmoy + datax[i];
      ymoy := ymoy + datay[i];
    end;
  xmoy := xmoy/n;
  ymoy := ymoy/n;
  sxx := 0.0;
  sxy := 0.0;
  for i := 0 to n-1 do
    begin
      x := datax[i] - xmoy;
      y := datay[i] - ymoy;
      sxx := sxx + sqr(x);
      sxy := sxy + x*y;
    end;
  if ( sxx = 0.0 ) then
    begin
      a := 0.0;
      b := 0.0;
    end
  else
    begin
      a := sxy/sxx;
      b := ymoy - a*xmoy;
    end;
end;


{ ------  matrix properties  ------ }

function  matnonneg(n : integer;a : rmat_type) : boolean;
{ check if matrix a of size nxn is non negative }
var i,j : integer;
begin
  for i:= 1 to n do
    for j := 1 to n do
      if ( a[i,j] < 0.0 ) then
        begin
          matnonneg := false;
          exit;
        end;
  matnonneg := true;
end;

function  matpos(n : integer;a : rmat_type) : boolean;
{ check if matrix a of size nxn is positive }
var i,j : integer;
begin
  for i := 1 to n do
    for j := 1 to n do
      if ( a[i,j] <= 0.0 ) then
        begin
          matpos := false;
          exit;
        end;
  matpos := true;
end;

function  matirr(n : integer;a : rmat_type) : boolean;
{ check if non negative matrix a of size nxn is irreducible <=> (a+I)^(n-1) positive }
var i,j,p : integer;
    b,c : rmat_type;
begin
  for i:= 1 to n do
    for j := 1 to n do
      if j<>i then b[i,j] := a[i,j] else b[i,j] := a[i,j] + 1.0;
  p := 1;
  repeat
    matprod(n,b,b,c);
    b := c;
    p := 2*p;
  until ( p >= n-1 );
  matirr := matpos(n,b);
end;

function  matprim(n : integer;a : rmat_type) : boolean;
{ check if non negative matrix a of size nxn is primitive <=> a^(n-1)(n-2) positive }
var p,m : integer;
    b : rmat_type;
begin
  p := 1;
  m := (n-1)*(n-2);
  repeat
    matprod(n,a,a,b);
    a := b;
    p := 2*p;
  until ( p >= m );
  matprim := matpos(n,a);
end;


{ ------ dominant eigenvalue by the power method ------ }

procedure matvalvecd1(n : integer;a : rmat_type;var w : rvec_type;var lambda1 : extended);
{ compute dominant eigenvalue lambda1 and associated right eigenvector w }
{ of primitive matrix a of size nxn by the power method: iteration of x := ax }
label 1;
const eps = 0.000000001;
      tmax = 10000;
var t,i,j : integer;
    w1 : rvec_type;
    sum,lamb,lamb1 : extended;
begin
  for i := 1 to n do w[i] := 1.0/n; { initial guess }
  lamb := 1.0;
  t := 0;
  repeat
    t := t + 1;
    lamb1 := lamb;
    for i := 1 to n do
      begin
        sum := 0.0;
        for j := 1 to n do
          sum := sum + a[i,j]*w[j];
        w1[i] := sum;
      end;
    sum := 0.0;
    for i := 1 to n do sum := sum + w1[i];
    if ( sum > 0.0 ) then
      for i := 1 to n do w[i] := w1[i]/sum
    else
      goto 1;
    lamb := sum;
  until ( abs(lamb1 - lamb) < eps ) or ( t > tmax );
1:
  lambda1 := lamb;
end;

procedure matvalvec1(n : integer;a : rmat_type;var w,v : rvec_type;var lambda1 : extended);
{ compute dominant eigenvalue lambda1 and associated left and right eigenvectors v,w }
{ of primitive matrix a of size nxn by the power method }
label 1;
const eps = 0.000000001;
      tmax = 10000;
var t,i,j : integer;
    w1,v1 : rvec_type;
    sum,lamb,lamb1 : extended;
begin
  for i := 1 to n do w[i] := 1.0/n;
  for j := 1 to n do v[j] := 1.0/n;
  lamb := 1.0;
  t := 0;
  repeat
    t := t + 1;
    lamb1 := lamb;
    for i := 1 to n do
      begin
        sum := 0.0;
        for j := 1 to n do
          sum := sum + a[i,j]*w[j];
        w1[i] := sum;
      end;
    for j := 1 to n do
      begin
        sum := 0.0;
        for i := 1 to n do
          sum := sum + v[i]*a[i,j];
        v1[j] := sum;
      end;
    sum := 0.0;
    for i := 1 to n do sum := sum + w1[i];
    if ( sum > 0.0 ) then
      for i := 1 to n do w[i] := w1[i]/sum
    else
      goto 1;
    sum := 0.0;
    for j := 1 to n do sum := sum + v1[j];
    if ( sum > 0.0 ) then
      for j := 1 to n do v[j] := v1[j]/sum
    else
      goto 1;
    lamb := sum;
  until ( abs(lamb1 - lamb) < eps ) or ( t > tmax );
1 :
  lambda1 := lamb;
end;

procedure matvecg1(n : integer;a : rmat_type;var v : rvec_type);
{ compute dominant eigenvalue lambda1 and associated left eigenvector v }
{ of primitive matrix a of size nxn by the power method }
label 1;
const tmax = 10000;
var t,i,j : integer;
    v1 : rvec_type;
    sum : extended;
begin
  for j := 1 to n do v[j] := 1.0/n;
  t := 0;
  repeat
    t := t + 1;
    for j := 1 to n do
      begin
        sum := 0.0;
        for i := 1 to n do
          sum := sum + v[i]*a[i,j];
        v1[j] := sum;
      end;
    sum := 0.0;
    for j := 1 to n do sum := sum + v1[j];
    if ( sum > 0.0 ) then
      for j := 1 to n do v[j] := v1[j]/sum
    else
      goto 1;
  until ( t > tmax );
1 :
end;

procedure matkovvecg(n : integer;a : rmat_type;var v : rvec_type);
{ compute stationary distribution v of irreducible Markov matrix a of size nxn }
{ (left eigenvector v associated with eigenvalue 1) by the power method }
const eps = 1.0E-9;
      tmax = 10000;
var i,j,t : integer;
    v1 : rvec_type;
    sum,d : extended;
begin
  for j := 1 to n do v[j] := 1.0/n;
  t := 0;
  repeat
    t := t + 1;
    for j := 1 to n do
      begin
        sum := 0.0;
        for i := 1 to n do sum := sum + v[i]*a[i,j];
        v1[j] := sum;
      end;
    d := 0.0;
    for j := 1 to n do d := d + abs(v1[j] - v[j]);
    for j := 1 to n do v[j] := v1[j];
  until ( d < eps ) or ( t >= tmax);
end;

end.
