program ulm;

{$MODE Delphi}

uses
  Forms, Interfaces,
  gulm in 'gulm.pas' {form_ulm},
  ggraph in 'ggraph.pas' {form_graph},
  ggraphset in 'ggraphset.pas' {form_graphset},
  gabout in 'gabout.pas' {form_about},
  grunset in 'grunset.pas' {form_runset},
  gviewvar in 'gviewvar.pas' {form_view_variables},
  gcalc in 'gcalc.pas' {form_calc},
  gedit in 'gedit.pas' {form_edit},
  ghighlighter in 'ghighlighter.pas' {TSynUlmHl},
  gviewall in 'gviewall.pas' {form_view_all},
  gtext in 'gtext.pas' {form_text},
  gtextset in 'gtextset.pas' {form_textset},
  gsensib in 'gsensib.pas' {form_sensib},
  gconfint in 'gconfint.pas' {form_confint},
  gconfint2 in 'gconfint2.pas' {form_confint2},
  gprop in 'gprop.pas' {form_prop},
  gmulti in 'gmulti.pas' {form_multi},
  gage in 'gage.pas' {form_age},
  gspec in 'gspec.pas' {form_spec},
  gcorrel in 'gcorrel.pas' {form_correl},
  glandscape in 'glandscape.pas' {form_landscape},
  glyap in 'glyap.pas' {form_lyap},
  gstocsensib in 'gstocsensib.pas' {form_stocsensib};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(Tform_ulm, form_ulm);
  Application.CreateForm(Tform_graph, form_graph);
  Application.CreateForm(Tform_graphset, form_graphset);
  Application.CreateForm(Tform_about, form_about);
  Application.CreateForm(Tform_runset, form_runset);
  Application.CreateForm(Tform_view_variables, form_view_variables);
  Application.CreateForm(Tform_calc, form_calc);
  Application.CreateForm(Tform_edit, form_edit);
  Application.CreateForm(Tform_view_all, form_view_all);
  Application.CreateForm(Tform_text, form_text);
  Application.CreateForm(Tform_textset, form_textset);
  Application.CreateForm(Tform_sensib, form_sensib);
  Application.CreateForm(Tform_confint, form_confint);
  Application.CreateForm(Tform_confint2, form_confint2);
  Application.CreateForm(Tform_prop, form_prop);
  Application.CreateForm(Tform_multi, form_multi);
  Application.CreateForm(Tform_age, form_age);
  Application.CreateForm(Tform_spec, form_spec);
  Application.CreateForm(Tform_correl, form_correl);
  Application.CreateForm(Tform_landscape, form_landscape);
  Application.CreateForm(Tform_lyap, form_lyap);
  Application.CreateForm(Tform_stocsensib, form_stocsensib);
  {$INCLUDE platform.inc}
  Application.Run;
end.
