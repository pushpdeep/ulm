{ ---------------------------------------------------------------------------- }
{ Copyright (c) 1995-2017, Stéphane Legendre and the ULM development team      }
{                                                                              }
{ This file is part of ULM.                                                    }
{                                                                              }
{ ULM is free software: you can redistribute it and/or modify it               }
{ under the terms of the GNU General Public License as published               }
{ by the Free Software Foundation, either version 3 of the License,            }
{ or (at your option) any later version.                                       }
{                                                                              }
{ ULM is distributed in the hope that it will be useful,                       }
{ but WITHOUT ANY WARRANTY; without even the implied warranty                  }
{ of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                      }
{ See the GNU General Public License for more details.                         }
{                                                                              }
{ You should have received a copy of the GNU General Public License            }
{ along with ULM.  If not, see <http://www.gnu.org/licenses/>.                 }
{ ---------------------------------------------------------------------------- }

unit gulm;

{$MODE Delphi}

{  @@@@@@   main form - main program   @@@@@@  }

{ Here is the overall functioning and structure of the ULM program: }

{ 1) unit jglobvar.pas }
{ Declaration of gloval variables, }
{ description of internal objects handled by the program: }
{ model, matrix, vector, relation, function, variable, real number, list. }

{ 2) unit jsymb.pas }
{ Management of lists and internal objects. }

{ 3) unit jcompil.pas }
{ The "compiler" turns the declarations of the input model file xxx.ulm }
{ (defmod, defvec, defmat, defrel, deffun, defvar) }
{ into internal representation in the form of objects. }
{ Mathematical expressions are represented by lists. }

{ 4) unit jsyntax.pas }
{ Construction and analysis of lists, }
{ text output of objects. }

{ 5) unit jeval.pas }
{ Interpreter (list evaluator) : the kernel of the ULM program. }

{ 6) unit jinterp.pas }
{ Online command interpreter }
{ equivalent to what is performed by the tool-buttons of the main form. }

{ 7) unit jrun.pas, unit grunset.pas }
{ Run the model(s) for a number of time steps, }
{ form for setting parameters of the run and montecarlo commands.}

{ 8) unit jcarlo.pas }
{ Monte Carlo simulation. }
{ Run several trajectories of the model(s) for a number of time steps. }

{ 9) various tools: }
{ - unit jmatrix.pas: determination of matrix type }
{ - unit gprop.pas: form for computation of matrix properties, demographic descriptors }
{ - units gsensib.pas, gstocsensib.pas, glandscape.pas: forms for computation of sensitivities }
{ - units gmulti.pas, gage.pas: forms for multisite models, size-classified models }
{ - units gcorrel.pas, gspec.pas, glyap.pas: forms for computation of cross-correlation, power spectrum, Lyapunov exponent }
{ - units gconfint.pas, gconfint2.pas: forms for computation of confidence interval of dominant eigenvalue }
{ - unit gedit: form for displaying/editing model file(s) }
{ - unit gcalc: online desk calculator }
{ - units gviewvar, gviewall: display variables and objects }
{ - unit gabout: form about, information about the program }
{ - unit jutil.pas: some utilitary procedures }

{ 10) unit jmath.pas }
{ Mathematical library: random distributions, mathematical functions, linear algebra.}

{ 11) unit ggraph.pas, unit ggraphset.pas }
{ Graphical library, graphics settings. }

{ 12) units gtext, unit gtextset }
{ Results output, output settings. }

interface

uses LazFileUtils, SysUtils, Classes, Variants, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, ComCtrls, Menus, StdActns, ActnList, Buttons,
  FileUtil, LazUTF8, LCLType, LConvEncoding,
  ggraph, gtext;

type

  { tform_ulm }

  tform_ulm = class(TForm)
    Panel1: TPanel;
    Edit1: TEdit;
    Label1: TLabel;
    memo_interp: TMemo;
    StatusBar1: TStatusBar;
    ToolBar1: TToolBar;
    MainMenu1: TMainMenu;
    ActionList1: TActionList;
    filenew: TAction;
    fileopen: TAction;
    filesave: TAction;
    filesaveas: TAction;
    fileexit: TAction;
    filecompile: TAction;
    helpabout: TAction;
    EditCut1: TEditCut;
    EditCopy1: TEditCopy;
    EditPaste1: TEditPaste;
    run_init: TAction;
    run_run: TAction;
    stop_execution: TAction;
    run_settings: TAction;
    montecarlo_run: TAction;
    montecarlo_settings: TAction;
    view_variables: TAction;
    view_all: TAction;
    view_calculator: TAction;
    graph_new: TAction;
    graph_settings: TAction;
    text_new: TAction;
    text_settings: TAction;
    ImageList1: TImageList;
    File1: TMenuItem;
    New1: TMenuItem;
    Open1: TMenuItem;
    Save1: TMenuItem;
    SaveAs1: TMenuItem;
    nnn1: TMenuItem;
    Exit1: TMenuItem;
    Edit2: TMenuItem;
    Cut1: TMenuItem;
    Copy1: TMenuItem;
    Paste1: TMenuItem;
    Run1: TMenuItem;
    Init1: TMenuItem;
    MonteCarlo1: TMenuItem;
    Settings2: TMenuItem;
    View1: TMenuItem;
    variaBles1: TMenuItem;
    All1: TMenuItem;
    Text1: TMenuItem;
    Settings3: TMenuItem;
    Graphics1: TMenuItem;
    About1: TMenuItem;
    file_new_button: TToolButton;
    file_open_button: TToolButton;
    file_save_button: TToolButton;
    separator01: TToolButton;
    cut_button: TToolButton;
    copy_button: TToolButton;
    paste_button: TToolButton;
    separator02: TToolButton;
    init_button: TToolButton;
    compil_button: TToolButton;
    run_button: TToolButton;
    stop_button: TToolButton;
    separator03: TToolButton;
    montecarlo_button: TToolButton;
    separator04: TToolButton;
    prop_button: TToolButton;
    view_button: TToolButton;
    separator05: TToolButton;
    text_button: TToolButton;
    separator06: TToolButton;
    graph_button: TToolButton;
    Run2: TMenuItem;
    New2: TMenuItem;
    Calculator1: TMenuItem;
    Settings1: TMenuItem;
    MonteCarlo2: TMenuItem;
    Settings4: TMenuItem;
    graphnew1: TMenuItem;
    OpenDialog1: TOpenDialog;
    SaveDialog1: TSaveDialog;
    Compile1: TMenuItem;
    nnn2: TMenuItem;
    About2: TMenuItem;
    properties: TAction;
    sensitivities: TAction;
    multisite: TAction;
    matrix1: TMenuItem;
    properties1: TMenuItem;
    multisite1: TMenuItem;
    sensitivities1: TMenuItem;
    Landscape1: TMenuItem;
    Age1: TMenuItem;
    age: TAction;
    landscape: TAction;
    tools: TMenuItem;
    spectrum1: TMenuItem;
    Correlation1: TMenuItem;
    Lyapunov1: TMenuItem;
    spectrum: TAction;
    correlation: TAction;
    lyapunov: TAction;
    Stochasticsensitivities1: TMenuItem;
    stocsensib: TAction;
    ConfidenceInterval1: TMenuItem;
    confint: TAction;
    confint2: TAction;
    Confidenceinterval21: TMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure start_model;
    procedure status_file(filename : string);
    procedure status_run(s : string);
    procedure status_t_exec(ms : integer);
    procedure status_time;
    procedure status_traj;
    procedure fileexitExecute(Sender: TObject);
    procedure filenewExecute(Sender: TObject);
    procedure fileopenExecute(Sender: TObject);
    procedure filesaveExecute(Sender: TObject);
    procedure filesaveasExecute(Sender: TObject);
    procedure filecompileExecute(Sender: TObject);
    procedure run_runExecute(Sender: TObject);
    procedure run_initExecute(Sender: TObject);
    procedure run_settingsExecute(Sender: TObject);
    procedure montecarlo_runExecute(Sender: TObject);
    procedure montecarlo_settingsExecute(Sender: TObject);
    procedure graph_settingsExecute(Sender: TObject);
    procedure graph_newExecute(Sender: TObject);
    procedure stop_executionExecute(Sender: TObject);
    procedure view_variablesExecute(Sender: TObject);
    procedure view_calculatorExecute(Sender: TObject);
    procedure edit1_returnpressed(Sender: TObject);
    procedure view_allExecute(Sender: TObject);
    procedure helpaboutExecute(Sender: TObject);
    procedure text_newExecute(Sender: TObject);
    procedure text_settingsExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure procproc;
    procedure Edit1DblClick(Sender: TObject);
    procedure propertiesExecute(Sender: TObject);
    procedure sensitivitiesExecute(Sender: TObject);
    procedure multisiteExecute(Sender: TObject);
    procedure ageExecute(Sender: TObject);
    procedure spectrumExecute(Sender: TObject);
    procedure correlationExecute(Sender: TObject);
    procedure lyapunovExecute(Sender: TObject);
    procedure landscapeExecute(Sender: TObject);
    procedure stocsensibExecute(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure confintExecute(Sender: TObject);
    procedure confint2Execute(Sender: TObject);
  private
    procedure erreur(s : string);
    procedure init_defaults;
    procedure interp_commands;
    procedure set_actions0;
    procedure set_actions1;
    procedure myIdleHandler(Sender: TObject; var Done: Boolean);
  public
  end;

var   form_ulm : tform_ulm;

const maxform_graph = 6;
      maxform_text  = 6;

type  tab_form_graph_type = array[1..maxform_graph] of tform_graph;
      tab_form_text_type  = array[1..maxform_text]  of tform_text;

var   tab_form_graph : tab_form_graph_type; { array of pointers to graphic windows }
      nb_form_graph  : integer; { number of active graphic windows }
      i_form_graph   : integer; { index of current active graphic window }
      tab_form_text  : tab_form_text_type; { array of pointers to output windows }
      nb_form_text   : integer; { number of active output windows }

implementation

uses jglobvar,jsymb,jsyntax,jmath,jcompil,jeval,
     jinterp,jutil,jrun,jcarlo,
     grunset,ggraphset,gviewvar,gcalc,gedit,gviewall,gabout,gtextset,
     gsensib,gconfint,gconfint2,gstocsensib,gprop,gmulti,gage,
     gspec,gcorrel,glandscape,glyap;

{$R *.lfm}

const histmax = 30;

var  history : array[1..histmax] of string; { stack to memorize online commands }
     phist,qhist : integer; { pointers to this stack }

procedure tform_ulm.erreur(s : string);
{ unit specific error procedure }
begin
  erreur_('Commands - ' + s);
end;

procedure tform_ulm.init_defaults;
{ set default values }
begin
  nb_cycle := 50;
  dt_texte_interp := 10;
  nb_cycle_carlo := 50;
  nb_run_carlo := 100;
  seuil_ext := seuil_ext_def;
  seuil_div := seuil_div_def;
  param  := false;
  notext := false;
end;

procedure tform_ulm.set_actions0;
{ disable tool buttons, i.e. when the model is not compiled }
begin
  run_run.Enabled  := false;
  stop_execution.Enabled  := false;
  run_init.Enabled := false;
  run_settings.Enabled := false;
  montecarlo_run.Enabled := false;
  montecarlo_settings.Enabled := false;
  view_variables.Enabled := false;
  view_all.Enabled := false;
  view_calculator.Enabled := false;
  graph_new.Enabled := false;
  graph_settings.Enabled := false;
  text_new.Enabled := false;
  text_settings.Enabled := false;
  properties.Enabled := false;
  age.Enabled := false;
  sensitivities.Enabled := false;
  confint.Enabled := false;
  confint2.Enabled := false;
  stocsensib.Enabled := false;
  multisite.Enabled := false;
  landscape.Enabled := false;
  spectrum.Enabled := false;
  correlation.Enabled := false;
  lyapunov.Enabled := false;
end;

procedure tform_ulm.set_actions1;
{ enable tool buttons,  i.e. when the model is compiled }
begin
  run_run.Enabled  := true;
  stop_execution.Enabled  := true;
  run_init.Enabled := true;
  run_settings.Enabled := true;
  montecarlo_run.Enabled := true;
  montecarlo_settings.Enabled := true;
  view_variables.Enabled := true;
  view_all.Enabled := true;
  view_calculator.Enabled := true;
  graph_new.Enabled := true;
  graph_settings.Enabled := true;
  text_new.Enabled := true;
  text_settings.Enabled := true;
  properties.Enabled := true;
  age.Enabled := true;
  multisite.Enabled := true;
  sensitivities.Enabled := true;
  confint.Enabled := true;
  confint2.Enabled := true;
  stocsensib.Enabled := true;
  landscape.Enabled := true;
  spectrum.Enabled := true;
  correlation.Enabled := true;
  lyapunov.Enabled := true;
end;

procedure tform_ulm.start_model;
var f : integer;
begin
  init_math; { set random generator seed }
  init_symb; { init lists and objects }


   {Compile the current model file if its not empty.}
   if Length(form_edit.memo1.Lines.Text) = 0 then ShowMessage('Please load a model first.')
   else
   begin
      lines_compil := form_edit.memo1.Lines; { lines of the input model file }
      compilation; { compile model file }
      if not form_edit.Visible then form_edit.Show;
   end;

  if compiled then
    begin
      run_initExecute(nil); { initialize }
      init_texte_interp;
      { init forms: }
      for f := 1 to maxform_text do
	with tab_form_text[f] do init_text;
      for f := 1 to maxform_graph do
	with tab_form_graph[f] do init_graph;
      status_file(nomfic);
      form_edit.status(nomfic);
      form_view_all.init_viewall;
      form_prop.init_prop;
      form_age.init_age;
      form_sensib.init_sensib;
      form_confint.init_confint;
      form_confint2.init_confint2;
      form_stocsensib.init_stoc_sensib;
      form_multi.init_multi;
      form_landscape.init_landscape;
      form_spec.init_spec;
      form_correl.init_correl;
      form_lyap.init_lyap;
      set_actions1;
    end;
end;

procedure tform_ulm.FormCreate(Sender: TObject);
{ creation of main form }
begin
  Left := 0;
  Width := Trunc(Screen.Width / 2) - 8;
{$IFDEF LCLcarbon}
  Top := 23;
{$ELSE}
  Top := 0;
{$ENDIF}
  Height := Trunc(2 * Screen.Height / 3);
  { initializations: }
  init_math;
  init_symb;
  init_syntax;
  init_defaults;
  init_compilation;
  init_interp;
  modelfileopened := false;
  inputfileopened := false;
  outputfile      := false;
  nomfic    := '';
  nomficin  := '';
  nomficout := '';
  { program run string: ulm.exe xxx.ulm yyy.txt zzz.txt }
  { program parameters are optional file names }
  if ( ParamCount > 0 ) and LazFileUtils.fileexistsUTF8(paramstr(1)) { *Converted from FileExists* } then
    begin
      nomfic := ParamStr(1); { name of model file xxx.ulm passed as a parameter }
      lines_compil.LoadFromFile(nomfic); { load lines to be compiled }
      modelfileopened := true;
    end;
  if ( ParamCount > 1 ) and LazFileUtils.fileexistsUTF8(paramstr(2)) { *Converted from FileExists* } then
    begin
      nomficin := ParamStr(2); { name of command file yyy.txt passed as a parameter }
      lines_interp.LoadFromFile(nomficin);
      inputfileopened := true;
      if ( Paramcount > 2 ) then { output file }
	begin
	  nomficout := ParamStr(3); { name of output file zzz.txt passed as a parameter }
	  outputfile := true;
	  AssignFile(ficout,nomficout);
	  rewrite(ficout);
	end;
    end;
  set_actions0; { disable tool buttons }
  Application.OnIdle := myIdleHandler; { interruption handler }
  KeyPreview := true;
  runstop := false;
  phist := 0;
end;

procedure tform_ulm.myIdleHandler(Sender: TObject; var Done: Boolean);
{ interruption handler }
begin
end;

procedure tform_ulm.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
{ shif-escape -> interrupt program }
begin
  if ( Key = vk_escape ) and ( Shift*[ssShift,ssAlt,ssCtrl] = [ssShift] ) then
    begin
      runstop := true;
      status_run('Stopped');
    end;
end;

procedure tform_ulm.procproc;
{ interruption handler }
begin
  Application.ProcessMessages;
end;

procedure tform_ulm.interp_commands;
{ execute commands from command file (batch) }
var i : integer;
    s : string;
begin
  start_model;
  for i := 0 to lines_interp.Count - 1 do
    begin
      s := lines_interp[i];
      iwriteln('> ' + s);
      interp(s);
    end;
  if outputfile then CloseFile(ficout);
  fileexitExecute(nil);
end;

procedure create_form_graph;
{ create forms for graphics }
var i : integer;
begin
  nb_form_graph := 1;
  i_form_graph  := 1;
  tab_form_graph[1] := form_graph;
  for i := 2 to maxform_graph do
    tab_form_graph[i] := tform_graph.Create(nil);
  for i := 1 to maxform_graph do with tab_form_graph[i] do
    begin
      ifg := i;
      Caption := 'Graphics <' + IntToStr(ifg) +'>';
      Left := Trunc(Screen.Width / 2) + 1 + 8; {8px is the borderwidth of Window Vista's window decorations}
      Width := Screen.Width - Left;
{$IFDEF LCLcarbon}
      Top  := 50 * (i - 1) + 23;
{$ELSE}
      Top  := 50 * (i - 1);
{$ENDIF}
    end;
end;

procedure create_form_text;
{ create forms for results output }
var i : integer;
begin
  nb_form_text := 1;
  tab_form_text[1] := form_text;
  for i := 2 to maxform_text do
    tab_form_text[i] := tform_text.Create(nil);
  for i := 1 to maxform_text do with tab_form_text[i] do
    begin
      ift := i;
      Caption := 'Text <' + IntToStr(ift) +'>';
      Left := Trunc(Screen.Width / 2) + 1 + 8; {8px is the borderwidth of Window Vista's window decorations}
      Width := Screen.Width - Left;
      Top := Trunc(Screen.Height / 3) + 50 * (i - 1);
      Height := Screen.Height - Top - 30; {30px is height of the titlebar in Window Vista}
    end;
end;

procedure tform_ulm.FormShow(Sender: TObject);
{ show main form }
begin
  create_form_graph;
  create_form_text;
  if modelfileopened then with form_edit do
    begin
      memo1.Lines := lines_compil; { display lines of model file in edit window }
      newpage(nomfic);
      status(nomfic);
      Show;
    end;
  form_graph.Show;
  if inputfileopened then interp_commands; { command file (batch) }
end;

{ ------ status bar ------ }

procedure tform_ulm.status_file(filename : string);
begin
  statusbar1.Panels[0].Text := ExtractFileName(filename);
end;

procedure tform_ulm.status_run(s : string);
begin
  statusbar1.Panels[1].Text := s;
  iwriteln('> ' + s);
end;

procedure tform_ulm.status_t_exec(ms : integer);
begin
  statusbar1.Panels[2].Text := s_ecri_t_exec(ms);
  if outputfile then iwriteln(s_ecri_t_exec(ms));
end;

procedure tform_ulm.status_time;
begin
  if not notext then statusbar1.Panels[3].Text := 't = ' + IntToStr(t__);
end;

procedure tform_ulm.status_traj;
begin
  if not notext then statusbar1.Panels[3].Text := 'traj = ' + IntToStr(traj__);
end;

{ ------ command interpreter - edit panel ------ }

procedure tform_ulm.edit1_returnpressed(Sender: TObject);
{ online commands in small interpreter edit panel }
var s : string;
begin
  if not compiled then exit;
  s := tronque(minuscule(edit1.Text));
  iwriteln('> ' + s);
  if ( s <> '' ) then { memorize command in stack history }
    begin
      phist := (phist + 1) mod (histmax + 1);
      if ( phist = 0 ) then phist := 1;
      history[phist] := s;
      qhist := phist;
    end;
  edit1.Clear;
  interp(s); { interpret/execute command }
end;

procedure tform_ulm.Edit1DblClick(Sender: TObject);
{ recall past commands by double click }
begin
  edit1.Text := history[qhist];
  qhist := qhist - 1;
  if ( qhist = 0 ) then qhist := histmax;
end;

{ ------ actions for files ------ }

procedure tform_ulm.filenewExecute(Sender: TObject);
begin
  with form_edit do
    begin
      nomfic := 'untitled.ulm';
      memo1.Clear;
      newpage(nomfic);
      if not Visible then Show;
    end;
end;

procedure tform_ulm.fileopenExecute(Sender: TObject);
var s : string;
var encdng : string;
var encdd : boolean;
begin
  if opendialog1.Execute then with form_edit do
    begin
      nomfic := opendialog1.FileName;
      s := ReadFileToString(nomfic);
      encdng := LConvEncoding.GuessEncoding(s);
      if (encdng = LConvEncoding.EncodingUTF8) or (encdng = LConvEncoding.UTF8BOM) then
        memo1.Lines.Text := s
      else
        begin
          memo1.Lines.Text := LConvEncoding.ConvertEncodingToUTF8(s, encdng, encdd);
          if encdd = true then
	    ShowMessage('Incorrect encoding (' + encdng + '). Content was converted to UTF8')
          else
	    ShowMessage('There may have been a problem opening the file. Make sure it is UTF8 encoded')
        end;
      modelfileopened := true;
      newpage(nomfic);
      if not Visible then Show;
    end;
end;

procedure tform_ulm.filesaveExecute(Sender: TObject);
begin
  if ( nomfic = 'untitled.ulm' ) then
    filesaveasExecute(nil)
  else
    if ( nomfic <> '' ) then
       { If the directory is not writable, Switch to SaveAs } 
       if LazFileUtils.DirectoryIsWritable (ExtractFilePath(ExcludeTrailingPathDelimiter(nomfic))) then
	   form_edit.memo1.Lines.SaveToFile(nomfic)
       else
           begin
	      ShowMessage('You do not have the right to write in this directory, please select another location.');
	      filesaveasExecute(nil);
	   end;
end;

procedure tform_ulm.filesaveasExecute(Sender: TObject);
begin
  with savedialog1 do
    begin

       { If the directory is not writable, SaveAs default to UserDir } 
       if LazFileUtils.DirectoryIsWritable (ExtractFilePath(ExcludeTrailingPathDelimiter(nomfic))) then
	  begin
	     InitialDir := ExtractFilePath(nomfic);
	  end
       else
	  begin
	     nomfic := ConcatPaths([GetUserDir, ExtractFileName(nomfic)]);
	     InitialDir := GetUserDir;
	  end;
       FileName := nomfic;

       if Execute then with form_edit do
	begin
	  if LazFileUtils.fileexistsUTF8(FileName) { *Converted from FileExists* } then
	    if MessageDlg('Overwrite file ' + ExtractFileName(FileName) + '?',
	       mtConfirmation,[mbYes,mbNo],0) <> mrYes then exit;
	  nomfic := FileName;
	   
	  { If the directory is not writable, Prompt again instead of failing.} 
	   if LazFileUtils.DirectoryIsWritable (ExtractFilePath(ExcludeTrailingPathDelimiter(nomfic))) then
	     begin
	        memo1.Lines.SaveToFile(nomfic);
		status(nomfic);
		pagecontrol1.ActivePage.Caption := ExtractFileName(nomfic);
	     end
	   else
	      begin
		 ShowMessage('You do not have the right to write in this directory, please select another location.');
		 filesaveasExecute(nil);
	      end
	end;
    end;
end;

{ ------ tool buttons ------ }

procedure tform_ulm.filecompileExecute(Sender: TObject);
{ compile model file }
begin
  start_model;
end;

procedure tform_ulm.run_initExecute(Sender: TObject);
{ init }
begin
  init_eval;
  status_run('Init ' + IntToStr(graine0-graine00+1));
  status_time;
end;

procedure tform_ulm.run_runExecute(Sender: TObject);
{ run model }
var ms : integer;
begin
  status_run('Run ' + IntToStr(nb_cycle));
  ms := clock;
  if param then
    run_param(nb_cycle,xparam,param_min,param_max,param_pas)
  else
    run(nb_cycle);
  status_t_exec(clock - ms);
  status_time;
end;

procedure tform_ulm.run_settingsExecute(Sender: TObject);
{ set parameters of run command }
begin
  form_runset.Show;
end;

procedure tform_ulm.montecarlo_runExecute(Sender: TObject);
{ run Monte Carlo procedure }
var ms : integer;
begin
  status_run('Montecarlo ' + IntToStr(nb_cycle_carlo)+ ' ' +
	     IntToStr(nb_run_carlo));
  ms := clock;
  carlo(nb_cycle_carlo,nb_run_carlo,seuil_ext,seuil_div);
  status_t_exec(clock - ms);
  status_time;
end;

procedure tform_ulm.montecarlo_settingsExecute(Sender: TObject);
{ set parameters of Monte Carlo procedure }
begin
  run_settingsExecute(nil);
end;

procedure tform_ulm.graph_settingsExecute(Sender: TObject);
{ graphics settings }
begin
  with form_graphset do
    begin
      fg := tab_form_graph[i_form_graph];
      Show;
    end;
end;

procedure tform_ulm.graph_newExecute(Sender: TObject);
{ show new graphics window }
var f : integer;
begin
  nb_form_graph := nb_form_graph + 1;
  if ( nb_form_graph > maxform_graph ) then
    begin
      erreur('no more than ' + IntToStr(maxform_graph) +
	     ' graphic windows');
      nb_form_graph := maxform_graph;
      exit;
    end;
  for f := 1 to nb_form_graph do with tab_form_graph[f] do
    if not Visible then Show;
end;

procedure tform_ulm.stop_executionExecute(Sender: TObject);
{ stop execution! }
begin
  runstop := true;
  status_run('Stopped');
end;

procedure tform_ulm.view_calculatorExecute(Sender: TObject);
{ open desk calculator }
begin
  form_calc.Show;
end;

procedure tform_ulm.view_variablesExecute(Sender: TObject);
{ display all variables }
begin
  form_view_variables.Show;
end;

procedure tform_ulm.view_allExecute(Sender: TObject);
{ display all objects }
begin
  form_view_all.Show;
end;

procedure tform_ulm.helpaboutExecute(Sender: TObject);
{ show window about }
begin
  form_about.Show;
end;

procedure tform_ulm.text_newExecute(Sender: TObject);
{ show new results output window }
var f : integer;
begin
  if ( nb_form_text = maxform_text ) then
    begin
      erreur('no more than ' + IntToStr(maxform_text) +
	     ' text windows');
      exit;
    end;
  for f := 1 to maxform_text do with tab_form_text[f] do
    if ( not Visible ) then
      begin
	Visible := true;
	nb_form_text := nb_form_text + 1;
	break;
      end;
end;

procedure tform_ulm.text_settingsExecute(Sender: TObject);
{ settings of results output }
begin
  with form_textset do
    begin
      ft := form_text;
      Show;
    end;
end;

procedure tform_ulm.fileexitExecute(Sender: TObject);
{ exit }
begin
  Close;
end;

procedure tform_ulm.FormClose(Sender: TObject; var action: TCloseAction);
{ on program close leave the place clean }
var action1 : TCloseAction;
begin
  if form_edit = nil then exit;
  action := caFree;
  with form_edit do FormClose(nil,action1);
  if ( action1 = caNone ) then action := caNone else exit;
end;

procedure tform_ulm.propertiesExecute(Sender: TObject);
{ show window of matrix properties }
begin
  with form_prop do Show;
end;

procedure tform_ulm.sensitivitiesExecute(Sender: TObject);
{ show window of matrix sensitivities }
begin
  with form_sensib do Show;
end;

procedure tform_ulm.confintExecute(Sender: TObject);
{ show window of confidence interval on growth rate }
begin
  with form_confint do Show;
end;

procedure tform_ulm.confint2Execute(Sender: TObject);
{ show window of confidence interval 2 on growth rate }
begin
  with form_confint2 do Show;
end;

procedure tform_ulm.stocsensibExecute(Sender: TObject);
{ show window of stochastic sensitivities }
begin
  with form_stocsensib do Show;
end;

procedure tform_ulm.multisiteExecute(Sender: TObject);
{ show window of multisite matrix properties }
begin
  with form_multi do Show;
end;

procedure tform_ulm.ageExecute(Sender: TObject);
{ show window of size-classified matrix age properties }
begin
  with form_age do Show;
end;

procedure tform_ulm.spectrumExecute(Sender: TObject);
{ show window for power spectrum computation }
begin
  with form_spec do Show;
end;

procedure tform_ulm.correlationExecute(Sender: TObject);
{ show window for cross-correlation computation }
begin
  with form_correl do Show;
end;

procedure tform_ulm.lyapunovExecute(Sender: TObject);
{ show window for Lyapunov exponent computation }
begin
  with form_lyap do Show;
end;

procedure tform_ulm.landscapeExecute(Sender: TObject);
{ show window for sensitivity curves computation }
begin
  with form_landscape do Show;
end;

procedure tform_ulm.FormDestroy(Sender: TObject);
{ on program close release memory }
var i : integer;
begin
  for i := 2 to maxform_text  do tab_form_text[i].Free;
  for i := 2 to maxform_graph do tab_form_graph[i].Free;
  for i := 1 to fic_nb do CloseFile(fic[i].f);
end;

end.
