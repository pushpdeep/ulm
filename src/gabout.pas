{ ---------------------------------------------------------------------------- }
{ Copyright (c) 1995-2017, Stéphane Legendre and the ULM development team      }
{                                                                              }
{ This file is part of ULM.                                                    }
{                                                                              }
{ ULM is free software: you can redistribute it and/or modify it               }
{ under the terms of the GNU General Public License as published               }
{ by the Free Software Foundation, either version 3 of the License,            }
{ or (at your option) any later version.                                       }
{                                                                              }
{ ULM is distributed in the hope that it will be useful,                       }
{ but WITHOUT ANY WARRANTY; without even the implied warranty                  }
{ of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                      }
{ See the GNU General Public License for more details.                         }
{                                                                              }
{ You should have received a copy of the GNU General Public License            }
{ along with ULM.  If not, see <http://www.gnu.org/licenses/>.                 }
{ ---------------------------------------------------------------------------- }

unit gabout;

{$MODE Delphi}

interface

uses
  SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, LCLIntf;

type

  { tform_about }

  tform_about = class(TForm)
    AboutPanel: TPanel;
    label_ULM: TLabel;
    Image1: TImage;
    label_version: TLabel;
    label_ecoevol: TLabel;
    Image2: TImage;
    label_contrib: TLabel;
    label_contribnames: TLabel;
    label_devteam: TLabel;
    label_devteamnames: TLabel;
    label_homepage: TLabel;
    url_homepage: TLabel;
    label_gitlab: TLabel;
    url_gitlab: TLabel;
    label_licensetext: TLabel;
    label_licensetitle: TLabel;
    OKButton: TBitBtn;
    procedure OKButtonClick(Sender: TObject);
    procedure HomepageURLClick(Sender: TObject);
    procedure HomepageURLMouseEnter(Sender: TObject);
    procedure HomepageURLMouseLeave(Sender: TObject);
    procedure GitLabURLClick(Sender: TObject);
    procedure GitLabURLMouseEnter(Sender: TObject);
    procedure GitLabURLMouseLeave(Sender: TObject);
  private
  public
  end;

var  form_about: tform_about;

implementation

{$R *.lfm}

procedure tform_about.OKButtonClick(Sender: TObject);
begin
  Close;
end;

procedure tform_about.HomepageURLClick(Sender: TObject);
begin
  OpenURL('http://' + url_homepage.Caption); 
end;


procedure tform_about.HomepageURLMouseEnter(Sender:TObject);
begin
  url_homepage.Font.Style := [fsUnderline];
end;

procedure tform_about.HomepageURLMouseLeave(Sender:TObject);
begin
  url_homepage.Font.Style := [];
end;       

procedure tform_about.GitLabURLClick(Sender: TObject);
begin
  OpenURL('https://' + url_gitlab.Caption); 
end;

procedure tform_about.GitLabURLMouseEnter(Sender:TObject);
begin
  url_gitlab.Font.Style := [fsUnderline];
end;

procedure tform_about.GitLabURLMouseLeave(Sender:TObject);
begin
  url_gitlab.Font.Style := [];
end;       


end.
