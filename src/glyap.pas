{ ---------------------------------------------------------------------------- }
{ Copyright (c) 1995-2017, Stéphane Legendre and the ULM development team      }
{                                                                              }
{ This file is part of ULM.                                                    }
{                                                                              }
{ ULM is free software: you can redistribute it and/or modify it               }
{ under the terms of the GNU General Public License as published               }
{ by the Free Software Foundation, either version 3 of the License,            }
{ or (at your option) any later version.                                       }
{                                                                              }
{ ULM is distributed in the hope that it will be useful,                       }
{ but WITHOUT ANY WARRANTY; without even the implied warranty                  }
{ of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                      }
{ See the GNU General Public License for more details.                         }
{                                                                              }
{ You should have received a copy of the GNU General Public License            }
{ along with ULM.  If not, see <http://www.gnu.org/licenses/>.                 }
{ ---------------------------------------------------------------------------- }

unit glyap;

{  @@@@@@   form for computing the Lyapunov exponent   @@@@@@  }

{ The Lyapunov exponent u describes the type of dynamics: }
{ * u < 0 -- stable equilibrium or periodicity            }
{   attractor = 1 point or a finite number of points      }
{ * u ~ 0 -- quasiperiodicity                             }
{   attractor = homeomorphic to a circle                  }
{ * u > 0 -- chaotic dynamics                             }
{   strange attractor                                     }

{ if 2 population vectors initially differ by eps  }
{ they differ at time t by by delta = eps*exp(u*t) }
{ thus u can be estimated by (1/t)*ln(delta/eps)   }

{$MODE Delphi}

interface

uses SysUtils,Classes,Graphics,Controls,Forms,Dialogs,StdCtrls,Grids,
     ExtCtrls,jglobvar;

type
  tform_lyap = class(TForm)
    left_panel: TPanel;
    label_model: TLabel;
    edit_modele: TEdit;
    label_tlag: TLabel;
    edit_tlag: TEdit;
    button_run: TButton;
    button_init: TButton;
    stringgrid1: TStringGrid;
    label_nb_cyc: TLabel;
    edit_nb_cyc: TEdit;
    procedure erreur(s : string);
    procedure init_lyap;
    procedure maj_lyap(u : integer; val : extended);
    procedure run_lyap(m,nb_cyc,tlag : integer);
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    function  check_param : boolean;
    procedure button_runClick(Sender: TObject);
    procedure button_initClick(Sender: TObject);
  private
    nb_cyc   : integer; { number of time steps for the computation }
    tlag     : integer; { time lag to display the estimated Lyapunov exponent }
    x_modele : integer; { pointer to model }
  public
    interp_ : boolean;
  end;

var form_lyap: tform_lyap;

implementation

uses jutil,jsymb,jmath,jsyntax,jeval,
     gulm;

{$R *.lfm}

procedure tform_lyap.erreur(s : string);
{ unit specific error procedure }
begin
  erreur_('Lyapunov - ' + s);
end;

procedure tform_lyap.init_lyap;
begin
  with stringgrid1 do RowCount := 1;
  edit_modele.Text := s_ecri_model(x_modele);
  edit_nb_cyc.Text := IntToStr(nb_cyc);
  edit_tlag.Text   := IntToStr(tlag);
  interp_ := false;
end;

procedure tform_lyap.FormCreate(Sender: TObject);
begin
  { default values: }
  x_modele := 1;
  nb_cyc := 1000;
  tlag   := 100;
  with stringgrid1 do
    begin
      FixedCols := 1;
      FixedRows := 1;
      ColCount  := 2;
      RowCount  := 1;
      Cells[0,0] := '  t     ';
      Cells[1,0] := ' Lyapunov ';
      AutoSizeColumns();
    end;
end;

procedure tform_lyap.maj_lyap(u : integer; val : extended);
var r : integer;
begin
  with stringgrid1 do
    begin
      r := RowCount;
      RowCount := RowCount + 1;
      Cells[0,r] := IntToStr(u);
      Cells[1,r] := s_ecri_val(val);
      AutoSizeColumns()
    end;
end;

procedure tform_lyap.FormActivate(Sender: TObject);
begin
  edit_modele.Text := s_ecri_model(x_modele);
  edit_nb_cyc.Text := IntToStr(nb_cyc);
  edit_tlag.Text   := IntToStr(tlag);
  AutoSize := False;
end;


procedure tform_lyap.run_lyap(m,nb_cyc,tlag : integer);
{ compute the Lyapunov expopnent of model m over nb_cyc time steps }
{ results displayed every tlag time steps }
{ the method is to measure the divergence between 2 population vectors }
{ v and v_eps that initially differ by a small amount }
const eps = 0.00001;
label 1;
var i,j,icyc,ms : integer;
    lyap,lyapunov,d0,dd : extended;
    v,v_eps,dv : rvec_type;
    aaa : rmat_type;

procedure maj_var;
{ update all variables }
var e : integer;
begin
  for e := 1 to ordre_eval_nb do with variable[ordre_eval[e]] do
    val := eval(exp,exp_type);
end;

begin
  form_ulm.status_run('Lyapunov ' + IntToStr(nb_cyc));
  ms := clock;
  with modele[m] do
    begin
      lyap := 0.0;
      if ( xmat <> 0 ) then with vec[xvec] do { matrix-type model }
        for i := 1 to size do v_eps[i] := val[i] + eps/size
      else { relation-type model }
        for i := 1 to size do with rel[xrel[i]] do v_eps[i] := val + eps/size;
      d0 := eps;
      for icyc := 1 to nb_cyc do { run nb_cyc time steps }
        begin
          run_t;
          if ( xmat <> 0 ) then { matrix-type model }
            begin
              for i := 1 to size do with vec[xvec] do
                begin
                  v[i] := variable[exp[i]].val;
                  variable[exp[i]].val := v_eps[i];
                end;
              maj_var; { update all variables }
              { update all matrix entries }
              { some entries depend on the population vector }
              for i := 1 to size do
                for j := 1 to size do with mat[xmat] do
                  aaa[i,j] := eval(exp[i,j],exp_type[i,j]);
              matvec(size,aaa,v_eps,v_eps); { update population vector }
            end
          else { relation-type model }
            begin
              for i := 1 to size do with rel[xrel[i]] do
                begin
                  v[i] := variable[xvar].val;
                  variable[xvar].val := v_eps[i];
                end;
              maj_var; { update all variables }
              { update all relations }
              { some relations depend on the population vector }
              for i := 1 to size do with rel[xrel[i]] do
                v_eps[i] := eval(exp,exp_type);
            end;
          for i := 1 to size do dv[i] := v[i] - v_eps[i];
          dd := vecnorm(size,dv); { distance between v and v_esp (euclidian norm) }
          if ( dd = 0.0 ) then dd := d0;
          for i := 1 to size do v_eps[i] := v[i] + d0*dv[i]/dd; { rescale v_esp }
          lyap := lyap + ln0(dd/d0); { use average }
          lyapunov := lyap/icyc; { Lyapunov exponent }
          if ( xmat <> 0 ) then { matrix-type model }
            for i := 1 to size do with vec[xvec] do
              variable[exp[i]].val := v[i]
          else { relation-type model }
            for i := 1 to size do with rel[xrel[i]] do
              variable[xvar].val := v[i];
          maj_var; { update all variables }
          if ( t__ mod tlag = 0 ) then
            if interp_ then
              iwriteln('   ' + IntToStr(t__) + Format('%10.4f',[lyapunov]))
            else
              maj_lyap(t__,lyapunov);
          with form_ulm do { interruption procedure }
            begin
              procproc;
              if runstop then
                begin
                  runstop := false;
                  goto 1;
                end;
              if ( t__ mod 100 = 0 ) then status_time;
            end;
        end;
    end;
1 :
  with form_ulm do
    begin
      status_t_exec(clock - ms);
      status_time;
    end;
end;

function  tform_lyap.check_param : boolean;
{ check the validity of the parameter values entered }
var x,tx,n :integer;
begin
  check_param := false;
  trouve_obj(edit_modele.Text,x,tx);
  if ( x = 0 ) or ( tx <> type_modele ) then
    begin
      erreur('Unknown model name');
      edit_modele.Text := s_ecri_model(x_modele);
      exit;
    end;
  x_modele := x;
  if not est_entier(edit_nb_cyc.Text,n) then
    begin
      erreur('Number of time steps: integer expected');
      edit_nb_cyc.Text := IntToStr(nb_cyc);
      exit;
    end;
  if ( n <= 0 ) then
    begin
      erreur('Number of time steps: positive integer expected');
      edit_nb_cyc.Text := IntToStr(nb_cyc);
      exit;
    end;
  nb_cyc := n;
  if not est_entier(edit_tlag.Text,n) then
    begin
      erreur('Time lag: integer expected');
      edit_tlag.Text := IntToStr(tlag);
      exit;
    end;
  if ( n <= 0 ) then
    begin
      erreur('Time lag: positive integer expected');
      edit_tlag.Text := IntToStr(tlag);
      exit;
    end;
  tlag := n;
  FormActivate(nil);
  check_param := true;
end;

procedure tform_lyap.button_runClick(Sender: TObject);
begin
  if not check_param then exit;
  if ( t__ = 0 ) then stringgrid1.RowCount := 1;
  run_lyap(x_modele,nb_cyc,tlag);
end;

procedure tform_lyap.button_initClick(Sender: TObject);
begin
  with form_ulm do run_initExecute(nil);
  with stringgrid1 do RowCount := 1;
end;

end.
