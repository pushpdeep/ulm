{ ---------------------------------------------------------------------------- }
{ Copyright (c) 1995-2017, Stéphane Legendre and the ULM development team      }
{                                                                              }
{ This file is part of ULM.                                                    }
{                                                                              }
{ ULM is free software: you can redistribute it and/or modify it               }
{ under the terms of the GNU General Public License as published               }
{ by the Free Software Foundation, either version 3 of the License,            }
{ or (at your option) any later version.                                       }
{                                                                              }
{ ULM is distributed in the hope that it will be useful,                       }
{ but WITHOUT ANY WARRANTY; without even the implied warranty                  }
{ of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                      }
{ See the GNU General Public License for more details.                         }
{                                                                              }
{ You should have received a copy of the GNU General Public License            }
{ along with ULM.  If not, see <http://www.gnu.org/licenses/>.                 }
{ ---------------------------------------------------------------------------- }

unit qcarlo;

{  @@@@@@   Monte Carlo   @@@@@@  }

interface

procedure carlo(nb_cycle_carlo,nb_run_carlo: integer;seuil_ext,seuil_div : extended);
procedure gscat(nb_steps : integer);

const  maxvargraph  = 4;        { nombre max de graphes simultanes }
       maxgraph     = 100000;    { nombre max de cycles representables }

type   tab_graph_type = array[0..maxgraph] of extended;

var    dt_graph         : integer;   { intervalle d'echantillonnage en temps }
       t_graph_traj     : integer;   { pas en temps pour graphique }
       vargraph_x       : integer;   { variable en x }
       nb_vargraph_y    : integer;   { nombre de variables en y }
       vargraph_y       : array[1..maxvargraph] of integer; { variables en y }
       valgraph_x       : tab_graph_type; { tableau des valeurs x }
       valgraph_y       : array[1..maxvargraph] of tab_graph_type; { tableau des valeurs y }
       gscatter         : boolean;  { indicateur scatter }
       regress_a        : extended; { coeff a droite de regression a*x + b }
       regress_b        : extended; { coeff b droite de regression }

implementation

uses  qglobvar,qmath,qsyntax,qeval,qrun,qinterp;

const maxtexte_interp = 100;

type  tab_texte_interp_type = array[0..maxtexte_interp] of extended;

var   ext,dvg,ext_time,div_time : array[1..modele_nb_max] of integer;
      ext_time_moy,ext_time_var,div_time_moy,div_time_var,
      nsom,nsom2,nmoy,nmoy2,nmoy_ne,nvar_ne,nmax_ne,nmin_ne,
      lamb_moy,lamb_var,lnlamb_moy,lnlamb_var,lamb2_moy,lamb2_var : array[1..modele_nb_max] of extended;
      fini,diverg : array[1..modele_nb_max] of boolean;
      wmoy : array[1..modele_nb_max] of rvec_type;
      ext_t : array[1..modele_nb_max] of array[0..maxtexte_interp] of integer;
      nt_moy,nt_var,
      nt_moy_ne,nt_var_ne : array[1..modele_nb_max] of tab_texte_interp_type;

const maxtext = 10000;

type  tab_text_type = array[0..maxtext] of extended;

var   dt_text : integer; { intervalle d'echantillonnage en temps }
      t_text  : integer; { pas en temps pour text }
      valtext_moy,valtext_var,
      valtext_moy_ne,valtext_var_ne : array[1..maxvartexte_interp] of tab_text_type;
      varmoy,varvar,varmoy_ne,varvar_ne : array[1..maxvartexte_interp] of extended;
      ext_tt : array[0..maxtext] of integer; { pour calcul proba ext }

procedure gscat(nb_steps : integer);
{ scatter plot + regress line }
{ run : serie des valeurs }
{ montecarlo : un point pour la derniere valeur de chaque trajectoire }
var i,k : integer;
    tabx,taby : vecvec_type;
    a,b : extended;
begin
  SetLength(tabx,nb_steps);
  SetLength(taby,nb_steps);
  for k := 1 to nb_vargraph_y do
    begin
      for i := 1 to nb_steps do
        begin
          tabx[i-1] := valgraph_x[i];
          taby[i-1] := valgraph_y[k][i];
        end;
      coeff_regress(nb_steps,tabx,taby,a,b);
      regress_a := a;
      regress_b := b;
    end;
end;

procedure carlo(nb_cycle_carlo,nb_run_carlo: integer;seuil_ext,seuil_div : extended);
label 1;
var irun,icyc,t_texte_interp,delta_texte_interp : integer;

{ ------  "graphique" utilise pour scatter  ------ }

procedure run_graph_scatter;
var k : integer;
begin
  if ( irun <= maxgraph ) then
    begin
      valgraph_x[irun] := variable[vargraph_x].val;
      for k := 1 to nb_vargraph_y do
        valgraph_y[k][irun] := variable[vargraph_y[k]].val;
    end;
end;

procedure carlo_graph_scatter;
begin
  if ( icyc = nb_cycle_carlo ) then run_graph_scatter;
end;

procedure carlo_graph(icyc : integer);
begin
  if gscatter then carlo_graph_scatter;
end;

procedure carlo_fin_graph_scatter;
begin
  gscat(imin(nb_run_carlo,maxgraph));
end;

procedure carlo_fin_graph;
begin
  if gscatter then carlo_fin_graph_scatter
end;

{ ------  texte fic  ------ }

procedure carlo_fic(irun : integer);
var i,k : integer;
begin
  for i := 1 to fic_nb do with fic[i] do
    begin
      write(f,irun:6);
      for k := 1 to var_fic_nb do with variable[var_fic[k]] do
        write(f,' ',val:1:precis[k]);
      writeln(f);
      Flush(f);
    end;
end;

{ ------  texte interp  ------ }

procedure init_delta_texte_interp;
begin
  if ( nb_cycle_carlo <= 50 )  then 
    delta_texte_interp := 1
  else 
  if ( nb_cycle_carlo <= 500 ) then 
    delta_texte_interp := 10
  else
  if ( nb_cycle_carlo <= 5000 ) then 
    delta_texte_interp := 100
  else 
  if ( nb_cycle_carlo <= 50000 ) then 
    delta_texte_interp := 1000
  else
    delta_texte_interp := nb_cycle_carlo div maxtexte_interp;
end;

procedure carlo_init_texte_interp2;
var x,i : integer;
begin
  for x := 1 to modele_nb do with modele[x] do
    for i := 0 to maxtexte_interp do
      begin
        ext_t[x][i]     := 0;
        nt_moy[x][i]    := 0.0;
        nt_var[x][i]    := 0.0;
        nt_moy_ne[x][i] := 0.0;
        nt_var_ne[x][i] := 0.0;
      end;
  init_delta_texte_interp;
end;

procedure carlo_run_init_texte_interp2;
begin
  t_texte_interp := 0;
end;

{procedure carlo_ext_text_interp(x : integer);
begin
  if texte_interp then
    writeln('run ',irun:1,' model ',s_ecri_model(x),' ext time = ',icyc:1);
end;

procedure carlo_div_text_interp(x : integer);
begin
  if texte_interp then
    writeln('run ',irun:1,' model ',s_ecri_model(x),' div time = ',icyc:1);
end;}

procedure carlo_texte_interp2(icyc : integer);
var x,j : integer;
    a : extended;
begin
  if ( icyc mod delta_texte_interp = 0 ) then
    begin
      t_texte_interp := t_texte_interp + 1;
      j := t_texte_interp;
      for x := 1 to modele_nb do with modele[x] do
        begin
          a := pop;
          nt_moy[x][j] := nt_moy[x][j] + a;
          nt_var[x][j] := nt_var[x][j] + a*a;
          if fini[x] then
            ext_t[x][j] := ext_t[x][j] + 1
          else
            begin
              nt_moy_ne[x][j] := nt_moy_ne[x][j] + a;
              nt_var_ne[x][j] := nt_var_ne[x][j] + a*a;
            end;
        end;
    end;
end;

procedure carlo_fin_texte_interp2(x : integer);
var it,icyc,n_ext : integer;
    a,b,pe : extended;
begin
  it := 0;
  writeln('t':5,'pe(t)':10,'pop(t)':10,'SE':10,'pop*(t)':10,'SE':10);
  for icyc := 1 to nb_cycle_carlo do
    if ( icyc mod delta_texte_interp = 0 ) then
      begin
        it := it + 1;
        n_ext := nb_run_carlo - ext_t[x][it];
        pe := ext_t[x][it]/nb_run_carlo;
        a  := nt_moy[x][it]/nb_run_carlo;
        write(icyc:5,pe:10:4,' ',a:9:1);
        b  := nt_var[x][it]/nb_run_carlo - a*a;
        if ( b >= 0.0 ) then
          write(' ',sqrt(b/nb_run_carlo):9:1)
        else
          write('-':10);
        if ( n_ext > 0 ) then
          begin
            a := nt_moy_ne[x][it]/n_ext;
            write(a:10:1);
            b := nt_var_ne[x][it]/n_ext - a*a;
            if ( b >= 0.0 ) then
              write(' ',sqrt(b/n_ext):9:1)
            else
              write('-':10);
          end
        else
          write('-':10,'-':10);
        writeln;
      end;
end;

{ ------  text ------ }

procedure carlo_init_texte_interp;
var k : integer;
begin
  for k := 1 to nb_vartexte_interp do
    begin
      varmoy[k]    := 0.0;
      varvar[k]    := 0.0;
      varmoy_ne[k] := 0.0;
      varvar_ne[k] := 0.0;
    end;
end;

procedure carlo_init_text;
var k,i : integer;
begin
  carlo_init_texte_interp;
  dt_text := delta_texte_interp;
  for i := 1 to maxtext do ext_tt[i] := 0;
  for k := 1 to nb_vartexte_interp do
    for i := 0 to maxtext do
      begin
        valtext_moy[k][i]    := 0.0;
        valtext_var[k][i]    := 0.0;
        valtext_moy_ne[k][i] := 0.0;
        valtext_var_ne[k][i] := 0.0;
      end;
  for k := 1 to nb_vartexte_interp do with variable[vartexte_interp[k]] do
    begin
      valtext_moy[k][0]    := val;
      valtext_moy_ne[k][0] := val;
    end;
  write('t':5,'pe(t)':10);
  for k := 1 to nb_vartexte_interp do with variable[vartexte_interp[k]] do
    write(s_ecri_dic(nom):10,'SE':10,s_ecri_dic(nom)+'*':10,'SE':10);
  writeln;
  write(0:5,0.0:10:1);
  for k := 1 to nb_vartexte_interp do with variable[vartexte_interp[k]] do
    begin
      xwrite9(val);
      write('-':10);
      xwrite9(val);
      write('-':10);
    end;
  writeln;
end;

procedure carlo_texte_interp;
var k,x : integer;
    a : extended;
begin
  for k := 1 to nb_vartexte_interp do
    begin
      a := variable[vartexte_interp[k]].val;
      varmoy[k] := varmoy[k] + a;
      varvar[k] := varvar[k] + a*a;
      x := 1; {jjjjj}
      if not fini[x] then
        begin
          varmoy_ne[k] := varmoy_ne[k] + a;
          varvar_ne[k] := varvar_ne[k] + a*a;
        end;
    end;
end;

procedure carlo_text(icyc : integer);
var k,x,j : integer;
    a : extended;
begin
  if ( t_text >= maxtext ) then exit;
  if ( icyc mod dt_text  = 0 ) then
    begin
      t_text := t_text + 1;
      j := t_text;
      for k := 1 to nb_vartexte_interp do
        begin
          a := variable[vartexte_interp[k]].val;
          valtext_moy[k][j] := valtext_moy[k][j] + a;
          valtext_var[k][j] := valtext_var[k][j] + a*a;
        end;
      x := 1; {ccccc}
      if fini[x] then
        ext_tt[j] := ext_tt[j] + 1
      else
        for k := 1 to nb_vartexte_interp do
          begin
            a := variable[vartexte_interp[k]].val;
            valtext_moy_ne[k][j] := valtext_moy_ne[k][j] + a;
            valtext_var_ne[k][j] := valtext_var_ne[k][j] + a*a;
          end;
    end;
end;

procedure carlo_run_init_text;
begin
  t_text := 0;
end;

procedure carlo_fin_texte_interp;
var k,x,n_ext : integer;
    a,b : extended;
begin
  writeln('At t = ',nb_cycle_carlo:1,':');
  writeln('Mean value [SE]:');
  for k := 1 to nb_vartexte_interp do
    begin
      a := varmoy[k]/nb_run_carlo;
      b := varvar[k]/nb_run_carlo - a*a;
      if ( b >= 0.0 ) then
        writeln('  ',s_ecri_var(vartexte_interp[k]),' = ',a:1:4,' [',
                sqrt(b/nb_run_carlo):1:4,']')
      else
        writeln('  ',s_ecri_var(vartexte_interp[k]),' = ',a:1:4,' [-]')
    end;
  writeln('Mean value over non extinct trajectories [SE]:');
  for k := 1 to nb_vartexte_interp do
    begin
      x := 1; {jjjjj}
      n_ext := nb_run_carlo - ext[x];
      if ( n_ext > 0.0 ) then
        begin
          a := varmoy_ne[k]/n_ext;
          b := varvar_ne[k]/n_ext - a*a;

          if ( b >= 0.0 ) then
            writeln('  ',s_ecri_var(vartexte_interp[k]),'* = ',a:1:4,' [',
                    sqrt(b/n_ext):1:4,']')
          else
            writeln('  ',s_ecri_var(vartexte_interp[k]),'* = ',a:1:4,' [-]');
        end;
    end;
end;

procedure carlo_fin_text;
var k,i,it,n_ext : integer;
    a,b,pe : extended;
begin
  it := 0;
  for i := 1 to nb_cycle_carlo do
    if ( i mod dt_text = 0 ) then
      begin
        if ( it >= maxtext ) then break;
        it := it + 1;
        n_ext := nb_run_carlo - ext_tt[it];
        pe := ext_tt[it]/nb_run_carlo;
        write(it*dt_text:5,pe:10:4);
        for k := 1 to nb_vartexte_interp do with variable[vartexte_interp[k]] do
          begin
            a := valtext_moy[k][it]/nb_run_carlo;
            valtext_moy[k][it] := a;
            xwrite9(a);
            b := valtext_var[k][it]/nb_run_carlo - a*a;
            if ( b >= 0.0 ) then
              begin
                b := sqrt(b/nb_run_carlo);
                valtext_var[k][it] := b;
                xwrite9(b);
              end
            else
              write('-':10);
            if ( n_ext > 0 ) then
              begin
                a := valtext_moy_ne[k][it]/n_ext;
                valtext_moy_ne[k][it] := a;
                xwrite9(a);
                b := valtext_var_ne[k][it]/n_ext - a*a;
                if ( b >= 0.0 ) then
                  begin
                    b := sqrt(b/n_ext);
                    valtext_var_ne[k][it] := b;
                    xwrite9(b);
                  end
                else
                  write('-':10);
              end
            else
              write('-':10,'-':10);
          end;
      writeln;
    end;
  carlo_fin_texte_interp;
end;

{ ------ procedure MonteCarlo ------ }

procedure test_extinct;
var x: integer;
begin
  for x := 1 to modele_nb do with modele[x] do
    if not fini[x] then
      begin 
        nsom[x] := nsom[x] + pop; 
        if ( pop < seuil_ext ) or ( pop = 0.0 ) then { extinction }
          begin 
            fini[x] := true;
            ext[x]  := ext[x] + 1;
            ext_time[x] := icyc;
            {carlo_ext_text_interp(x); }
          end
        else 
          if ( not diverg[x] and ( pop > seuil_div )) then { divergence }
            begin 
              diverg[x] := true; 
              dvg[x]    := dvg[x] + 1;
              div_time[x] := icyc;
              {carlo_div_text_interp(x); }
            end;
      end;
end;

procedure carlo_run;
var x,i : integer;
    a : extended;
begin
  carlo_texte_interp;
  for x := 1 to modele_nb do with modele[x] do
    begin 
      nmoy[x] := nmoy[x] + pop; 
      a := (ln0(pop) - ln0(pop0))/nb_cycle_carlo;
      lnlamb_moy[x] := lnlamb_moy[x] + a;
      lnlamb_var[x] := lnlamb_var[x] + a*a;
      a := exp(a);
      lamb_moy[x] := lamb_moy[x] + a; 
      lamb_var[x] := lamb_var[x] + a*a;
      if not fini[x] then
        begin 
          nmoy2[x] := nmoy2[x] + pop; 
          nsom2[x] := nsom2[x] + nsom[x]; 
          nmoy_ne[x] := nmoy_ne[x] + pop; 
          nvar_ne[x] := nvar_ne[x] + pop*pop; 
          nmax_ne[x] := max(nmax_ne[x],pop); 
          nmin_ne[x] := min(nmin_ne[x],pop); 
          if ( nsom[x] = pop ) then 
            a := 0.0 
          else 
            a := (nsom[x] - pop0)/(nsom[x] - pop);
          lamb2_moy[x] := lamb2_moy[x] + a; 
          lamb2_var[x] := lamb2_var[x] + a*a; 
        end
      else 
        begin 
          a := ext_time[x];
          ext_time_moy[x] := ext_time_moy[x] + a;
          ext_time_var[x] := ext_time_var[x] + a*a;
        end; 
      if diverg[x] then 
        begin 
          a := div_time[x];
          div_time_moy[x] := div_time_moy[x] + a; 
          div_time_var[x] := div_time_var[x] + a*a; 
        end; 
      if not fini[x] then
        if ( xvec <> 0 ) then
          for i := 1 to size do
            wmoy[x][i] := wmoy[x][i] + vec[xvec].val[i]/pop
        else
          for i := 1 to size do
            wmoy[x][i] := wmoy[x][i] + rel[xrel[i]].val/pop;
    end;
end;

procedure fin_carlo;
var x,i : integer;
    a,b,n_ext : extended;
begin
  for x := 1 to modele_nb do with modele[x] do
    begin
      n_ext := nb_run_carlo - ext[x];
      writeln('Model ',s_ecri_model(x),' (extinction threshold = ',seuil_ext:1:2,
                                       '; escape threshold = ',seuil_div:1:2,')');
      if ( n_ext > 0.0 ) then
        begin
          writeln('  Non extinct population size (pop*):');
          writeln('    min  = ',nmin_ne[x]:1:2);
          writeln('    max  = ',nmax_ne[x]:1:2);
          a := nmoy_ne[x]/n_ext;
          writeln('    mean = ',a:1:2);
          b := nvar_ne[x]/n_ext - a*a;
          if ( b >= 0.0 ) then 
            begin
              writeln('      sigma = ',sqrt(b):1:4);
              writeln('      SE = ',sqrt(b/n_ext):1:4);
            end; 
        end;
      if gscatter then
        writeln('  Regression: slope = ', regress_a:1:4,
                ' intercept = ', regress_b:1:4);
      writeln('  Probability of escape: ',dvg[x]/nb_run_carlo:1:4);
      if ( dvg[x] > 0.0 ) then 
        begin
          a := div_time_moy[x]/nb_run_carlo;
          b := div_time_var[x]/nb_run_carlo - a*a;
          if ( b >= 0.0 ) then
            writeln('  Mean escape time [SE]: ',a:1:4,sqrt(b/nb_run_carlo):1:4)
          else
            writeln('  Mean escape time [SE]: ',a:1:4,' [-]');
          a := div_time_moy[x]/dvg[x];
          b := div_time_var[x]/dvg[x] - a*a;
          if ( b >= 0.0 ) then
            writeln('  Mean escape time over escape trajectories [SE]: ',
                    a:1:4,' [',sqrt(b/nb_run_carlo):1:4,']')
          else
            writeln('  Mean escape time over escape trajectories [SE]: ',a:1:4,' [-]');
          end;
      writeln('  Probability of extinction: ',ext[x]/nb_run_carlo:1:4);
      if ( ext[x] > 0.0 ) then
        begin
          a := ext_time_moy[x]/nb_run_carlo;
          b := ext_time_var[x]/nb_run_carlo - a*a;
          if ( b >= 0.0 ) then
            writeln('  Mean extinction time [SE]: ',a:1:4,' [',sqrt(b/nb_run_carlo):1:4,']')
          else
            writeln('  Mean extinction time [SE]: ',a:1:4,' [-]');
          a := ext_time_moy[x]/ext[x];
          b := ext_time_var[x]/ext[x] - a*a;
          if ( b >= 0.0 ) then
            writeln('  Mean extinction time over extinct trajectories [SE]: ',
                    a:1:4,' [',sqrt(b/nb_run_carlo):1:4,']')
          else
            writeln('  Mean extinction time over extinct trajectories [SE]: ',a:1:4,' [-]');
        end;
      a := lnlamb_moy[x]/nb_run_carlo;
      b := lnlamb_var[x]/nb_run_carlo - a*a;
      writeln('  Stochastic growth rate: ',exp(a):1:6);
      if ( b >= 0.0 ) then
        writeln('      Logarithmic growth rate [SE]: ',a:1:6,' [',sqrt(b/nb_run_carlo):1:6,']')
      else
        writeln('      Logarithmic growth rate [SE]: ',a:1:6,' [-]');
      a := lamb_moy[x]/nb_run_carlo;
      b := lamb_var[x]/nb_run_carlo - a*a;
      if ( b >= 0.0 ) then
        writeln('  Mean growth rate [SE]: ',a:1:6,' [',sqrt(b/nb_run_carlo):1:6,']')
      else
        writeln('  Mean growth rate [SE]: ',a:1:6,' [-]');
      a := exp((ln0(nmoy[x]/nb_run_carlo) - ln0(pop0))/nb_cycle_carlo);
      writeln('  Growth rate of the mean pop: ',a:1:6);
      if ( n_ext > 0.0 ) then
        begin 
          a := lamb2_moy[x]/n_ext;
          b := lamb2_var[x]/n_ext - a*a;
          if ( b >= 0.0 ) then
            writeln('  Mean growth rate2 [SE]: ',a:1:6,' [',sqrt(b/n_ext):1:6,']')
          else
            writeln('  Mean growth rate2 [SE]: ',a:1:6,' [-]');
          if ( (nsom2[x] - nmoy2[x]) > 0.0 ) then 
            begin 
              a := (nsom2[x] - n_ext*pop0)/(nsom2[x] - nmoy2[x]);
              writeln('  Growth rate2 of the mean pop: ',a:1:6);
            end; 
        end; 
      if ( n_ext > 0.0 ) then
        begin
          for i := 1 to size do wmoy[x][i] := wmoy[x][i]/n_ext;
          writeln('  Mean scaled population structure:');
          for i := 1 to size do write(wmoy[x][i]:8:2);
          writeln;
        end;
      carlo_fin_texte_interp2(x);
    end;
end;

procedure init_traj;
var x: integer;
begin
  for x := 1 to modele_nb do
    begin
      fini[x]   := false;
      diverg[x] := false;
      nsom[x]   := modele[x].pop0;
    end;
end;

procedure init_carlo;
var x,i : integer;
begin
  for x := 1 to modele_nb do with modele[x] do
    begin
      ext[x] := 0;
      dvg[x] := 0;
      ext_time_moy[x] := 0.0; 
      ext_time_var[x] := 0.0; 
      div_time_moy[x] := 0.0;
      div_time_var[x] := 0.0;
      nmoy[x]  := 0.0; 
      nmoy2[x] := 0.0; 
      nsom2[x] := 0.0; 
      nmoy_ne[x]    := 0.0;
      nvar_ne[x]    := 0.0;
      nmax_ne[x]    := 0.0;
      nmin_ne[x]    := maxextended;
      lamb_moy[x]   := 0.0;
      lamb_var[x]   := 0.0;
      lnlamb_moy[x] := 0.0;
      lnlamb_var[x] := 0.0;
      lamb2_moy[x]  := 0.0;
      lamb2_var[x]  := 0.0;
      for i := 1 to size do wmoy[x][i] := 0.0;
    end;
end;

begin
  init_eval1;
  init_carlo;
  carlo_init_texte_interp2;
  carlo_init_text;
  for irun := 1 to nb_run_carlo do
    begin
      graine := graine0 + irun-1;
      init_eval1;
      init_traj;
      carlo_run_init_texte_interp2;
      carlo_run_init_text;
      for icyc := 1 to nb_cycle_carlo do
        begin
          run_t;
          test_extinct;
          carlo_graph(icyc);
          carlo_text(icyc);
          carlo_texte_interp2(icyc);
        end;
      carlo_run;
      carlo_fic(irun);
    end;
  carlo_fin_graph;
  carlo_fin_text;
  fin_carlo;
1:
  graine := graine0;
  init_eval1;
  writeln('Init');
end;

end.
