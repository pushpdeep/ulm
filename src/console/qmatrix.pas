{ ---------------------------------------------------------------------------- }
{ Copyright (c) 1995-2017, Stéphane Legendre and the ULM development team      }
{                                                                              }
{ This file is part of ULM.                                                    }
{                                                                              }
{ ULM is free software: you can redistribute it and/or modify it               }
{ under the terms of the GNU General Public License as published               }
{ by the Free Software Foundation, either version 3 of the License,            }
{ or (at your option) any later version.                                       }
{                                                                              }
{ ULM is distributed in the hope that it will be useful,                       }
{ but WITHOUT ANY WARRANTY; without even the implied warranty                  }
{ of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                      }
{ See the GNU General Public License for more details.                         }
{                                                                              }
{ You should have received a copy of the GNU General Public License            }
{ along with ULM.  If not, see <http://www.gnu.org/licenses/>.                 }
{ ---------------------------------------------------------------------------- }

unit qmatrix;

interface

uses qglobvar,qmath;

function  mattimedep(x : integer) : boolean;
function  matvecdep(x,y : integer) : boolean;
function  matrandom(x : integer): boolean;
function  matleslie(n : integer;a : rmat_type ) : boolean;
function  matleslie2(n : integer;a : rmat_type ) : boolean;
function  matclassetaille(n : integer;a : rmat_type ) : boolean;
function  matmultisite(n : integer;a : rmat_type;var p : integer) : boolean;
function  test_matmultisite(n,p : integer;a : rmat_type) : boolean;
procedure proprietes(x : integer);
procedure sensibilites(m,x : integer);
procedure stoc_sensib(m,x,nb_cyc : integer);
procedure run_lyap(m,nb_cyc,tlag : integer);

implementation

uses qsymb,qsyntax,qeval;

var lambda : cvec_type;  { valeurs propres complexes }
    lambda1 : extended;  { valeur propre dominante }
    r : extended;        { taux d'accroissement }
    v,w : rvec_type;     { vecteurs propres gauche, droite }
    leslie   : boolean;  { indicateur matrice Leslie }
    leslie2  : boolean;  { indicateur matrice extended Leslie }
    multi    : boolean;  { indicateur matrice multisite }
    sizeclass : boolean; { indicateur matrice classe taille }
    x_mat : integer;     { matrice }
    psite : integer;     { nombre de sites si multi }
    mp : rmat_type;      // markov matrices
    matf,mats,matn : rmat_type; // A = S + F, N = (I - S)^{-1}
    vp : rvec_type;      // left eigenvector  mp

procedure erreur_matrix(s : string);
begin
  writeln('Error: Matrix - ',s);
end;

{ ------ proprietes des matrices ------ }

function  matleslie(n : integer;a : rmat_type ) : boolean;
{  a est une matrice de leslie ? }
var i,j : integer;
    b : boolean;
begin
  b := true;
  for j := 1 to n do b := b and ( a[1,j] >= 0.0 );
  for i := 2 to n do b := b and ( a[i,i-1] > 0.0 ) and ( a[i,i-1] <= 1.0 );
  for i := 2 to n do
    for j := 1 to n do
      if ( j <> i-1 ) then b := b and ( a[i,j] = 0.0 );
  matleslie := b;
end;

function  matleslie2(n : integer;a : rmat_type ) : boolean;
{ a est une matrice de leslie etendue ( 0 < a[n,n] < 1 ) ? }
var i,j : integer;
    b : boolean;
begin
  b := true;
  for j := 1 to n do b := b and ( a[1,j] >= 0.0 );
  for i := 2 to n do b := b and ( a[i,i-1] > 0.0 ) and ( a[i,i-1] <= 1.0 );
  for i := 2 to n-1 do
    for j := i to n do
      b := b and ( a[i,j] = 0.0 );
  for j := 1 to n-2 do
    for i := j+2 to n do
      b := b and ( a[i,j] = 0.0 );
  b := b and ( a[n,n] > 0.0  ) and ( a[n,n] < 1.0 );
  matleslie2 := b;
end;

function  matclassetaille(n : integer;a : rmat_type ) : boolean;
{ a est une matrice de modele en classes de taille ? }
var i,j : integer;
    b : boolean;
begin
  b := true;
  for j := 1 to n do b := b and ( a[1,j] >= 0.0 );
  for i := 2 to n do b := b and ( a[i,i] > 0.0 ) and ( a[i,i] < 1.0 );
  for i := 2 to n do b := b and ( a[i,i-1] > 0.0 ) and ( a[i,i-1] <= 1.0 );
  for i := 2 to n-1 do
    for j := i+1 to n do
      b := b and ( a[i,j] = 0.0 );
  for i := 3 to n do
    for j := 1 to i-2 do
      b := b and ( a[i,j] = 0.0 );
  matclassetaille := b;
end;

function  test_matmultisite(n,p : integer;a : rmat_type) : boolean;
{ teste si a est une matrice multisite dont les blocs sont leslie ou leslie2 }
{ avec p = nombre de sites  }
var  i,j,m,ig,jg,k : integer;
     b,b1,b2 : boolean;
     g : rmat_type;
begin
  m := n div p; { nb de classes d'age des sous-populations }
  b := true;
  b1 := true;
  b2 := true;
  { test que les blocs diagonaux sont tous leslie ou leslie2: }
  for k := 1 to p do
    begin
      ig := 0;
      for i := (k-1)*m + 1 to k*m do
        begin
          ig := ig + 1;
          jg := 0;
          for j := (k-1)*m + 1 to k*m do
            begin
              jg := jg + 1;
              g[ig,jg] := a[i,j];
            end;
        end;
      b1 := b1 and matleslie(m,g);
      b2 := b2 and matleslie2(m,g);
    end;
  { test que les blocs non diagonaux ne contiennent que des termes entre 0 et 1: }
  b := b and (b1 or b2);
  for k := 1 to p do
    for i := (k-1)*m + 1 to k*m do
      for j := k*m + 1 to n do
        begin
          b := b and ( a[i,j] >= 0.0 ) and ( a[i,j] <= 1.0 );
          b := b and ( a[j,i] >= 0.0 ) and ( a[j,i] <= 1.0 );
        end;
  test_matmultisite := b;
end;

function  matmultisite(n : integer;a : rmat_type;var p : integer) : boolean;
{ a est une matrice multisite dont les blocs sont leslie ou leslie2 }
{ retourne p = nombre de sites }
{ correction 10/04/2002 }
{ correction 05/07/2004 }
label 1;
var q : integer;
begin
  { chercher p diviseur de n }
  p := 1;
  matmultisite := false;
  q := 1;
1:
  q := q + 1;
  {if ( q > round(sqrt(n) + 1 ) then exit;}
  if ( n div q < 2 ) then exit;
  if ( n mod q <> 0 ) then goto 1;
  if test_matmultisite(n,q,a) then
    begin
      p := q;
      matmultisite := true;
      exit;
    end;
  goto 1;
end;

function  mattimedep(x : integer) : boolean;
{ la matrice d'indice x est une matrice time-dependent ? }
var i,j : integer;
    b : boolean;
begin
  b := false;
  with mat[x] do
  for i := 1 to size do
    for j := 1 to size do
      b := b or occur2(exp[i,j],exp_type[i,j],xtime);
  mattimedep := b;
end;

function  matvecdep(x,y : integer) : boolean;
{ la matrice d'indice x est une matrice vector-dependent ( density-dependent ) ? }
var i,j,k : integer;
    b : boolean;
begin
  b := false;
  with mat[x] do
  for i := 1 to size do
    for j := 1 to size do 
      for k := 1 to size do 
        b := b or occur2(exp[i,j],exp_type[i,j],vec[y].exp[k]);
  matvecdep := b;
end;

function  matrandom(x : integer): boolean;
{ la matrice d'indice x est une matrice dependant de fonctions aleatoires ? }
var i,j,z,tz : integer;
    b : boolean;
begin
  b := false;
  with mat[x] do
  for i := 1 to size do
    for j := 1 to size do
      begin 
        z  := exp[i,j];
        tz := exp_type[i,j];
        b  := b or occur_op1(z,tz,op1_rand);
        b  := b or occur_op1(z,tz,op1_gauss);
        b  := b or occur_op1(z,tz,op1_ber);
        b  := b or occur_op1(z,tz,op1_poisson);
        b  := b or occur_op1(z,tz,op1_geom);
        b  := b or occur_op1(z,tz,op1_expo);
        b  := b or occur_op1(z,tz,op1_gamm);
        b  := b or occur_fun(z,tz,fun_betaf);
        b  := b or occur_fun(z,tz,fun_beta1f);
        b  := b or occur_fun(z,tz,fun_binomf);
        b  := b or occur_fun(z,tz,fun_nbinomf);
        b  := b or occur_fun(z,tz,fun_nbinom1f);
        b  := b or occur_fun(z,tz,fun_gaussf);
        b  := b or occur_fun(z,tz,fun_lognormf);
        b  := b or occur_fun(z,tz,fun_tabf);
      end;
  matrandom := b;
end;

{ ------ valeurs propres, vecteurs propres ------ }

procedure dem_leslie(x : integer);
var i,j : integer;
    f,p,phi,pp : rvec_type;
    a,q,r0,tt,tc,tb,s,e,h,fi,sigma2,s1,s2,s3,kappa,gamma : extended;
begin
  with mat[x] do
    begin
      for j := 1 to size   do f[j] := val[1,j]; { fertilities }
      for i := 1 to size-1 do p[i] := val[i+1,i]; { survival probabilities }
      a := 1.0;
      q := 1.0;
      for i := 1 to size do
        begin
          if ( i >= 2 ) then q := q*p[i-1];
          phi[i] := f[i]*q; { maternity functions }
          a := a/lambda1;
          pp[i] := phi[i]*a;
        end;
      r0 := 0.0;
      tb := 0.0;
      tc := 0.0;
      s  := 0.0;
      e  := 0.0;
      for i := 1 to size do
        begin
          r0 := r0 + phi[i];   { net reproductive rate }
          tc := tc + i*phi[i]; { cohort generation time }
          tb := tb + i*pp[i];  { mean generation length }
          s  := s + pp[i]*ln0(pp[i]); { demographic entropy }
          e  := e + pp[i]*ln0(phi[i]); { demographic uncertainty }
        end;
      tc := tc/r0;
      s := -s;
      h  := s/tb;
      fi := e/tb;
      s1 := 0.0;
      s2 := 0.0;
      s3 := 0.0;
      for i := 1 to size do
        begin
          q  := -i*fi + ln0(phi[i]);
          s1 := s1 + i*pp[i]*q;
          s2 := s2 + pp[i]*q*q;
          s3 := s3 + pp[i]*q*q*q;
        end;
      sigma2 := s2/tb;
      kappa  := s3 - 3.0*s3*s1/tb + 2.0*s2;
      gamma  := kappa + 2.0*sigma2;
      if ( r <> 0.0 ) then tt := ln(r0)/r else tt := 0.0; { T }
      writeln('Net reproductive rate R0 = ',r0:1:4);
      writeln('Generation time T = ',tt:1:4);
      writeln('Cohort generation time Tc = ',tc:1:4);
      writeln('Mean generation length Tbar = ',tb:1:4);
      writeln('Demographic entropy S = ',s:1:4);
      writeln('Demographic uncertainty E = ',e:1:4);
      writeln('Entropy rate H = ',h:1:4);
      writeln('Reproductive potential Phi = ',fi:1:4);
      writeln('Entropic variance Sigma2 = ',sigma2:1:4);
      writeln('Correlation index Gamma = ' ,gamma:1:4);
    end;
end;

procedure dem_leslie2(x : integer);
{ on suppose survie adulte 0 < a(n,n) < 1 }
const eps = 0.00001;
var i,j,n : integer;
    f,p,phi,pp : rvec_type;
    a,b,d,q,r0,tt,tc,tb,s,e,h,fi,sigma2,s1,s2,s3,d2,d3,kappa,gamma : extended;
begin
  with mat[x] do
    begin
      n := size;
      for j := 1 to n   do f[j] := val[1,j]; { fertilities }
      for i := 1 to n-1 do p[i] := val[i+1,i];
      p[n] := val[n,n]; { survival probabilities }
      a := 1.0;
      q := 1.0;
      for i := 1 to n do
        begin
          if ( i >= 2 ) then q := q*p[i-1];
          phi[i] := f[i]*q; { net fertility functions }
          a := a/lambda1;
          pp[i] := phi[i]*a;
        end;;
      r0 := 0.0;
      tc := 0.0;
      tb := 0.0;
      s  := 0.0;
      e  := 0.0;
      for i := 1 to n-1 do
        begin
          r0 := r0 + phi[i];
          tc := tc + i*phi[i];
          tb := tb + i*pp[i];  { mean generation length }
          s  := s + pp[i]*ln0(pp[i]);  { demographic entropy }
          e  := e + pp[i]*ln0(phi[i]); { demographic uncertainty }
        end;
      r0 := r0 + phi[n]/(1.0 - p[n]); { net reproductive rate }
      tc := tc + phi[n]*(n - (n-1)*p[n])/sqr(1 - p[n]);
      tc := tc/r0; { mean age of the mothers }
      q := p[n]/lambda1;
      {if ( lambda1 > p[n] ) then}
        begin
          tb := tb + pp[n]*(n - (n-1)*q)/sqr(1.0 - q);  { mean generation length }
          s  := s + pp[n]*(ln0(pp[n])/(1.0 - q) + q*ln0(q)/sqr(1.0 - q));
          e  := e + pp[n]*(ln0(phi[n])/(1.0 - q) + q*ln0(p[n])/sqr(1.0 - q));
        end;
      s := -s;
      h := s/tb;
      fi := e/tb;
      s1 := 0.0;
      s2 := 0.0;
      s3 := 0.0;
      for i := 1 to n do
        begin
          q := -i*fi + ln0(phi[i]);
          s1 := s1 + i*pp[i]*q;
          s2 := s2 + pp[i]*q*q;
          s3 := s3 + pp[i]*q*q*q;
        end;
      a := -n*fi + ln0(phi[n]);
      b := pp[n];
      i := 0;
      repeat
        i := i + 1;
        q := a + i*ln0(p[n]- fi);
        b := b*p[n]/lambda1;
        s1 := s1 + (n + i)*b*q;
        d2 := s2;
        s2 := s2 + b*q*q;
        d3 := s3;
        s3 := s3 + b*q*q*q;
        d := max(abs(s2 - d2),abs(s3 - d3));
      until ( i > 1000 ) or ( d < eps );
      sigma2 := s2/tb;
      kappa  := s3 - 3.0*s3*s1/tb + 2.0*s2;
      gamma  := kappa + 2.0*sigma2;
      if ( r <> 0.0 ) then tt := ln(r0)/r else tt := 0.0;
      writeln('Net reproductive rate R0 = ',r0:1:4);
      writeln('Generation time T = ',tt:1:4);
      writeln('Cohort generation time Tc = ',tc:1:4);
      writeln('Mean generation length Tbar = ',tb:1:4);
      writeln('Demographic entropy S = ',s:1:4);
      writeln('Demographic uncertainty E = ',e:1:4);
      writeln('Entropy rate H = ',h:1:4);
      writeln('Reproductive potential Phi = ',fi:1:4);
      writeln('Entropic variance Sigma2 = ',sigma2:1:4);
      writeln('Correlation index Gamma = ' ,gamma:1:4);
    end;
end;

procedure prop_gene(x : integer);
var bool : boolean;

function s_ecri_bool(b : boolean) : string;
begin
  if b then s_ecri_bool := 'YES' else s_ecri_bool := 'NO';
end;

begin
  with mat[x] do
    begin
      writeln('Matrix ' + s_ecri_mat(x_mat));
      writeln('Time dependent -> ' + s_ecri_bool(mattimedep(x)));
      writeln('Vector dependent -> ' + s_ecri_bool(matvecdep(x,modele[xmodele].xvec)));
      writeln('Random -> ' + s_ecri_bool(matrandom(x)));
      bool := matnonneg(size,val);
      writeln('Non negative -> ' + s_ecri_bool(bool));
      if bool then
        begin
          writeln('Positive -> ' + s_ecri_bool(matpos(size,val)));
          writeln('Primitive -> ' + s_ecri_bool(matprim(size,val)));
          writeln('Irreducible -> ' + s_ecri_bool(matirr(size,val)));
          leslie := matleslie(size,val);
          writeln('Leslie -> ' + s_ecri_bool(leslie));
          leslie2 := matleslie2(size,val);
          writeln('Extended Leslie -> ' + s_ecri_bool(leslie2));
          sizeclass := matclassetaille(size,val);
          writeln('Size classified -> ' + s_ecri_bool(sizeclass));
          multi := matmultisite(size,val,psite);
          if multi then
            writeln('Multisite -> ' + s_ecri_bool(multi),' [', psite:1,' sites]')
          else
            writeln('Multisite -> ' + s_ecri_bool(multi));
         end;
    end;
end;

procedure eigenval(x : integer;var err : boolean);
begin
  err := true;
  with mat[x] do
    begin
      matvalprop(size,val,lambda);
      if err_math then
        begin
          erreur_matrix('Error in eigenvalues computation');
          err_math := false;
          exit;
        end;
      if ( lambda[1].im <> 0.0 ) then
        begin
          erreur_matrix('No real dominant eigenvalue found');
          exit;
        end;
      lambda1 := lambda[1].re;
      if ( lambda1 <= 0.0 ) then
        begin
          erreur_matrix('Dominant eigenvalue <= 0');
          exit;
        end;
    end;
  err := false;
end;

procedure eigenval1(x : integer;var err : boolean);
var i : integer;
    z : cmx;
    pp,rho : extended;
begin
  eigenval(x,err);
  if err then exit;
  if ( cmxmod(lambda[2]) <> 0.0 ) then
    rho := lambda1/cmxmod(lambda[2])
  else
    rho := 0.0;
  if ( cmxarg(lambda[2]) <> 0.0 ) then
    pp := dpi/cmxarg(lambda[2])
  else
    pp := 0.0;
  r := ln0(lambda1);
  writeln('Eigenvalues:');
  with mat[x] do
    begin
      writeln('     Modulus     Re        Im           Arg');
      for i := 1 to size do
        begin
          z := lambda[i];
          writeln(i:3,cmxmod(z):10:6,z.re:10:4,z.im:10:4,cmxarg(z)*360.0/dpi:10:1);
        end;
    end;
  writeln('Growth rate lambda = ',lambda1:1:6);
  writeln('Intrinsic rate of increase r = ',r:1:4);
  writeln('Damping ratio rho = ',rho:1:4);
  writeln('Period P = ',pp:1:4,' (Rad)');
  err := false;
end;

procedure eigenvec(x : integer; var err : boolean);
var i : integer;
    vv,ww : cvec_type;
begin
  err := true;
  with mat[x] do
    begin
      matvecpropdroite(size,val,lambda[1],ww);
      if err_math then
        begin
          err_math := false;
          exit;
        end;
      for i := 1 to size do w[i] := ww[i].re;
      vecnormalise1(size,w);
      matvecpropgauche(size,val,lambda[1],vv);
      if err_math then
        begin
          err_math := false;
          exit;
        end;
      for i := 1 to size do v[i] := vv[i].re;
      vecnormalise1(size,v);
    end;
  err := false;
  writeln('       Reproductive value  Stable age distribution');
  for i := 1 to mat[x].size do writeln(i:3,v[i]:10:4,'          ',w[i]:10:4);
end;

procedure returntime(x : integer);
var i,j,a,n : integer;
    vq : rvec_type;
begin
  with mat[x] do
    begin
      n := size;
      for i := 1 to n do
        for j := 1 to n do
          mp[i,j] := val[i,j]*w[j]/(lambda1*w[i]);
      matkovvecg(n,mp,vp);
      writeln('Computation of return times:');
      a := 0;
      for i := 1 to n do
        for j := 1 to n do
          if ( mp[i,j] > 0.0 ) then
            begin
              a := a + 1;
              vq[a] := mp[i,j]*vp[i];
              if ( vq[a] = 0.0 ) then
                writeln(a:4, ' i = ',i:4,' vp = ', vp[i]:1:4)
              else
                writeln('   ',a:4,'   ',j:4,' -> ',i:4,
                        '   Ta',a:1,' = ',1.0/vq[a]:1:4,' ',
                        '   1/Ta',a:1,' = ',vq[a]:1:4);
            end;
      writeln('Return time TZ to set of transitions Z is given by 1/TZ = 1/Ta + 1/Tb + ... with a, b, ... in Z');
    end;
end;

procedure gentime(x : integer);
var i,j,a,n : integer;
    sum,tb : extended;
begin
  with mat[x] do
    begin
      n := size;
      // deja calcule dans entropy
      {for i := 1 to n do
        for j := 1 to n do
          mp[i,j] := val[i,j]*w[j]/(lambda1*w[i]);
      matkovvecg(n,mp,vp);}
      sum := 0.0;
      for a := 1 to repro_nb do
        sum := sum + mp[repro_i[a],repro_j[a]]*vp[repro_i[a]];
      if ( sum > 0.0 ) then
        tb := 1.0/sum
      else
        tb := 0.0;
      writeln('Mean generation length T = ',tb:1:4);
    end;
end;

(*procedure gentime(x : integer);
// formule T = lambda*vw/vFw
var i,j,a,n : integer;
    sum,pscal,tb : extended;
begin
  with mat[x] do
    begin
      n := size;
      { deja fait dans netreprorate
      // A = S + F
      for i := 1 to n do
        for j := 1 to n do
          begin
            mats[i,j] := val[i,j];
            matf[i,j] := 0.0;
          end;
      for a := 1 to repro_nb do
        begin
          matf[repro_i[a],repro_j[a]] := val[repro_i[a],repro_j[a]];
          mats[repro_i[a],repro_j[a]] := 0.0;
        end;}
      pscal := 0.0;
      for i := 1 to n do pscal := pscal + v[i]*w[i];
      for i := 1 to n do
        begin
          sum := 0.0;
          for j := 1 to n do sum := sum + matf[i,j]*w[j];
          vp[i] := sum;
        end;
      sum := 0;
      for j := 1 to n do sum := sum + v[j]*vp[j];
      if ( sum > 0.0 ) then
        tb := lambda1*pscal/sum
      else
        tb := 0.0;
      writeln('Mean generation length T = ',tb:1:4);
    end;
end;*)

procedure netreprorate(x : integer);
// calcul net reproductive rate R0
// valeur propre dominante de FN
const eps = 1.0E-12;
var i,j,k,a,n : integer;
    sum,norm,norm1,r0,t0 : extended;
    ma,mb : rmat_type;
    lamb : cvec_type;
begin
  with mat[x] do
    begin
      n := size;
      // A = S + F
      for i := 1 to n do
        for j := 1 to n do
          begin
            mats[i,j] := val[i,j];
            matf[i,j] := 0.0;
          end;
      for a := 1 to repro_nb do
        begin
          matf[repro_i[a],repro_j[a]] := val[repro_i[a],repro_j[a]];
          mats[repro_i[a],repro_j[a]] := 0.0;
        end;
      // calcul N = I + S + S^2 + ... = (I - S)^{-1}
      for i := 1 to n do
        for j := 1 to n do
          if ( i = j ) then
            matn[i,j] := 1.0
          else
            matn[i,j] := 0.0; // N = I
      for i := 1 to n do
        for j := 1 to n do
          ma[i,j] := matn[i,j]; // ma = I
      norm := 1.0;
      a := 0;
      repeat
        a := a + 1;
        for i := 1 to n do
          for j := 1 to n do
            begin
              sum := 0.0;
              for k := 1 to n do sum := sum + ma[i,k]*mats[k,j];
              mb[i,j] := sum;
            end;
        for i := 1 to n do
          for j := 1 to n do
            ma[i,j] := mb[i,j]; // ma = S^k
        norm1 := norm;
        norm := 0.0;
        for i := 1 to n do
          for j := 1 to n do
            norm := max(norm,abs(ma[i,j]));
        for i := 1 to n do
          for j := 1 to n do
            matn[i,j] := matn[i,j] + ma[i,j]; // N = I + S + S^2 + ...
      until ( a >= 10000 ) or ( norm < eps );
      if ( norm1 = norm ) then // pb convergence }
        begin
          writeln('R0 - convergence');
          exit;
        end;
      for i := 1 to n do
        for j := 1 to n do
          begin
            sum := 0.0;
            for k := 1 to n do sum := sum + matf[i,k]*matn[k,j];
            mb[i,j] := sum; // mb = FN
          end;
      matvalprop(n,mb,lamb);
      if err_math then
        begin
          writeln('R0 - Error in eigenvalue computation FN');
          err_math := false;
          exit;
        end;
      if ( lamb[1].im <> 0.0 ) then
        begin
          writeln('R0 - No real dominant eigenvalue found');
          exit;
        end;
      r0 := lamb[1].re;
      if ( r0 <= 0.0 ) then
        begin
          r0 := 0.0;
          exit;
        end;
      writeln('Net reproductive rate R0 = ',r0:1:4);
      if ( r <> 0.0 ) then t0 := ln(r0)/r else t0 := 0.0; // T0
      writeln('R0 generation time T0 = ',t0:1:4);
    end;
end;

procedure entropy(x : integer;var err : boolean);
{ compute entropy rate H }
var n,i,j : integer;
    h,hi : extended;
begin
  with mat[x] do
    begin
      n := size;
      for i := 1 to n do
        if ( w[i] = 0.0 ) then // matrice reductible
          begin
            err := true;
            writeln('Entropy rate - matrix is reducible');
            exit;
          end;
      for i := 1 to n do
        for j := 1 to n do
          mp[i,j] := val[i,j]*w[j]/(lambda1*w[i]);
      matkovvecg(n,mp,vp);
      h := 0.0;
      for i := 1 to n do
        begin
          hi := 0.0;
          for j := 1 to n do hi := hi + mp[i,j]*ln0(mp[i,j]);
          h := h + vp[i]*hi;
        end;
      h := -h;
      writeln('Entropy rate H = ',h:1:4);
    end;
end;

procedure dem_qq(x : integer);
var err : boolean;
begin
  with mat[x] do
    begin
      err := false;
      entropy(x,err);
      if err then exit;
      if ( repro_nb = 0 ) then exit;
      netreprorate(x);
      gentime(x);
    end;
end;

procedure proprietes(x : integer);
var i : integer;
    err : boolean;
begin
  if ( x = 0 ) then
    begin
      erreur_matrix('No matrix-type model');
      exit;
    end;
  prop_gene(x);
  eigenval1(x,err);
  if err then exit;
  eigenvec(x,err);
  if leslie then
    dem_leslie(x)
  else
    if leslie2 then
      dem_leslie2(x)
    else
      dem_qq(x);
  returntime(x);
end;


{ ------ sensibilites ------ }

var mat_sens : rmat_type; { matrice des sensibilites }
    mat_elas : rmat_type; { matrice des elasticites }
    sens_x_tot,elas_x_tot : extended; { sensibilite, elasticite totale }
    var_lambda,cv_lambda : extended; {variance et coeff de variation de lambda }

procedure sens_lambda1(m : integer;var err : boolean);
var i,j : integer;
    r : extended;
begin
  err := true;
  with mat[m] do
    begin
      r := vecpscal(size,v,w);
      if ( r = 0.0 ) then exit;
      for i := 1 to size do
        for j := 1 to size do
          mat_sens[i,j] := v[i]*w[j]/r;
      for i := 1 to size do
        for j := 1 to size do
          mat_elas[i,j] := val[i,j]*mat_sens[i,j]/lambda1;
      r := 0.0;
      for i := 1 to size do
        for j := 1 to size do
          r := r + sqr(mat_sens[i,j]);
      var_lambda := r;
      r  := 0.0;
      for i := 1 to size do
        for j := 1 to size do
          r := r + sqr(mat_elas[i,j]);
      cv_lambda  := sqrt(r);
    end;
  err := false;
end;

procedure sens_lambda1_x(m,x : integer; var err : boolean);
{ sensibilites de la matrice m par rapport a la variable x }
var i,j,y,ty : integer;
    r,som : extended;
begin
  err := true;
  with mat[m] do
    begin
      r := vecpscal(size,v,w);
      if ( r = 0.0 ) then exit;
      for i := 1 to size do
        for j := 1 to size do
          mat_sens[i,j] := v[i]*w[j]/r;
      som := 0.0;
      for i := 1 to size do
        for j := 1 to size do
          begin
            deriv(exp[i,j],exp_type[i,j],x,y,ty);
            if err_eval then
              begin
                err_eval := false;
                exit;
              end;
            r := eval(y,ty);
            mat_sens[i,j] := mat_sens[i,j]*r;
            mat_elas[i,j] := variable[x].val*mat_sens[i,j]/lambda1;
            som := som + mat_sens[i,j];
          end;
      sens_x_tot := som;
      elas_x_tot := som*variable[x].val/lambda1;
    end;
  err := false;
end;

procedure sensibilites(m,x : integer);
var i,j : integer;
    err : boolean;
begin
  with mat[m] do
    begin
      writeln('Matrix ',s_ecri_mat(m));
      eigenval(m,err);
      if err then exit;
      eigenvec(m,err);
      if err then exit;
      writeln('lambda = ',lambda1:1:6);
      if ( x = 0 ) then
        begin
          sens_lambda1(m,err);
          if err then exit;
          writeln('Assuming a same variance v on matrix entries:');
          writeln('  Var(lambda) =  ',var_lambda:1:4,'*v');
          writeln('Assuming a same coeff. of variation c on matrix entries:');
          writeln('   CV(lambda) =  ',cv_lambda:1:4,'*c');
          writeln('Sensitivities:');
          for i := 1 to size do
            begin
              for j := 1 to size do write(mat_sens[i,j]:10:4);
              writeln;
            end;
          writeln('Elasticities:');
          for i := 1 to size do
            begin
              for j := 1 to size do write(mat_elas[i,j]:10:4);
              writeln;
            end;
        end
      else
        begin
          sens_lambda1_x(m,x,err);
          if err then exit;
          writeln('Sensitivity lambda to ' + s_ecri_var(x),' = ',sens_x_tot:1:4);
          writeln('Elasticity  lambda to ' + s_ecri_var(x),' = ',elas_x_tot:1:4);
        end;
    end;
end;


{ ------ sensibilites stochastiques ------ }

var lambda_moy : extended; { mean dominant eigenvalue }
    mat_deriv_exp,mat_deriv_exp_type : imat_type; { matrice des derivees }

procedure construc_mat_deriv(m,x : integer);
var i,j : integer;
begin
  with mat[m] do
    for i := 1 to size do
      for j := 1 to size do
        begin
          deriv(exp[i,j],exp_type[i,j],x,mat_deriv_exp[i,j],mat_deriv_exp_type[i,j]);
          if err_eval then exit;
        end;
end;

procedure run_stoc_sensib(m,x,nb_cyc : integer; var err : boolean);
var i,j,icyc : integer;
    h,r,bv,ev,lambda1,som,soms,some : extended;
    v,w : rvec_type;
    lambda,vv,ww : cvec_type;
    b,e : rmat_type;
begin
  err := true;
  if ( x <> 0 ) then
    begin
      construc_mat_deriv(m,x); { reconstruit a chaque fois ... }
      if err_eval then exit;
    end;
  with mat[m] do
    begin
      soms := 0.0;
      some := 0.0;
      for i := 1 to size do
        for j := 1 to size do
          begin
            b[i,j] := 0.0;
            e[i,j] := 0.0;
          end;
      lambda_moy := 0.0;
      matvalprop(size,val,lambda);
      if err_math then exit;
      matvecpropdroite(size,val,lambda[1],ww);
      if err_math then exit;
      lambda1 := lambda[1].re;
      lambda_moy := lambda_moy + lambda1;
      for i := 1 to size do w[i] := ww[i].re;
      vecnormalise1(size,w);
      for icyc := 1 to nb_cyc do
        begin
          run_t;
          matvec(size,val,w,w);
          matvalprop(size,val,lambda);
          if err_math then exit;
          lambda1 := lambda[1].re;
          if ( lambda1 = 0.0 ) then exit;
          lambda_moy := lambda_moy + lambda1;
          matvecpropgauche(size,val,lambda[1],vv);
          if err_math then exit;
          for i := 1 to size do v[i] := vv[i].re;
          r := v[1];
          if ( r <> 0.0 ) then
            for i := 1 to size do v[i] := v[i]/r;
          h := vecpscal(size,v,w);
          if ( h = 0.0 ) then exit;
          som := 0.0;
          for i := 1 to size do
            for j := 1 to size do
              begin
                bv := v[i]*w[j]/h;
                if ( x <> 0 ) then
                  begin
                    r := eval(mat_deriv_exp[i,j],mat_deriv_exp_type[i,j]);
                    if err_eval then exit;
                    bv  := bv*r;
                    som := som + bv;
                    ev := variable[x].val*bv/lambda1;
                  end
                else
                  ev := val[i,j]*bv/lambda1;
                b[i,j] := ((icyc-1)*b[i,j] + bv)/icyc;
                e[i,j] := ((icyc-1)*e[i,j] + ev)/icyc;
              end;
          if ( x <> 0 ) then
            begin
              soms := ((icyc-1)*soms + som)/icyc;
              some := ((icyc-1)*some + som*(variable[x].val/lambda1))/icyc;
            end;
          matvecpropdroite(size,val,lambda[1],ww);
          if err_math then exit;
          for i := 1 to size do w[i] := ww[i].re;
          vecnormalise1(size,w);
        end;
      lambda_moy := lambda_moy/(1+nb_cyc);
      for i := 1 to size do
        for j := 1 to size do
          begin
            mat_sens[i,j] := b[i,j];
            mat_elas[i,j] := e[i,j];
          end;
      if ( x <> 0 ) then
        begin
          sens_x_tot := soms;
          elas_x_tot := some;
        end;
    end;
  err := false;
end;

procedure affichage_interp(m,x,nb_cyc : integer);
var i,j,n : integer;
begin
  n := mat[m].size;
  writeln('Stochastic sensitivities to lambda (', nb_cyc:1,' time steps):');
  for i := 1 to n do
    begin
      for j := 1 to n do write(mat_sens[i,j]:8:4);
      writeln;
    end;
  writeln('Stochastic elasticities to lambda (',nb_cyc:1,' time steps):');
  for i := 1 to n do
    begin
      for j := 1 to n do write(mat_elas[i,j]:10:4);
      writeln;
    end;
  if ( x <> 0 ) then
    begin
      writeln('Stochastic sensitivity lambda to ' + s_ecri_var(x),' = ',sens_x_tot:1:4);
      writeln('Stochastic elasticity  lambda to ' + s_ecri_var(x),' = ',elas_x_tot:1:4);
    end;
end;

procedure stoc_sensib(m,x,nb_cyc : integer);
var err : boolean;
begin
  run_stoc_sensib(m,x,nb_cyc,err);
  if err then
    begin
      writeln('Stochastic Sensitivities - Error at t = ',t__:1);
      err_math := false;
      err_eval := false;
      exit;
    end;
  affichage_interp(m,x,nb_cyc);
end;

{ ------ exposant de Lyapunov ------ }

procedure run_lyap(m,nb_cyc,tlag : integer);
const eps = 0.00001;
label 1;
var i,j,icyc : integer;
    lyap,lyapunov,d0,dd : extended;
    v,v_eps,dv : rvec_type;
    aaa : rmat_type;

procedure maj_var;
var e : integer;
begin
  for e := 1 to ordre_eval_nb do with variable[ordre_eval[e]] do
    val := eval(exp,exp_type);
end;

begin
  with modele[m] do
    begin
      lyap := 0.0;
      if ( xmat <> 0 ) then with vec[xvec] do
        for i := 1 to size do v_eps[i] := val[i] + eps/size
      else
        for i := 1 to size do with rel[xrel[i]] do v_eps[i] := val + eps/size;
      d0 := eps;
      for icyc := 1 to nb_cyc do
        begin
          run_t;
          if ( xmat <> 0 ) then
            begin
              for i := 1 to size do with vec[xvec] do
                begin
                  v[i] := variable[exp[i]].val;
                  variable[exp[i]].val := v_eps[i];
                end;
              maj_var;
             { pour reevaluer les variables qui dependent
               du vecteur de population on reevalue tout }
              for i := 1 to size do
                for j := 1 to size do with mat[xmat] do
                  aaa[i,j] := eval(exp[i,j],exp_type[i,j]);
              matvec(size,aaa,v_eps,v_eps);
            end
          else
            begin
              for i := 1 to size do with rel[xrel[i]] do
                begin
                  v[i] := variable[xvar].val;
                  variable[xvar].val := v_eps[i];
                end;
              maj_var;
              for i := 1 to size do with rel[xrel[i]] do
                v_eps[i] := eval(exp,exp_type);
            end;
          for i := 1 to size do dv[i] := v[i] - v_eps[i];
          dd := vecnorm(size,dv);
          if ( dd = 0.0 ) then dd := d0;
          for i := 1 to size do v_eps[i] := v[i] + d0*dv[i]/dd;
          lyap := lyap + ln0(dd/d0);
          lyapunov := lyap/icyc;
          if ( xmat <> 0 ) then
            for i := 1 to size do with vec[xvec] do
              variable[exp[i]].val := v[i]
          else
            for i := 1 to size do with rel[xrel[i]] do
              variable[xvar].val := v[i];
          maj_var;
          if ( t__ mod tlag = 0 ) then writeln('   ',t__:5,' ',lyapunov:10:6);
        end;
    end;
1 :
end;

end.
