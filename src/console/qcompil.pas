{ ---------------------------------------------------------------------------- }
{ Copyright (c) 1995-2017, Stéphane Legendre and the ULM development team      }
{                                                                              }
{ This file is part of ULM.                                                    }
{                                                                              }
{ ULM is free software: you can redistribute it and/or modify it               }
{ under the terms of the GNU General Public License as published               }
{ by the Free Software Foundation, either version 3 of the License,            }
{ or (at your option) any later version.                                       }
{                                                                              }
{ ULM is distributed in the hope that it will be useful,                       }
{ but WITHOUT ANY WARRANTY; without even the implied warranty                  }
{ of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                      }
{ See the GNU General Public License for more details.                         }
{                                                                              }
{ You should have received a copy of the GNU General Public License            }
{ along with ULM.  If not, see <http://www.gnu.org/licenses/>.                 }
{ ---------------------------------------------------------------------------- }

unit qcompil;

{  @@@@@@   compilation   @@@@@@  }

interface

var compiled : boolean;

procedure compilation;

implementation

uses qglobvar,qsymb,qsyntax;

var  xobj,size,truc,iligne : integer;
     etat : integer;
     compil_init : boolean;
     nb_defvar,nb_deffun,nb_defrel,nb_defvec,nb_defmat : integer;
     exp1,exp_type1 : imat_type;
     repro_nb1 : integer;
     repro_i1,repro_j1 : ivec_type;
     line_num : integer;
     err_compil : boolean;

procedure erreur_compil(s,st : string);
begin
  writeln('Error: Compile - ',s);
  writeln('  ',st,' -> line number ',line_num:1);
  err_compil := true;
end;

procedure compil_variable(s : string);
{ defvar xxx = exp | defvar x1 x2 ... xn = exp }
var pos,nom,x,tx,i,ns : integer;
    s1,s2 : string;
begin
  case etat of
  1 : begin
        pos := position(s,'=');
        if ( pos = 0 ) then
          begin
            erreur_compil(s,'''='' expected');
            exit;
          end;
        coupe(s,pos,s1,s2);
        s1 := tronque(s1);
        s2 := propre(tronque(s2));
        lirexp(s2,x,tx);
        if err_syntax then
          begin
            err_syntax := false;
            erreur_compil(s2,'');
            exit;
          end;
        if ( x = 0 ) then
          begin
            erreur_compil(s2,'');
            exit;
          end;
        separe(s1,' ',ns,lines_separe);
        for i := 1 to ns do
          begin
            s1  := lines_separe[i];
            nom := trouve_dic(s1);
            if ( nom = 0 ) then
              begin
                if not est_nom(s1) then
                  begin
                    erreur_compil(s1,'unproper name');
                    exit;
                  end;
                if est_reserve(s1) then
                  begin
                    erreur_compil(s1,'reserved word');
                    exit;
                  end;
                nom  := cre_dic(s1);
                xobj := cre_variable(nom);
              end
            else
              begin
                xobj := trouve_variable(nom);
                if ( xobj = 0  ) then
                  begin
                    erreur_compil(s,'name already exists');
                    exit;
                  end;
                if ( xobj = xtime ) then
                  begin
                    erreur_compil(s,'t = time is predefined');
                    exit;
                  end;
                if ( variable[xobj].exp_type <> type_inconnu ) then
                  begin
                    erreur_compil(s,'variable already declared');
                    exit;
                  end;
                if ( variable[xobj].xrel <> 0 ) and ( tx <> type_ree ) then
                  begin
                    erreur_compil(s2,'relation-variable must be set to a real value');
                    exit;
                  end;
              end;
            set_variable(xobj,x,tx);
          end;
        nb_defvar := nb_defvar + ns;
        etat := 10;
      end;
  else;
  end;
end;

procedure compil_fun(s : string);
{ deffun fff(x1, ...,xn) = exp }
var pos,ls,nom,x,tx,nb_arg1,i,ns : integer;
    s1,s2 : string;
    xarg1 : larg_type;
begin
  case etat of
  1 : begin
        pos := position(s,'=');
        if ( pos = 0 ) then
          begin
            erreur_compil(s,'''='' expected');
            exit;
          end;
        coupe(s,pos,s1,s2);
        s1 := tronque(s1);
        ls := length(s1);
        if ( s1[ls] <> ')' ) then
          begin
            erreur_compil(s,''')'' expected');
            exit;
          end;
        pos := position(s1,'(');
        if ( pos = 0 ) then
          begin
            erreur_compil(s,'''('' expected');
            exit;
          end;
        s := s1;
        coupe(s,pos,s1,s);
        s1 := tronque(s1);
        if est_reserve(s1) then
          begin
            erreur_compil(s1,'reserved word');
            exit;
          end;
        ls := length(s);
        s  := sous_chaine(s,1,ls-1);
        if ( s = '' ) then
          begin
            erreur_compil(s,'function argument(s) missing');
            exit;
          end;
        nom := trouve_dic(s1);
        if ( nom = 0 ) then 
          begin
            if not est_nom(s1) then
              begin
                erreur_compil(s1,'unproper name');
                exit;
              end;
            nom  := cre_dic(s1);
            xobj := cre_fun(nom);
          end
        else
          begin
            xobj := trouve_fun(nom);
            if ( xobj = 0 ) then
              begin
                erreur_compil(s,'name already exists');
                exit;
              end;
          end;
        separe(s,',',ns,lines_separe);
        for i := 1 to ns do
          begin
            s1 := lines_separe[i];
            if ( s1 = '' ) then
              begin
                erreur_compil(s,'syntax');
                exit;
              end;
            nom := trouve_dic('_'+s1);
            if ( nom = 0 ) then
              begin
                if not est_nom(s1) then
                  begin
                    erreur_compil(s1,'unproper name');
                    exit;
                  end;
                nom  := cre_dic('_'+s1);
              end;
            xarg1[i] := cre_arg(nom);
          end;
        nb_arg1 := ns;
        set_fun(xobj,nb_arg1,xarg1);
        fun_compil := xobj;
        s2 := propre(s2);
        lirexp(s2,x,tx); 
        if err_syntax then
          begin
            err_syntax := false;
            erreur_compil(s2,'');
            exit;
          end;
        if ( x = 0 ) then
          begin
            erreur_compil(s2,'');
            exit;
          end;
        nb_deffun := nb_deffun + 1;
        fun[xobj].exp := x;
        fun[xobj].exp_type := tx;
        fun_compil := 0; 
        etat := 10;
      end;
   else;
   end;
end;

procedure compil_rel(s : string);
{ defrel rrr }
{ var = exp  }
var pos,nom,var1,exp1,exp_type1 : integer;
    s1,s2 : string;
begin
  case etat of
  1 : begin
        nom := trouve_dic(s);
        if ( nom = 0 ) then 
          begin
            if not est_nom(s) then
              begin
                erreur_compil(s,'unproper name');
                exit;
              end;
            nom  := cre_dic(s);
            xobj := cre_rel(nom);
          end
        else
          begin
            xobj := trouve_rel(nom);
            if ( xobj = 0 ) then
              begin
                erreur_compil(s,'name already exists');
                exit;
              end
            else
              if ( rel[xobj].xmodele = 0 ) then { cas relation non liee a un modele }
                begin
                  erreur_compil(s,'name already exists');
                  exit;
                end
          end;
        etat := 2;
      end;
  2 : begin
        pos := position(s,'=');
        if ( pos = 0 ) then
          begin
            erreur_compil(s,'''='' expected');
            exit;
          end;
        coupe(s,pos,s1,s2);
        s1  := tronque(s1);
        s2  := propre(s2);
        nom := trouve_dic(s1);
        if ( nom = 0 ) then 
          begin
            if not est_nom(s1) then
              begin
                erreur_compil(s1,'unproper name');
                exit;
              end;
            nom  := cre_dic(s1);
            var1 := cre_variable(nom);
          end
        else
          var1 := trouve_variable(nom);
        lirexp(s2,exp1,exp_type1);
        if err_syntax then
          begin
            err_syntax := false;
            erreur_compil(s2,'');
            exit;
          end;
        if ( exp1 = 0 ) then
          begin
            erreur_compil(s2,'');
            exit;
          end;
        set_rel(xobj,exp1,exp_type1,var1);
        variable[var1].xrel   := xobj;
        nb_defrel := nb_defrel + 1;
        etat := 10;
      end;
   else;
   end;
end;

procedure compil_vec(s : string);
{ defvec vvv(k)   }
{ n1, ..., nk     }
var pos1,pos2,nom,i,x,tx,ns : integer;
    exp1,exp_type1 : ivec_type;
    s1,s2 : string;
begin
  case etat of
  1 : begin
        pos1 := position(s,'(');
        pos2 := position(s,')');
        if ( ( pos2 <> length(s) ) or ( pos2 <= pos1 ) ) then
          begin
            erreur_compil(s,'syntax');
            exit;
          end;
        s  := sous_chaine(s,1,pos2-1);
        coupe(s,pos1,s1,s2);
        s1  := tronque(s1);
        s2  := tronque(s2);
        if not est_entier(s2,size) then
          begin
            erreur_compil(s,'integer expected');
            exit;
          end;
        if ( size > matmax ) then
          begin
            erreur_compil(s2,'vector size too big');
            exit;
          end;
        nom := trouve_dic(s1);
        if ( nom = 0 ) then
          begin
            erreur_compil(s1,'vector unrelated to a model');
            exit;
          end
        else
          begin
            xobj := trouve_vec(nom);
            if ( xobj = 0  ) then
              begin
                erreur_compil(s,'name already exists');
                exit;
              end;
            if ( vec[xobj].xmodele <> 0 ) then
              if ( modele[vec[xobj].xmodele].size <> size ) then
                begin
                  erreur_compil(s,'size mismatch');
                  exit;
                end;
          end;
        etat := 2;
      end;
  2 : begin
        separe(s,',',ns,lines_separe);
        if ( ns <> size ) then
          begin
            erreur_compil(s,'wrong number of entries in vector');
            exit;
          end;
        for i := 1 to ns do
          begin
            s1 := lines_separe[i];
            if ( s1 = '' ) then
              begin
                erreur_compil(s,'syntax');
                exit;
              end;
            lirexp(s1,x,tx);
            if ( tx <> type_variable ) then
              begin
                erreur_compil(s,'variable name expected');
                exit;
              end;
            exp1[i] := x;
            exp_type1[i] := tx;
            variable[x].xvec := xobj;
          end;
        set_vec(xobj,exp1,exp_type1);
        nb_defvec := nb_defvec + 1;
        etat := 10;
      end;
   else;
   end;
end;

procedure compil_mat(s : string);
{ defmat aaa(size)  }
{ a11, ..., a1k     }
{   ...               }
{ ak1, ..., akk     }
var pos1,pos2,nom,j,x,tx,ns : integer;
    s1,s2 : string;
begin
  case etat of
  1 : begin
        pos1 := position(s,'(');
        pos2 := position(s,')');
        if ( ( pos2 <> length(s) ) or ( pos2 <= pos1 ) ) then
          begin
            erreur_compil(s,'syntax');
            exit;
          end;
        s := sous_chaine(s,1,pos2-1);
        coupe(s,pos1,s1,s2);
        s1 := tronque(s1);
        s2 := tronque(s2);
        if ( not est_entier(s2,size) ) then
          begin
            erreur_compil(s2,'integer expected');
            exit;
          end;
        if ( size > matmax ) then
          begin
            erreur_compil(s2,'size too large');
            exit;
          end;
        nom := trouve_dic(s1);
        if ( nom = 0 ) then 
          erreur_compil(s1,'matrix not related to a model')
        else
          begin
            xobj := trouve_mat(nom);
            if ( xobj = 0  ) then
              begin
                erreur_compil(s,'name already exists');
                exit;
              end;
            if ( mat[xobj].size <> size ) then
              begin
                erreur_compil(s,'size mismatch');
                exit;
              end;
          end;
        iligne := 0;
        repro_nb1 := 0;
        etat := 2;
      end;
  2 : begin
        iligne := iligne + 1;
        if ( iligne > size ) then
          begin
            erreur_compil(s,'wrong number of rows in matrix');
            exit;
          end;
        separe(s,',',ns,lines_separe);
        if ( ns <> size ) then
          begin
            erreur_compil(s,'wrong number of entries in matrix line');
            exit;
          end;
        for j := 1 to ns do
          begin
            s1 := propre(lines_separe[j]);
            if ( s1 = '' ) then
              begin
                erreur_compil(s,'syntax');
                exit;
              end;
            if ( s1[length(s1)] = '#' ) then
              begin
                repro_nb1 := repro_nb1 + 1;
                s1 := sous_chaine(s1,1,length(s1)-1);
                repro_i1[repro_nb1] := iligne;
                repro_j1[repro_nb1] := j;
              end;
            lirexp(s1,x,tx);
            exp1[iligne,j] := x;
            exp_type1[iligne,j] := tx;
          end;
        if ( iligne = size ) then
          begin
            set_mat(xobj,exp1,exp_type1,repro_nb1,repro_i1,repro_j1);
            nb_defmat := nb_defmat + 1;
            etat := 10;
          end; 
      end;
   else;
   end;
end;

procedure compil_modele(s : string);
{ defmod mmm(size)     }
{ rel : r1, ..., rsize }
{   or                 }
{ defmod mmm(size)     }
{ mat: aaa             }
{ vec: vvv             }
var pos1,pos2,nom,i,x,mat1,vec1,ns : integer;
    relat : ivec_type;
    s1,s2 : string;
begin
  case etat of
  1 : begin
        pos1 := position(s,'(');
        pos2 := position(s,')');
        if ( ( pos2 <> length(s) ) or ( pos2 <= pos1 ) ) then
          begin
            erreur_compil(s,'syntax');
            exit;
          end;
        s := sous_chaine(s,1,pos2-1);
        coupe(s,pos1,s1,s2);
        s1 := tronque(s1);
        s2 := tronque(s2);
        if not est_entier(s2,size) then
          begin
            erreur_compil(s2,'integer expected');
            exit;
          end;
        if ( size > matmax ) then
          begin
            erreur_compil(s2,'size too large');
            exit;
          end;
        nom := trouve_dic(s1);
        if ( nom = 0 ) then 
          nom := cre_dic(s1)
        else
          begin
            erreur_compil(s,'name already exists');
            exit;
          end;
        xobj := cre_modele(nom);
        modele[xobj].size := size;
        etat := 2;
      end;
  2 : begin
        pos1 := position(s,':');
        if ( pos1 = 0 ) then
          begin
            erreur_compil(s,'''rel:'' or ''mat:'' expected');
            exit;
          end;
        coupe(s,pos1,s1,s2);
        s1 := tronque(s1);
        s2 := tronque(s2);
        if ( s1 <> 'rel' ) and ( s1 <> 'mat' ) then
          begin
            erreur_compil(s,'keyword ''rel'' or ''mat'' expected');
            exit;
          end;
        if ( s1 = 'mat' ) then
          begin
            nom := trouve_dic(s2);
            if ( nom = 0 ) then
              begin
                if not est_nom(s2) then
                  begin
                    erreur_compil(s2,'unproper name');
                    exit;
                  end;
                nom  := cre_dic(s2);
                mat1 := cre_mat(nom,size);
              end
            else
              begin
                erreur_compil(s,'name already used');
                exit;
              end;
            modele[xobj].xmat := mat1;
            mat[mat1].xmodele := xobj;
            etat := 3;
            exit;
          end;
        { relations }
        separe(s2,',',ns,lines_separe);
        if ( ns <> size ) then
          begin
            erreur_compil(s,'wrong number of relations in model');
            exit;
          end;
        for i := 1 to ns do
          begin
            s1 := lines_separe[i];
            if ( s1 = '' ) then
              begin
                erreur_compil(s,'syntax');
                exit;
              end;
            nom := trouve_dic(s1);
            if ( nom = 0 ) then
              begin
                if not est_nom(s1) then
                  begin
                    erreur_compil(s,'unproper name');
                    exit;
                  end;
                nom := cre_dic(s1);
                relat[i] := cre_rel(nom);
              end
            else
              begin
                erreur_compil(s,'relation declared before related model');
                exit;
              end;
          end;
        for i := 1 to size do with modele[xobj] do
          begin
            x := relat[i];
            xrel[i]:= x;
            rel[x].xmodele := xobj;
          end;
        etat := 10;
     end;
  3 : begin
        pos1 := position(s,':');
        if ( pos1 = 0 ) then
          begin
            erreur_compil(s,'''vec:'' expected');
            exit;
          end;
        coupe(s,pos1,s1,s2);
        s1 := tronque(s1);
        s2 := tronque(s2);
        if ( s1 <> 'vec' ) then
          begin
            erreur_compil(s,'keyword ''vec'' expected');
            exit;
          end;
        if not est_nom(s2) then
          begin
            erreur_compil(s2,'unproper name');
            exit;
          end;
        nom := trouve_dic(s2);
        if ( nom = 0 ) then
          begin
            nom  := cre_dic(s2);
            vec1 := cre_vec(nom,size);
          end
        else
          begin
            erreur_compil(s,'name already used');
            exit;
          end;
        modele[xobj].xvec := vec1;
        vec[vec1].xmodele := xobj;
        etat := 10;
      end;
   else;
   end;
end;

function  token(s : string) : integer;
begin
  if ( s = 'defvar' ) then
    token := type_variable
  else
  if ( s = 'deffun' ) then
    token := type_fun
  else
  if ( s = 'defrel' ) then
    token := type_rel
  else
  if ( s = 'defvec' ) then
    token := type_vec
  else
  if ( s = 'defmat' ) then
    token := type_mat
  else
  if ( s = 'defmod'  ) then 
    token := type_modele 
  else
    token := type_inconnu;
end;

procedure compile(s : string);
var pos,ls : integer;
    s1,s2  : string;
begin
  s := tronque(minuscule(s));
  ls := length(s);
  if ( ls > 0 ) and ( s[1] = '{' ) then exit;
  if ( etat = 0 ) then
    if ( ls = 0 ) then exit else etat := 1;
  case etat of
  1 : begin
        pos := position(s,' ');
        coupe(s,pos,s1,s2);
        s1 := tronque(s1);
        s2 := tronque(s2);
        if compil_init and ( s1 <> 'defmod' ) then
          begin
            erreur_compil(s,'model must be declared first');
            exit;
          end
        else
          compil_init := false;
       truc := token(s1);
       case truc of
          type_variable : compil_variable(s2);
          type_fun      : compil_fun(s2);
          type_rel      : compil_rel(s2);
          type_vec      : compil_vec(s2);
          type_mat      : compil_mat(s2);
          type_modele   : compil_modele(s2);
         else
            begin
              erreur_compil(s,'unknown keyword');
              exit;
            end;
        end; 
      end;
  2 : begin
        case truc of
          type_variable : compil_variable(s);
          type_fun      : compil_fun(s);
          type_rel      : compil_rel(s);
          type_mat      : compil_mat(s);
          type_vec      : compil_vec(s);
          type_modele   : compil_modele(s);
          else erreur_compil(s,'lost 2!');
        end;
      end;
  3 : begin
        case truc of
          type_modele   : compil_modele(s);
          else erreur_compil(s,'lost 3!');
        end;
      end;

  10 : begin
        if ( ls <> 0 ) then
          begin
            erreur_compil(s,'blank line expected');
            exit;
          end;
        etat := 0;
       end;
  else;
  end;
end;

procedure coherence_compil; 
var x,i,irel,imat,ivec : integer;
begin
  if ( modele_nb = 0 ) then
    begin
      erreur_compil('','no model declared');
      exit;
    end;
  for x := 1 to variable_nb do with variable[x] do
    if ( exp_type = type_inconnu ) then
      begin
        erreur_compil(s_ecri_var(x),'variable not declared');
        exit;
      end;
  for x := 1 to variable_nb do with variable[x] do
    if ( exp_type <> type_ree) and ( xrel <> 0 ) then
      begin
        erreur_compil(s_ecri_var(x),
        'relation-variable must be set to a real value');
        exit;
      end;
  for x := 1 to fun_nb do with fun[x] do
    if ( exp_type = type_inconnu ) then
      begin
        erreur_compil(s_ecri_fun(x),'function not declared');
        exit;
      end;


  imat := 0;

  ivec := 0;
  irel := 0;
  for x := 1 to modele_nb do with modele[x] do
    begin
      if ( xmat <> 0 )    then imat := imat + 1;
      if ( xvec <> 0 )    then ivec := ivec + 1;
      if ( xrel[1] <> 0 ) then irel := irel + size;
    end;
  if ( imat <> ivec ) then
    begin
      erreur_compil('','mismatch in model declaration');
      exit;
    end;
  if ( imat <> nb_defmat ) then
    begin
      erreur_compil('','model declaration does not match DEFMAT declaration');
      exit;
    end;
  if ( ivec <> nb_defvec ) then
    begin
      erreur_compil('','model declaration does not match DEFVEC declaration');
      exit;
    end;
  if ( irel > nb_defrel )  then
    begin
      erreur_compil('','model declaration does not match DEFREL declaration');
      exit;
    end;


  for x := 1 to modele_nb do with modele[x] do
    if ( xrel[1] <> 0 ) then {jjjjj}
    for i := 1 to size do
      if ( rel[xrel[i]].exp_type = type_inconnu ) then
        begin
          erreur_compil(s_ecri_rel(xrel[i]),
          'undefined relation in model ' + s_ecri_model(x));
          exit;
        end;

  if ( nb_defvar <> variable_nb - variable_nb_predef {- nb_defmut} ) then
    begin
      erreur_compil('','missing declaration of variable');
      exit;
    end;
  if ( nb_deffun <> fun_nb - fun_nb_predef ) then
    begin
      erreur_compil('','missing declaration of function');
      exit;
    end;
  if ( nb_defmat <> mat_nb ) then
    begin
      erreur_compil('','matrix number does not match DEFMAT number');
      exit;
    end;

  crea_var   := false;
  crea_fun   := false;
end;

procedure init_compil;
begin
  compil_init  := true;
  etat         := 0; 
  nb_defvar    := 0;
  nb_deffun    := 0;
  nb_defrel    := 0;
  nb_defvec    := 0;
  nb_defmat    := 0;
  fun_compil   := 0;
  crea_var     := true;
  crea_fun     := true;
end;


procedure compilation;
label 1;
var s : string;
begin
  init_compil;
  line_num := 0;
  err_compil := false;
  compiled := false;
  while not eof(ficmodele) do
    begin
      readln(ficmodele,s);
      line_num := line_num + 1;
      s := tronque(minuscule(s));
      {writeln(line_num:1,':',s);}
      compile(s);
      if err_compil then goto 1;
    end;
  coherence_compil;
  if err_compil then goto 1;
  compiled := true;
  writeln('-> file ',nomficmodele,' has been compiled');
1:
  close(ficmodele);
end;

end.
