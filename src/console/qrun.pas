{ ---------------------------------------------------------------------------- }
{ Copyright (c) 1995-2017, Stéphane Legendre and the ULM development team      }
{                                                                              }
{ This file is part of ULM.                                                    }
{                                                                              }
{ ULM is free software: you can redistribute it and/or modify it               }
{ under the terms of the GNU General Public License as published               }
{ by the Free Software Foundation, either version 3 of the License,            }
{ or (at your option) any later version.                                       }
{                                                                              }
{ ULM is distributed in the hope that it will be useful,                       }
{ but WITHOUT ANY WARRANTY; without even the implied warranty                  }
{ of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                      }
{ See the GNU General Public License for more details.                         }
{                                                                              }
{ You should have received a copy of the GNU General Public License            }
{ along with ULM.  If not, see <http://www.gnu.org/licenses/>.                 }
{ ---------------------------------------------------------------------------- }

unit qrun;

{  @@@@@@   run modele   @@@@@@  }

interface

procedure run(nb_cycle : integer);
procedure run_param(nb_cycle,xp : integer;amin,amax,da : extended);

implementation

uses   qglobvar,qmath,qsymb,qsyntax,qeval,qinterp,qcarlo;

{ ------  "graphique" utilise pour scatter  ------ }

procedure run_graph_traj;
var x,k : integer;
begin
  if ( t_graph_traj <= maxgraph ) then
    begin
      valgraph_x[t_graph_traj] := variable[vargraph_x].val;
      for k := 1 to nb_vargraph_y do
        begin
          x := vargraph_y[k];
          with variable[x] do valgraph_y[k][t_graph_traj] := val
        end;
    end;
end;

procedure run_init_graph_traj;
begin
  dt_graph := (nb_cycle + maxgraph - 1) div maxgraph;
  run_graph_traj;
end;

procedure run_init_graph;
begin
  t_graph_traj  := 0;
  run_init_graph_traj;
end;

procedure run_graph(icyc : integer);
begin
  if ( icyc mod dt_graph = 0 ) then
    begin
      t_graph_traj := t_graph_traj + 1;
      run_graph_traj;
    end;
end;

procedure run_fin_graph;
begin
  if ( t_graph_traj > 0 ) then
    if gscatter then
      begin
        gscat(imin(t_graph_traj,maxgraph));
        writeln('  Regression: slope = ', regress_a:1:4,
                ' intercept = ', regress_b:1:4);
      end;
end;


{ ------  texte fic  ------ }

procedure run_fic(icyc : integer);
var i,k : integer;
begin
  if ( icyc mod dt_texte_interp <> 0 ) then exit;
  for i := 1 to fic_nb do with fic[i] do
    begin
      write(f,t__:6);
      for k := 1 to var_fic_nb do with variable[var_fic[k]] do
        write(f,' ',val:1:precis[k]);
      writeln(f);
    end;
end;

procedure run_init_fic;
begin
  run_fic(0);
end;

procedure run_fin_fic;
var i : integer;
begin
  for i := 1 to fic_nb do Flush(fic[i].f);
end;

{ ------  texte interp  ------ }

procedure run_init_texte_interp;
var j : integer;
begin
  if texte_interp then
    begin
      write('t':5);
      for j := 1 to nb_vartexte_interp do
        with variable[vartexte_interp[j]] do write(s_ecri_dic(nom):10);
      writeln;
      write(t__:5);
      for j := 1 to nb_vartexte_interp do
        with variable[vartexte_interp[j]] do xwrite9(val);
      writeln;
    end;
end;

procedure run_texte_interp(icyc : integer);
var j : integer;
begin
  if texte_interp then
    if ( icyc mod dt_texte_interp = 0 ) then
      begin
        write(t__:5);
        for j := 1 to nb_vartexte_interp do
          with variable[vartexte_interp[j]] do
            xwrite9(val);
        writeln;
      end;
end;

procedure run_fin_texte_interp;
var j : integer;
begin
  write('t = ',t__:1);
  for j := 1 to nb_vartexte_interp do
    with variable[vartexte_interp[j]] do
      write(' ',s_ecri_dic(nom),' = ',s_ecri_val(val));
  writeln;
end;

{ ------  procedure run  ------ }

var p1 : array[1..modele_nb_max] of extended;
    t1 : integer;

procedure init_run;
var x : integer;
begin
  t1 := t__;
  for x := 1 to modele_nb do with modele[x] do p1[x] := pop;
end;

procedure fin_run;
var x : integer;
    a : extended;
begin
  for x := 1 to modele_nb do with modele[x] do
    begin
      writeln('Model ',s_ecri_dic(nom),' -> pop = ',pop:1:1);
      a := exp((ln0(pop) - ln0(pop0))/t__);
      writeln('  growth rate from [t = 0] -> ',a:1:6);
      if ( t1 > 0 ) then
        if ( t__ > t1 ) then
          begin
            a := exp((ln0(pop) - ln0(p1[x]))/(t__ - t1));
            writeln('  growth rate from [t = ', t1:1,'] -> ',a:1:6);
          end;
    end;
end;

procedure run(nb_cycle : integer);
var icyc : integer;
begin
  init_run;
  run_init_texte_interp;
  run_init_graph;
  run_init_fic;
  for icyc := 1 to nb_cycle do
    begin
      run_t;
      run_graph(icyc);
      run_texte_interp(icyc);
      run_fic(icyc);
    end;
  run_fin_graph;
  if not texte_interp then run_fin_texte_interp;
  run_fin_fic;
  fin_run;
end;

procedure run_param(nb_cycle,xp : integer;amin,amax,da : extended);
var icyc : integer;
    a,j : extended;
begin
  a := amin;
  j := 0.0;
  variable[xp].exp := ree_reg1;
  while ( a <= amax ) do 
    begin
      ree[ree_reg1].val := a;
      init_eval;  {init_eval1 ? jjjjj}
      writeln(s_ecri_variable(xp));
      init_run;
      for icyc := 1 to nb_cycle do
        begin
          run_t;
          {run_texte_interp(icyc);}
        end;
      run_fin_texte_interp;
      fin_run;
      j := j + 1.0;
      a := amin + j*da;
    end;
  param := false;
  variable[xparam].exp_type := param_exp_type_sav;
  variable[xparam].exp := param_exp_sav;
  init_eval;
  writeln('Init');
end;

end.

