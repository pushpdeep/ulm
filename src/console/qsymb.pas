{ ---------------------------------------------------------------------------- }
{ Copyright (c) 1995-2017, Stéphane Legendre and the ULM development team      }
{                                                                              }
{ This file is part of ULM.                                                    }
{                                                                              }
{ ULM is free software: you can redistribute it and/or modify it               }
{ under the terms of the GNU General Public License as published               }
{ by the Free Software Foundation, either version 3 of the License,            }
{ or (at your option) any later version.                                       }
{                                                                              }
{ ULM is distributed in the hope that it will be useful,                       }
{ but WITHOUT ANY WARRANTY; without even the implied warranty                  }
{ of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                      }
{ See the GNU General Public License for more details.                         }
{                                                                              }
{ You should have received a copy of the GNU General Public License            }
{ along with ULM.  If not, see <http://www.gnu.org/licenses/>.                 }
{ ---------------------------------------------------------------------------- }

unit qsymb;

{  @@@@@@   gestion symbolique   @@@@@@  }

interface

uses qglobvar;

function  cons(ca,typ,cd : integer) : integer;
function  list(x,tx : integer) : integer;

procedure init_pile;
procedure push(x : integer);
function  pop : integer;

function  cre_dic(mot : string) : integer;
function  s_get_dic(x : integer) : string;
function  trouve_dic(s : string) : integer; 

function  cre_ree(val1 : extended) : integer;

function  trouve_variable(nom1 : integer) : integer;
function  cre_variable(nom1 : integer) : integer;
procedure set_variable(x,exp1,exp_type1: integer);

function  trouve_fun(nom1 : integer) : integer;
function  cre_fun(nom1 : integer) : integer;
procedure set_fun(x,nb_arg1 : integer;xarg1 : larg_type);
function  cre_arg(nom1 : integer) : integer;

function  trouve_rel(nom1 : integer) : integer;
function  cre_rel(nom1 : integer) : integer;
procedure set_rel(x : integer;exp1,exp_type1,var1 : integer);

function  trouve_vec(nom1 : integer) : integer;
function  cre_vec(nom1,size1 : integer) : integer;
procedure set_vec(x : integer;exp1,exp_type1 : ivec_type);
function  trouve_mat(nom1 : integer) : integer;
function  cre_mat(nom1,size1 : integer) : integer;
procedure set_mat(x : integer;exp1,exp_type1 : imat_type;
                  repro_nb1 : integer;repro_i1,repro_j1 : ivec_type);

function  trouve_modele(nom1 : integer) : integer;
function  cre_modele(nom1 : integer) : integer;

function  trouve_fic(nam1 : string) : integer;
function  cre_fic(nam1 : string) : integer;

procedure trouve_obj(s : string;var x,tx : integer);
procedure init_symb;

var  err_symb : boolean;

implementation

const pile_nb_max = 1000;

var   pile : array[1..pile_nb_max] of integer;
      sp   : integer;

procedure erreur_symb(s : string);
begin
  writeln('Error: Symb - ',s);
end;

{ ------  listes  ------ } 
 
procedure init_lis; 
var x : integer; 
begin
  for x := 1 to lis_nb_max do lis[x].cdr := x + 1;
  lis[lis_nb_max].cdr := 0;
  lis_lib := 1; 
  lis_nb  := 0; 
end; 
 
procedure mark_lis(x : integer); 
var z : integer; 
begin 
  while ( x <> 0 ) do with lis[x] do
    begin 
      z := cdr; 
      if ( z < 0 ) then exit; 
      cdr := -z - 1; 
      if ( car_type = type_lis ) then mark_lis(car); 
      x := z; 
    end;  
end; 
 
procedure gc(ca,typ,cd : integer); 
var x,i,j,n : integer;
begin 
  if ( typ = type_lis ) then mark_lis(ca); 
  if ( cd <> 0 ) then mark_lis(cd); 
  mark_lis(dic);
  for x := 1 to sp do mark_lis(pile[x]);
  for x := 1 to variable_nb do with variable[x] do
    if ( exp_type = type_lis ) then mark_lis(exp); 
  for x := 1 to fun_nb do with fun[x] do
    if ( exp_type = type_lis ) then mark_lis(exp);
  for x := 1 to rel_nb do with rel[x] do
    if ( exp_type = type_lis ) then mark_lis(exp); 
  {for x := 1 to vec_nb do with vec[x] do
    for i := 1 to size do
      if ( exp_type[i] = type_lis ) then mark_lis(exp[i]); }
  for x := 1 to mat_nb do with mat[x] do
    for i := 1 to size do
      for j := 1 to size do
        if ( exp_type[i,j] = type_lis ) then mark_lis(exp[i,j]);
  n := 0; 
  for x := 1 to lis_nb_max do with lis[x] do
    if ( cdr >= 0 ) then 
      begin 
        cdr := lis_lib;
        lis_lib := x; 
        n := n + 1; 
      end 
    else 
      cdr := -cdr - 1; 
  lis_nb := lis_nb - n;
  {iwriteln('-> gc : ' + IntToStr(n) + ' cells free');}
end; 
 
function  cons(ca,typ,cd : integer) : integer; 
var x : integer; 
begin 
  if ( lis_lib = 0 ) then gc(ca,typ,cd); 
  if ( lis_lib = 0 ) then 
    begin 
      erreur_symb('no more room for lists');
      halt;
    end; 
  x := lis_lib; 
  with lis[x] do
    begin 
      lis_lib  := cdr; 
      car_type := typ; 
      car := ca; 
      cdr := cd; 
    end; 
  lis_nb := lis_nb + 1; 
  cons := x; 
end;

function  list(x,tx : integer) : integer;
begin
  list := cons(x,tx,0);
end;

{ ------ pile ------ }

procedure init_pile;
begin
  sp := 0;
end;

procedure push(x : integer);
begin
  sp := sp + 1;
  if ( sp > pile_nb_max ) then
    begin
      erreur_symb('ULM stack overflow');
      halt; { !!! }
    end;
  pile[sp] := x;
end;

function  pop : integer;
begin
  pop := pile[sp];
  sp  := sp - 1;
end;

{ ------  dictionnaire  ------ }

procedure init_dic; 
begin 
  dic     := 0; 
  dic_nb  := 0; 
  char_nb := 0;
end; 
 
function  cre_chaine(mot : string) : integer; 
var x : integer; 
    n,i : byte;
begin 
  n := length(mot); 
  x := 0;
  for i := n downto 1 do x := cons(ord(mot[i]),type_char,x);
  cre_chaine := x; 
end; 
 
function  cre_dic(mot : string) : integer;
var n : byte; 
    x : integer;
begin 
  n := length(mot);
  x := cre_chaine(mot);
  dic := cons(x,type_lis,dic); 
  dic_nb  := dic_nb + 1; 
  char_nb := char_nb + n;
  cre_dic := x; 
end; 
 
function  s_get_dic(x : integer): string;
var s : string;
begin 
  s := '';
  while ( x <> 0 ) do with lis[x] do
    begin 
      s := s + chr(car);
      x := cdr; 
    end;
  s_get_dic := s;
end; 
 
function  trouve_dic(s : string) : integer; 
var x : integer; 
    mot : string; 
begin 
  x := dic;
  while ( x <> 0 ) do with lis[x] do
    begin
      mot := s_get_dic(car);
      if ( mot = s ) then 
        begin
          trouve_dic := car;
          exit;
        end;  
      x := cdr;   
    end;
  trouve_dic := 0;
end; 

{ ------  reels  ------ }

procedure mark_ree_lis(x : integer);
begin
  while ( x <> 0 ) do with lis[x] do
    begin
      if ( car_type = type_lis ) then mark_ree_lis(car);
      if ( car_type = type_ree ) then ree[car].mark := 1;
      x := cdr;
    end;
end;

procedure mark_ree;
var x,i,j : integer;
begin
  for x := 1 to ree_nb_max do ree[x].mark := 0;
  ree[ree_zero].mark := 1;
  ree[ree_un  ].mark := 1;
  ree[ree_deux].mark := 1;
  ree[ree_reg1].mark := 1;
  ree[ree_reg2].mark := 1;
  for x := 1 to variable_nb do with variable[x] do
    begin
      if ( exp_type = type_ree ) then ree[exp].mark := 1;
      if ( exp_type = type_lis ) then mark_ree_lis(exp);
    end;
  for x := 1 to fun_nb do with fun[x] do
    begin
      if ( exp_type = type_ree ) then ree[exp].mark := 1;
      if ( exp_type = type_lis ) then mark_ree_lis(exp);
    end;
  for x := 1 to vec_nb do with vec[x] do
    for i := 1 to size do
      begin
        if ( exp_type[i] = type_ree ) then ree[exp[i]].mark := 1;
        if ( exp_type[i] = type_lis ) then mark_ree_lis(exp[i]);
      end;
  for x := 1 to mat_nb do with mat[x] do
    for i := 1 to size do
      for j := 1 to size do
        begin
          if ( exp_type[i,j] = type_ree ) then ree[exp[i,j]].mark := 1;
          if ( exp_type[i,j] = type_lis ) then mark_ree_lis(exp[i,j]);
        end;
end;        
        
procedure libere_ree;
var x,y,n: integer;
begin
  mark_ree;
  n := 0;
  x := ree_deb;
  while ( x <> 0 ) do
    begin
      y := ree[x].suiv;
      if ( y <> 0 ) then
        if ( ree[y].mark = 0 ) then
          begin
            ree[x].suiv := ree[y].suiv;
            ree[y].suiv := ree_lib;
            ree_lib := y;
            n := n + 1;
          end
        else
          x := y
      else
       x := y;
    end;
  ree_nb := ree_nb - n;
  {iwriteln('-> ' + IntToStr(n) + ' reals free');}
end;

function  cre_ree(val1 : extended) : integer;
var x : integer;
begin
  if ( ree_lib = 0 ) then libere_ree;
  if ( ree_lib = 0 ) then
    begin
      erreur_symb('too many real constants');
      halt;
    end;
  if ( val1 = 0.0 ) then 
    begin
      cre_ree := ree_zero;
      exit;
    end;
  if ( val1 = 1.0 ) then 
    begin
      cre_ree := ree_un;
      exit;
    end;
  if ( val1 = 2.0 ) then 
    begin
      cre_ree := ree_deux;
      exit;
    end;
  x := ree_lib;
  with ree[x] do
    begin
      val     := val1;
      ree_lib := suiv;
      suiv    := ree_deb;
    end;
  ree_deb := x;
  ree_nb  := ree_nb + 1;
  cre_ree := x;
end;

procedure init_ree;
var x : integer;
begin
  for x := 1 to ree_nb_max do ree[x].suiv := x + 1;
  ree[ree_nb_max].suiv := 0;
  ree_zero := 1;
  ree[ree_zero].val  := 0.0;
  ree[ree_zero].suiv := 0;
  ree_un   := 2;
  ree[ree_un  ].val  := 1.0;
  ree[ree_un  ].suiv := 1;
  ree_deux := 3;
  ree[ree_deux].val  := 2.0;
  ree[ree_deux].suiv := 2;
  ree_reg1 := 4;
  ree[ree_reg1].val  := 0.0;
  ree[ree_reg1].suiv := 3;
  ree_reg2 := 5;
  ree[ree_reg2].val  := 0.0;
  ree[ree_reg2].suiv := 4;
  ree_lib := 6; 
  ree_deb := 5;
  ree_nb  := 3;
end;

{ ------  variables  ------ }
 
function  trouve_variable(nom1 : integer) : integer;
var x : integer;
begin
  for x := 1 to variable_nb do with variable[x] do
    if ( nom  = nom1 ) then
      begin
        trouve_variable := x;
        exit;
      end;
  trouve_variable := 0;
end;

function  cre_variable(nom1 : integer) : integer;
var i : integer;
begin
  variable_nb := variable_nb + 1;
  if ( variable_nb > variable_nb_max ) then
    begin
      erreur_symb('too many variables');
      halt;
    end;
  with variable[variable_nb] do
    begin
      nom  := nom1;
      xrel := 0;
      xvec := 0;
      exp  := 0;
      exp_type := type_inconnu;
      val0 := 0.0;
      val  := 0.0;
      te   := 0;
      for i := 1 to maxprev do prev[i] := 0.0;
    end;
  cre_variable := variable_nb;
end;

procedure set_variable(x,exp1,exp_type1: integer);
begin
  with variable[x] do
    begin
      exp_type := exp_type1;
      exp := exp1;
    end;
end;

function  cre_variable_0(s : string;a : extended) : integer;
var nom,x : integer;
begin
  nom := cre_dic(s);
  x   := cre_variable(nom);
  set_variable(x,cre_ree(a),type_ree);
  variable_nb_predef := variable_nb_predef + 1;
  cre_variable_0 := x;
end;

procedure init_variable; 
begin
  variable_nb := 0; 
  variable_nb_predef := 0; 
  xtime := cre_variable_0('t',0.0); 
end; 

{ ------  fonctions  ------ }

function  trouve_arg(nom1 : integer) : integer;
var x : integer;
begin
  for x := 1 to arg_nb do with arg[x] do
    if ( nom  = nom1 ) then
      begin
        trouve_arg := x;
        exit;
      end;
  trouve_arg := 0;
end;

function  cre_arg(nom1 : integer) : integer;
begin
  arg_nb := arg_nb + 1;
  if ( arg_nb > arg_nb_max ) then
    begin
      erreur_symb('too many function arguments');
      halt;
    end;
  with arg[arg_nb] do
    begin
      nom := nom1;
      val := 0.0;
    end;
  cre_arg := arg_nb;
end;

procedure init_arg;
begin
  arg_nb := 0;
end;

function  trouve_fun(nom1 : integer) : integer;
var x : integer;
begin
  for x := 1 to fun_nb do with fun[x] do
    if ( nom  = nom1 ) then
      begin
        trouve_fun := x;
        exit;
      end;
  trouve_fun := 0;
end;

function  cre_fun(nom1 : integer) : integer;
var i : integer;
begin
  fun_nb := fun_nb + 1;
  if ( fun_nb > fun_nb_max ) then
    begin
      erreur_symb('too many functions');
      halt;
    end;
  with fun[fun_nb] do
    begin
      nom    := nom1;
      nb_arg := 0;
      for i := 1 to larg_nb_max do xarg[i] := 0;
      exp := 0;
      exp_type := type_inconnu;
    end;
  cre_fun := fun_nb;
end;

procedure set_fun(x,nb_arg1 : integer;xarg1 : larg_type);
begin
  with fun[x] do
    begin
      nb_arg := nb_arg1;
      xarg   := xarg1;
    end;
end;

function  cre_fun_0(s : string;nb_arg : integer) : integer;
var nom,x,i : integer;
    xarg : larg_type;
begin
  nom := cre_dic(s);
  x := cre_fun(nom);
  for i := 1 to nb_arg do xarg[i] := cre_arg(1);
  set_fun(x,nb_arg,xarg);
  fun[x].exp := 0;
  fun[x].exp_type := type_lis;
  fun_nb_predef := fun_nb_predef + 1;
  cre_fun_0 := x;
end;

procedure init_fun;
begin
  fun_nb := 0;
  fun_nb_predef := 0;
  fun_if        := cre_fun_0('if',3);
  fun_min       := cre_fun_0('min',0);
  fun_max       := cre_fun_0('max',0);
  fun_stepf     := cre_fun_0('stepf',3);
  fun_gaussf    := cre_fun_0('gaussf',2);
  fun_lognormf  := cre_fun_0('lognormf',2);
  fun_binomf    := cre_fun_0('binomf',2);
  fun_poissonf  := cre_fun_0('poissonf',2);
  fun_nbinomf   := cre_fun_0('nbinomf',2);
  fun_nbinom1f  := cre_fun_0('nbinom1f',2);
  fun_betaf     := cre_fun_0('betaf',2);
  fun_beta1f    := cre_fun_0('beta1f',2);
  fun_tabf      := cre_fun_0('tabf',0);
  fun_bicof     := cre_fun_0('bicof',2);
  fun_gratef    := cre_fun_0('gratef',1);
  fun_bdf       := cre_fun_0('bdf',4);
  fun_lambdaf   := cre_fun_0('lambdaf',2);
  fun_prevf     := cre_fun_0('prevf',2);
  fun_textf     := cre_fun_0('textf',1);

  fun_meanf      := cre_fun_0('meanf',1);
  fun_variancef  := cre_fun_0('variancef',1);
  fun_skewnessf  := cre_fun_0('skewnessf',1);
  fun_cvf        := cre_fun_0('cvf',1);
  fun_meanzf     := cre_fun_0('meanzf',1);
  fun_variancezf := cre_fun_0('variancezf',1);
  fun_cvzf       := cre_fun_0('cvzf',1);
  fun_nzf        := cre_fun_0('nzf',1);
  fun_nef        := cre_fun_0('nef',1);
  fun_nif        := cre_fun_0('nif',1);
  fun_extratef   := cre_fun_0('extratef',1);
  fun_immratef   := cre_fun_0('immratef',1);

end;

{ ------  relations  ------ }

procedure init_rel;
begin
  rel_nb := 0;
end;

function  trouve_rel(nom1 : integer) : integer;
var x : integer;
begin
  for x := 1 to rel_nb do with rel[x] do
    if ( nom = nom1 ) then
      begin
        trouve_rel := x;
        exit;
      end;
  trouve_rel := 0;
end;

function  cre_rel(nom1 : integer) : integer;
begin
  rel_nb := rel_nb + 1;
  if ( rel_nb > rel_nb_max ) then
    begin
      erreur_symb('too many relations');
      halt;
    end;
  with rel[rel_nb] do
    begin
      nom := nom1;
      xmodele := 0;
      exp_type := type_inconnu;
      exp  := 0;
      val0 := 0.0;
      val  := 0.0;
    end;
  cre_rel := rel_nb;
end;

procedure set_rel(x : integer;exp1,exp_type1,var1 : integer); 
begin 
  with rel[x] do
    begin 
      exp  := exp1; 
      exp_type := exp_type1; 
      xvar := var1; 
    end; 
end;

{ ------  vecteurs  ------ }

procedure init_vec;
begin
  vec_nb := 0;
end;

function  trouve_vec(nom1 : integer) : integer;
var x : integer;
begin
  for x := 1 to vec_nb do with vec[x] do
    if ( nom = nom1 ) then
      begin
        trouve_vec := x;
        exit;
      end;
  trouve_vec := 0;
end;

function  cre_vec(nom1,size1 : integer) : integer;
var i : integer;
begin
  vec_nb := vec_nb + 1;
  if ( vec_nb > vec_nb_max ) then
    begin
      erreur_symb('too many vectors');
      halt;
    end;
  with vec[vec_nb] do
    begin
      nom  := nom1;
      size := size1;
      xmodele := 0;
      for i := 1 to size do 
        begin
          exp_type[i] := type_inconnu;
          exp[i]  := 0;
          val0[i] := 0.0;
          val[i]  := 0.0;
        end;
    end;
  cre_vec := vec_nb;
end;

procedure set_vec(x : integer;exp1,exp_type1 : ivec_type);
begin
  with vec[x] do
    begin
      exp_type := exp_type1;
      exp  := exp1;
    end;
end;

{ ------  matrices  ------ }

procedure init_mat;
begin
  mat_nb := 0;
end;

function  trouve_mat(nom1 : integer) : integer;
var x : integer;
begin
  for x := 1 to mat_nb do with mat[x] do
    if ( nom = nom1 ) then
      begin
        trouve_mat := x;
        exit;
      end;
  trouve_mat := 0;
end;

function  cre_mat(nom1,size1 : integer) : integer;
var i,j : integer;
begin
  mat_nb := mat_nb + 1;
  if ( mat_nb > mat_nb_max ) then
    begin
      erreur_symb('too many matrices');
      halt;
    end;
  with mat[mat_nb] do
    begin
      nom  := nom1;
      size := size1;
      xmodele := 0;
      for i := 1 to size do
        for j := 1 to size do
          begin
            exp_type[i,j] := type_inconnu;
            exp[i,j]  := 0;
            val0[i,j] := 0.0;
            val[i,j]  := 0.0;
          end;
    end;
  cre_mat := mat_nb;
end;

procedure set_mat(x : integer;exp1,exp_type1 : imat_type;
                  repro_nb1 : integer;repro_i1,repro_j1 : ivec_type);
begin
  with mat[x] do
    begin
      exp_type := exp_type1;
      exp      := exp1;
      repro_nb := repro_nb1;
      repro_i  := repro_i1;
      repro_j  := repro_j1;
    end;
end;

{ ------  modeles  ------ }

procedure init_modele;
begin
  modele_nb := 0;
end;

function  trouve_modele(nom1 : integer) : integer;
var x : integer;
begin
  for x := 1 to modele_nb do with modele[x] do
    if ( nom = nom1 ) then
      begin
        trouve_modele := x;
        exit;
      end;
  trouve_modele := 0;
end;

function  cre_modele(nom1 : integer) : integer;
var i : integer;
begin
  modele_nb := modele_nb + 1;
  if ( modele_nb > modele_nb_max ) then
    begin
      erreur_symb('too many models');
      halt;
    end;
  with modele[modele_nb] do
    begin
      nom  := nom1;
      size := 0;
      xmat := 0;
      xvec := 0;
      for i := 1 to matmax do xrel[i] := 0;
      pop0 := 0;
      pop  := 0;
    end;
  cre_modele := modele_nb;
end;

{ ------  fichiers de sortie  ------ }

procedure init_fic;
begin
  fic_nb := 0;
end;

function  trouve_fic(nam1 : string) : integer;
var i : integer;
begin
  for i := 1 to fic_nb do with fic[i] do
    if ( nam = nam1 ) then
      begin
        trouve_fic := i;
        exit;
      end;
  trouve_fic := 0;
end;

function  cre_fic(nam1 : string) : integer;
var i : integer;
begin
  fic_nb := fic_nb + 1;
  if ( fic_nb > fic_nb_max ) then
    begin
      erreur_symb('too many files');
      exit;
    end;
  with fic[fic_nb] do
    begin
      nam := nam1;
      var_fic_nb := 0;
      for i := 1 to var_fic_nb_max do
        begin
          var_fic[i] := 0;
          precis[i]  := 4;
        end;
    end;
  cre_fic := fic_nb;
end;

{ ------ }

procedure trouve_obj(s : string;var x,tx : integer);
var nom : integer;
begin
  x  := 0;
  tx := 0;
  nom := trouve_dic(s);
  if ( nom = 0 ) then exit;
  x := trouve_variable(nom);
  if ( x <> 0 ) then
    begin
      tx := type_variable;
      exit
    end;
  x := trouve_fun(nom);
  if ( x <> 0 ) then
    begin
      tx := type_fun;
      exit
    end;
  x := trouve_rel(nom);
  if ( x <> 0 ) then
    begin
      tx := type_rel;
      exit
    end;
  x := trouve_vec(nom);
  if ( x <> 0 ) then
    begin
      tx := type_vec;
      exit
    end;
  x := trouve_mat(nom);
  if ( x <> 0 ) then
    begin
      tx := type_mat;
      exit
    end;
  x := trouve_modele(nom);
  if ( x <> 0 ) then
    begin
      tx := type_modele;
      exit
    end;
end;

{ ------  init  ------ }

procedure init_symb;
begin
  init_lis;
  init_pile;
  init_dic; 
  init_ree;
  init_variable;
  init_arg;
  init_fun;
  init_rel;
  init_vec;
  init_mat;
  init_modele;
  init_fic;
end;

end.
