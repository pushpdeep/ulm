{ ---------------------------------------------------------------------------- }
{ Copyright (c) 1995-2017, Stéphane Legendre and the ULM development team      }
{                                                                              }
{ This file is part of ULM.                                                    }
{                                                                              }
{ ULM is free software: you can redistribute it and/or modify it               }
{ under the terms of the GNU General Public License as published               }
{ by the Free Software Foundation, either version 3 of the License,            }
{ or (at your option) any later version.                                       }
{                                                                              }
{ ULM is distributed in the hope that it will be useful,                       }
{ but WITHOUT ANY WARRANTY; without even the implied warranty                  }
{ of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                      }
{ See the GNU General Public License for more details.                         }
{                                                                              }
{ You should have received a copy of the GNU General Public License            }
{ along with ULM.  If not, see <http://www.gnu.org/licenses/>.                 }
{ ---------------------------------------------------------------------------- }

unit gprop;

{$MODE Delphi}

{  @@@@@@   form for displaying matrix properties   @@@@@@  }

interface

uses SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, ExtCtrls, Grids,
     jglobvar, jmath;

type
  Tform_prop = class(TForm)
    Panel1: TPanel;
    groupbox_prop: TGroupBox;
    Label_matrix: TLabel;
    Edit_matrix: TEdit;
    Label_nonneg: TLabel;
    Label_pos: TLabel;
    label_prim: TLabel;
    Label_irr: TLabel;
    Label_leslie: TLabel;
    Label_leslie2: TLabel;
    Label_multi: TLabel;
    Label_sizeclass: TLabel;
    Label_random: TLabel;
    Label_vecdep: TLabel;
    Label_timedep: TLabel;
    GroupBox_eigen: TGroupBox;
    StringGrid_eigenval: TStringGrid;
    StringGrid_eigenvec: TStringGrid;
    edit_lambda: TEdit;
    label_lambda: TLabel;
    groupbox_dem: TGroupBox;
    label_growth_rate: TLabel;
    label_r: TLabel;
    label_rho: TLabel;
    label_p: TLabel;
    label_r0: TLabel;
    label_t0: TLabel;
    label_tc: TLabel;
    label_tb: TLabel;
    label_s: TLabel;
    label_e: TLabel;
    label_h: TLabel;
    label_phi: TLabel;
    label_sigma2: TLabel;
    label_gamma: TLabel;
    spacer_1 : TLabel;
    spacer_2 : TLabel;
    spacer_3 : TLabel;
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure init_prop;
    procedure erreur(s : string);
    procedure proprietes(x : integer);
    procedure prop_gene(x : integer);
    procedure eigenval(x : integer;var err : boolean);
    procedure eigenvec(x : integer);
    procedure dem_leslie(x : integer);
    procedure dem_leslie2(x : integer);
    procedure dem_qq(x : integer);
    procedure entropy(x : integer;var err : boolean);
    procedure returntime(x : integer);
    procedure gentime(x : integer);
    procedure netreprorate(x : integer);
    procedure Edit_matrixReturnPressed(Sender: TObject);
  private
    lambda : cvec_type; { complex eigenvalues }
    v,w : rvec_type;    { left and right eigenvectors associated with lambda1 }
    lambda1 : extended; { dominant eigenvalue }
    r : extended;       { intrinsic rate of natural increase }
    rho : extended;     { damping ratio }
    pp : extended;      { period }
    r0 : extended;      { net reproductive rate }
    t0 : extended;      { R0-generation time }
    tc : extended;      { cohort generation time }
    tb : extended;      { mean generation length }
    h  : extended;      { entropy rate }
    fi : extended;      { phi Demetrius }
    sigma2 : extended;  { variance Demetrius }
    gamma  : extended;  { parameter Demetrius }
    s  : extended;      { demographic entropy }
    e  : extended;      { demographic uncertainty }
    leslie   : boolean; { indicator that matrix is Leslie }
    leslie2  : boolean; { indicator that matrix is extended Leslie }
    multi    : boolean; { indicator that matrix is multisite }
    sizeclass : boolean; { indicator that matrix is size-classified }
  public
    x_mat : integer;    { pointer to matrix }
    psite : integer;    { number of sites for multisite matrix }
    interp_ : boolean;  { indicator to write output in main window }
  end;

var form_prop: Tform_prop;
    mp : rmat_type;  { markov matrix associated with population matrix }
    matf,mats,matn : rmat_type; // decomposition A = S + F, N = (I - S)^{-1}
    vp : rvec_type;  { left eigenvector of markov matrix mp }

implementation

uses jutil,jsymb,jmatrix,jsyntax;

{$R *.lfm}

procedure tform_prop.erreur(s : string);
{ unit specific error procedure }
begin
  erreur_('Properties - ' + s);
end;

procedure tform_prop.dem_leslie(x : integer);
{ demographic descriptors for Leslie matrix of index x }
var i,j,ic : integer;
    f,p,phi,pp : rvec_type;
    a,q,s1,s2,s3,kappa,maxp,maxv : extended;
begin
  with mat[x] do
    begin
      for j := 1 to size   do f[j] := val[1,j]; { fertilities }
      for i := 1 to size-1 do p[i] := val[i+1,i]; { survival probabilities }
      a := 1.0;
      q := 1.0;
      maxp := 0.0;
      maxv := 0.0;
      ic   := 0;
      for i := 1 to size do
        begin
          if ( i >= 2 ) then q := q*p[i-1];
          phi[i] := f[i]*q; { maternity functions }
          a := a/lambda1;
          pp[i] := phi[i]*a;
          if ( phi[i] > maxp ) then
            begin
              ic := i;
              maxp := phi[i];
              maxv := pp[i];
            end;
        end;
      r0 := 0.0;
      tb := 0.0;
      tc := 0.0;
      s  := 0.0;
      e  := 0.0;
      for i := 1 to size do
        begin
          r0 := r0 + phi[i];   { net reproductive rate }
          tc := tc + i*phi[i]; { cohort generation time }
          tb := tb + i*pp[i];  { mean generation length }
          s  := s + pp[i]*ln0(pp[i]); { demographic entropy }
          e  := e + pp[i]*ln0(phi[i]); { demographic uncertainty }
        end;
      tc := tc/r0;
      s := -s;
      h  := s/tb;
      fi := e/tb;
      s1 := 0.0;
      s2 := 0.0;
      s3 := 0.0;
      for i := 1 to size do
        begin
          q  := -i*fi + ln0(phi[i]);
          s1 := s1 + i*pp[i]*q;
          s2 := s2 + pp[i]*q*q;
          s3 := s3 + pp[i]*q*q*q;
        end;
      sigma2 := s2/tb;
      kappa  := s3 - 3.0*s3*s1/tb + 2.0*s2;
      gamma  := kappa + 2.0*sigma2;
      if ( r <> 0.0 ) then t0 := ln(r0)/r else t0 := 0.0; { T0 }
      label_r0.Caption := 'Net reproductive rate R₀ = ' + Format('%1.4g',[r0]);
      if interp_ then iwriteln(label_r0.Caption);
      label_t0.Caption := 'R₀ generation time T₀ = ' + Format('%1.4g',[t0]);
      if interp_ then iwriteln(label_t0.Caption);
      label_tc.Caption := 'Cohort generation time Tc = ' + Format('%1.4g',[tc]);
      if interp_ then iwriteln(label_tc.Caption);
      label_tb.Caption := 'Mean generation length T = ' + Format('%1.4g',[tb]);
      if interp_ then iwriteln(label_tb.Caption);
      label_s.Caption := 'Demographic entropy S = ' + Format('%1.4g',[s]);
      if interp_ then iwriteln(label_s.Caption);
      label_e.Caption := 'Demographic uncertainty E = ' + Format('%1.4g',[e]);
      if interp_ then iwriteln(label_e.Caption);
      label_h.Caption := 'Entropy rate H = ' + Format('%1.4g',[h]);
      if interp_ then iwriteln(label_h.Caption);
      label_phi.Caption := 'Reproductive potential Phi = ' + Format('%1.4g',[fi]);
      if interp_ then iwriteln(label_phi.Caption);
      label_sigma2.Caption := 'Entropic variance Sigma2 = ' + Format('%1.4g',[sigma2]);
      if interp_ then iwriteln(label_sigma2.Caption);
      label_gamma.Caption := 'Correlation index Gamma = ' + Format('%1.4g',[gamma]);
      if interp_ then iwriteln(label_gamma.Caption);
      if interp_ then
          iwriteln('Age at max progeny = ' + IntToStr(ic) +
                   ', MaxP = ' + Format('%1.4g',[maxp]) +
                   ', MaxV = ' + Format('%1.4g',[maxv]));
    end;
end;

procedure tform_prop.dem_leslie2(x : integer);
{ demographic descriptors for extended Leslie matrix of index x }
{ it is assumed that adult survival rate 0 < a(n,n) < 1 }
const eps = 0.00001;
var i,j,n : integer;
    f,p,phi,pp : rvec_type;
    a,b,d,q,s1,s2,s3,kappa,d2,d3 : extended;
begin
  with mat[x] do
    begin
      n := size;
      for j := 1 to n   do f[j] := val[1,j]; { fertilities }
      for i := 1 to n-1 do p[i] := val[i+1,i];
      p[n] := val[n,n]; { survival probabilities }
      a := 1.0;
      q := 1.0;
      for i := 1 to n do
        begin
          if ( i >= 2 ) then q := q*p[i-1];
          phi[i] := f[i]*q; { net fertility functions }
          a := a/lambda1;
          pp[i] := phi[i]*a;
        end;
      r0 := 0.0;
      tc := 0.0;
      tb := 0.0;
      s  := 0.0;
      e  := 0.0;
      for i := 1 to n-1 do
        begin
          r0 := r0 + phi[i];   { net reproductive rate }
          tc := tc + i*phi[i]; { cohort generation time }
          tb := tb + i*pp[i];  { mean generation length }
          s  := s + pp[i]*ln0(pp[i]);  { demographic entropy }
          e  := e + pp[i]*ln0(phi[i]); { demographic uncertainty }
        end;
      r0 := r0 + phi[n]/(1.0 - p[n]); { net reproductive rate }
      tc := tc + phi[n]*(n - (n-1)*p[n])/sqr(1 - p[n]);
      tc := tc/r0; { cohort generation time }
      q  := p[n]/lambda1;
      tb := tb + pp[n]*(n - (n-1)*q)/sqr(1.0 - q);  { mean generation length }
      s  := s + pp[n]*(ln0(pp[n])/(1.0 - q) + q*ln0(q)/sqr(1.0 - q));
      e  := e + pp[n]*(ln0(phi[n])/(1.0 - q) + q*ln0(p[n])/sqr(1.0 - q));
      s  := -s;
      h  := s/tb;
      fi := e/tb;
      s1 := 0.0;
      s2 := 0.0;
      s3 := 0.0;
      for i := 1 to n do
        begin
          q := -i*fi + ln0(phi[i]);
          s1 := s1 + i*pp[i]*q;
          s2 := s2 + pp[i]*q*q;
          s3 := s3 + pp[i]*q*q*q;
        end;
      a := -n*fi + ln0(phi[n]);
      b := pp[n];
      i := 0;
      repeat
        i := i + 1;
        q := a + i*ln0(p[n]- fi);
        b := b*p[n]/lambda1;
        s1 := s1 + (n + i)*b*q;
        d2 := s2;
        s2 := s2 + b*q*q;
        d3 := s3;
        s3 := s3 + b*q*q*q;
        d := max(abs(s2 - d2),abs(s3 - d3));
      until ( i > 1000 ) or ( d < eps );
      sigma2 := s2/tb;
      kappa  := s3 - 3.0*s3*s1/tb + 2.0*s2;
      gamma  := kappa + 2.0*sigma2;
      if ( r <> 0.0 ) then t0 := ln(r0)/r else t0 := 0.0;
      label_r0.Caption := 'Net reproductive rate R₀ = ' + Format('%1.4g',[r0]);
      if interp_ then iwriteln(label_r0.Caption);
      label_t0.Caption := 'R₀ generation time T₀ = ' + Format('%1.4g',[t0]);
      if interp_ then iwriteln(label_t0.Caption);
      label_tc.Caption := 'Cohort generation time Tc = ' + Format('%1.4g',[tc]);
      if interp_ then iwriteln(label_tc.Caption);
      label_tb.Caption := 'Mean generation length T = ' + Format('%1.4g',[tb]);
      if interp_ then iwriteln(label_tb.Caption);
      label_s.Caption := 'Demographic entropy S = ' + Format('%1.4g',[s]);
      if interp_ then iwriteln(label_s.Caption);
      label_e.Caption := 'Demographic uncertainty E = ' + Format('%1.4g',[e]);
      if interp_ then iwriteln(label_e.Caption);
      label_h.Caption := 'Entropy rate H = ' + Format('%1.4g',[h]);
      if interp_ then iwriteln(label_h.Caption);
      label_phi.Caption := 'Reproductive potential Phi = ' + Format('%1.4g',[fi]);
      if interp_ then iwriteln(label_phi.Caption);
      label_sigma2.Caption := 'Entropic variance Sigma2 = ' + Format('%1.4g',[sigma2]);
      if interp_ then iwriteln(label_sigma2.Caption);
      label_gamma.Caption := 'Correlation index Gamma = ' + Format('%1.4g',[gamma]);
      if interp_ then iwriteln(label_gamma.Caption);
    end;
end;

procedure tform_prop.returntime(x : integer);
{ return times in complex general life cycle }
var i,j,a,n : integer;
    vq : rvec_type;
begin
  with mat[x] do
    begin
      n := size;
      for i := 1 to n do
        for j := 1 to n do
          mp[i,j] := val[i,j]*w[j]/(lambda1*w[i]); // Markov matrix
      matkovvecg(n,mp,vp); // stationary distribution
      iwriteln('Computation of return times:');
      a := 0;
      for i := 1 to n do
        for j := 1 to n do
          if ( mp[i,j] > 0.0 ) then
            begin
              a := a + 1;
              vq[a] := mp[i,j]*vp[i];
              if ( vq[a] = 0.0 ) then
                iwriteln(IntToStr(a) + ' i = ' + IntToStr(i) +
                         ' vp = ' + FloatToStr(vp[i]))
              else
                iwriteln('   ' + IntToStr(a) + '   ' +
                         IntToStr(j) + '->' + IntToStr(i) +
                         '    Ta'  + IntToStr(a) + ' = ' + s_ecri_val(1.0/vq[a]) + ' ' +
                         '   1/Ta' + IntToStr(a) + ' = ' + s_ecri_val(vq[a]));
            end;
      iwriteln('Return time TZ to set of transitions Z is given by 1/TZ = 1/Ta + 1/Tb + ... with a, b, ... in Z');
    end;
end;

procedure  tform_prop.gentime(x : integer);
{ generation time T (mean generation length) }
{ T = return time to reproductive transitions }
var a : integer;
    sum : extended;
begin
  with mat[x] do
    begin
      { stationary distribution vp computed in procedure entropy }
      sum := 0.0;
      for a := 1 to repro_nb do
        sum := sum + mp[repro_i[a],repro_j[a]]*vp[repro_i[a]];
      if ( sum > 0.0 ) then
        tb := 1.0/sum
      else
        tb := 0.0;
      label_tb.Caption := 'Mean generation length T = ' + Format('%1.4g',[tb]);
      if interp_ then iwriteln(label_tb.Caption);
    end;
end;

procedure tform_prop.netreprorate(x : integer);
{ net reproductive rate R0 of general matrix }
{ R0 = dominant eigenvalue of matrix FN }
const eps = 1.0E-12;
var i,j,k,a,n : integer;
    sum,norm,norm1 : extended;
    ma,mb : rmat_type;
    lamb : cvec_type;
begin
  with mat[x] do
    begin
      n := size;
      { decomposition A = S + F }
      for i := 1 to n do
        for j := 1 to n do
          begin
            mats[i,j] := val[i,j];
            matf[i,j] := 0.0;
          end;
      for a := 1 to repro_nb do
        begin
          matf[repro_i[a],repro_j[a]] := val[repro_i[a],repro_j[a]];
          mats[repro_i[a],repro_j[a]] := 0.0;
        end;
      // computation of N = I + S + S^2 + ... = (I - S)^{-1}
      for i := 1 to n do
        for j := 1 to n do
          if ( i = j ) then
            matn[i,j] := 1.0
          else
            matn[i,j] := 0.0; // N = I
      for i := 1 to n do
        for j := 1 to n do
          ma[i,j] := matn[i,j]; // ma = I
      norm := 1.0;
      a := 0;
      repeat
        a := a + 1;
        for i := 1 to n do
          for j := 1 to n do
            begin
              sum := 0.0;
              for k := 1 to n do sum := sum + ma[i,k]*mats[k,j];
              mb[i,j] := sum;
            end;
        for i := 1 to n do
          for j := 1 to n do
            ma[i,j] := mb[i,j]; // ma = S^k
        norm1 := norm;
        norm := 0.0;
        for i := 1 to n do
          for j := 1 to n do
            norm := max(norm,abs(ma[i,j]));
        for i := 1 to n do
          for j := 1 to n do
            matn[i,j] := matn[i,j] + ma[i,j]; // N = I + S + S^2 + ...
      until ( a >= 10000 ) or ( norm < eps );
      if ( norm1 = norm ) then { convergence problem }
        begin
          iwriteln('R₀ - convergence');
          exit;
        end;
      for i := 1 to n do
        for j := 1 to n do
          begin
            sum := 0.0;
            for k := 1 to n do sum := sum + matf[i,k]*matn[k,j];
            mb[i,j] := sum; // mb = FN
          end;
      matvalprop(n,mb,lamb); { dominant eigenvalue }
      if err_math then
        begin
          erreur('R₀ - Error in eigenvalue computation FN');
          err_math := false;
          exit;
        end;
      if ( lamb[1].im <> 0.0 ) then
        begin
          erreur('R₀ - No real dominant eigenvalue found');
          exit;
        end;
      r0 := lamb[1].re;
      if ( r0 <= 0.0 ) then
        begin
          r0 := 0.0;
          exit;
        end;
      label_r0.Caption := 'Net reproductive rate R₀ = ' + Format('%1.4g',[r0]);
      if interp_ then iwriteln(label_r0.Caption);
      if ( r <> 0.0 ) then t0 := ln(r0)/r else t0 := 0.0; // T0
      label_t0.Caption := 'R₀-generation time T₀ = ' + Format('%1.4g',[t0]);
      if interp_ then iwriteln(label_t0.Caption);
    end;
end;

procedure tform_prop.entropy(x : integer;var err : boolean);
{ entropy rate H of general matrix }
var n,i,j : integer;
    hi : extended;
begin
  with mat[x] do
    begin
      n := size;
      for i := 1 to n do
        if ( w[i] = 0.0 ) then { reducible matrix }
          begin
            err := true;
            iwriteln('matrix is reducible');
            exit;
          end;
      for i := 1 to n do
        for j := 1 to n do
          mp[i,j] := val[i,j]*w[j]/(lambda1*w[i]);
      matkovvecg(n,mp,vp);
      h := 0.0;
      for i := 1 to n do
        begin
          hi := 0.0;
          for j := 1 to n do hi := hi + mp[i,j]*ln0(mp[i,j]);
          h := h + vp[i]*hi;
        end;
      h := -h;
      label_h.Caption := 'Entropy rate H = ' + Format('%1.4g',[h]);
      if interp_ then iwriteln(label_h.Caption);
    end;
end;

procedure tform_prop.dem_qq(x : integer);
{ demographic descriptors of general matrix }
var err : boolean;
begin
  with mat[x] do
    begin
      label_r0.Caption := '';
      label_t0.Caption := '';
      label_tc.Caption := '';
      label_tb.Caption := '';
      label_s.Caption  := '';
      label_e.Caption  := '';
      err := false;
      entropy(x,err);
      if err then label_h.Caption := '';
      label_phi.Caption := '';
      label_sigma2.Caption := '';
      label_gamma.Caption  := '';
      if ( repro_nb = 0 ) then exit;
      netreprorate(x);
      gentime(x);
    end;
end;

procedure tform_prop.prop_gene(x : integer);
{ type of matrix }
var  bool : boolean;

function s_ecri_bool(b : boolean) : string;
begin
  if b then s_ecri_bool := 'yes' else s_ecri_bool := 'no';
end;

begin
  with mat[x] do
    begin
      edit_matrix.Text := s_ecri_mat(x_mat);
      if interp_ then iwriteln('Matrix ' + edit_matrix.Text);
      label_timedep.Caption := 'Time dependent: ' + s_ecri_bool(mattimedep(x));
      if interp_ then iwriteln(label_timedep.Caption);
      label_vecdep.Caption  := 'Vector dependent: ' + s_ecri_bool(matvecdep(x,modele[xmodele].xvec));
      if interp_ then iwriteln(label_vecdep.Caption);
      label_random.Caption  := 'Random: ' + s_ecri_bool(matrandom(x));
      if interp_ then iwriteln(label_random.Caption);
      bool := matnonneg(size,val);
      label_nonneg.Caption := 'Non negative: ' + s_ecri_bool(bool);
      if interp_ then iwriteln(label_nonneg.Caption);
      if bool then
        begin
          label_pos.Caption  := 'Positive: ' + s_ecri_bool(matpos(size,val));
          if interp_ then iwriteln(label_pos.Caption);
          label_prim.Caption := 'Primitive: ' + s_ecri_bool(matprim(size,val));
          if interp_ then iwriteln(label_prim.Caption);
          label_irr.Caption  := 'Irreducible: ' + s_ecri_bool(matirr(size,val));
          if interp_ then iwriteln(label_irr.Caption);
          leslie := matleslie(size,val);
          label_leslie.Caption := 'Leslie: ' + s_ecri_bool(leslie);
          if interp_ then iwriteln(label_leslie.Caption);
          leslie2 := matleslie2(size,val);
          label_leslie2.Caption := 'Extended Leslie: ' + s_ecri_bool(leslie2);
          if interp_ then iwriteln(label_leslie2.Caption);
          sizeclass := matclassetaille(size,val);
          label_sizeclass.Caption := 'Size classified: ' + s_ecri_bool(sizeclass);
          if interp_ then iwriteln(label_sizeclass.Caption);
          multi := matmultisite(size,val,psite);
          if multi then
            label_multi.Caption := 'Multisite: ' + s_ecri_bool(multi)+
                                   ' [' + IntToStr(psite) + ' sites]'
          else
            label_multi.Caption := 'Multisite: ' + s_ecri_bool(multi);
            if interp_ then iwriteln(label_multi.Caption);
         end
      else
        begin
          label_pos.Caption := '';
          label_prim.Caption := '';
          label_irr.Caption  := '';
          label_leslie.Caption := '';
          label_leslie2.Caption := '';
          label_sizeclass.Caption := '';
          label_multi.Caption := '';
        end;
    end;
end;

procedure tform_prop.eigenval(x : integer;var err : boolean);
{ eigenvalues of non negative matrix }
var i : integer;
    z : cmx;
begin
  err := true;
  with mat[x] do
    begin
      matvalprop(size,val,lambda);
      if err_math then
        begin
          erreur('Error in eigenvalues computation');
          err_math := false;
          exit;
        end;
    end;
  if ( lambda[1].im <> 0.0 ) then
    begin
      erreur('No real dominant eigenvalue found');
      exit;
    end;
  lambda1 := lambda[1].re; { dominant eigenvalue }
  if ( lambda1 <= 0.0 ) then exit;
  if ( cmxmod(lambda[2]) <> 0.0 ) then
    rho := lambda1/cmxmod(lambda[2]) { damping ratio }
  else
    rho := 0.0;
  if ( cmxarg(lambda[2]) <> 0.0 ) then
    pp := dpi/cmxarg(lambda[2]) { period }
  else
    pp := 0.0;
  r := ln0(lambda1); { intrinsic rate of increase }
  edit_lambda.Text := Format('%10.6g',[lambda1]);
  with mat[x],stringgrid_eigenval do
    begin
      FixedCols := 1;
      FixedRows := 1;
      ColCount  := 5;
      RowCount  := size + 1;
      Cells[1,0] := 'Modulus';
      Cells[2,0] := 'Re';
      Cells[3,0] := 'Im';
      Cells[4,0] := 'Arg °';
      if interp_ then iwriteln('     Modulus     | Re        | Im         | Arg°');
      for i := 1 to size do
        begin
          z := lambda[i];
          Cells[0,i] := Format('%3d',[i]);
          Cells[1,i] := Format('%10.6f',[cmxmod(z)]);
          Cells[2,i] := Format('%10.4f',[z.re]);
          Cells[3,i] := Format('%10.4f',[z.im]);
          Cells[4,i] := Format('%10.1f',[cmxarg(z)*360.0/dpi]);
          if interp_ then
            iwriteln(Cells[0,i] + Cells[1,i] + Cells[2,i] + Cells[3,i] + Cells[4,i]);
        end;
      StringGrid_eigenval.AutoSizeColumns(); 
    end;
  label_growth_rate.Caption := 'Growth rate lambda = ' + Format('%1.6g',[lambda1]);
  if interp_ then iwriteln(label_growth_rate.Caption);
  label_r.Caption := 'Intrinsic rate of increase r = ' + Format('%1.4g',[r]);
  if interp_ then iwriteln(label_r.Caption);
  label_rho.Caption := 'Damping ratio rho = ' + Format('%1.4g',[rho]);
  if interp_ then iwriteln(label_rho.Caption);
  label_p.Caption := 'Period P = ' + Format('%1.4g',[pp]) + ' (Rad)';
  if interp_ then iwriteln(label_p.Caption);
  err := false;
end;

procedure tform_prop.eigenvec(x : integer);
{ left and right eigenvectors associated with dominant eigenvalue of matrix }
var i : integer;
    vv,ww : cvec_type;
begin
  with mat[x] do
    begin
      matvecpropdroite(size,val,lambda[1],ww); { right }
      if err_math then
        begin
          err_math := false;
          exit;
        end;
      for i := 1 to size do w[i] := ww[i].re;
      vecnormalise1(size,w);
      matvecpropgauche(size,val,lambda[1],vv); { left }
      if err_math then
        begin
          err_math := false;
          exit;
        end;
      for i := 1 to size do v[i] := vv[i].re;
      vecnormalise1(size,v);
      with stringgrid_eigenvec do
        begin
          FixedCols := 1;
          FixedRows := 1;
          ColCount  := 3;
          RowCount  := size + 1;
          Cells[1,0] := 'Rep. Value';
          Cells[2,0] := 'Stable Dist.';
          if interp_ then iwriteln('     Reproductive value | Stable distribution');
          for i := 1 to size do
            begin
              Cells[0,i] := Format('%3d',[i]);
              Cells[1,i] := Format('%10.4f',[v[i]]);
              Cells[2,i] := Format('%10.4f',[w[i]]);
              if interp_ then
                iwriteln(Cells[0,i]+ '     ' + Cells[1,i] + '         ' + Cells[2,i]);
            end;
          StringGrid_eigenvec.AutoSizeColumns(); 
        end;
    end;
end;

procedure Tform_prop.FormActivate(Sender: TObject);
begin
  proprietes(x_mat);
  AutoSize := False;
end;

procedure Tform_prop.proprietes(x : integer);
{ properties: type of matrix, demographic descriptors }
var err : boolean;
begin
  if ( x = 0 ) then
    begin
      erreur('No matrix-type model');
      exit;
    end;
  prop_gene(x); { type of matrix }
  eigenval(x,err); { eigenvalues }
  if err then exit;
  eigenvec(x); { eigenvectors }
  { demographic descriptors according to matrix type }
  if leslie then
    dem_leslie(x)
  else
    if leslie2 then
      dem_leslie2(x)
    else
      dem_qq(x);
  if interp_ then returntime(x);
end;

procedure Tform_prop.FormCreate(Sender: TObject);
begin
  x_mat := 0;
end;

procedure Tform_prop.init_prop;
var i : integer;
begin
  init_form(form_prop);
  x_mat := 0;
  for i := 1 to modele_nb do
    if ( modele[i].xmat <> 0 ) then
      begin
        x_mat := modele[i].xmat;
        exit;
      end;
end;

procedure Tform_prop.Edit_matrixReturnPressed(Sender: TObject);
var x,tx : integer;
begin
  trouve_obj(tronque(minuscule(edit_matrix.Text)),x,tx);
  if ( x = 0 ) or ( tx <> type_mat ) then
    begin
      erreur('unknown matrix name');
      edit_matrix.Text := s_ecri_mat(x_mat);
      exit;
    end;
  x_mat := x;
  FormActivate(nil);
end;

end.
