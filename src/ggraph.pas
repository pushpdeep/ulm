{ ---------------------------------------------------------------------------- }
{ Copyright (c) 1995-2017, Stéphane Legendre and the ULM development team      }
{                                                                              }
{ This file is part of ULM.                                                    }
{                                                                              }
{ ULM is free software: you can redistribute it and/or modify it               }
{ under the terms of the GNU General Public License as published               }
{ by the Free Software Foundation, either version 3 of the License,            }
{ or (at your option) any later version.                                       }
{                                                                              }
{ ULM is distributed in the hope that it will be useful,                       }
{ but WITHOUT ANY WARRANTY; without even the implied warranty                  }
{ of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                      }
{ See the GNU General Public License for more details.                         }
{                                                                              }
{ You should have received a copy of the GNU General Public License            }
{ along with ULM.  If not, see <http://www.gnu.org/licenses/>.                 }
{ ---------------------------------------------------------------------------- }

unit ggraph;

{$MODE Delphi}

{  @@@@@@   form for graphics   @@@@@@  }

interface

uses  LazFileUtils, SysUtils, Classes, Variants, FileUtil, Graphics, Controls,
  Forms, Dialogs, ExtCtrls, ComCtrls, Menus, ActnList;

const  maxvargraph  = 4;      { maximum number of trajectories on the paintbox }
       maxgraph     = 10000;  { maximum number of cycles that can be displayed }
       { e.g., to display 100000 time steps, trajectories are sampled every 10 time steps }
       maxcolors    = 16;     { number of basic colors }
       max_arc_en_ciel = 255; { number of rainbow colors }
       maxsmalltab  = 10;

       { codes for graphic procedures: }
       graf_efface  = 0; { erase graphics }
       graf_traj    = 1; { trajectory (run command) }
       graf_distrib = 2; { distribution }
       graf_carlo   = 3; { trajectory (montecarlo command) }
       graf_scat    = 4; { scatter plot }
       graf_courb   = 5; { curve }
       graf_land    = 6; { fitness/sensitivity landscape }

type   tab_graph_type = array[0..maxgraph] of extended;
       ismalltab_type = array[0..maxsmalltab] of integer;
       rsmalltab_type = array[0..maxsmalltab] of extended;

type

  { tform_graph }

  tform_graph = class(TForm)
    clear: TAction;
    graphset: TAction;
    fileexit: TAction;
    file_saveas: TAction;
    file_save: TAction;
    ActionList1: TActionList;
    ImageList1: TImageList;
    MainMenu1: TMainMenu;
    File1: TMenuItem;
    Exit1: TMenuItem;
    Clear1: TMenuItem;
    Clear2: TMenuItem;
    N1: TMenuItem;
    Settings2: TMenuItem;
    Save2: TMenuItem;
    Save1: TMenuItem;
    Settings1: TMenuItem;
    StatusBar1: TStatusBar;
    SaveDialog1: TSaveDialog;
    ToolBar1: TToolBar;
    file_save_button: TToolButton;
    graph_settings_button: TToolButton;
    graph_clear_button: TToolButton;
    ToolButton4: TToolButton;
    PaintBox1: TPaintBox;
    procedure init_graph;
    procedure tab_col;
    procedure efface(Sender: TObject);
    procedure efface_notgplus(Sender: TObject);
    procedure gtraj(nb_steps : integer);
    procedure gcourb(nb_steps : integer);
    procedure gland(i0,nb_courb : integer; nbk : ismalltab_type;val_lab : rsmalltab_type;
                    xminl,xmaxl,yminl,ymaxl,x0,y0 : extended);
    procedure gdistrib(nb_steps : integer);
    procedure gcarlo(nb_steps : integer);
    procedure gscat(nb_steps : integer);
    procedure calcul_echelle(amin,amax : extended;var pas,a1 : extended);
    function  get_col_aec(a : extended) : integer;
    function  var_color(x : integer) : integer;
    procedure repaint1(Sender: TObject);
    procedure fileexitExecute(Sender: TObject);
    procedure graphsetExecute(Sender: TObject);
    procedure status_run(nb_steps : integer);
    procedure status_carlo(nb_steps,nb_traj : integer);
    procedure status_var;
    procedure status_distrib;
    procedure status_dt_graph;
    procedure status_gplus;
    procedure status;
    procedure status_spec(x_var,nb_cycle : integer);
    procedure status_correl(x1,x2,nb_cycle,nb_val : integer);
    procedure status_landscape(x,y : integer);
    procedure status_regress(a,b : extended);
    procedure status_distrib_m(m,sd : extended);
    procedure gjoli;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure file_saveExecute(Sender: TObject);
    procedure file_saveExecute2(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure graph_clear_buttonClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    iax,ibx,iay,iby,imargex,imargey : integer; { bounds for paintbox frame }
    iax_bbb,ibx_bbb,iay_bbb,iby_bbb,imargex_bbb,imargey_bbb : integer; { bounds for bitmap bbb }
    vx : tab_graph_type; { current graphic vector x }
    vy : tab_graph_type; { current graphic vector y }
    p  : array[0..maxgraph] of TPoint; { array for points (vx,vy) }
    nb_steps_sav : integer; { to memorize current nb of points in trajectory }
    i0_sav : integer; { to memorize index (procedure gland) }
    nb_courb_sav : integer; { to memorize nb of curves (gland) }
    nbk_sav : ismalltab_type; { to memorize nb of points of curves (gland) }
    val_lab_sav : rsmalltab_type; { to memorize value associated with label on curve (gland) }
    xminl_sav,xmaxl_sav,yminl_sav,ymaxl_sav,x0_sav,y0_sav : extended; { to memorize bounds (gland) }
    grafgraf : integer; { to memorize code of current graphic procedure for repaint }
    bbb : TBitmap;      { bitmap to superimpose graphics }
    procedure ligne(x1,y1,x2,y2 : extended;col : integer);
    procedure coord(x,y : extended;var ix,iy : integer);
    procedure coord2(x,y : extended;var ix,iy : integer);
    procedure coord_bbb(x,y : extended;var ix,iy : integer);
    procedure coord2_bbb(x,y : extended;var ix,iy : integer);
    procedure cercle(x,y,r : extended;col : integer);
    procedure points(n: integer; vx,vy: tab_graph_type;col : integer);
    procedure vect(n: integer;vx,vy : tab_graph_type;col : integer);
    procedure rectangle(x1,y1,x2,y2 : extended;col : integer);
    procedure rect2(x1,y1,x2,y2 : extended;col : integer);
    procedure rect2_bbb(x1,y1,x2,y2 : extended;col : integer);
    procedure rectfull(x1,y1,x2,y2 : extended;col : integer);
    procedure textenum(x,y : extended;a : extended;col : integer);
    procedure axes(xmi,xma,ymi,yma : extended);
    function  get_color(k : integer) : integer;
    procedure calcul_bornes(nb,nb_ski,nb_y : integer);
    procedure regress_line(a,b : extended;col : integer);
  public
    nomficgraph: string;
    colors : array[0..maxcolors] of integer; { basic colors table }
    xmin,xmax,ymin,ymax : extended; { current bounds on graphics }
    distrib     : boolean;        { indicator distribution mode on/off }
    distrib0    : boolean;        { indicator 0 included in distribution on/off }
    d_distrib   : extended;       { interval length of distribution }
    coeff_aec   : extended;       { coefficient for colors in procedure gtraj_group }
    gplus       : boolean;        { indicator to superimpose graphics on/off }
    bord        : boolean;        { indicator to display axes on/off }
    grid        : boolean;        { indicator to display grid on/off }
    xscale      : boolean;        { indicator to fix bounds of x-axis on/off }
    yscale      : boolean;        { indicator to fix bounds of y-axis on/off }
    line0       : boolean;        { indicator to draw lines between points on/off }
    black_and_white : boolean;    { indicator black on white mode on/off }
    white_and_black : boolean;    { indicator white on black mode on/off }
    gminmax     : boolean;        { indicator display min and max in montecarlo trajectory on/off }
    gsigma      : boolean;        { indicator display sigma in montecarlo trajectory on/off }
    gscatter    : boolean;        { indicator scatter-plot on/off }
    gregress    : boolean;        { indicator plot regression line in scatter-plot on/off }
    regress_a   : extended;       { a-coeff regression line a*x + b }
    regress_b   : extended;       { b-coeff regression line a*x + b }
    nb_skip     : integer;        { nb of initial values to skip on display }
    dt_graph    : integer;        { sampling interval for graphics }
    t_graph_traj     : integer;   { time step for graphics (i.e., according to sampling interval) }
    vargraph_x       : integer;   { pointer to x-variable (in abscissa) }
    nb_vargraph_y    : integer;   { number of y-variables (in ordinate) }
    vargraph_y       : array[1..maxvargraph] of integer; { array of pointers to y-variables }
    vargraph_y_col   : array[1..maxvargraph] of integer; { array of colors of y-variables }
    valgraph_x       : tab_graph_type; { array of x-values }
    valgraph_x_min   : tab_graph_type; { array of min x-values (montecarlo) }
    valgraph_x_max   : tab_graph_type; { array of max x-values (montecarlo) }
    valgraph_x_sigma : tab_graph_type; { array of sigma x-values (montecarlo) }
    valgraph_y       : array[1..maxvargraph] of tab_graph_type; { arrays of y-values }
    valgraph_y_sigma : array[1..maxvargraph] of tab_graph_type; { arrays of sigma y-values (montecarlo) }
    valgraph_y_min   : array[1..maxvargraph] of tab_graph_type; { arrays of min y-values (montecarlo) }
    valgraph_y_max   : array[1..maxvargraph] of tab_graph_type; { arrays of max y-values (montecarlo) }
    ifg : integer; { index of graphic form }
  end;

var   form_graph: tform_graph;

implementation

uses  jglobvar,jmath,jsyntax,ggraphset,gulm;

{$R *.lfm}

{ ------ color procedures ------ }

var   arc_en_ciel : array[0..max_arc_en_ciel] of integer; { table of rainbow colors }
      aec_nb : integer; {nb of rainbow colors }
      gris_en_ciel : array[0..max_arc_en_ciel] of integer; { table of rainbow colors, but in gray (!) }

function rgb(r,g,b : integer) : integer;
begin
  rgb := r + 256*(g + 256*b);
end;

procedure tform_graph.tab_col;
{ create table of 16 basic colors }
begin
  colors[0]  := rgb(  0,  0, 80);     { deep blue (background) }
  colors[1]  := rgb(255,  0,  0);     { red }
  colors[2]  := rgb(  0,255,  0);     { green }
  colors[3]  := rgb(  0,  0,255);     { blue }
  colors[4]  := rgb(255,255,  0);     { yellow }
  colors[5]  := rgb(255,  0,255);     { magenta }
  colors[6]  := rgb(  0,255,255);     { cyan }
  colors[7]  := rgb(255,255,255);     { white }
  colors[8]  := rgb(191,  0,  0);     { dark red }
  colors[9]  := rgb(  0,191,  0);     { dark green }
  colors[10] := rgb(  0,  0,191);     { dark blue }
  colors[11] := rgb(191,191,  0);     { dark yellow }
  colors[12] := rgb(191,  0,191);     { dark magenta }
  colors[13] := rgb(  0,191,191);     { dark cyan }
  colors[14] := rgb(191,191,191);     { gray }
  colors[15] := rgb(128,128,255);     { violet }
  colors[16] := rgb(255,128,  0);     { orange }
end;

procedure tab_arc_en_ciel;
{ create table of 255 rainbow colors / 255 shades of gray }
var i,k : integer;
begin
  arc_en_ciel[0] := rgb(0,0,80); { deep blue (background) }
  i := 0;
  k := -5;
  repeat
    i := i + 1;
    k := k + 5;
    arc_en_ciel[i] := rgb(255,k,0);
  until ( k = 255 );
  k := -5;
  repeat
    i := i + 1;
    k := k + 5;
    arc_en_ciel[i] := rgb(255-k,255,0);
  until ( k = 255 );
  k := -5;
  repeat
    i := i + 1;
    k := k + 5;
    arc_en_ciel[i] := rgb(0,255,k);
  until ( k = 255 );
  k := -5;
  repeat
    i := i + 1;
    k := k + 5;
    arc_en_ciel[i]:= rgb(0,255-k,255);
  until ( k = 255 );
  k := 0;
  repeat
    i := i + 1;
    k := k + 5;
    arc_en_ciel[i]:= rgb(0,0,255-k);
  until ( 255-k = 0 );
  aec_nb := i-1;
  for i := aec_nb + 1 to 255 do arc_en_ciel[i] := clWhite;
  k := 0;
  gris_en_ciel[0] := clWhite;
  for i := 1 to 255 do
    begin
      gris_en_ciel[i] := rgb(255-k,255-k,255-k);
      k := k + 1;
    end;
end;

function  tform_graph.var_color(x : integer) : integer;
{ returns color attributed to variable x }
var col : integer;
begin
  if ( x > maxcolors ) then x := x mod maxcolors + 1;
  col := colors[x];
  if ( x = 0 ) then col := clBackground;
  if black_and_white then col := clBlack;
  if white_and_black then col := clWhite;
  var_color := col;
end;

function  tform_graph.get_color(k : integer) : integer;
{ returns color attributed to the kth y-variable }
var col : integer;
begin
  col := vargraph_y_col[k];
  if black_and_white then col := clBlack;
  if white_and_black then col := clWhite;
  get_color := col;
end;

procedure tform_graph.FormCreate(Sender: TObject);
{ create form }
begin
  tab_col;
  tab_arc_en_ciel;
  nomficgraph := '';
  with paintbox1 do
     begin
       ClientWidth  := form_graph.Width;
       ClientHeight := ClientWidth;
       iax := round(ClientWidth/10.0);
       ibx := round(ClientWidth/12.0);
       iay := round(ClientHeight/15.0);
       iby := round(ClientHeight/12.0);
       imargex := iax + ibx;
       imargey := iay + iby;
     end;
  bbb := TBitmap.Create;
  with bbb do
     begin
       Height  := paintbox1.ClientHeight;
       Width   := paintbox1.ClientWidth;
       iax_bbb := iax;
       ibx_bbb := ibx;
       iay_bbb := iay;
       iby_bbb := iby;
       imargex_bbb := imargex;
       imargey_bbb := imargey;
     end;
   file_save.Enabled := false;
   file_saveas.Enabled := false;
   graphset.Enabled := false;
   efface(nil);
end;

procedure tform_graph.init_graph;
{ set defaults for graphics }
var i,k,x,j : integer;
begin
  line0  := true;
  bord   := true;
  grid   := false;
  gplus  := false;
  black_and_white := false;
  white_and_black := false;
  xmin := 0.0;
  xmax := 1.0;
  ymin := 0.0;
  ymax := 1.0;
  xscale := false;
  yscale := false;
  distrib  := false;
  distrib0 := false;
  d_distrib := 1.0;
  coeff_aec := 2.718;
  gminmax  := false;
  gsigma   := false;
  gscatter := false;
  gregress := false;
  nb_skip := 0;
  dt_graph := 1;
  for i := 0 to maxgraph do
    begin
      vx[i] := 0.0;
      vy[i] := 0.0;
      valgraph_x[i]       := 0.0;
      valgraph_x_sigma[i] := 0.0;
      valgraph_x_min[i]   := 0.0;
      valgraph_x_max[i]   := 0.0;
      for k := 1 to maxvargraph do
        begin
          valgraph_y[k][i] := 0.0;
          valgraph_y_sigma[k][i] := 0.0;
          valgraph_y_min[k][i]   := 0.0;
          valgraph_y_max[k][i]   := 0.0;
        end;
    end;
  { default variables to display }
  vargraph_x := xtime; { time in abscissa }
  k := 0;
  for x := 1 to modele_nb do with modele[x] do
    if ( xmat <> 0 ) then
      for i := 1 to size do
        if ( k <= maxvargraph-1 ) then
          begin
            k := k + 1;
            vargraph_y[k] := vec[xvec].exp[i];
          end
        else
    else
      for i := 1 to size do
        if ( k <= maxvargraph-1 ) then
          begin
            k := k + 1;
            vargraph_y[k] := rel[xrel[i]].xvar;
          end;
  nb_vargraph_y := k;
  for k := 1 to nb_vargraph_y do
    begin
      j := vargraph_y[k];
      if ( j > maxcolors ) then j := j mod maxcolors + 1;
      vargraph_y_col[k] := colors[j];
    end;
  for k := nb_vargraph_y+1 to maxvargraph do vargraph_y[k] := 0;
  for k := nb_vargraph_y+1 to maxvargraph do vargraph_y_col[k] := clBackground;
  nb_steps_sav := maxgraph;
  file_save.Enabled := true;
  file_saveas.Enabled := true;
  graphset.Enabled := true;
  efface(nil);
  grafgraf := graf_efface;
  status;
end;

{ ------  status bar procedures  ------  }

procedure tform_graph.status_run(nb_steps : integer);
begin
  statusbar1.Panels[0].Text := 'Run ' + IntToStr(nb_steps);
end;

procedure tform_graph.status_carlo(nb_steps,nb_traj : integer);
begin
  statusbar1.Panels[0].Text :=
    'MonteCarlo ' + IntToStr(nb_steps)+ ' ' + IntToStr(nb_traj);
end;

procedure tform_graph.status_var;
var k : integer;
    s,s1 : string;
begin
  s := s_ecri_var(vargraph_x);
  s := 'X: ' + s;
  statusbar1.Panels[1].Text := s;
  s := '';
  for k := 1 to nb_vargraph_y do
    begin
      s1 := s_ecri_var(vargraph_y[k]);
      s := s + ' ' + s1;
    end;
  s := 'Y:' + s;
  statusbar1.Panels[2].Text := s;
end;

procedure tform_graph.status_distrib;
var s : string;
begin
  if distrib then
    begin
      s := FloatToStr(d_distrib);
      if distrib0 then
        s := 'Dist0 ' + s
      else
        s := 'Dist ' + s;
    end
  else
    s := '';
  statusbar1.Panels[3].Text := s;
end;

procedure tform_graph.status_dt_graph;
begin
  statusbar1.Panels[4].Text := 'Dt = ' + IntToStr(dt_graph);
end;

procedure tform_graph.status_gplus;
begin
  if gplus then
    statusbar1.Panels[5].Text := '+'
  else
    statusbar1.Panels[5].Text := '';
end;

procedure tform_graph.status;
begin
  statusbar1.Panels[0].Text := '';
  status_var;
  status_distrib;
  status_dt_graph;
  status_gplus;
end;

procedure tform_graph.status_spec(x_var,nb_cycle : integer);
begin
  statusbar1.Panels[0].Text := 'Log power spectrum ' + IntToStr(nb_cycle);
  statusbar1.Panels[1].Text := 'X: frequency';
  statusbar1.Panels[2].Text := 'Y: ' + s_ecri_var(x_var);
  statusbar1.Panels[3].Text := '';
  statusbar1.Panels[4].Text := '';
  status_gplus;
end;

procedure tform_graph.status_correl(x1,x2,nb_cycle,nb_val : integer);
begin
  statusbar1.Panels[0].Text := 'Correlation ' + IntToStr(nb_cycle) + ' ' + IntToStr(nb_val);
  statusbar1.Panels[1].Text := 'X: t';
  statusbar1.Panels[2].Text := 'Y: ' + s_ecri_var(x1) + '*' + s_ecri_var(x2);
  statusbar1.Panels[3].Text := '';
  status_dt_graph;
  status_gplus;
end;

procedure tform_graph.status_landscape(x,y : integer);
begin
  statusbar1.Panels[0].Text := 'Fitness landscape';
  statusbar1.Panels[1].Text := 'X: ' + s_ecri_var(x);
  statusbar1.Panels[2].Text := 'Y: ' + s_ecri_var(y);
  statusbar1.Panels[3].Text := '';
  status_dt_graph;
  status_gplus;
end;

procedure tform_graph.status_regress(a,b : extended);
begin
  statusbar1.Panels[6].Text := 'a = ' + s_ecri_val(a) + ' b = ' + s_ecri_val(b);
end;

procedure tform_graph.status_distrib_m(m,sd : extended);
begin
  statusbar1.Panels[6].Text := 'm = ' + s_ecri_val(m) + ' SD = ' + s_ecri_val(sd);
end;

{ ------ basic graphic procedures ----- }

procedure tform_graph.efface(Sender: TObject);
{ erase graphics }
var col : integer;
begin
  col := colors[0];
  if black_and_white then col := clWhite;
  if white_and_black then col := clBlack;
  rect2(0.0,0.0,1.0,1.0,col);
  rect2_bbb(0.0,0.0,1.0,1.0,col);
end;

procedure tform_graph.efface_notgplus(Sender: TObject);
{ erase graphics, accounting for superimposition (gplus = true) }
var col : integer;
begin
  col := colors[0];
  if black_and_white then col := clWhite;
  if white_and_black then col := clBlack;
  rect2(0.0,0.0,1.0,1.0,col);
  if not gplus then rect2_bbb(0.0,0.0,1.0,1.0,col);
end;

procedure tform_graph.coord(x,y : extended;var ix,iy : integer);
{ return graphic coordinates (ix,iy) of point (x,y) }
begin
  with paintbox1 do
    begin
      ix := round(iax + x*(ClientWidth-imargex));
      iy := round(iby + (1.0-y)*(ClientHeight-imargey));
    end;
end;

procedure tform_graph.coord_bbb(x,y : extended;var ix,iy : integer);
{ coordinates for bitmap bbb }
begin
  with bbb do
    begin
      ix := round(iax_bbb + x*(Width-imargex_bbb));
      iy := round(iby_bbb + (1.0-y)*(Height-imargey_bbb));
    end;
end;

procedure tform_graph.coord2(x,y : extended;var ix,iy : integer);
{ coordinates for whole frame paintbox }
begin
  with paintbox1 do
    begin
      ix := round(x*ClientWidth);
      iy := round((1.0-y)*ClientHeight);
    end;
end;

procedure tform_graph.coord2_bbb(x,y : extended;var ix,iy : integer);
{ coordinates for whole frame bitmap }
begin
  with bbb do
    begin
      ix := round(x*Width);
      iy := round((1.0-y)*Height);
    end;
end;

procedure tform_graph.ligne(x1,y1,x2,y2 : extended;col : integer);
{ draw line of color col from (x1,y1) to (x2,y2) }
var ix1,iy1,ix2,iy2 : integer;
begin
  coord(x1,y1,ix1,iy1);
  coord(x2,y2,ix2,iy2);
  with paintbox1.Canvas do
    begin
      Pen.Color := col;
      MoveTo(ix1,iy1);
      LineTo(ix2,iy2);
    end;
  coord_bbb(x1,y1,ix1,iy1);
  coord_bbb(x2,y2,ix2,iy2);
  with bbb.Canvas do
    begin
      Pen.Color := col;
      MoveTo(ix1,iy1);
      LineTo(ix2,iy2);
    end;
end;

procedure tform_graph.cercle(x,y,r : extended;col : integer);
{ draw circle of color col, center (x,y) and radius r }
var ix1,iy1,ix2,iy2 : integer;
begin
  coord(x-r,y-r,ix1,iy1);
  coord(x+r,y+r,ix2,iy2);
  with paintbox1.Canvas do
    begin
      Pen.Color := col;
      Ellipse(ix1,iy1,ix2,iy2);
    end;
  coord_bbb(x-r,y-r,ix1,iy1);
  coord_bbb(x+r,y+r,ix2,iy2);
  with bbb.Canvas do
    begin
      Pen.Color := col;
      Ellipse(ix1,iy1,ix2,iy2);
    end;
end;

procedure tform_graph.points(n: integer; vx,vy: tab_graph_type;col : integer);
{ draw set of n points of color col, defined by arrays (vx,vy) }
var i,ix,iy : integer;
begin
  with paintbox1.Canvas do
    begin
      Pen.Color := col;
      for i := 0 to n do
        begin
          coord(vx[i],vy[i],ix,iy);
	  Pixels[ix,iy] := col;
        end;
    end;
  with bbb.Canvas do
    begin
      Pen.Color := col;
      for i := 0 to n do
        begin
          coord_bbb(vx[i],vy[i],ix,iy);
	  Pixels[ix,iy] := col;
        end;
    end;
end;

procedure tform_graph.vect(n: integer;vx,vy : tab_graph_type;col : integer);
{ draw polyline of n points of color col, defined by arrays (vx,vy) }
var i : integer;
begin
  for i := 0 to n do coord(vx[i],vy[i],p[i].x,p[i].y);
  with paintbox1.Canvas do
    begin
      Pen.Color := col;
      Polyline(p,0,n+1);
    end;
  for i := 0 to n do coord_bbb(vx[i],vy[i],p[i].x,p[i].y);
  with bbb.Canvas do
    begin
      Pen.Color := col;
      Polyline(p,0,n+1);
    end;
end;

procedure tform_graph.rectangle(x1,y1,x2,y2 : extended;col : integer);
{ draw rectangle of color col, defined by diagonal points (x1,y1) and (x2,y2) }
begin
  coord(x1,y1,p[0].x,p[0].y);
  coord(x2,y1,p[1].x,p[1].y);
  coord(x2,y2,p[2].x,p[2].y);
  coord(x1,y2,p[3].x,p[3].y);
  coord(x1,y1,p[4].x,p[4].y);
  with paintbox1.Canvas do
    begin
      Pen.Color := col;
      Polyline(p,0,5);
    end;
  coord_bbb(x1,y1,p[0].x,p[0].y);
  coord_bbb(x2,y1,p[1].x,p[1].y);
  coord_bbb(x2,y2,p[2].x,p[2].y);
  coord_bbb(x1,y2,p[3].x,p[3].y);
  coord_bbb(x1,y1,p[4].x,p[4].y);
  with bbb.Canvas do
    begin
      Pen.Color := col;
      Polyline(p,0,5);
    end;
end;

procedure tform_graph.rect2(x1,y1,x2,y2 : extended;col : integer);
{ draw rectangle over whole frame paintbox }
var ix1,iy1,ix2,iy2 : integer;
begin
  with paintbox1.Canvas do
    begin
      coord2(x1,y1,ix1,iy1);
      coord2(x2,y2,ix2,iy2);
      Brush.Color := col;
      Polygon([Point(ix1,iy1),Point(ix2,iy1),Point(ix2,iy2),Point(ix1,iy2)]);
    end;
end;

procedure tform_graph.rect2_bbb(x1,y1,x2,y2 : extended;col : integer);
{ draw rectangle over whole frame bitmap }
var ix1,iy1,ix2,iy2 : integer;
begin
  with bbb.Canvas do
    begin
      coord2_bbb(x1,y1,ix1,iy1);
      coord2_bbb(x2,y2,ix2,iy2);
      Brush.Color := col;
      Polygon([Point(ix1,iy1),Point(ix2,iy1),Point(ix2,iy2),Point(ix1,iy2)]);
    end;
end;

procedure tform_graph.rectfull(x1,y1,x2,y2 : extended;col : integer);
{ draw plain rectangle of color col, defined by diagonal points (x1,y1) and (x2,y2) }
var ix1,iy1,ix2,iy2 : integer;
begin
  with paintbox1.Canvas do
    begin
      coord(x1,y1,ix1,iy1);
      coord(x2,y2,ix2,iy2);
      Pen.Color   := col;
      Brush.Color := col;
      Polygon([Point(ix1,iy1),Point(ix2,iy1),Point(ix2,iy2),Point(ix1,iy2)]);
    end;
  with bbb.Canvas do
    begin
      coord_bbb(x1,y1,ix1,iy1);
      coord_bbb(x2,y2,ix2,iy2);
      Pen.Color   := col;
      Brush.Color := col;
      Polygon([Point(ix1,iy1),Point(ix2,iy1),Point(ix2,iy2),Point(ix1,iy2)]);
    end;
end;

procedure tform_graph.textenum(x,y : extended;a : extended;col : integer);
{ display text of numerical value a and color col at position (x,y)  }
var ix,iy : integer;
    s : string;
begin
  s := Format('%1.6g',[a]);
  with paintbox1.Canvas do
    begin
      coord(x,y,ix,iy);
      Font.Size := round(72*imax(ClientHeight,ClientWidth)/PixelsPerInch/60.0);
      Font.Name := 'Times New Roman';
      Font.Color := col;
      TextOut(ix,iy,s);
    end;
  with bbb.Canvas do
    begin
      coord_bbb(x,y,ix,iy);
      Font.Size := round(72*imax(Height,Width)/PixelsPerInch/60.0);
      Font.Name := 'Times New Roman';
      Font.Color := col;
      TextOut(ix,iy,s);
    end;
end;

{ ------  procedures to display axes  ------ }

function  expo(x : extended) : integer;
{ return exponent e of x = a*10^e }
var n : integer;
begin
  x := abs(x);
  if ( x >= 1.0 ) then
    begin
      n := -1;
      repeat
        x := x/10.0;
        n := n + 1;
      until x < 1.0;
    end
  else
    begin
      n := 0;
      repeat
        x := x*10.0;
        n := n - 1;
      until x > 1.0;
    end;
  expo := n;
end;

function puis10(n : integer) : extended;
{ return 10^n }
var i : integer;
    a,d : extended;
begin
  if ( n >= 0 ) then
    d := 10.0
  else
    begin
      n := -n;
      d := 0.1;
    end;
  a := 1.0;
  for i := 1 to n do a := a*d;
  puis10 := a;
end;

procedure tform_graph.calcul_echelle(amin,amax : extended;var pas,a1 : extended);
{ returns scale interval pas and first value a1 on axis }
{ given min and max values amin, amax }
var  k,id : integer;
     d,coeff : extended;
begin
  d := amax - amin;
  k := expo(d)-1;
  coeff := puis10(k);
  d  := d/coeff;
  id := trunc(d);
  id := (id div 10 + 1)*10;
  if ( id > 50 ) then
    pas := 10.0
  else
    if ( id > 20 ) then
      pas := 5.0
    else
      pas := 2.0;
  pas := pas*coeff;
  a1  := amin/pas;
  if ( abs(a1) < bigint ) then
    begin
      id := trunc(a1);
      a1 := id*pas;
    end
  else
    a1 := amin;
  if ( a1 > amin ) then a1 := a1 - pas;
end;

procedure tform_graph.axes(xmi,xma,ymi,yma : extended);
{ draw x-axis (min = xmi, max = xma) and y-axis (min = ymi, max = yma) }
const dd = 0.01;
var col,i : integer;
    x1,xpas,y1,ypas,x,x0,y,y0,xx,yy,ex,ey,dx,dy,xa1,ya1,xa2,ya2 : extended;
begin
  calcul_echelle(xmi,xma,xpas,x1);
  calcul_echelle(ymi,yma,ypas,y1);
  col := clWhite;
  if black_and_white then col := clBlack;
  rectangle(0.0,0.0,1.0,1.0,col);
  ex := xma - xmi;
  xx := x1;
  x0 := (x1 - xmi)/ex;
  x  := x0;
  dx := xpas/ex;
  ya1 := 0.0;
  ya2 := dd;
  if grid then ya2 := 1.0;
  i := 0;
  repeat
    if ( x >= 0.0 ) and ( x <= 1.0 ) then
      begin
        ligne(x,ya1,x,ya2,col);
        textenum(x,-dd,xx,col);
      end;
    i := i + 1;
    x  := x0 + i*dx;
    xx := x1 + i*xpas;
  until ( x > 1.0 );
  ey := yma - ymi;
  yy := y1;
  y0 := (y1 - ymi)/ey;
  y  := y0;
  dy := ypas/ey;
  xa1 := 0.0;
  xa2 := dd;
  if grid then xa2 := 1.0;
  i := 0;
  repeat
    if ( y >= 0.0 ) and ( y <= 1.0 ) then
      begin
        ligne(xa1,y,xa2,y,col);
        textenum(-8*dd,y + 2*dd,yy,col);
      end;
    i  := i + 1;
    y  := y0 + i*dy;
    yy := y1 + i*ypas;
  until ( y > 1.0 );
end;

procedure tform_graph.calcul_bornes(nb,nb_ski,nb_y : integer);
{ compute bounds on graphics according to x-values and y-values }
var i,i0,k : integer;
begin
  i0 := 0;
  if ( grafgraf = graf_distrib ) and not distrib0 then i0 := 1;
  if not xscale then
    begin
      xmin :=  maxextended;
      xmax := -maxextended;
      for i := i0 to nb do
        begin
          xmin := min(xmin,valgraph_x[i+nb_ski]);
          xmax := max(xmax,valgraph_x[i+nb_ski]);
        end;
      if ( grafgraf = graf_carlo ) then
        begin
          if gminmax then
            for i := 0 to nb do
              begin
                xmin := min(xmin,valgraph_x_min[i+nb_ski]);
                xmax := max(xmax,valgraph_x_max[i+nb_ski]);
              end;
          if gsigma then
            for i := 0 to nb do
              begin
                xmin := min(xmin,valgraph_x[i+nb_ski] - valgraph_x_sigma[i+nb_ski]);
                xmax := max(xmax,valgraph_x[i+nb_ski] + valgraph_x_sigma[i+nb_ski]);
              end;
        end;
    end;
  if not yscale then
    begin
      ymin :=  maxextended;
      ymax := -maxextended;
      for k := 1 to nb_y do
        for i := i0 to nb do
          begin
            ymin := min(ymin,valgraph_y[k][i+nb_ski]);
            ymax := max(ymax,valgraph_y[k][i+nb_ski]);
          end;
      if ( grafgraf = graf_carlo ) then
        begin
          if gminmax then
            for k := 1 to nb_y do
              for i := 0 to nb do
                begin
                  ymin := min(ymin,valgraph_y_min[k][i+nb_ski]);
                  ymax := max(ymax,valgraph_y_max[k][i+nb_ski]);
                end;
          if gsigma then
            for k := 1 to nb_y do
              for i := 0 to nb do
                begin
                  ymin := min(ymin,valgraph_y[k][i+nb_ski] - valgraph_y_sigma[k][i+nb_ski]);
                  ymax := max(ymax,valgraph_y[k][i+nb_ski] + valgraph_y_sigma[k][i+nb_ski]);
                end;
        end;
    end;
  if ( xmax = xmin ) then
    begin
      xmax := xmin + 1.0;
      xmin := xmin - 1.0;
    end;
  if ( ymax = ymin ) then
    begin
      ymax := ymin + 1.0;
      ymin := ymin - 1.0;
    end;
end;

procedure tform_graph.gtraj(nb_steps : integer);
{ draw trajectories of nb_steps time steps }
var i,k,col : integer;
    ex,ey : extended;
begin
  nb_steps_sav := nb_steps; { memorize nb of time steps }
  grafgraf := graf_traj; { memorize procedure code }
  nb_steps := nb_steps-nb_skip;
  calcul_bornes(nb_steps,nb_skip,nb_vargraph_y); { compute graphic bounds }
  { normalize [0,1]x[0,1]: }
  ex := xmax - xmin;
  ey := ymax - ymin;
  for i := 0 to nb_steps do vx[i] := (valgraph_x[i+nb_skip]-xmin)/ex;
  for k := 1 to nb_vargraph_y do
    begin
      for i := 0 to nb_steps do vy[i] := (valgraph_y[k][i+nb_skip]-ymin)/ey;
      col := get_color(k); { set color }
      if line0 then vect(nb_steps,vx,vy,col); { polyline }
      col := clWhite;
      if black_and_white then col := clBlack;
      points(nb_steps,vx,vy,col); { points }
    end;
  if bord then axes(xmin,xmax,ymin,ymax); { axes }
end;

procedure tform_graph.gcourb(nb_steps : integer);
{ draw curve (unit gspec.pas) values 0 ... nb_steps-1 }
var i,col,nb_ski,nb_y : integer;
    ex,ey : extended;
begin
  nb_steps_sav := nb_steps;
  grafgraf := graf_courb;
  nb_ski := 0;
  nb_y := 1;
  calcul_bornes(nb_steps,nb_ski,nb_y);
  ex := xmax - xmin;
  ey := ymax - ymin;
  for i := 0 to nb_steps do vx[i] := (valgraph_x[i]-xmin)/ex;
  for i := 0 to nb_steps do vy[i] := (valgraph_y[1][i]-ymin)/ey;
  col := clWhite;
  if black_and_white then col := clBlack;
  vect(nb_steps,vx,vy,col);
  if bord then axes(xmin,xmax,ymin,ymax);
end;

procedure tform_graph.gland(i0,nb_courb : integer; nbk : ismalltab_type;val_lab : rsmalltab_type;
                            xminl,xmaxl,yminl,ymaxl,x0,y0 : extended);
{ draw set of curves of fitness landscape }
const dd = 0.01;
var i,j,k,n,col : integer;
    x1,y1,ex,ey : extended;
begin
  i0_sav := i0;
  nb_courb_sav := nb_courb;
  for k := i0 to nb_courb do nbk_sav[k] := nbk[k];
  for k := i0 to nb_courb do val_lab_sav[k] := val_lab[k];
  xminl_sav := xminl;
  xmaxl_sav := xmaxl;
  yminl_sav := yminl;
  ymaxl_sav := ymaxl;
  x0_sav := x0;
  y0_sav := y0;
  grafgraf := graf_land;
  ex := xmaxl - xminl;
  ey := ymaxl - yminl;
  j := 0;
  for k := i0 to nb_courb do
    begin
      n := nbk[k]-1;
      for i := 0 to n do
        begin
          vx[i] := (valgraph_x[j]-xminl)/ex;
          vy[i] := (valgraph_y[1][j]-yminl)/ey;
          j := j + 1;
        end;
      if ( k = 0 ) then
        i := n div 3
      else
        i := n div 2;
      x1 := vx[i] + 2.0*dd;
      y1 := vy[i] + 2.0*dd;
      if ( k = 0 ) then
        begin
          col := clRed;
          if black_and_white then col := clBlack;
          if white_and_black then col := clWhite;
          cercle((x0-xminl)/ex,(y0-yminl)/ey,1.5*dd,col);
        end
      else
        col := clWhite;
      if black_and_white then col := clBlack;
      if white_and_black then col := clWhite;
      textenum(x1,y1,val_lab[k],col);
      vect(n,vx,vy,col);
    end;
  if bord then axes(xminl,xmaxl,yminl,ymaxl);
end;

procedure tform_graph.gdistrib(nb_steps : integer);
{ draw distribution of nb_steps boxes }
var  i,k,col,i0 : integer;
     ex,ey,y : extended;
     m,sd : extended;
begin
  nb_steps_sav := nb_steps;
  grafgraf := graf_distrib;
  if distrib0 then i0 := 0 else i0 := 1;
  calcul_bornes(nb_steps,nb_skip,nb_vargraph_y);
  ex := xmax - xmin;
  ey := ymax - ymin;
  for i := i0 to nb_steps do vx[i] := (valgraph_x[i]-xmin)/ex;
  for k := 1 to nb_vargraph_y do
    begin
      col := get_color(k);
      for i := i0 to nb_steps-1 do
        begin
          y := (valgraph_y[k][i]-ymin)/ey;
          if ( y > 0 ) then rectangle(vx[i],0.0,vx[i+1],y,col);
        end;
    end;
  if bord then axes(xmin,xmax,ymin,ymax);
  m := 0.0;
  for i := i0 to nb_steps do m := m + i*d_distrib*valgraph_y[1][i];
  sd := 0.0;
  for i := i0 to nb_steps do sd := sd + sqr(i*d_distrib)*valgraph_y[1][i];
  if ( sd >= m*m ) then
    sd := sqrt(sd-m*m)
  else
    sd := 0.0;
  status_distrib_m(m,sd);
end;

procedure tform_graph.gcarlo(nb_steps : integer);
{ draw montecarlo trajectories of nb_steps time steps }
var i,k,delta,col : integer;
    ex,ey,x,xa,xb,y,ya,yb : extended;
begin
  nb_steps_sav := nb_steps;
  grafgraf := graf_carlo;
  calcul_bornes(nb_steps,nb_skip,nb_vargraph_y);
  ex := xmax - xmin;
  ey := ymax - ymin;
  delta := 1*dt_graph;
  if ( nb_steps > 100 )  then delta := 10*dt_graph;
  if ( nb_steps > 1000 ) then delta := 100*dt_graph;
  for i := 0 to nb_steps do vx[i] := (valgraph_x[i]-xmin)/ex;
  for k := 1 to nb_vargraph_y do
    begin
      for i := 0 to nb_steps do vy[i] := (valgraph_y[k][i]-ymin)/ey;
      col := get_color(k);
      if line0 then vect(nb_steps,vx,vy,col);
      points(nb_steps,vx,vy,clWhite);
      if gsigma then { plot confidence interval sigma }
        for i := 1 to nb_steps do
          if ( i mod delta = 0 ) then
            begin
              x  := vx[i];
              ya := (valgraph_y[k][i]-valgraph_y_sigma[k][i]-ymin)/ey;
              yb := (valgraph_y[k][i]+valgraph_y_sigma[k][i]-ymin)/ey;
              ligne(x,ya,x,yb,col);
              y  := vy[i];
              xa := (valgraph_x[i]-valgraph_x_sigma[i]-xmin)/ex;
              xb := (valgraph_x[i]+valgraph_x_sigma[i]-xmin)/ex;
              ligne(xa,y,xb,y,col);
            end;
    end;
  if gminmax then { plot min and max values across trajectories }
    begin
      for i := 1 to nb_steps do
        if ( i mod delta = 0 ) then
          begin
            x := (valgraph_x_min[i]-xmin)/ex;
            for k := 1 to nb_vargraph_y do
              begin
                y := (valgraph_y_min[k][i]-ymin)/ey;
                col := get_color(k);
                cercle(x,y,0.004,col);
              end;
          end;
      for i := 1 to nb_steps do
        if ( i mod delta = 0 ) then
          begin
            x := (valgraph_x_max[i]-xmin)/ex;
            for k := 1 to nb_vargraph_y do
              begin
                y := (valgraph_y_max[k][i]-ymin)/ey;
                col := get_color(k);
                cercle(x,y,0.004,col);
              end;
          end;
    end;
  if bord then axes(xmin,xmax,ymin,ymax);
end;

procedure tform_graph.regress_line(a,b : extended;col : integer);
{ draw regression line y = a*x + b }
var x1,y1,x2,y2 : extended;
begin
  x1 := 0.0;
  y1 := b;
  x2 := 1.0;
  y2 := a + b;
  if ( y1 > 1.0 ) and ( a < 0.0 ) then
    begin
      x1 := (1.0 - b)/a;
      y1 := 1.0;
    end;
  if ( y1 < 0.0 ) and ( a > 0.0 ) then
    begin
      x1 := - b/a;
      y1 := 0.0;
    end;
  if ( y2 > 1.0 ) and ( a > 0.0 ) then
    begin
      x2 := (1.0 - b)/a;
      y2 := 1.0;
    end;
  if ( y2 < 0.0 ) and ( a < 0.0 ) then
    begin
      x2 := -b/a;
      y2 := 0.0;
    end;
  ligne(x1,y1,x2,y2,col);
end;

procedure tform_graph.gscat(nb_steps : integer);
{ draw scatter plot of nb_steps points with regression line }
{ run procedure: trajectory points }
{ montecarlo procedure: last point of each replicate trajectory }
var i,k,col : integer;
    ex,ey,x,y : extended;
    tabx,taby : vecvec_type;
    a,b : extended;
begin
  nb_steps_sav := nb_steps;
  grafgraf := graf_scat;
  calcul_bornes(nb_steps,0,nb_vargraph_y);
  ex := xmax - xmin;
  ey := ymax - ymin;
  for i := 1 to nb_steps do
    begin
      x := (valgraph_x[i]-xmin)/ex;
      for k := 1 to nb_vargraph_y do
        begin
          y := (valgraph_y[k][i]-ymin)/ey;
          col := get_color(k);
          cercle(x,y,0.004,col);
        end;
    end;
  if bord then axes(xmin,xmax,ymin,ymax);
  SetLength(tabx,nb_steps);
  SetLength(taby,nb_steps);
  for k := 1 to nb_vargraph_y do
    begin
      for i := 1 to nb_steps do
        begin
          tabx[i-1] := valgraph_x[i];
          taby[i-1] := valgraph_y[k][i];
        end;
      coeff_regress(nb_steps,tabx,taby,a,b); { regression line coefficients a, b }
      status_regress(a,b);
      regress_a := a;
      regress_b := b;
      if gregress then
        begin
          col := get_color(k);
          for i := 1 to nb_steps do
            begin
              tabx[i-1] := (valgraph_x[i]-xmin)/ex;
              taby[i-1] := (valgraph_y[k][i]-ymin)/ey;
            end;
          coeff_regress(nb_steps,tabx,taby,a,b);
          regress_line(a,b,col);
        end;
    end;
end;

function  tform_graph.get_col_aec(a : extended) : integer;
{ get rainbow/gray color associated with real number a }
var k : integer;
begin
  if black_and_white or white_and_black then
    begin
      k := trunc(255*a) + 1;
      if black_and_white then
        get_col_aec := gris_en_ciel[k]
      else
        get_col_aec := gris_en_ciel[255-k+1]
    end
  else
    begin
      k := trunc(aec_nb*(1.0 - a)) + 1;
      get_col_aec := arc_en_ciel[k];
    end;
end;

procedure tform_graph.gjoli;
{ display rainbow colors in rectangular array of boxes }
var  i,j,k,col : integer;
     x1,y1,x2,y2,dx,dy : extended;
begin
  dx := 1.0/16;
  dy := 1.0/16;
  k := 0;
  for i := 0 to 15 do
    begin
      y1 := i*dy;
      y2 := y1 + dx;
      for j := 0 to 15 do
        begin
          x1 := j*dx;
          x2 := x1 + dx;
          col := arc_en_ciel[k];
          rectfull(x1,y1,x2,y2,col);
          k := k + 1;
        end;
    end;
end;

procedure tform_graph.repaint1(Sender: TObject);
{ repaint procedure (not handled by the system!) }
begin
  with paintbox1 do
    begin
      iax := round(ClientWidth/10.0);
      ibx := round(ClientWidth/12.0);
      iay := round(ClientHeight/15.0);
      iby := round(ClientHeight/12.0);
      imargex := iax + ibx;
      imargey := iay + iby;
      if ( ClientWidth = bbb.Width ) and ( ClientHeight = bbb.Height ) then
        begin
          Canvas.Draw(0,0,bbb);
          exit;
        end;
    end;
  efface_notgplus(nil); { erase graphics when no superimposition }
  case grafgraf of { grafgraf has memorized the graphic procedure to perform for repaint }
    graf_efface  :;
    graf_traj    : gtraj(nb_steps_sav);
    graf_distrib : gdistrib(nb_steps_sav);
    graf_carlo   : gcarlo(nb_steps_sav);
    graf_scat    : gscat(nb_steps_sav);
    graf_courb   : gcourb(nb_steps_sav);
    graf_land    : gland(i0_sav,nb_courb_sav,nbk_sav,val_lab_sav,xminl_sav,xmaxl_sav,yminl_sav,ymaxl_sav,x0_sav,y0_sav);
    else;
  end;
end;

procedure tform_graph.fileexitExecute(Sender: TObject);
{ close graphic window }
begin
  nb_form_graph := nb_form_graph - 1;
  Visible := false;
end;

procedure tform_graph.graphsetExecute(Sender: TObject);
{ popup window for graphics settings }
begin
  with form_graphset do
    begin
      fg := Self;
      Show;
    end;
end;

procedure tform_graph.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  fileexitExecute(nil);
end;

procedure tform_graph.file_saveExecute(Sender: TObject);
{ save graphics in paintbox as bitmap file }
label 1;
var r1,r2 : TRect;
    bm : TBitmap;
    w1,h1 : integer;
begin
  if gplus then
    begin
      file_saveExecute2(nil); { case of superimposition }
      exit;
    end;
  if ( nomficgraph = '' ) then
    nomficgraph := s_ecri_model(1) + '.bmp';
  bm := TBitmap.Create;
  try
    with paintbox1 do
      begin
        w1 := ClientWidth;
        h1 := ClientHeight;
        r1 := Rect(Left,Top,ClientWidth+Left,ClientHeight+Top);
      end;
    r2 := Rect(0,0,w1,h1);
    bm.Width  := w1;
    bm.Height := h1;
    bm.Canvas.CopyRect(r2,paintbox1.Canvas,r1);
    if inputfileopened then
      bm.SaveToFile(nomficgraph)
    else
      with savedialog1 do
        begin
	   { If the directory is not writable, default to UserDir } 
	   if not(LazFileUtils.DirectoryIsWritable (ExtractFilePath(ExcludeTrailingPathDelimiter(nomficgraph)))) then
	      nomficgraph := ConcatPaths([GetUserDir, ExtractFileName(nomficgraph)]);
	   
	   FileName := nomficgraph;
	   Filter := 'Bitmap file|*.bmp|All files|*.*';
	   DefaultExt := '.bmp';
	   InitialDir := ExtractFilePath(nomficgraph);
	   if Execute then
	      if LazFileUtils.fileexistsUTF8(FileName) { *Converted from FileExists* } then
		 if MessageDlg('Overwrite file ' + ExtractFileName(FileName) + '?',
			       mtConfirmation,[mbYes,mbNo],0) <> mrYes then goto 1;
	   nomficgraph := FileName;
	   if LazFileUtils.DirectoryIsWritable (ExtractFilePath(ExcludeTrailingPathDelimiter(nomficgraph))) then
	     begin
		bm.SaveToFile(nomficgraph);
	     end
	    else
	       begin
		  ShowMessage('You do not have the right to write in this directory, please select another location.');
		  goto 1;
	       end;
        end;
1:
  finally
    bm.Free;
  end;
  status;
end;

procedure tform_graph.file_saveExecute2(Sender: TObject);
{ save graphics of bitmap bbb as bitmap file (superimposition) }
label 1;
begin
  if ( nomficgraph = '' ) then
    nomficgraph := s_ecri_model(1) + '.bmp';
  if inputfileopened then
    bbb.SaveToFile(nomficgraph)
  else
    with savedialog1 do
      begin
        FileName := nomficgraph;
	Filter := 'Bitmap file|*.bmp|All files|*.*';
        DefaultExt := '.bmp';
        InitialDir := ExtractFilePath(nomficgraph);
        if Execute then
          if LazFileUtils.fileexistsUTF8(FileName) { *Converted from FileExists* } then
            if MessageDlg('Overwrite file ' + ExtractFileName(FileName) + '?',
              mtConfirmation,[mbYes,mbNo],0) <> mrYes then goto 1;
	 nomficgraph := FileName;
	if LazFileUtils.DirectoryIsWritable (ExtractFilePath(ExcludeTrailingPathDelimiter(nomficgraph))) then
	     begin
		bbb.SaveToFile(nomficgraph);
	     end
	    else
	       begin
		  ShowMessage('You do not have the right to write in this directory, please select another location.');
		  goto 1;
	       end;
      end;
1:
  status;
end;

procedure tform_graph.graph_clear_buttonClick(Sender: TObject);
{ button erase graphics }
begin
  grafgraf := graf_efface;
  efface(nil);
end;

procedure tform_graph.FormDestroy(Sender: TObject);
begin
  bbb.Free; { release memory }
end;

end.
