{ ---------------------------------------------------------------------------- }
{ Copyright (c) 1995-2017, Stéphane Legendre and the ULM development team      }
{                                                                              }
{ This file is part of ULM.                                                    }
{                                                                              }
{ ULM is free software: you can redistribute it and/or modify it               }
{ under the terms of the GNU General Public License as published               }
{ by the Free Software Foundation, either version 3 of the License,            }
{ or (at your option) any later version.                                       }
{                                                                              }
{ ULM is distributed in the hope that it will be useful,                       }
{ but WITHOUT ANY WARRANTY; without even the implied warranty                  }
{ of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                      }
{ See the GNU General Public License for more details.                         }
{                                                                              }
{ You should have received a copy of the GNU General Public License            }
{ along with ULM.  If not, see <http://www.gnu.org/licenses/>.                 }
{ ---------------------------------------------------------------------------- }

unit jglobvar;

{$MODE Delphi}


{  @@@@@@  global variables and internal objects  @@@@@@  }


interface

const  pisur2 = pi/2.0;
       bigint = 2147483647; { largest 32 bits integer }
       hortab = chr(9);     { character horizontal tabulation }

{ ------  utilitary types  ------ }

const  maxextended = 1.0e+4932; { maximum value of floating point real numbers (extended) }
       minextended = 4.0e-4932; { minimum value of floating point real numbers (extended) }

const  matmax = 100;

type   rmat_type = array[1..matmax,1..matmax] of extended;
       imat_type = array[1..matmax,1..matmax] of integer;
       rvec_type = array[1..matmax] of extended;
       ivec_type = array[1..matmax] of integer;
       rvec3_type = array[1..matmax,1..matmax,1..matmax] of extended;

{ ------  codes of internal types  ------ }

const  type_lis      =  0;
       type_ree      =  1;
       type_op1      =  3;
       type_op2      =  4;
       type_variable =  5;
       type_fun      =  6;
       type_arg      =  7;
       type_vec      =  8;
       type_mat      =  9;
       type_rel      = 10;
       type_modele   = 11;
       type_char     = 20;
       type_inconnu  = 100;


{ ------ dictionary ------ }
 
var    dic : integer;     { pointer to dictionary entry in list space (array lis) }
       dic_nb : integer;  { number of words in the dictionary }
       char_nb : integer; { number of characters in the dictionary }


{ ------  lists  ------ }

const  lis_nb_max = 50000;

type   lis_type = record                { LISP-like lists }
                    car_type : integer; { internal type of car cell content }
                    car      : integer; { car cell - pointer to list or internal object }
                    cdr      : integer; { cdr cell - pointer to list or 0 (nil) }
                  end;
                     
type   alis       = array[1..lis_nb_max] of lis_type;
var    lis        : alis;     { array of list space }
       lis_nb     : integer;  { number of allocated cells in array lis }
       lis_lib    : integer;  { pointer to first free cell in array lis }


{ ------  codes for mathematical operators   ------ }

const  op2_plus    =  1; { binary operator + is referenced by the constant 1 }
       op2_mult    =  2; { ... }
       op2_moins   =  3;
       op2_div     =  4;
       op2_puis    =  5;
       op2_infe    =  6;
       op2_supe    =  7;
       op2_mod     =  8;
       op2_convol  = 10;
       
const  op1_moins   =  101; { unary operator - is referenced by the constant 101 }
       op1_cos     =  102; { ... }
       op1_sin     =  103;
       op1_tan     =  104;
       op1_acos    =  105;
       op1_asin    =  106;
       op1_atan    =  107;
       op1_ln      =  108;
       op1_log     =  109;
       op1_exp     =  110;
       op1_fact    =  111;
       op1_sqrt    =  112;
       op1_abs     =  113;
       op1_trunc   =  114;
       op1_round   =  115;
       op1_ln0     =  116;

       op1_gauss   =  130;
       op1_rand    =  131;
       op1_ber     =  132;
       op1_gamm    =  133;
       op1_poisson =  134;
       op1_geom    =  135;
       op1_expo    =  136;

                      
{ ------  real numbers  ------ }

const  ree_nb_max = 2000;

type   ree_type = record
                     val  : extended; { value }
                     mark : integer;  { mark for garbage collector }
                     suiv : integer;  { pointer to next cell in array ree }
                   end;

var    ree        : array[1..ree_nb_max] of ree_type; { cells for real numbers }
       ree_nb     : integer; { number of allocated cells in array ree }
       ree_lib    : integer; { pointer to first free cell in array ree }
       ree_deb    : integer; { pointer to first user allocated cell }
       ree_zero   : integer; { pointer to value 0 }
       ree_un     : integer; { pointer to value 1 }
       ree_deux   : integer; { pointer to value 2 }
       ree_reg1   : integer; { register 1 }
       ree_reg2   : integer; { register 2 }


{ ------  variables  ------ }  

const  variable_nb_max = 5000;

const  maxprev = 100;
       maxprev1 = maxprev + 1;

type   prev_type = array[0..maxprev] of extended;

type   variable_type = record
                         nom    : integer;   { name of variable - pointer to dictionary word in array lis }
                         xrel   : integer;   { pointer to associated relation in array rel or 0 }
                         xvec   : integer;   { pointer to associated vector in array vec or 0 }
                         exp_type : integer; { type of associated expression }
                         exp    : integer;   { pointer to associated expression in array lis }
                         val0   : extended;  { initial value }
                         val    : extended;  { current value }
                         prev   : prev_type; { array to memorize previous values }
                         te     : integer;   { extinction time val < seuil_ext (extinction threshold) }
                         sum    : extended;  { sum of values along time }
                         sumz   : extended;  { sum of values >= 0 or >= seuil_ext }
                         sum2   : extended;  { sum of squares of values }
                         sum2z  : extended;  { sum of squares of values >= 0 or >= seuil_ext }
                         sum3   : extended;  { sum of cubes of values }
                         moy    : extended;  { time average }
                         variance  : extended; { variance }
                         skewness  : extended; { skewness }
                         moyz   : extended;    { time average of values >= 0 or >= seuil_ext }
                         variancez : extended; { variance of values >= 0 or >= seuil_ext }
                         nz     : integer;   { number of values = 0 or < seuil-ext }
                         ne     : integer;   { number of extinctions (< seuil_ext) }
                         ni     : integer;   { number of immigrations }
                         er     : extended;  { extinction rate }
                         ir     : extended;  { immigration rate }
                       end;

var    variable    : array[1..variable_nb_max] of variable_type;
       variable_nb : integer; { number of allocated cells in array variable }
       variable_nb_predef : integer; { number of predefined variables (only one: t) }
       xtime : integer;  { pointer to variable t = time in array variable }
       pprev : integer;  { pointer to previous values of the variables in array variable[x].prev }


{ ------  functions  ------ }

const  fun_nb_max   = 100;
       larg_nb_max  = 30;

type   larg_type  = array[1..larg_nb_max] of integer;

       rlarg_type = array[1..larg_nb_max] of extended;

type   fun_type = record
                    nom       : integer;   { name of function - pointer to dictionary word in array lis }
                    nb_arg    : integer;   { number of arguments }
                    xarg      : larg_type; { array of pointers to arguments in array arg }
                    exp_type  : integer;   { type of associated expression }
                    exp       : integer;   { associated expression - pointer to list in array lis }
                  end;

var    fun           : array[1..fun_nb_max] of fun_type;
       fun_nb        : integer; { number of allocated cells in array fun }
       fun_nb_predef : integer; { number of predefined functions, those that follow: }
       fun_if        : integer; { pointer to function if in array fun }
       fun_min       : integer; { ... }
       fun_max       : integer;
       fun_stepf     : integer;
       fun_gaussf    : integer;
       fun_lognormf  : integer;
       fun_binomf    : integer;
       fun_poissonf  : integer;
       fun_nbinomf   : integer;
       fun_nbinom1f  : integer;
       fun_betaf     : integer;
       fun_beta1f    : integer;
       fun_tabf      : integer;
       fun_bicof     : integer;
       fun_gratef    : integer;
       fun_bdf       : integer;
       fun_lambdaf   : integer;
       fun_prevf     : integer;

       fun_textf      : integer;
       fun_meanf      : integer;
       fun_variancef  : integer;
       fun_skewnessf  : integer;
       fun_cvf        : integer;
       fun_meanzf     : integer;
       fun_variancezf : integer;
       fun_cvzf       : integer;
       fun_nzf        : integer;
       fun_nef        : integer;
       fun_nif        : integer;
       fun_extratef   : integer;
       fun_immratef   : integer;


{ ------  function arguments  ------ }

const  arg_nb_max = 1000;

type   arg_type = record
                    nom : integer;  { name of argument - pointer to dictionary word in array lis }
                    val : extended; { current value }
                  end;

var    arg      : array[1..arg_nb_max] of arg_type;
       arg_nb   : integer; { number of allocated cells in array arg }


{ ------  models  ------ }

const  modele_nb_max   = 5;

type   modele_type = record
                       nom    : integer;   { name of model - pointer to dictionary word in array lis }
                       size   : integer;   { number of relations or matrix and vector size }
                       xmat   : integer;   { pointer to associated matrix in array mat or 0 }
                       xvec   : integer;   { pointer to associated vector in array vec or 0 }
                       xrel   : ivec_type; { array of pointers to associated relations in array rel }
                       pop0   : extended;  { initial total number of individuals }
                       pop    : extended;  { current total number of individuals }
                     end;

var    modele      : array[1..modele_nb_max] of modele_type;
       modele_nb   : integer; { number of allocated cells in array modele }


{ ------  vectors  ------ }

const  vec_nb_max = modele_nb_max;

type   vec_type = record
                    nom       : integer;   { name of vector - pointer to dictionary word in array lis }
                    xmodele   : integer;   { pointer to associated model in array modele }
                    size      : integer;   { vector size }
                    exp_type  : ivec_type; { array of expression types of vector entries }
                    exp       : ivec_type; { array of pointers to expressions (array lis) of vector entries }
                    val0      : rvec_type; { array of initial values of vector entries }
                    val       : rvec_type; { array of current values of vector entries }
                  end;

var    vec      : array[1..vec_nb_max] of vec_type;
       vec_nb   : integer; { number of allocated cells in array vec }


{ ------  matrices  ------ }                       

const  mat_nb_max = modele_nb_max;

type   mat_type = record
                    nom       : integer;    { name of matrix - pointer to dictionary word in array lis }
                    xmodele   : integer;    { pointer to associated model in array modele }
                    size      : integer;    { matrix size }
                    exp_type  : imat_type;  { array of expression types of matrix entries }
                    exp       : imat_type;  { array of pointers to expressions (array lis) of matrix entries }
                    val0      : rmat_type;  { array of initial values of matrix entries }
                    val       : rmat_type;  { array of current values of matrix entries }
                    repro_nb  : integer;    { number of reproductive transitions f_ij }
                    repro_i   : ivec_type;  { array of indices i of f_ij }
                    repro_j   : ivec_type;  { array of indices j of f_ij }
                  end;

var    mat      : array[1..mat_nb_max] of mat_type;
       mat_nb   : integer; { number of allocated cells in array mat }
       

{ ------  relations  ------ }                       

const  rel_nb_max = 500;

type   rel_type = record
                     nom       : integer;  { name of relation - pointer to dictionary word in array lis }
                     xmodele   : integer;  { pointer to associated model in array modele or 0 }
                     exp_type  : integer;  { type of associated expression }
                     exp       : integer;  { pointer to associated expression in array lis }
                     xvar      : integer;  { pointer to associated variable in array variable }
                     val0      : extended; { initial value of the relation }
                     val       : extended; { current value of the relation }
                   end;

var    rel      : array[1..rel_nb_max] of rel_type;
       rel_nb   : integer; { number of allocated cells in array rel }


{ ------  output files  ------ }

const  fic_nb_max = 5;
       var_fic_nb_max = 10;

type   var_fic_type = array[1..var_fic_nb_max] of integer;

type   fic_type = record
                    nam : string;           { name of file }
                    f   : TextFile;         { file pointer }
                    var_fic_nb : integer;   { number of variables in the output file }
                    var_fic : var_fic_type; { array of pointers to variables in array variable }
                    precis  : var_fic_type; { array of precision associated with the variable }
                  end;

var    fic : array[1..fic_nb_max] of fic_type;
       fic_nb : integer; { number of allocated cells in array fic }

       
{ ------  global variables  ------ }

const  seuil_ext_def = 1.0;        { default extinction threshold }
       seuil_div_def = 10000000.0; { default divergence threshold }

var    t__     : integer; { current value of time (variable t) }
       traj__  : integer; { current trajectory index (montecarlo procedure) }

       nomfic : string;           { name of input model file (*.ulm file) }
       modelfileopened : boolean; { indicator that model file is opened }
       nomficin : string;         { name of input command file (batch) }
       inputfileopened : boolean; { indicator that input batch file is opened }
       nomficout : string;        { name of output text file }
       outputfile : boolean;      { indicator that output text file is opened }
       ficout : text;             { output text file }
       nb_cycle : integer;        { number of time steps of the run command }
       nb_cycle_carlo : integer;  { number of time steps of the montecarlo command }
       nb_run_carlo : integer;    { number of trajectories of the montecarlo command }
       seuil_ext : extended;      { extinction threshold in the montecarlo procedure }
       seuil_div : extended;      { divergence threshold in the montecarlo procedure }
       param  : boolean;          { indicator for parameter variation (command param) }
       xparam : integer;          { pointer to current parameter variable in array variable }
       param_exp_type_sav : integer; { type of saved expression type of parameter variable }
       param_exp_sav : integer;   { pointer to saved expression of parameter variable in array lis }
       param_min : extended;      { lower bound for parameter variation (command param) }
       param_max : extended;      { upper bound for parameter variation (command param) }
       param_pas : extended;      { step value  for parameter variation (command param) }
       notext  : boolean;         { indicator for no text output in main window }
       runstop : boolean;         { indicator for stopping execution }


implementation

end.
