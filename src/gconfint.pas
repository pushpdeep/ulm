{ ---------------------------------------------------------------------------- }
{ Copyright (c) 1995-2017, Stéphane Legendre and the ULM development team      }
{                                                                              }
{ This file is part of ULM.                                                    }
{                                                                              }
{ ULM is free software: you can redistribute it and/or modify it               }
{ under the terms of the GNU General Public License as published               }
{ by the Free Software Foundation, either version 3 of the License,            }
{ or (at your option) any later version.                                       }
{                                                                              }
{ ULM is distributed in the hope that it will be useful,                       }
{ but WITHOUT ANY WARRANTY; without even the implied warranty                  }
{ of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                      }
{ See the GNU General Public License for more details.                         }
{                                                                              }
{ You should have received a copy of the GNU General Public License            }
{ along with ULM.  If not, see <http://www.gnu.org/licenses/>.                 }
{ ---------------------------------------------------------------------------- }

unit gconfint;

{$MODE Delphi}

{ @@@@@@  form to display confidence interval of growth rate  @@@@@@ }
{ @@@@@@  knowing standard deviations of matrix entries       @@@@@@ }

interface

uses SysUtils, Graphics, Controls, Forms, Dialogs, StdCtrls, ExtCtrls,
     Grids, Buttons, 
     jglobvar,jmath;

type
  tform_confint = class(TForm)
    left_panel: TPanel; 
    right_panel: TPanel; 
    top_panel: TPanel; 
    stringgrid_sigma: TStringGrid;
    label_sens: TLabel;
    label_mat: TLabel;
    label_lambda: TLabel;
    Edit_mat: TEdit;
    label_sigma_lambda: TLabel;
    label_lambda_plus: TLabel;
    label_lambda_moins: TLabel;
    edit_z_alpha: TEdit;
    label_z_alpha: TLabel;
    label_confidence: TLabel;
    ghost_label1: TLabel;
    ghost_label2: TLabel;
    ghost_label3: TLabel;
    ghost_label4: TLabel;
    button_ok: TBitBtn;
    procedure FormCreate(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure init_confint;
    procedure erreur(s : string);
    procedure Edit_matReturnPressed(Sender: TObject);
    procedure eigenval(x : integer; var err : boolean);
    procedure eigenvec(x : integer; var err : boolean);
    procedure confint(m : integer);
    procedure sigma_lambda1(m : integer; var err : boolean);
    procedure stringgrid_sigmaSelectCell(Sender: TObject; ACol,
      ARow: Integer; var CanSelect: Boolean);
    procedure stringgrid_sigmaSetEditText(Sender: TObject; ACol, 
      ARow: Integer; const Value: WideString);
    procedure edit_z_alphaReturnPressed(Sender: TObject);
    procedure button_okClick(Sender: TObject);
  private
    n : integer;        { size of matrix }
    x_mat : integer;    { pointer to current matrix }
    lambda : cvec_type; { complex eigenvalues }
    v,w : rvec_type;    { left, right eigenvectors associated with dominant eigenvalue }
    lambda1 : extended; { lambda1 = dominant eigenvalue }
    mat_sens : rmat_type;  { matrix of sensitivities }
    mat_sigma : rmat_type; { matrix of standard deviations }
    sigma_lambda : extended; { standard deviation of lambda1 }
    z_alpha : extended; { lambda1 +/- z_alpha*sigma_lambda }
    alpha : extended;   { % confidence interval }
  public
  end;

var form_confint: tform_confint;

implementation

uses jutil,jsymb,jsyntax;

{$R *.lfm}

procedure tform_confint.erreur(s : string);
{ unit specific error procedure }
begin
  erreur_('Confidence Interval - ' + s);
end;

procedure tform_confint.FormCreate(Sender: TObject);
begin
  x_mat := 0;
  z_alpha := 1.0;
end;

procedure tform_confint.eigenval(x : integer;var err : boolean);
begin
  err := true;
  with mat[x] do
    begin
      matvalprop(size,val,lambda);
      if err_math then
        begin
          erreur('Error in eigenvalues computation');
          err_math := false;
          exit;
        end;
      if ( lambda[1].im <> 0.0 ) then
        begin
          erreur('No real dominant eigenvalue found');
          exit;
        end;
      lambda1 := lambda[1].re;
      if ( lambda1 <= 0.0 ) then
        begin
          erreur('Dominant eigenvalue <= 0');
          exit;
        end;
    end;
  err := false;
end;

procedure tform_confint.eigenvec(x : integer; var err : boolean);
var i : integer;
    vv,ww : cvec_type;
begin
  err := true;
  with mat[x] do
    begin
      matvecpropdroite(size,val,lambda[1],ww);
      if err_math then
        begin
          err_math := false;
          exit;
        end;
      for i := 1 to size do w[i] := ww[i].re;
      vecnormalise1(size,w);
      matvecpropgauche(size,val,lambda[1],vv);
      if err_math then
        begin
          err_math := false;
          exit;
        end;
      for i := 1 to size do v[i] := vv[i].re;
      vecnormalise1(size,v);
    end;
  err := false;
end;

procedure tform_confint.sigma_lambda1(m : integer;var err : boolean);
{ compute standard deviation of dominant eigenvalue using sensitivies }
var i,j : integer;
    h,a : extended;
begin
  err := true;
  with mat[m] do
    begin
      h := vecpscal(size,v,w);
      if ( h = 0.0 ) then exit;
      for i := 1 to size do
        for j := 1 to size do
          mat_sens[i,j] := v[i]*w[j]/h; { matrix of sensitivities }
      h := 0.0;
      for i := 1 to size do
        for j := 1 to size do
          begin
            a := mat_sigma[i,j];
            h := h + sqr(mat_sens[i,j]*a);
          end;
      if ( h > 0.0 ) then
        sigma_lambda := sqrt(h)
      else
        sigma_lambda := 0.0;
    end;
  err := false;
end;

procedure tform_confint.confint(m : integer);
var i,j : integer;
    err,valid : boolean;
    a : extended;
begin
  n := mat[m].size;
  { read standard deviations of matrix entries in stringgrid }
  for i := 1 to n do
    for j := 1 to n do
      begin
        valid := TryStrToFloat(stringgrid_sigma.Cells[j,i],a);
        if valid then
          mat_sigma[i,j] := a
        else
          begin
            erreur('Invalid entry: row ' + IntToStr(i) + ', column ' + IntToStr(j));
            stringgrid_sigma.Cells[j,i] := FloatToStr(mat_sigma[i,j]);
          end;
      end;
  edit_mat.Text := s_ecri_mat(m);
  eigenval(m,err); { eigenvalues }
  if err then exit;
  eigenvec(m,err); { eigenvectors }
  if err then exit;
  label_lambda.Caption := 'Lambda = ' + Format('%1.6g',[lambda1]);
  sigma_lambda1(m,err);
  if err then exit;
  label_sigma_lambda.caption := 'Sigma_lambda = ' + Format('%10.6g',[sigma_lambda]);
  label_lambda_plus.caption  := 'Lambda + z*sigma = ' + Format('%10.6g',[lambda1 + z_alpha*sigma_lambda]);
  label_lambda_moins.caption := 'Lambda - z*sigma = ' + Format('%10.6g',[lambda1 - z_alpha*sigma_lambda]);
  alpha := 2.0*phinormal(z_alpha) - 1.0; { use cumulative distribution of the normal distribution }
  label_confidence.caption := '---> with ' + Format('%1.2g',[alpha*100.0]) + '% confidence';
end;

procedure tform_confint.FormActivate(Sender: TObject);
var i,j : integer;
begin
  if ( x_mat = 0 ) then
    begin
      erreur('No matrix-type model');
      edit_mat.Text := '';
      exit;
    end;
  n := mat[x_mat].size;
  with stringgrid_sigma do
    begin
      FixedCols := 1;
      FixedRows := 1;
      ColCount  := n + 1;
      RowCount  := n + 1;
      for i := 1 to ColCount-1 do Cells[i,0] := IntToStr(i);
      for i := 1 to RowCount-1 do Cells[0,i] := IntToStr(i);
      for i := 1 to n do
        for j := 1 to n do
          Cells[j,i] := FloatToStr(mat_sigma[i,j]);
      Constraints.MinHeight := imin((RowCount + 2) * RowHeights[1],
                                   Trunc(Screen.Height / 2));
    end;
  stringgrid_sigma.AutoSizeColumns();
  confint(x_mat); { display confidence interval }
  edit_z_alpha.Text := FloatToStr(z_alpha);
  AutoSize := False;
end;

procedure Tform_confint.init_confint;
var i,j,k : integer;
begin
  init_form(form_confint);
  x_mat := 0;
  z_alpha := 1.0;
  for k := 1 to modele_nb do
    if ( modele[k].xmat <> 0 ) then
      begin
        x_mat := modele[k].xmat;
        n := mat[x_mat].size;
        for i := 1 to n do
          for j := 1 to n do
            if ( mat[x_mat].val[i,j] > 0.0 ) then
              mat_sigma[i,j] := 0.1 { default value }
            else
              mat_sigma[i,j] := 0.0;
        exit;
      end;
end;

procedure Tform_confint.Edit_matReturnPressed(Sender: TObject);
{ name of matrix }
var x,tx : integer;
begin
  trouve_obj(tronque(minuscule(edit_mat.Text)),x,tx);
  if ( x = 0 ) or ( tx <> type_mat ) then
    begin
      erreur('Matrix: unknown matrix name');
      edit_mat.Text := s_ecri_mat(x_mat);
      exit;
    end;
  x_mat := x;
  FormActivate(nil);
end;

procedure tform_confint.stringgrid_sigmaSelectCell(Sender: TObject; ACol,
  ARow: Integer; var CanSelect: Boolean);
begin
  canselect := ( acol > 0 ) and ( arow > 0 );
end;

procedure tform_confint.stringgrid_sigmaSetEditText(Sender: TObject; ACol,
  ARow: Integer; const Value: WideString);
begin
  stringgrid_sigma.AutoSizeColumns();
end;

procedure tform_confint.edit_z_alphaReturnPressed(Sender: TObject);
var a : extended;
    valid : boolean;
begin
  valid := TryStrToFloat(edit_z_alpha.Text,a);
  if valid then
    z_alpha := a
  else
    erreur('Invalid entry: parameter z');
  FormActivate(nil);
end;

procedure tform_confint.button_okClick(Sender: TObject);
begin
  confint(x_mat);
end;

end.
