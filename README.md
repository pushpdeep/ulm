# ULM: Unified Life Models - Population Dynamics Modelling

The ULM computer program has been designed to study
a large panel of population dynamics models,
for research or teaching purposes:

- Ecology
- Management, conservation biology
- Deterministic and stochastic discrete time dynamical systems


## Obtaining Ulm

- You can download some (slightly outdated) binaries for your platform
  on the
  [project homepage](https://www.biologie.ens.fr/~legendre/ulm/ulm.html).
- You can compile ULM from source using Free Pascal and the libraries included
  with the [Lazarus IDE](http://www.lazarus-ide.org/):
- The minimum version to compile ULM is lazarus 1.6. In particular if you encounter compilation issues, make sure that you are using an up-to-date version of the Lazarus IDE.

``` bash
# You will need the lazbuild tool included with lazarus (`sudo apt-get install lazarus` should be enough on debian/ubuntu)
cd ulm/
./configure # (Optional: Use '--prefix=~/.local' for user install)
make
sudo make install
```


### Compile with Qt

To compile ULM with Qt, you first need to install some additional dependencies:

``` bash
# On debian/ubuntu systems
sudo apt install lcl-qt4
```

You can then run
``` bash
./configure --ws=qt # (Optional: Use '--prefix=~/.local' for user install)
make
sudo make install
```

### Preparing the distributable files

#### On GNU/Linux

Generate the `dist/ulm_6.0.tar.gz` by running:
```bash
./configure
make dist
```

Generate the `deb/ulm_6.0_amd64.deb` by running (See `deb/README.md` for more information):
```bash
cd deb/
make
```

#### On Windows

Generate the `dist/ulm_6.0.zip` by compiling ulm with the lazarus IDE and putting the following files in an zip compressed file:
`ulm.exe  models\ docs\pdf\ulmref.pdf LICENSE.txt CHANGELOG.md README.md`.

#### On MacOs

Generate the `build/ulm_6.0.dmg` file by running:

```bash
./configure #(you might have to add your CodeSignature in the Makefile.in beforehand)
make macos-dmg
```

### Prepare the file for hosting
- Copy all the distributable files in `website/dist/`.
- Update the links in `website/ulm.html` if necessary.
- Host the folder `website`.

## File description

- `ulm.lpr`		Lazarus project file (Delphi `ulm.dpr`).

The file `ulm.lpr` lists the units `*.pas` that contain Pascal visual
components. The name of these units begins with `g`, e. g., `gulm.pas`.

Names of units without Pascal visual components begin wih `j`,
e. g., `jmath.pas`.

The Lazarus files `*.lfm` (Delphi `*.xfm`) describe each window ('`form`').
For example, the file `gulm.lfm` is associated with the unit `gulm.pas`.

- `gulm.pas`		window for main program

- `ggraph.pas`		window for graphics and graphic procedures
- `ggraphset.pas`	window for setting graphics
- `gabout.pas`		window about, about the ULM program
- `grunset.pas`		window for setting the run command
- `gviewvar.pas`	window for displaying variables of the ULM model
- `gcalc.pas`		window for online desktop calculator
- `gedit.pas`		window where the model file is displayed and can be edited
- `ghighlighter.pas`    syntax highlight of model file in edit window
- `gviewall.pas`	window for displaying all ULM objects
- `gtext.pas`		window for displaying results
- `gtextset.pas`	window for setting the text window
- `gsensib.pas`		window for computing sensitivities
- `gstocsensib.pas`	window for computing stochastic sensitivities
- `gconfint.pas`	window for confidence interval
- `gconfint2.pas`	window for confidence interval 2
- `gprop.pas`		window for matrix properties
- `gmulti.pas`		window for properties of multisite matrix
- `gage.pas`		window for age computation in size-classified matrix
- `gspec.pas`		window for computing power spectrum
- `gcorrel.pas`		window for computing correlation
- `glyap.pas`		window for computing Lyapunov exponent
- `glandscape.pas`	window for computing fitness landscape

---

- `jglobvar.pas`	declaration of global variables of the ULM program

- `jsymb.pas`		management of lists and ULM objects
- `jsyntax.pas`		management of input and output
- `jmath.pas`		mathematical library
- `jcompil.pas`		'compilation' of the ULM model input file
- `jeval.pas`		ULM evaluator
- `jmatrix.pas`		computation of matrix descriptors
- `jinterp.pas`		ULM command interpreter
- `jutil.pas`		some utilitary procedures
- `jrun.pas`		procedure for running the ULM model
- `jcarlo.pas`		Monte Carlo procedure

---

- `icons/ulm.ico`   icon of the ULM program


## Contributors

ULM was originally developped by Stéphane Legendre with contributions
from Jean Clobert, Régis Ferrière, Frédéric Gosselin, Jean-Dominique
Lebreton and François Sarrazin.

In 2016 a group of students (François Bienvenu, Guilhem Doulcier, Hugo
Gruson and Maxime Woringer) worked with Stéphane to open its sources
and make it compile with Free Pascal/Lazarus on all modern operating systems.

## Contact
- Email: legendre (at) ens (dot) fr
- Homepage: https://www.biologie.ens.fr/~legendre/index.html

Address:

	Stéphane Legendre
	Team of Mathematical Eco-Evolution
	Ecole Normale Supérieure
	46 rue d'Ulm
	75005 Paris
	France
	PARIS

## License

Copyright (c) 1995-2017, Stéphane Legendre and the ULM development team

ULM is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your
option) any later version.

ULM is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License
along with ULM.  If not, see <http://www.gnu.org/licenses/>.
