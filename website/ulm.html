<!DOCTYPE html>
<html>

<head>
  <link rel="icon" type="image/ico" href="./favicon.ico"/>
  <link rel="stylesheet" href="./style.css" type="text/css" />
  <meta charset="utf-8">
  <meta name="description" content="The ULM computer program has been designed
  to study a large panel of population dynamics models, for research or
  teaching purposes.">
  <title>Unified Life Models</title>
</head>
<body>
<main>

  <header>
    <h1>Unified Life Models</h1>
    <div id="hook">
      <img alt="ULM icon" src="ulm.svg">
      <p> Study a large panel of
	population dynamics models for research or teaching in the
	fields of ecology, conservation and management biology. </p>
    </div>    
  </header>

<p style="clear:both">ULM (<span style="font-variant:small-caps">Unified Life
Models</span>, Legendre &amp; Clobert 1995, Ferrière &amp; <i>al.</i> 1996) is
an <a href="https://gitlab.com/ecoevomath/ulm">open-source</a> software
enabling the simulation and analysis of deterministic and stochastic discrete
time dynamical systems for population dynamics modeling. ULM works natively on
Windows, Linux and macOS.</p>

<div id="download">
<div id="Version">Download Ulm 6.0 - June 2017</div>
<div class="download"><a href="dist/ulm_6.0_amd64.tar.gz"> GNU/Linux (tar.gz)</a>  <a href="dist/ulm_6.0_amd64.deb">(deb)</a>  </div>
<div class="download"><a href="dist/ulm_6.0_win.zip"> Windows (zip) </a> </div>
<div class="download"><a href="dist/ulm_6.0_macOs.dmg"> MacOs (dmg)</a> </div>
<a href="#installation"> Installation instructions and alternative architectures</a>
</div>

<h1>Features</h1>

  <p>Models are described using a simple declaration language, close
    to the mathematical formulation. The system can be studied
    interactively by means of simple commands, producing convenient
    graphics and numerical results.</p>

<div class="doc"> <a href="ulmref.pdf">Read the documentation (pdf, 354Ko)</a> </div>
	

<h2>Model</h2>
<div class="screen">
  <a href="screenshot01.png"><img src="screenshot01.png"></a>
  A chaotic attractor <em>(Cazelles & Ferrière 1992).</em>
</div>

<ul>
  <li>any species life cycle graph (matrix models, Caswell 1989, 2000)</li>
  <li>inter and intra-specific competition, density dependence (non linear systems)</li>
  <li>environmental stochasticity (random processes, Tuljapurkar 1990)</li>
  <li>demographic stochasticity (branching processes)</li>
  <li>metapopulations, migrations</li>
</ul>

<p>Stochastic models are handled via Monte Carlo simulation.</p>

<h2>Compute</h2>
<div class="screen">
  <a href="screenshot02.png"><img src="screenshot02.png"></a>
  Computing Lyapunov exponents.
</div>

<ul>
  <li>Population trajectories</li>
  <li>Growth rate, population structure and reproductive values</li>
  <li>Sensitivities and elasticities to changes in parameters</li>
  <li>Generation time and net reproductive rate</li>
  <li>Fitness landscapes</li>
  <li>Probability of extinction or quasi-extinction, time of extinction</li>
  <li>Lyapunov exponents and bifurcation diagrams</li>
</ul>

<h1>Quick Start</h1>

<p>The ULM distribution includes the executable, example model files (in the
<code>models</code> folder), and the <a href="ulmref.pdf">documentation</a> in
a pdf format.</p>

<ul>
  <li>Start the ULM program.</li>
  <li>Open a model file or create your own.</li>
  <li>Click <code>compile</code> to process the file,
  and <code>run</code> to run the model.</li>
</ul>
    
<h1 id="installation">Installation</h1>

<h2>Windows</h2>

<ul>
  <li>Download the <a href="ulm">ULM installer</a>.</li>
  <li>Double click on the <code>autoulm.exe</code> file and provide the path of
  installation (e.g. <code>C:\Program Files\Ulm</code>).</li>
  <li>Start ULM by double clicking on <code>ulm.exe</code>.</li>
</ul>

<h2>Linux</h2>

<p>For users of Debian-based distributions (such as Ubuntu):</p>
<ul>
  <li>Download the ULM package that corresponds to your
  architecture and distribution: <a href="dist/ulm_6.0_amd64.tar.gz">ulm_6.0_amd64.tar.gz</a>, <a href="dist/ulm_6.0_amd64.deb">ulm_6.0_amd64.deb</a> and for 32 bits machines: <a href="dist/ulm_6.0_i386.deb">ulm_6.0_i386.deb</a>.</li>
  <li>Install it by running <code>dkpg -i ulm.deb</code>.</li>
  <li>Start ULM by running the command <code>ulm</code>.</li>
</ul>

<p>The official version of ULM on Linux uses GTK, but a Qt version is
also available for Qt-based DE users: <a href="dist/ulmqt_6.0_amd64.deb">ulmqt_6.0_amd64.deb</a></p>

<h2>macOS</h2>

<ul>
  <li>Download the ULM package <a href="dist/ulm_6.0.dmg">ulm_6.0.dmg</a>.</li>
  <li>Drop the Ulm icon to <code>Applications</code>.
  <li>Start Ulm by running the program <code>ulm</code>.</li>
</ul>

<h2 id="compiling">Compiling from source</h2>

<p>The source code can be downloaded from our
<a href="https://gitlab.com/ecoevomath/ulm">git repository</a>.
Compilation instructions are in the 
<a href="https://gitlab.com/ecoevomath/ulm/blob/master/README.md">README.md</a> file.</p>

<h1>References</h1>

<ul>
<li>Bienvenu F &amp; S Legendre. 2015. A new approach to the generation time in
matrix population models. American Naturalist 185:834-843.</li>

<li>Caswell H. 1989. Matrix Population Models. Sinauer, Sunderland,
Massachussets, USA.</li>

<li>Caswell H. 2000. Matrix Population Models: Construction, Analysis, and
Interpretation. 2nd edition. Sinauer, Sunderland, Massachussets, USA.</li>

<li>Ferrière R, F Sarrazin, S Legendre &amp; J-P Baron. 1996. Matrix population
models applied to viability analysis and conservation: Theory and practice with
ULM software.  Acta OEcologica 17:629-656.</li>

<li>Legendre S &amp; J Clobert. 1995. ULM, a software for conservation and
evolutionary biologists. Journal of Applied Statistics 22:817-834.</li>

<li>Legendre S, J Clobert, AP M&oslash;ller &amp; G Sorci. 1999. Demographic
stochasticity and the social mating system in the process of extinction of
small populations: The case of passerines introduced to New Zealand.  American
Naturalist 153:449-463.</li>

<li>Legendre S. 2004. Influence of age structure and mating system on population
viability. In Evolutionary Conservation Biology (Ferrière R, U Dieckmann &amp;
D Couvet eds.), Cambridge University Press, pp.  41-58.</li>

<li>Schoener TW, J Clobert, S Legendre &amp; DA Spiller. 2003. Life-history
models of extinction: A test with island spiders. American Naturalist
162:558-573.</li>

<li>Tuljapurkar S. 1990. Population Dynamics in Variable Environments. Lecture
Notes in Biomathematics, Springer Verlag, Germany.</li>
</ul>

<h1>People</h1>
<h2>Author</h2>
<p id="address"><strong>Stéphane Legendre</strong>
Team of Eco-Evolutionary Mathematics
Ecole normale supérieure
46 rue d'Ulm
75005 Paris
France
</p>

<h2>ULM development team</h2>

<p>François Bienvenu, Guilhem Doulcier, Hugo Gruson and Maxime Woringer.</p>

<h2>Contributors</h2>

<p>Jean Clobert, Régis Ferrière, Frédéric Gosselin, Jean-Dominique Lebreton,
François Sarrazin, Karl-Michael Schindler, Alexis Simon.</p>

<h2>Thanks</h2>

<p>The developpers of Free Pascal and Lazarus.</p>
<p>All the people who used and supported ULM along the years.</p>

<h2>Contact</h2>

<p>Issues, feature requests and feedback can be reported
<a href="https://gitlab.com/ecoevomath/ulm">on our issue tracker</a> or
by <a href="mailto:legendre@ens.fr">writing to Stéphane Legendre</a>.</p>

<h1>Links</h1>

<ul>
  <li><a href="https://gitlab.com/ecoevomath/ulm">ULM's GitLab repository</a></li>
  <li><a href="https://www.biologie.ens.fr/~legendre/index.html">Stéphane
    Legendre's website</a></li>
  <li><a href="https://www.lazarus-ide.org/">Lazarus</a></li>
</ul>

<div id="Update">last update: June 18th 2017</div>
</main>
</body>
</html>
