#!/bin/sh

prefix=/usr/local
lazbuild_cmd=lazbuild
build_doc=true
fpc_cmd=fpc

## ===== LAZARUS ====
echo "Checking for lazarus tools..."
command -v ${lazbuild_cmd} >/dev/null 2>&1 || { echo >&2 "ULM requires 'lazbuild' but it's not installed.  Aborting."; exit 1; }

## ===== LaTex ======
echo "Checking for LaTex to compile docs..."
command -v pdflatex >/dev/null 2>&1 || { echo >&2 "ULM documentation requires 'pdflatex' but it's not installed.  Docs won't be compiled."; build_doc=false; }
command -v bibtex >/dev/null 2>&1 || { echo >&2 "ULM documentation requires 'bibtex' but it's not installed.  Docs won't be compiled."; build_doc=false; }

check_latex_package () {
  if [ $(kpsewhich ${1}.sty) ]; then
      echo "-- Latex package: ${1}.sty FOUND"
  else
      echo "-- Latex package: ${1}.sty NOT FOUND";
      echo >&2 "ULM documentation requires the '${1}' LaTex package but it's not installed.  Docs won't be compiled."
      build_doc=false;
  fi
}

check_latex_class () {
  if [ $(kpsewhich ${1}.cls) ]; then
      echo "-- Latex class: ${1}.cls FOUND"
  else
      echo "-- Latex class: ${1}.cls NOT FOUND";
      echo >&2 "ULM documentation requires the '${1}' LaTex class but it's not installed.  Docs won't be compiled."
      build_doc=false;
  fi
}

check_latex_class scrartcl
check_latex_package inputenc
check_latex_package fourier
check_latex_package tabularx
check_latex_package babel
check_latex_package microtype
check_latex_package amsmath
check_latex_package amssymb
check_latex_package amsthm
check_latex_package xcolor
check_latex_package graphicx
check_latex_package hyperref
check_latex_package url
check_latex_package listings
check_latex_package tikz
check_latex_package tikzscale
check_latex_package tkz-graph
check_latex_package xspace
check_latex_package sectsty
check_latex_package fancyhdr
check_latex_package booktabs

## ===== Handle prefixes =====
for arg in "$@"; do
    case "$arg" in
    --prefix=*)
        prefix=`echo $arg | sed 's/--prefix=//'`
        ;;
    --lazbuild=*)
        lazbuild_cmd=`echo $arg | sed 's/--lazbuild=//'`
        ;;
    --widgetset=*)
        widgetset=$arg
        ;;
    --ws=*)
        widgetset=$arg
        ;;
    --os=*)
        os=`echo $arg | sed "s/--os=//"`
        ;;
    --cpu=*)
        cpu=`echo $arg | sed "s/--cpu=//"`
        ;;
    --fpc=*)
	fpc_cmd=`echo $arg | sed "s/--fpc=//"`
	;;
    --help)
        echo 'usage: ./configure [options]'
        echo 'options:'
        echo '  --prefix=<path>: installation prefix'
        echo '  --lazbuild=<path>: lazarus executable'
	echo '  --fpc=<path>: path to fpc executable'
        echo '  --widgetset=<widgetset> or --ws=<widgetset>: widgetset'
        echo '  --os=<os>: target operating system'
	echo '  --cpu=<cpu>: target processor'
	echo 'all invalid options are silently ignored'
        exit 0
        ;;

    esac
done

## ===== Update templates =====
echo 'generating makefile ...'
echo "prefix = $prefix" > Makefile
echo "lazbuild = $lazbuild_cmd" >> Makefile
echo "fpc = $fpc_cmd" >> Makefile
echo "build_doc=$build_doc" >> Makefile
echo "widgetset=$widgetset" >> Makefile
cat Makefile.in >> Makefile
echo 'generating shortcut ...'
sed -e "s#DESTDIR#$prefix#" ./icons/ulm.desktop.tp > ./icons/ulm.desktop

cp ./src/ulm.lpi.tp ./src/ulm.lpi
sed -i "s/\$OS/$os/" ./src/ulm.lpi
sed -i "s/\$CPU/$cpu/" ./src/ulm.lpi
if [ "$lazbuild_cmd" = "typhonbuild64" ]; then
     sed -i 's/"SynEdit"/"bs_SynEdit"/' ./src/ulm.lpi
fi

echo
echo './configure done, here is a summary:'
echo
echo '-- Building ULM: true'
echo '-- Building doc:' $build_doc


echo 'configuration complete, type make to build.'
