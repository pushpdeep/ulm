{ Two-sex model for Grizzly bear population (Ursus arctos horriblis)
{ with demographic stochasticity and environmental stochasticity.

{ **** increasing population ****

{ See related files griza_0.ulm, griza_2s.ulm, griza_2sd.ulm.

{ Mills LS, SG Hayes, C Baldwin, MJ Wisdom, J Citta, DJ Mattson & K Murphy. 1996.
{ Factors leading to different viability predictions for a Grizzly bear data set.
{ Conservation Biology 10:863-873.

{ Example of commands: to be compared with the results of Mills et al.
{	increasing population (lambda = 1.02)
{	initial population size: 125 males + 125 females
{
{	graph t n
{	text  n
{	montecarlo 48 500 2     ( run Monte Carlo simulation
{				( 48 time steps, 500 repetitions,
{				( extinction threshold = 2 (0 or 1 individual left)
{	montecarlo 48 500 100	( run Monte Carlo simulation
{				( 48 time steps, 500 repetitions,
{				( extinction threshold = 100 individuals
{ Results:
{	growth rate estimator (mean growth rate) = 0.9884
{	probability of extinction = 0.018
{	mean time to extinction = 39.0 years
{	average population size (standard error) = 339.0 (20.8)
{	average non extinct population size (standard error) = 345.9 (21.0)
{	probability of quasi-extinction (< 100 individuals) = 0.528
{	mean time to quasi-extinction (< 100 individuals) = 22.1 years


defmod griza(60)
rel:f1,f2,f3,f4,f5,f6,f7,f8,f9,f10,f11,f12,f13,f14,f15,f16,f17,f18,f19,f20,f21,f22,f23,f24,f25,f26,f27,f28,f29,f30,m1,m2,m3,m4,m5,m6,m7,m8,m9,m10,m11,m12,m13,m14,m15,m16,m17,m18,m19,m20,m21,m22,m23,m24,m25,m26,m27,m28,m29,m30

{ ---- relations for females ----

defrel f1
nf1 = nf1a

defrel f2
nf2 = binomf(nf1,s1)

defrel f3
nf3 = binomf(nf2,s1)

defrel f4
nf4 = binomf(nf3,s1)

defrel f5
nf5 = binomf(nf4,s1)

defrel f6
nf6 = binomf(nf5,sa)

defrel f7
nf7 = binomf(nf6,sa)

defrel f8
nf8 = binomf(nf7,sa)

defrel f9
nf9 = binomf(nf8,sa)

defrel f10
nf10 = binomf(nf9,sb)

defrel f11
nf11 = binomf(nf10,sb)

defrel f12
nf12 = binomf(nf11,sb)

defrel f13
nf13 = binomf(nf12,sb)

defrel f14
nf14 = binomf(nf13,sb)

defrel f15
nf15 = binomf(nf14,sb)

defrel f16
nf16 = binomf(nf15,sc)

defrel f17
nf17 = binomf(nf16,sc)

defrel f18
nf18 = binomf(nf17,sc)

defrel f19
nf19 = binomf(nf18,sc)

defrel f20
nf20 = binomf(nf19,sc)

defrel f21
nf21 = binomf(nf20,sc)

defrel f22
nf22 = binomf(nf21,sc)

defrel f23
nf23 = binomf(nf22,sc)

defrel f24
nf24 = binomf(nf23,sc)

defrel f25
nf25 = binomf(nf24,sc)

defrel f26
nf26 = binomf(nf25,sc)

defrel f27
nf27 = binomf(nf26,sc)

defrel f28
nf28 = binomf(nf27,sc)

defrel f29
nf29 = binomf(nf28,sd)

defrel f30
nf30 = binomf(nf29,se)

{ ---- relations for males ----

defrel m1
nm1 = nm1a

defrel m2
nm2 = binomf(nm1,s1)

defrel m3
nm3 = binomf(nm2,s1)

defrel m4
nm4 = binomf(nm3,s1)

defrel m5
nm5 = binomf(nm4,s1)

defrel m6
nm6 = binomf(nm5,sa)

defrel m7
nm7 = binomf(nm6,sa)

defrel m8
nm8 = binomf(nm7,sa)

defrel m9
nm9 = binomf(nm8,sa)

defrel m10
nm10 = binomf(nm9,sb)

defrel m11
nm11 = binomf(nm10,sb)

defrel m12
nm12 = binomf(nm11,sb)

defrel m13
nm13 = binomf(nm12,sb)

defrel m14
nm14 = binomf(nm13,sb)

defrel m15
nm15 = binomf(nm14,sb)

defrel m16
nm16 = binomf(nm15,sc)

defrel m17
nm17 = binomf(nm16,sc)

defrel m18
nm18 = binomf(nm17,sc)

defrel m19
nm19 = binomf(nm18,sc)

defrel m20
nm20 = binomf(nm19,sc)

defrel m21
nm21 = binomf(nm20,sc)

defrel m22
nm22 = binomf(nm21,sc)

defrel m23
nm23 = binomf(nm22,sc)

defrel m24
nm24 = binomf(nm23,sc)

defrel m25
nm25 = binomf(nm24,sc)

defrel m26
nm26 = binomf(nm25,sc)

defrel m27
nm27 = binomf(nm26,sc)

defrel m28
nm28 = binomf(nm27,sc)

defrel m29
nm29 = binomf(nm28,sd)

defrel m30
nm30 = binomf(nm29,se)

{ initial population size according to stable age distribution:
defvar nf1 = 17

defvar nf2 = 15

defvar nf3 = 12

defvar nf4 = 11

defvar nfj = nf1+nf2+nf3+nf4

defvar nf5 = 9

defvar nf6 = 8

defvar nf7 = 6

defvar nf8 = 6

defvar nfa = nf5+nf6+nf7+nf8

defvar nf9 = 5

defvar nf10 = 4

defvar nf11 = 4

defvar nf12 = 3

defvar nf13 = 3

defvar nf14 = 2

defvar nfb = nf9+nf10+nf11+nf12+nf13+nf14

defvar nf15 = 2

defvar nf16 = 2

defvar nf17 = 2

defvar nf18 = 2

defvar nf19 = 2

defvar nf20 = 1

defvar nf21 = 1

defvar nf22 = 1

defvar nf23 = 1

defvar nf24 = 1

defvar nf25 = 1

defvar nf26 = 1

defvar nf27 = 1

defvar nf28 = 1

defvar nf29 = 1

defvar nf30 = 0

defvar nfc = nf15+nf16+nf17+nf18+nf19+nf20+nf21+nf22+nf23+nf24+nf25+nf26+nf27+nf28+nf29+nf30

defvar nf = nfj+nfa+nfb+nfc

defvar nm1 = 17

defvar nm2 = 15

defvar nm3 = 12

defvar nm4 = 11

defvar nmj = nm1+nm2+nm3+nm4

defvar nm5 = 9

defvar nm6 = 8

defvar nm7 = 6

defvar nm8 = 6

defvar nma = nm5+nm6+nm7+nm8

defvar nm9 = 5

defvar nm10 = 4

defvar nm11 = 4

defvar nm12 = 3

defvar nm13 = 3

defvar nm14 = 2

defvar nmb = nm9+nm10+nm11+nm12+nm13+nm14

defvar nm15 = 2

defvar nm16 = 2

defvar nm17 = 2

defvar nm18 = 2

defvar nm19 = 2

defvar nm20 = 1

defvar nm21 = 1

defvar nm22 = 1

defvar nm23 = 1

defvar nm24 = 1

defvar nm25 = 1

defvar nm26 = 1

defvar nm27 = 1

defvar nm28 = 1

defvar nm29 = 1

defvar nm30 = 0

defvar nmc = nm15+nm16+nm17+nm18+nm19+nm20+nm21+nm22+nm23+nm24+nm25+nm26+nm27+nm28+nm29+nm30

defvar nm = nmj+nma+nmb+nmc

defvar n = nm + nf

{ survival rates with environmental stochasticity:
defvar s10 = 0.87

{ standard deviation
defvar s1d = 0.3*s10

defvar s1 = beta1f(s10,s1d)

defvar sa0 = 0.86

defvar sad = 0.3*sa0

defvar sa = beta1f(sa0,sad)

defvar sb0 = 0.87

defvar sbd = 0.3*sb0

defvar sb = beta1f(sb0,sbd)

defvar sc0 = 0.94

defvar scd = 0.3*sc0

{defvar sc = beta1f(sc0,scd)
defvar sc = min(max(gaussf(sc0,scd),0),1)

defvar sd0 = 0.50

defvar sdd = 0.3*sd0

defvar sd = beta1f(sd0,sdd)

defvar se0 = 0.25

defvar sed = 0.3*se0

defvar se = beta1f(se0,sed)

{ fecundity rates with environmental stochasticity:
deffun f(c1,c2,c3) = c1 + 2*c2 + 3*c3

defvar ca1 = 0.40

defvar ca2 = 0.40

defvar ca3 = 0.20

defvar ga = 0.23

{ proportion of reproductive females
defvar nfar = binomf(nfa,ga)

defvar fa0 = f(ca1,ca2,ca3)

{ standard deviation
defvar fad = 0.3*fa0

defvar fa = max(gaussf(fa0,fad),0)

{ offspring
defvar pa = poisson(fa) @ nfar

{ female sex ratio
defvar sigma = 0.5

defvar cb1 = 0.05

defvar cb2 = 0.40

defvar cb3 = 0.55

defvar gb = 0.29

{ proportion of reproductive females
defvar nfbr = binomf(nfb,gb)

defvar fb0 = f(cb1,cb2,cb3)

defvar fbd = 0.3*fb0

defvar fb = max(gaussf(fb0,fbd),0)

{ offspring
defvar pb = poisson(fb) @ nfbr

defvar cc1 = 0.60

defvar cc2 = 0.40

defvar cc3 = 0.00

defvar gc = 0.36

{ proportion of reproductive females
defvar nfcr = binomf(nfc,gc)

defvar fc0 = f(cc1,cc2,cc3)

defvar fcd = 0.3*fc0

defvar fc = max(gaussf(fc0,fcd),0)

{ offspring
defvar pc = poisson(fc) @ nfcr

{ total number of offspring
defvar p = pa + pb + pc

{ female offspring
defvar nf1a = binomf(p,sigma)

{ male offspring
defvar nm1a = p - nf1a
