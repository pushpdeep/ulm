#! /usr/bin/env python3
# -*-coding: utf-8 -*-

"""
Script to generate the .ico file from the .svg
"""

from subprocess import call

def create_png(size):
    pop = call(['inkscape', '-z', '-e', 'ulm{}.png'.format(size),
                '-w {}'.format(size), '-h {}'.format(size), 'ulm.svg'])

sizes = [16, 20, 24, 32, 40, 48, 60, 72, 128, 256, 512, 768]

for s in sizes:
    create_png(s)

create_ico = ['convert']
create_ico.extend(['ulm{}.png'.format(s) for s in [16, 20, 24, 32, 40, 48, 60, 72, 128, 256, 512, 768]])
create_ico.append('ulm.ico')

call(create_ico)
