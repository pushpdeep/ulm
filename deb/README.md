A Debian package for ULM
------------------------

The `deb/` folder is dedicated to the build of Debian `.deb` files. The build is mostly automated by the use of a `Makefile`. The Makefile is used to automate simple the creation/clean of the architecture. It is not part of the Debian packaging manual.

## Makefile
In a nutshell (**carefully** read the *reference* before editing anything -- or at least be comfortable with the Debian packaging procedure), building a Debian package works as follows:

1. The *upstream tarball* is downloaded and renamed
2. The *upstream tarball* is decompressed
3. a Debian-specific `debian/` file is created, containing packaging-specific instructions
4. the package is built.

The `Makefile` automates this process as follows:

### make 

1. Create a `tar` archive of the ULM directory (excluding the `.git` directory and `deb/`)
2. Uncompress it and rename it to follow the packaging guidelines
3. Copy the `debian/` directory to the `ulm-x.x` subdirectory
4. Build the Debian package

### make clean
Removes the `tar` archive and its decompressed counterpart.


## References

- [Packaging in Debian](https://wiki.debian.org/Packaging/Intro?action=show&redirect=IntroDebianPackaging)
- [Copyright file](https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/#header-paragraph)
