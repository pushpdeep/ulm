%%% =================== defmod ==============================
\keywd{defmod}{declaration of model}

A model describes a population whose dynamics are driven by a set of discrete time relations. These relations can be put in matrix form, using a population matrix and a population vector.

\paragraph{matrix-type model}~\\

\begin{tabular}{ll}
    \codelineeditor{defmod}{model\_name(k)}{declaration of model of size $k$}
    \codelineeditor{mat:}{aaa}{name of matrix}
    \codelineeditor{vec:}{vvv}{name of vector}
\end{tabular}\\

Example: file \texttt{pass\_0.ulm}.\\

\begin{tabular}{ll}
    \codelineeditor{defmod}{passerine(2)}{model \textit{passerine} of size 2}
    \codelineeditor{mat:}{a}{matrix $a$}
    \codelineeditor{vec:}{w}{vector $w$}
\end{tabular}

The matrix and vector are to be declared elsewhere in the file, using the \textbf{defmat} and \textbf{defvec} keywords.

\paragraph{relation-type model}~\\

\begin{tabular}{ll}
    \codelineeditor{defmod}{model\_name(k)}{declaration of model of size $k$}
    \codelineeditor{rel:}{rel1, \ldots, relk}{names of $k$ relations}
\end{tabular}\\

Example: file \texttt{met\_esd.ulm}.\\

\begin{tabular}{ll}
    \codelineeditor{defmod}{metepeira\_esd(5)}{declaration of model of size 5}
    \codelineeditor{rel:}{r1, r2, r3, r4, r5}{5 relations: r1, \ldots, r5}
\end{tabular}\\

The relations are to be declared elsewhere in the file, using the \textbf{defrel} keyword.

A single model file may include several models.

%%% =================== defmat ==============================
\keywd{defmat}{declaration of matrix}

\begin{tabular}{ll}
    \codelineeditor{defmat}{matrix\_name(k)}{declaration of matrix of size $k$}
    \codelineeditor{}{a11, \ldots, a1k}{first line of matrix entries}
    \codelineeditor{}{a21, \ldots, a2k}{second line of matrix entries}
    \codelineeditor{}{\vdots}{}
    \codelineeditor{}{ak1, \ldots, akk}{$k$-th line of matrix entries}
\end{tabular}\\

Example:

\begin{tabular}{ll}
    \codelineeditor{defmat}{a(2)}{$2\times 2$ matrix for passerine model file \texttt{pass\_0.ulm}}
    \codelineeditor{}{sigma*s0*f1,	sigma*s0*f2}{matrix entries}
    \codelineeditor{}{s, v}{}
\end{tabular}\\


%%% =================== defvec ==============================
\keywd{defvec}{declaration of vector}

\begin{tabular}{ll}
    \codelineeditor{defvec}{vector\_name(k)}{declaration of vector of size $k$}
    \codelineeditor{}{n1, \ldots, nk}{names of $k$ variables that are the vector entries}
\end{tabular}\\

Example:

\begin{tabular}{ll}
    \codelineeditor{defvec}{w(2)}{population vector for passerine model file \texttt{pass\_0.ulm}}
    \codelineeditor{}{}{}
    \codelineeditor{}{n1, n2}{names of vector variables}
\end{tabular}\\


%%% =================== defrel ==============================
\keywd{defrel}{declaration of relation}

\begin{tabular}{ll}
    \codelineeditor{defrel}{relation\_name}{}
    \codelineeditor{}{var\_name = expression}{expression for the relation}
\end{tabular}\\

Example: file \texttt{pass\_2s.ulm}

\begin{tabular}{ll}
    \codelineeditor{defrel}{rm1}{}
    \codelineeditor{}{nm1 = \mathkeywords{binomf}(pf1m+pf2m, sm0)}{}
\end{tabular}\\

\begin{tabular}{ll}
    \codelineeditor{defrel}{rm2}{}
    \codelineeditor{}{nm2 = \mathkeywords{binomf}(nm1, sm) + \mathkeywords{binomf}(nm2, vm)}{}
\end{tabular}\\

In this example, variables $nm1$ and $nm2$ are relation-variables. From one time step to the next, relation-variables are updated in parallel (and not sequentially), as would be the case in matrix form.


%%% =================== defvar ==============================
\keywd{defvar}{declaration of variable}

\begin{tabular}{ll}
    \codelineeditor{defvar}{variable\_name = expression}{}
\end{tabular}\\

There is one and only one predefined variable, whose name is $t$ for `time'. Variable $t$ takes values 0, 1, 2, \ldots as the system is run. Other variables are decalred by the user.

If \textit{variable\_name} if the name of variable pertaining to a vector (vector-variable, \defkeyword{defvec}) or to a relation (relation-variable, \defkeyword{defrel}), then \textit{expression} must be a real number, which is the initial value of the variable.\\

Examples:

\begin{tabular}{ll}
    \codelineeditor{defvar}{s0 = 0.2}{constant}
\end{tabular}\\

\begin{tabular}{ll}
    \codelineeditor{defvar}{n1 = 100}{relation-variable with initial value 100}
\end{tabular}\\

\begin{tabular}{ll}
    \codelineeditor{defvar}{phi = (1+\mathkeywords{sqrt}(5))/2}{constant}
\end{tabular}\\

\begin{tabular}{ll}
    \codelineeditor{defvar}{x = \mathkeywords{gaussf}(2, 0.1)}{random variable}
    & \textit{normal distribution with mean 2 and standard deviation 0.1}
\end{tabular}\\

\begin{tabular}{ll}
    \codelineeditor{defvar}{x = \mathkeywords{if}(t $>$ 10, x, 0)} {conditional}
\end{tabular}\\

\begin{tabular}{ll}
    \codelineeditor{defvar}{n1 n2 = 100}{shared declaration}
\end{tabular}\\


%%% =================== deffun ==============================
\keywd{deffun}{declaration of function}

\begin{tabular}{ll}
    \codelineeditor{deffun}{function\_name(arg1, \ldots, argN) = expression}{}
\end{tabular}\\

The arguments of the function have the names $arg1,\ldots, argN$.\\

Examples:

\begin{tabular}{ll}
    \codelineeditor{deffun}{som(v, n) = (1 - v $\hat{}$ (n+1)) / (1 - v)}{sum of a geometric series}
\end{tabular}\\

\begin{tabular}{ll}
    \codelineeditor{deffun}{fac(n) = \mathkeywords{if}(n, n*fac(n-1), 1)}{recursive definition of the factorial}
\end{tabular}\\

\begin{tabular}{ll}
    \codelineeditor{deffun}{alpha(s1, s2) = c*(1 - 1/(1 + d*\mathkeywords{exp}(-k*(s1 - s2)))) }{}
\end{tabular}\\


