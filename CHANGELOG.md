# Changelog for ULM version 6.0

## New features
- ULM is now released under a GPLv3+ licence
- GUI version `ulm` now works on OS X
- ULM now works with both Qt and GTK toolkits by respectively installing
`ulm-gtk` and `ulm-qt`
- Syntax highlighting
- Man pages for `ulm` and `ulmc`

## Improvements
- Improved readability of source code
- ULM now uses updated versions of system libraries
- ULM desktop icon now loads correctly on linux systems
- Windows now display properly and with the correct size on all operating
systems
- Documentation now uses LaTeX for better integration with git tools
- Addition of a stop button to interrupt long simulations

## Other changes
- New icon for the new version
